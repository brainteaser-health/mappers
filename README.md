# BRAINTEASER Data Mappers

The data mapper is the part of our software architecture dedicated to mapping the clinical data provided to us by the hospitals into a structured RDF graph compliant with our ontology.

Data came from different sources, each of them used to adopt its own notation and standards for managing knowledge, so different sources would likely state the same knowledge in two different ways, according to their data management system. Different mappers are needed to deal with this fact. Then the aim is to uniform all the knowledge provided by the partners and structure it according to a graph-based model. Moreover, we are going to collect data about ALS and MS, so it's pretty natural to have different needs in terms of information to map.

Since we need to deal with different sources of information as discussed above we chose a pluggable architecture for mappers: dedicated modules for digesting the information coming from different sources, then we collect everything in a single RDF graph employing a well-known python library rdflib, and at the end, we serialize in several formats (turtle, RDF/XML, JSON-LD).
We export data even in csv format, those will be used by the ML algorithms for prediction purposes.

Mappers are completely developed using python since it allows us to use plenty of libraries useful for our scopes and permits us to write code in a very fast and easy to read manner, so from one side it speeds up the development and from the other one give the possibility to clearly understand how the whole framework is supposed to works, with benefits for future improvements. For manipulating data we employ pandas, so managing the input reading and the data cleaning was carried out using pandas’ tools.
Follows all the external libraries and modules adopted during the mappers’ development:

* pandas, for data manipulation;
* rdflib, for managing RDF graphs;
* uuid, for generating UUID codes;
* mmh3, for the hashing phase;
* csv, for writing data into a CSV file;
* gc, for managing the garbage collector.

## MS Data Mapper ##
The MS Data Mapper is responsible for mapping the data of MS clinics of Pavia, Turin and in a future development also Madrid. In MS folder you can find all the source code.

The input datasets are located inside the *data/input* folder, Pavia and Turin have their own directory with their CSV input files. Other input documents are used runtime by the software to perform the mapping.

In the *data/output/serializations/* folder is possible to find all the output file produced, in particular the *global/* folder will contain all the serialized RDF graph and one more folder calle *log* with the log files produced during the mapper execution, while the folder *quality_report/* will cointain a summary about the data quality of the datasets.

In the folder *MS/mappers/* there are Python scripts, for each data source we have a dedicated module.

To run the MS Data Mapper follow the following instructions:

1. Move into MS folder;
```
cd MS
```

2. Use the package manager [pip](https://pypi.org/project/pip/) to install the required packages (declared into *requirements.txt*);
```
pip install -r requirements.txt
```

3. Use the [python](https://www.python.org/downloads/) command followed by *main.py* to run the MS Data Mapper.
```
python main.py
```

4. During the execution the program will ask you the type of the data ingestion filter that you'd prefer, ("strong" means that you will collect edss without accept any missing values, while "weak" means that you are fine to accept some missing values in the edss collected)

5. Before the serialization phase the program will ask you a name for saving the output (e.g. ms_retrospective), since we serialize in several formats you are not allowed to insert any extention in the file name because the program will do it for you.

Outputs can be found in *data/outputs/serializations* as dicussed above. Running queries directly from rdflib appears time computational unfeasible, so we commented all the related code, we will discuss this aspect in then last section. In *output* you can find the file *registry.csv* that is an example of the output obtained running the "registry" query.  

## ALS Data Mapper ##

The ALS Data Mapper is responsible for mapping the data of ALS clinics of Turin, Lisbon and in a future development also Madrid, and all the source code can be found in the *ALS* folder.

The input datasets are located inside the *dataset* folder and are subdivided as follows:

* *TURIN_Dataset-final_anonym_corrected_feb2022.xlsx*: anonymised Turin data;
* *Lisbon_Dataset1_FINAL_v18102021.xlsx*: : anonymised Lisbon data;
* *[ UL | LR | DP ]-named-individual.csv*: datasets for the onset limb type; 
* *esco-classification.csv*: [ESCO](https://ec.europa.eu/esco/portal/occupation) dataset for the occupations;
* *lisbon-[ alsfrs | fvc ].csv*: generated during the execution of the pre-proccesing operations on the *'ALSFRS'* and *'%FVC'* tabs of Lisbon dataset, with the aim of moving visits from multiple columns into multiple rows;
* *[ turin | lisbon ]-[ alsfrs | fvc ]-correct.csv*: generated during the execution of the pre-proccesing operations on the on the *'ALSFRS'* and *'%FVC'* tabs of Turin dataset and on the *lisbon-[ alsfrs | fvc ].csv* files, with the aim of obtaining the correct ALSFRS-R and pulmonary tests.

Python scripts are organized as follows:

* *common* folder: contains scripts common to all datasets for pre-processing operations, *static vars* sheet mapping and all those support functions. It also contains files with names/codes of namespaces, classes and named individuals used;
* *lisbon_als* and *turin_als* folders: contains the scripts for mapping the *'Static vars'*, *'ALSFRS-R'*, *'%FVC'* and *'OWD vars'* tabs of the Turin and Lisbon datasets respectively;
* *main.py* on root folder: contains the main Turin and Lisbon functions for managing data mapping.

To run the ALS Data Mapper follow the following instructions:

1. Move into ALS folder;
```
cd ALS
```

2. Use the package manager [pip](https://pypi.org/project/pip/) to install the required packages (declared into *requirements.txt*);
```
pip install -r requirements.txt
```

3. Use the [python](https://www.python.org/downloads/) command followed by *main.py* to run the ALS Data Mapper.
```
python main.py
```

At the end of execution, the following files are generated:

* *[ turin | lisbon | merged ].[ ttl | json | rdf]* inside the *output* folder: result of graphs serialization of Turin, Lisbon and also the merged version (Turin + Lisbon);
* *[ turin | lisbon ]-log.txt* inside the *output/log* folder: txt files reporting statistical data and mapping warnings;
* *[ turin | lisbon ]-patient-dictionary.csv* inside the *output/dictionary* folder: txt files representing the correspondence between the original patient ID and its generated URI.

Inside the *output* folder there are also other files that are static, non-generated files.
In particular, into the *statistics* folder there are some statistics on Turin, Lisbon and Madrid datasets, and also some statistic about the date of first visit and the delta time (date of first ALSFRS-R - date of first Pulmonary Test) of each patient.
While in the *csv* folder there is an example of data export in csv files for the static vars and visits dataset of Turin and Lisbon that was obtained by running SPARQL queries, contained in the *query* folder, in the [GraphDB](https://www.ontotext.com/products/graphdb/graphdb-free/) environment. More information about running these queries in the **Queries and CSV export** section.

## Queries and CSV export ##
For both data mappers (ALS and MS) we have provided functions to directly execute SPARQL queries to get exported data in CSV, ready to be consumed by AI models.
Unfortunately, the actual version of rdflib is not optimized to execute complex queries, nor implements all SPARQL constructs such as "if" and "exists".
Noticed that, the final result is that the time required to execute queries is not sustainable and the results are not completed.

We addressed this problem employing GraphBD. GraphDB is a triple store that let us create a repository to store both ontology schema and the actual RDF instance.
It gives the possibility to execute SPARQL queries, in a very efficient way, since it's optimized for it and exploits indexes to better perform the executions.

In the folder "mappers/ALS/query" we provide the queries for ALS, while in the folder "mappers/MS/query/" we provide the queries for MS. To execute them, and get the final exported CSV files, you need to:

1. download and install [GraphDB](https://www.ontotext.com/products/graphdb/graphdb-free/)
2. create a repository
3. import the ontology file and the RDF serialized file obtained after the execution of data mappers
4. execute the queries by cutting and pasting from our text file to the dedicated area in GraphDB

Then, GraphDB permits you to download results in whatever format you prefer, we suggest CSV.

### License ###
All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

### Acknowledgements ###
This work has been supported by the [BRAINTEASER](https://brainteaser.health/)  project,  funded by the European Union’s Horizon 2020 research and innovation programme under the grant agreement No GA101017598.
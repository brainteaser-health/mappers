'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# common_static_vars.py contains all common functions for the Static vars tab

# required libraries
import pandas as pd
from common import namespaces_als as ns
from common import classes_als as classes
from common import named_individuals_als as ni
from common import utils_als
from rdflib import Literal
from rdflib.namespace import XSD, RDF

# add a patient to the graph
# input: graph, dictionary for the patient URIs, patient ID and hospital name
# output: updated graph, patient URI and updated dictionary of all patient URIs
def addPatient(graph, patientDictionary, patientID, hospital):
    # get patient URI
    patientURI = utils_als.generateURI(hospital, 'patient', patientID)
    # add patient to graph
    graph.add((patientURI, RDF.type, classes.patient))
    # add ALS disease
    graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.als))
    # update dictionary
    patientDictionary[patientID] = patientURI
    #return the updated graph, the patient URI, and the patients dictionary
    return graph, patientURI, patientDictionary

# add birth year to patient
# input: graph, patientID, patient URI and the year of birth
# output: updated graph
def addBirthYear(graph, patientID, patientURI, birthYear):
    # check if the date is not null
    if not pd.isna(birthYear):
        graph.add((patientURI, ns.BTO_schema['yearOfBirth'], Literal(birthYear.year, datatype=XSD.gYear)))
    else:
        print('[WARNING] Year of birth not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add gender to patient
# input graph, patient ID, patient URI and the gender
# output: updated graph
def addGender(graph, patientID, patientURI, gender):
    # if gender = 1, male patient
    if not pd.isna(gender) and gender == 1:
        graph.add((patientURI, ns.BTO_schema['sex'], Literal('Male', datatype=RDF.langString)))
    # if gender = 2, female patient
    elif not pd.isna(gender) and gender == 2:
        graph.add((patientURI, ns.BTO_schema['sex'], Literal('Female', datatype=RDF.langString)))
    elif not pd.isna(gender) and gender != 1 and gender != 2:
        print('[WARNING] Gender not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Gender not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add ethnicity to patient
# input: graph, patient ID, patient URI and the ethnicity
# output: updated graph
def addEthnicity(graph, patientID, patientURI, ethnicity):
    # if ethnicity = 1, caucasian patient
    if not pd.isna(ethnicity) and ethnicity == 1:
        graph.add((patientURI, ns.BTO_schema['ethnicity'], ns.BTO_ni['Caucasian']))
    # if ethnicity = 2, african patient
    elif not pd.isna(ethnicity) and ethnicity == 2:
        graph.add((patientURI, ns.BTO_schema['ethnicity'], ns.BTO_ni['Black_African']))
    # if ethnicity = 3, asian patient
    elif not pd.isna(ethnicity) and ethnicity == 3:
        graph.add((patientURI, ns.BTO_schema['ethnicity'], ns.BTO_ni['Asian_ethnic_group']))
    elif not pd.isna(ethnicity) and ethnicity != 1 and ethnicity != 2 and ethnicity != 3:
        print('[WARNING] Ethnicity not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Ethnicity not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add height, weight and weightloss percentage to patient
# input: graph, patientID, patientURI, date of first visit, height, weight and weightloss
# output: updated graph
def addHeightWeightWeightloss(graph, patientID, patientURI, dateFirstVisit, height, weight, weightloss):
    if not pd.isna(dateFirstVisit):
        if not pd.isna(height) or not pd.isna(weight) or not pd.isna(weightloss):
            # get visit URI
            eventVisit = utils_als.generateURI()
            # add visit to the graph
            graph.add((eventVisit, RDF.type, classes.visit))
            # link visit to the patient
            graph.add((patientURI, ns.BTO_schema['undergo'], eventVisit))
            # add start date to visit
            graph.add((eventVisit, ns.BTO_schema['startDate'], Literal(dateFirstVisit.date(), datatype=XSD.date)))
            # get clinical assessment URI
            clinicalAssessment = utils_als.generateURI()
            # add clinical assessment to graph
            graph.add((clinicalAssessment, RDF.type, classes.clinical_assessment))
            # link clinical assessment to visit
            graph.add((eventVisit, ns.BTO_schema['consists'], clinicalAssessment))
            # add height to clinical assessment
            if not pd.isna(height):
                graph.add((clinicalAssessment, ns.BTO_schema['Height'], Literal(float(height), datatype=XSD.float)))
            else:
                print('[WARNING] Height not given for patient ID: %s' % (patientID))
            # add "weight at 1st visit" to clinical assessment
            if not pd.isna(weight):
                graph.add((clinicalAssessment, ns.BTO_schema['Weight'], Literal(float(weight), datatype=XSD.float)))
            else:
                print('[WARNING] Weight at first visit not given for patient ID: %s' % (patientID))
            # add "weightloss >10%" to clinical assessment
            if not pd.isna(weightloss) and weightloss == 1:
                graph.add(
                    (clinicalAssessment, ns.BTO_schema['moreThan10PercentWeightloss'], Literal(True, datatype=XSD.boolean)))
            elif not pd.isna(weightloss) and weightloss == 0:
                graph.add(
                    (clinicalAssessment, ns.BTO_schema['moreThan10PercentWeightloss'], Literal(False, datatype=XSD.boolean)))
            elif not pd.isna(weightloss) and weightloss != 0 and weightloss != 1:
                print('[WARNING] Weightloss not valid for patient ID: %s' % (patientID))
            else:
                print('[WARNING] Weightloss not given for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Date of first visit not given for patient ID: %s, (Cannot add height, weight and weightloss to patient!)' % (patientID))
    # return the updated graph
    return graph

# add limb type to onset
# input: graph, onset, limb predominant impairment, limb predominant side, and the limb type dataframes
# output: updated graph
def addLimbType(graph, onset, limbULvsLL, limbPredominantImpairment, limbPredominantSide, ULdataframe, DPdataframe, LRdataframe):
    #get the labels from the dataframes
    UL = str(ULdataframe.loc[ULdataframe.index == limbULvsLL].label.values[0])
    DP = str(DPdataframe.loc[DPdataframe.index == limbPredominantImpairment].label.values[0])
    LR = str(LRdataframe.loc[LRdataframe.index == limbPredominantSide].label.values[0])
    #merge the labels
    if (UL != 'nan' and DP != 'nan' and LR != 'nan'):
        label = UL + '-' + DP + '-' + LR + '-limb'
    elif (UL == 'nan' and DP != 'nan' and LR != 'nan'):
        label = DP + '-' + LR + '-limb'
    elif (UL != 'nan' and DP == 'nan' and LR != 'nan'):
        label = UL + '-' + LR + '-limb'
    elif (UL != 'nan' and DP != 'nan' and LR == 'nan'):
        label = UL + '-' + DP + '-limb'
    elif (UL == 'nan' and DP == 'nan' and LR != 'nan'):
        label = LR + '-limb'
    elif (UL != 'nan' and DP == 'nan' and LR == 'nan'):
        label = UL + '-limb'
    elif (UL == 'nan' and DP != 'nan' and LR == 'nan'):
        label = DP + '-limb'
    else:
        label = 'limb'
    # link the correct named individual to the graph
    graph.add((onset, ns.BTO_schema['site'], ns.BTO_ni[label]))
    # return the updated graph
    return graph

# add relative to patient
# input: graph, patientID, patient URI and the value of the relative
# output: updated graph
def addRelative(graph, patientID, patientURI, relativeValue):
    if not pd.isna(relativeValue):
        if relativeValue == 1:
            graph.add((patientURI, ns.BTO_schema['hasRelative'], ni.relative_with_als))
    else:
        print('[WARNING] ALS familiar history not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add NIV event to patient
# input: graph, patientID, patient URI, NIV value and date
# output: updated graph
def addNIV(graph, patientID, patientURI, NIVvalue, NIVdate):
    if not pd.isna(NIVvalue):
        if NIVvalue == 1:
            if not pd.isna(NIVdate):
                # get niv event uri
                nivEvent = utils_als.generateURI()
                # add to graph the niv event
                graph.add((nivEvent, RDF.type, classes.protocol_event))
                # add start date to niv event
                graph.add((nivEvent, ns.BTO_schema['startDate'], Literal(NIVdate.date(), datatype=XSD.date)))
                # link niv event to patient
                graph.add((patientURI, ns.BTO_schema['undergo'], nivEvent))
                # link the event to the NIV named individual
                graph.add((nivEvent, ns.BTO_schema['consists'], ni.niv))
            else:
                print('[WARNING] NIV date not given with NIV value = 1 for patient ID: %s,  (Cannot add NIV event to patient!)' % (patientID))
        elif NIVvalue == 0:
            if not pd.isna(NIVdate):
                print('[WARNING] NIV date given with NIV value = 0 for patient ID: %s,  (Cannot add NIV event to patient!)' % (patientID))
        else:
            print('[WARNING] NIV value not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] NIV value not given for patient ID: %s' % (patientID))
   # return the updated graph
    return graph

# add tracheostomy event to patient
# input: graph, patientID, patient URI, tracheostomy value and date
# output: updated graph
def addTracheostomy(graph, patientID, patientURI, tracheostomyValue, tracheostomyDate):
    if not pd.isna(tracheostomyValue):
        if (tracheostomyValue == 1):
            if not pd.isna(tracheostomyDate):
                # get tracheostomy event uri
                tracheostomyEvent = utils_als.generateURI()
                # add to graph the tracheostomy event
                graph.add((tracheostomyEvent, RDF.type, classes.protocol_event))
                # add tracheostomy date
                graph.add((tracheostomyEvent, ns.BTO_schema['startDate'], Literal(tracheostomyDate.date(), datatype=XSD.date)))
                # link the tracheostomy event to patient
                graph.add((patientURI, ns.BTO_schema['undergo'], tracheostomyEvent))
                # link the event to the Tracheotomy named individual
                graph.add((tracheostomyEvent, ns.BTO_schema['consists'], ni.tracheotomy))
            else:
                print(
                    '[WARNING] Tracheostomy date not given with tracheostomy value = 1 for patient ID: %s,  (Cannot add tracheostomy event to patient!)' % (
                        patientID))
        elif tracheostomyValue == 0:
            if not pd.isna(tracheostomyDate):
                print(
                    '[WARNING] Tracheostomy date given with rracheostomy value = 0 for patient ID: %s,  (Cannot add tracheostomy event to patient!)' % (
                        patientID))
        else:
            print('[WARNING] Tracheostomy value not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Tracheostomy value not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add PEG event to patient
# input: graph, patientID, patient URI, PEG value and date
# output: updated graph
def addPEG(graph, patientID, patientURI, PEGvalue, PEGdate):
    if not pd.isna(PEGvalue):
        if (PEGvalue == 1):
            if not pd.isna(PEGdate):
                # get peg event uri
                PEGEvent = utils_als.generateURI()
                # add to graph the peg event
                graph.add((PEGEvent, RDF.type, classes.protocol_event))
                # add start date to peg event
                graph.add((PEGEvent, ns.BTO_schema['startDate'], Literal(PEGdate.date(), datatype=XSD.date)))
                # link niv event to patient
                graph.add((patientURI, ns.BTO_schema['undergo'], PEGEvent))
                # link the event to the PEG named individual
                graph.add((PEGEvent, ns.BTO_schema['consists'], ni.peg))
            else:
                print('[WARNING] PEG date not given with PEG value = 1 for patient ID: %s,  (Cannot add PEG event to patient!)' % (
                        patientID))
        elif PEGvalue == 0:
            if not pd.isna(PEGdate):
                print('[WARNING] PEG date given with PEG value = 0 for patient ID: %s,  (Cannot add PEG event to patient!)' % (
                        patientID))
        else:
            print('[WARNING] PEG value not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] PEG value not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add before onset event to patient
# input: graph, patient ID, patient URI, weight before onset, major trauma and surgical intervention before onset
# output: updated graph
def addBeforeOnset(graph, patientID, patientURI, weightBeforeOnset,  majorTraumaValue, surgeryValue):
    # add "Before Onset" event and add "weight before 1st symptoms" to clinical assessment
    if (not pd.isna(weightBeforeOnset) and weightBeforeOnset < 999.0) or (not pd.isna(majorTraumaValue) and majorTraumaValue == 1) or (not pd.isna(surgeryValue) and surgeryValue == 1):
        # get before onset URI
        beforeOnset = utils_als.generateURI()
        # add before onset to graph
        graph.add((beforeOnset, RDF.type, classes.before_onset))
        # link before onset to patient
        graph.add((patientURI, ns.BTO_schema['undergo'], beforeOnset))
        # add major trauma
        if (not pd.isna(majorTraumaValue) and majorTraumaValue == 1):
            graph.add((beforeOnset, ns.BTO_schema['hasTrauma'], ni.major_trauma_before_onset))
        elif (not pd.isna(majorTraumaValue) and majorTraumaValue != 1 and  majorTraumaValue != 0):
            print('[WARNING] Major trauma before onset not valid for patient ID: %s' % (patientID))
        elif pd.isna(majorTraumaValue):
            print('[WARNING] Major trauma before onset not given for patient ID: %s' % (patientID))
        # add surgical intervention
        if (not pd.isna(surgeryValue) and surgeryValue == 1):
            graph.add((beforeOnset, ns.BTO_schema['consists'], ni.surgical_interventions_before_onset))
        elif (not pd.isna(surgeryValue) and surgeryValue != 1 and surgeryValue != 0):
            print('[WARNING] Surgical interventions before onset not valid for patient ID: %s' % (patientID))
        elif pd.isna(surgeryValue):
            print('[WARNING] Surgical interventions before onset not given for patient ID: %s' % (patientID))
        # add weight
        if (not pd.isna(weightBeforeOnset) and weightBeforeOnset < 999.0):
            # get clinical assessment URI
            clinicalAssessment = utils_als.generateURI()
            # add clinical assessment to "Before Onset"
            graph.add((clinicalAssessment, RDF.type, classes.clinical_assessment))
            # add weight to clinical assessment
            graph.add((clinicalAssessment, ns.BTO_schema['Weight'], Literal(float(weightBeforeOnset), datatype=XSD.float)))
            # link clinical assessment to "Before" onset
            graph.add((beforeOnset, ns.BTO_schema['consists'], clinicalAssessment))
        elif not pd.isna(weightBeforeOnset) and weightBeforeOnset == 999.0:
            print('[WARNING] Weight before first symptoms not valid for patient ID: %s' % (patientID))
        elif pd.isna(weightBeforeOnset):
            print('[WARNING] Weight before first symptoms not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add smoking behaviour to patient
# input: graph, patient ID, patient URI and smoking value
# output: updated graph and smoking event
def addSmoking(graph, patientID, patientURI, smokingValue):
    # initialize the smoke event with 999 for those who do not smoke
    event = '999'
    # 1 = smoker patient
    if not pd.isna(smokingValue) and smokingValue == 1:
        # get event URI
        event = utils_als.generateURI()
        # add event to graph
        graph.add((event, RDF.type, classes.event))
        # link event to patient
        graph.add((patientURI, ns.BTO_schema['undergo'], event))
        # add smoking
        graph.add((event, ns.BTO_schema['hasRegisteredBehaviour'], ni.smoking))
    elif not pd.isna(smokingValue) and smokingValue != 1 and smokingValue !=0:
        print('[WARNING] Smoking value not valid for patient ID: %s' % (patientID))
    elif pd.isna(smokingValue):
        print('[WARNING] Smoking value not given for patient ID: %s' % (patientID))
    #return the updated graph and the smoking event
    return graph, event

# add disease to patient
# input: graph, index, patient URI, and all the disease values
#        (bloodHypertension, diabetes, dyslipidemia, thyroidDisorder, autoimmuneDisorder, stroke, cardiacDisease, primaryCancer)
# output: updated graph
def addDisease(graph, patientID, patientURI, bloodHypertension, diabetes, dyslipidemia, thyroidDisorder, autoimmuneDisorder, stroke, cardiacDisease, primaryCancer):
    # add hypertension
    if not pd.isna(bloodHypertension) and bloodHypertension == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.hypertension))
    elif not pd.isna(bloodHypertension) and bloodHypertension !=1 and bloodHypertension !=0:
        print('[WARNING] Blood Hypertension value not valid for patient ID: %s' % (patientID))
    elif pd.isna(bloodHypertension):
        print('[WARNING] Blood Hypertension value not given for patient ID: %s' % (patientID))
    # add diabetes mellitus
    if not pd.isna(diabetes) and diabetes == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.diabetes))
    elif not pd.isna(diabetes) and diabetes !=1 and diabetes !=0:
        print('[WARNING] Diabetes value not valid for patient ID: %s' % (patientID))
    elif pd.isna(diabetes):
        print('[WARNING] Diabetes value not given for patient ID: %s' % (patientID))
    # add dyslipidemia
    if not pd.isna(dyslipidemia) and dyslipidemia == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.dyslipidemia))
    elif not pd.isna(dyslipidemia) and dyslipidemia !=1 and dyslipidemia !=0:
        print('[WARNING] Dyslipidemia value not valid for patient ID: %s' % (patientID))
    elif pd.isna(dyslipidemia):
        print('[WARNING] Dyslipidemia value not given for patient ID: %s' % (patientID))
    # add thyroid gland disorder
    if not pd.isna(thyroidDisorder) and thyroidDisorder == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.thyroid_disorder))
    elif not pd.isna(thyroidDisorder) and thyroidDisorder !=1 and thyroidDisorder !=0:
        print('[WARNING] Thyroid disorder value not valid for patient ID: %s' % (patientID))
    elif pd.isna(thyroidDisorder):
        print('[WARNING] Thyroid disorder value not given for patient ID: %s' % (patientID))
    # add autoimmune disease
    if not pd.isna(autoimmuneDisorder) and autoimmuneDisorder == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.autoimmune_disease))
    elif not pd.isna(autoimmuneDisorder) and autoimmuneDisorder != 1 and autoimmuneDisorder != 0:
        print('[WARNING] Autoimmune disorder value not valid for patient ID: %s' % (patientID))
    elif pd.isna(autoimmuneDisorder):
        print('[WARNING] Autoimmune disorder value not given for patient ID: %s' % (patientID))
    # add stroke disease
    if not pd.isna(stroke) and stroke == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.stroke))
    elif not pd.isna(stroke) and stroke != 1 and stroke != 0:
        print('[WARNING] Stroke value not valid for patient ID: %s' % (patientID))
    elif pd.isna(stroke):
        print('[WARNING] stroke value not given for patient ID: %s' % (patientID))
    # add cardiovascular disorder
    if not pd.isna(cardiacDisease) and cardiacDisease == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.cardiovascular_disorder))
    elif not pd.isna(cardiacDisease) and cardiacDisease != 1 and cardiacDisease != 0:
        print('[WARNING] Cardiac disease value not valid for patient ID: %s' % (patientID))
    elif pd.isna(cardiacDisease):
        print('[WARNING] Cardiac disease value not given for patient ID: %s' % (patientID))
    # add primary neoplasm
    if not pd.isna(primaryCancer) and primaryCancer == 1:
        graph.add((patientURI, ns.BTO_schema['hasDisease'], ni.primary_neoplasm))
    elif not pd.isna(primaryCancer) and primaryCancer != 1 and primaryCancer != 0:
        print('[WARNING] Primary cancer value not valid for patient ID: %s' % (patientID))
    elif pd.isna(primaryCancer):
        print('[WARNING] Primary cancer value not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add retired at diagnosis to patient
# input: graph, patient ID, patient URI and value of the cell 'Retired at the diagnosis'
# output: updated graph
def addRetiredAtDiagnosis(graph, patientID, patientURI, value):
    # 1 = true, 0 = false
    if not pd.isna(value):
        if (value == 1):
            graph.add((patientURI, ns.BTO_schema['retiredAtDiagnosis'], Literal(True, datatype=XSD.boolean)))
        elif (value == 0):
            graph.add((patientURI, ns.BTO_schema['retiredAtDiagnosis'], Literal(False, datatype=XSD.boolean)))
        else:
            print('[WARNING] Retired at the diagnosis value not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Retired at the diagnosis value not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph
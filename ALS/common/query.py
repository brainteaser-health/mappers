'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# query.py contains all the function to manage the SPARQL query

import csv
from pathlib import Path

# export in csv the static vars
# input: graph, file name
# output: csv file
def staticVarsQuery(graph, fileName):
    # define query
    query_string = """
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX BTO_schema: <https://w3id.org/brainteaser/ontology/schema/>
    PREFIX NCIT: <http://purl.obolibrary.org/obo/NCIT_>
    PREFIX BTO_ni: <https://w3id.org/brainteaser/ontology/named-individual/>
    PREFIX MAXO: <http://purl.obolibrary.org/obo/MAXO_>
    SELECT ?id ?onsetDate ?diagnosisDate ?deathDate ?NIV_date ?PEG_date ?Tracheotomy_date ?birthYear ?alive ?sex ?ethnicity ?height ?weight_before_onset ?weight ?moreThan10PercentWeightloss ?major_trauma_before_onset ?surgical_interventions_before_onset ?age_onset ?prevalentLMN ?prevalentUMN ?mixedMN ?onset_bulbar ?onset_axial ?onset_generalized ?onset_limbs ?onset_limb_type ?occupation ?retired_at_diagnosis ?ALS_familiar_history ?smoking ?smoking_startYear ?smoking_endYear ?dailyCigarettes ?packYear ?turin_FUS ?turin_SOD1 ?turin_TARDBP ?turin_C9orf72_kind ?lisbon_FUS_openbox ?lisbon_SOD1_openbox ?lisbon_TARDBP_openbox ?lisbon_C9orf72 ?hypertension ?diabetes ?dyslipidemia ?thyroid_disorder ?autoimmune_disease ?stroke ?cardiac_disease ?primary_neoplasm ?CK_level ?CK_lower_range ?CK_upper_range ?Albumin_level ?Albumin_lower_range ?Albumin_upper_range ?Creatinine_level ?Creatinine_lower_range ?Creatinine_upper_range ?Total_Cholesterol_level ?Total_Cholesterol_lower_range ?Total_Cholesterol_upper_range ?HDL_Cholesterol_level ?HDL_Cholesterol_lower_range ?HDL_Cholesterol_upper_range ?LDL_Cholesterol_level ?LDL_Cholesterol_lower_range ?LDL_Cholesterol_upper_range ?Triglycerides_level ?Triglycerides_lower_range ?Triglycerides_upper_range ?head_trauma_last_5_years ?head_trauma_more_than_5_years ?neck_trauma_last_5_years ?neck_trauma_more_than_5_years ?cervical_trauma_last_5_years ?cervical_trauma_more_than_5_years ?thoracic_trauma_last_5_years ?thoracic_trauma_more_than_5_years ?lumbo_sacral_trauma_last_5_years ?lumbo_sacral_trauma_more_than_5_years ?cervical_spine_surgery_last_5_years ?cervical_spine_surgery_more_than_5_years ?thoracic_spine_surgery_last_5_years ?thoracic_spine_surgery_more_than_5_years ?lumbo_sacral_spine_surgery_last_5_years ?lumbo_sacral_spine_surgery_more_than_5_years ?upper_limb_surgery_last_5_years ?upper_limb_surgery_more_than_5_years ?lower_limb_surgery_last_5_years ?lower_limb_surgery_more_than_5_years ?abdominal_surgery_last_5_years ?abdominal_surgery_more_than_5_years ?thoracic_surgery_last_5_years ?thoracic_surgery_more_than_5_years ?pelvic_surgery_last_5_years ?pelvic_surgery_more_than_5_years ?head_neck_surgery_last_5_years ?head_neck_surgery_more_than_5_years WHERE {
        ?idURI a NCIT:C16960 .
        # personal info
        optional{ ?idURI BTO_schema:yearOfBirth ?birthYear . }
        optional{ ?idURI BTO_schema:sex ?sex . }
        optional{ ?idURI BTO_schema:alive ?alive . }
        optional{ ?idURI BTO_schema:ethnicity  ?ethnicityURI . }
        optional{ ?idURI BTO_schema:dateOfDeath ?deathDate . } 
        optional{ ?idURI BTO_schema:hasOccupation ?occupationURI . }
        optional{ ?idURI BTO_schema:hasRelative ?relativeURI . }
        optional{ ?idURI BTO_schema:retiredAtDiagnosis ?retired_at_diagnosis . }
        bind( substr( (str(?idURI)), 48) as ?id)
        bind( substr( (str(?occupationURI)), 56) as ?occupation)
        bind( substr( (str(?ethnicityURI)), 56) as ?ethnicity)
        bind( substr( (str(?relativeURI)), 56) as ?ALS_familiar_history)
        # niv - peg - tracheo
        optional{ ?idURI BTO_schema:undergo ?eventNIVURI . ?eventNIVURI a NCIT:C74589 ; 
            BTO_schema:consists BTO_ni:Non-Invasive_Mechanical_Ventilation ; 
            BTO_schema:startDate ?NIV_date . }
        optional{ ?idURI BTO_schema:undergo ?eventPEGURI . ?eventPEGURI a NCIT:C74589 ;
            BTO_schema:consists BTO_ni:Percutaneous_Endoscopic_Gastrostomy ;
            BTO_schema:startDate ?PEG_date . }
        optional{ ?idURI BTO_schema:undergo ?eventTracheURI . ?eventTracheURI a NCIT:C74589 ;
            BTO_schema:consists BTO_ni:Tracheotomy ; 
            BTO_schema:startDate ?Tracheotomy_date . }
        # onset
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:startDate ?onsetDate . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:age_onset ?age_onset . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:axial ?onset_axial . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:bulbar ?onset_bulbar . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:generalized ?onset_generalized . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:limbs ?onset_limbs . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:site ?limbsite . }
        optional{ ?idURI BTO_schema:undergo ?eventOnsetURI . ?eventOnsetURI a NCIT:C25279 ;
            BTO_schema:consists ?CAURI . 
            ?CAURI a MAXO:0000487 ; 
                BTO_schema:mixedMN ?mixedMN ;
                BTO_schema:prevalentLMN ?prevalentLMN ;
                BTO_schema:prevalentUMN ?prevalentUMN . }
        bind( substr( (str(?limbsite)), 56) as ?onset_limb_type)
        # diagnosis                                                 
        optional{ ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220 . 
            ?eventDiagnosisURI BTO_schema:startDate ?diagnosisDate . }
        # before onset
        optional{ ?idURI BTO_schema:undergo ?eventBOURI . 
            ?eventBOURI a BTO_schema:Before_Onset ;
                BTO_schema:consists ?CABOURI . 
            ?CABOURI a MAXO:0000487 ;
                BTO_schema:Weight ?weight_before_onset . }
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventBOURI . ?eventBOURI a BTO_schema:Before_Onset ;
                BTO_schema:hasTrauma BTO_ni:Major_Truma_Before_Onset .}, "true"^^xsd:boolean, ?x = 0) as ?major_trauma_before_onset)
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventBOURI . ?eventBOURI a BTO_schema:Before_Onset ;
                BTO_schema:consists BTO_ni:Surgical_Interventions_Before_Onset .}, "true"^^xsd:boolean, ?x = 0) as ?surgical_interventions_before_onset)
        # height weight weightloss
        optional{ ?idURI BTO_schema:undergo ?eventCA . 
            ?eventCA a NCIT:C39564 ;
                BTO_schema:consists ?CAfirstvisit . 
            ?CAfirstvisit a MAXO:0000487 ;
                BTO_schema:Height ?height . }
        optional{ ?idURI BTO_schema:undergo ?eventCA . 
            ?eventCA a NCIT:C39564 ;
                BTO_schema:consists ?CAfirstvisit . 
            ?CAfirstvisit a MAXO:0000487 ;
                BTO_schema:Weight ?weight . }
        optional{ ?idURI BTO_schema:undergo ?eventCA . 
            ?eventCA a NCIT:C39564 ;
                BTO_schema:consists ?CAfirstvisit . 
            ?CAfirstvisit a MAXO:0000487 ;
                BTO_schema:moreThan10PercentWeightloss ?moreThan10PercentWeightloss . }
        # smoking
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a NCIT:C25499 ;
            BTO_schema:hasRegisteredBehaviour ?smokingURI . }, "true"^^xsd:boolean, ?x = 0) as ?smoking)
        optional{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a NCIT:C25499 ;
            BTO_schema:hasRegisteredBehaviour ?smokingURI .
            ?smokingURI BTO_schema:startYear ?smoking_startYear . }
        optional{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a NCIT:C25499 ;
            BTO_schema:hasRegisteredBehaviour ?smokingURI .
            ?smokingURI BTO_schema:endYear ?smoking_endYear . }
        optional{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a NCIT:C25499 ;
            BTO_schema:hasRegisteredBehaviour ?smokingURI .
            ?smokingURI BTO_schema:packYear ?packYear . }
        optional{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a NCIT:C25499 ;
            BTO_schema:hasRegisteredBehaviour ?smokingURI .
            ?smokingURI BTO_schema:dailyCigarettes ?dailyCigarettes . }
        # gene turin
        optional{ ?idURI BTO_schema:hasGene ?geneURI . ?geneURI a OGG:3000203228 ;
            BTO_schema:kind ?turin_C9orf72_kind . }
        bind( IF( exists{ ?idURI BTO_schema:hasGene BTO_ni:FUS .}, "true"^^xsd:boolean, ?x = 0) as ?turin_FUS)
        bind( IF( exists{ ?idURI BTO_schema:hasGene BTO_ni:SOD1 .}, "true"^^xsd:boolean, ?x = 0) as ?turin_SOD1)
        bind( IF( exists{ ?idURI BTO_schema:hasGene BTO_ni:TARDBP .}, "true"^^xsd:boolean, ?x = 0) as ?turin_TARDBP)
        # gene lisbon
        optional{ ?idURI BTO_schema:hasGene ?geneURI . ?geneURI a OGG:3000002521 ;
            BTO_schema:open_box ?lisbon_FUS_openbox . }
        optional{ ?idURI BTO_schema:hasGene ?geneURI . ?geneURI a OGG:3000006647 ;
            BTO_schema:open_box ?lisbon_SOD1_openbox . }
        optional{ ?idURI BTO_schema:hasGene ?geneURI . ?geneURI a OGG:3000023435 ;
            BTO_schema:open_box ?lisbon_TARDBP_openbox . }
        bind( IF( exists{ ?idURI BTO_schema:hasGene BTO_ni:C9orf72 .}, "true"^^xsd:boolean, ?x = 0) as ?lisbon_C9orf72)
        # diseases
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Hypertension . }, "true"^^xsd:boolean, ?x = 0) as ?hypertension)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Diabetes_Mellitus . }, "true"^^xsd:boolean, ?x = 0) as ?diabetes)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Dyslipidemia . }, "true"^^xsd:boolean, ?x = 0) as ?dyslipidemia)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Thyroid_Gland_Disorder . }, "true"^^xsd:boolean, ?x = 0) as ?thyroid_disorder)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Autoimmune_Disease . }, "true"^^xsd:boolean, ?x = 0) as ?autoimmune_disease)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:stroke . }, "true"^^xsd:boolean, ?x = 0) as ?stroke)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Cardiovascular_Disorder . }, "true"^^xsd:boolean, ?x = 0) as ?cardiac_disease)
        bind( IF( exists{ ?idURI BTO_schema:hasDisease BTO_ni:Primary_Neoplasm . }, "true"^^xsd:boolean, ?x = 0) as ?primary_neoplasm)
        # trauma
        # head
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ;
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI .
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:head . }, "true"^^xsd:boolean, ?x = 0) as ?head_trauma_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:head . }, "true"^^xsd:boolean, ?x = 0) as ?head_trauma_more_than_5_years )
        # neck
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:neck . }, "true"^^xsd:boolean, ?x = 0) as ?neck_trauma_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:neck . }, "true"^^xsd:boolean, ?x = 0) as ?neck_trauma_more_than_5_years )
        # cervical
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:cervical_region_of_vertebral_column . }, "true"^^xsd:boolean, ?x = 0) as ?cervical_trauma_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:cervical_region_of_vertebral_column . }, "true"^^xsd:boolean, ?x = 0) as ?cervical_trauma_more_than_5_years )
        # thoracic
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:thoracic_skeleton .}, "true"^^xsd:boolean, ?x = 0) as ?thoracic_trauma_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:thoracic_skeleton . }, "true"^^xsd:boolean, ?x = 0) as ?thoracic_trauma_more_than_5_years )
        # lumbo
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:lumbosacral_nerve_plexus . }, "true"^^xsd:boolean, ?x = 0) as ?lumbo_sacral_trauma_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:hasTrauma ?traumaURI.
            ?traumaURI a NCIT:C3671 ;
                BTO_schema:traumaArea BTO_ni:lumbosacral_nerve_plexus . }, "true"^^xsd:boolean, ?x = 0) as ?lumbo_sacral_trauma_more_than_5_years )
        # surgery
        # cervical
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:cervical_region_of_vertebral_column . }, "true"^^xsd:boolean, ?x = 0) as ?cervical_spine_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:cervical_region_of_vertebral_column .}, "true"^^xsd:boolean, ?x = 0) as ?cervical_spine_surgery_more_than_5_years )
        # thoracic spine
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:thoracic_spinal_cord . }, "true"^^xsd:boolean, ?x = 0) as ?thoracic_spine_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:thoracic_spinal_cord . }, "true"^^xsd:boolean, ?x = 0) as ?thoracic_spine_surgery_more_than_5_years )
        # lumbo
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:lumbosacral_nerve_plexus . }, "true"^^xsd:boolean, ?x = 0) as ?lumbo_sacral_spine_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:lumbosacral_nerve_plexus . }, "true"^^xsd:boolean, ?x = 0) as ?lumbo_sacral_spine_surgery_more_than_5_years )
        # upper limb
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:upper_limb . }, "true"^^xsd:boolean, ?x = 0) as ?upper_limb_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:upper_limb . }, "true"^^xsd:boolean, ?x = 0) as ?upper_limb_surgery_more_than_5_years )
        # lower limb
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:lower_limb . }, "true"^^xsd:boolean, ?x = 0) as ?lower_limb_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:lower_limb . }, "true"^^xsd:boolean, ?x = 0) as ?lower_limb_surgery_more_than_5_years )
        # abdominal
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:abdominal_fascia . }, "true"^^xsd:boolean, ?x = 0) as ?abdominal_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:abdominal_fascia . }, "true"^^xsd:boolean, ?x = 0) as ?abdominal_surgery_more_than_5_years )
        # thoracic
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:thoracic_skeleton . }, "true"^^xsd:boolean, ?x = 0) as ?thoracic_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:thoracic_skeleton .}, "true"^^xsd:boolean, ?x = 0) as ?thoracic_surgery_more_than_5_years )
        # pelvic
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:pelvic_complex . }, "true"^^xsd:boolean, ?x = 0) as ?pelvic_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:pelvic_complex . }, "true"^^xsd:boolean, ?x = 0) as ?pelvic_surgery_more_than_5_years )
        # head neck
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "in the last 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:head-neck . }, "true"^^xsd:boolean, ?x = 0) as ?head_neck_surgery_last_5_years )
        bind( IF( exists{ ?idURI BTO_schema:undergo ?eventURI . ?eventURI a BTO_schema:Before_Onset ; 
            BTO_schema:howLong "> 5 years"^^xsd:string ;
            BTO_schema:consists ?surgeryURI.
            ?surgeryURI a NCIT:C15329 ;
                BTO_schema:surgicalArea BTO_ni:head-neck . }, "true"^^xsd:boolean, ?x = 0) as ?head_neck_surgery_more_than_5_years )
        #blood test
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ;
                BTO_schema:CK_level ?CK_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:CK_lower_range ?CK_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:CK_upper_range ?CK_upper_range .}
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Albumin_level ?Albumin_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Albumin_lower_range ?Albumin_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Albumin_upper_range ?Albumin_upper_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Creatinine_level ?Creatinine_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Creatinine_lower_range ?Creatinine_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Creatinine_upper_range ?Creatinine_upper_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Total_Cholesterol_level ?Total_Cholesterol_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Total_Cholesterol_lower_range ?Total_Cholesterol_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Total_Cholesterol_upper_range ?Total_Cholesterol_upper_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:HDL_Cholesterol_level ?HDL_Cholesterol_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:HDL_Cholesterol_lower_range ?HDL_Cholesterol_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:HDL_Cholesterol_upper_range ?HDL_Cholesterol_upper_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:LDL_Cholesterol_level ?LDL_Cholesterol_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:LDL_Cholesterol_lower_range ?LDL_Cholesterol_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:LDL_Cholesterol_upper_range ?LDL_Cholesterol_upper_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Triglycerides_level ?Triglycerides_level . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Triglycerides_lower_range ?Triglycerides_lower_range . }
        optional { ?idURI BTO_schema:undergo ?eventDiagnosisURI . ?eventDiagnosisURI a NCIT:C15220;
            BTO_schema:consists ?BloodURI . 
            ?BloodURI a NCIT:C49286 ; 
                BTO_schema:Triglycerides_upper_range ?Triglycerides_upper_range . }
    }ORDER BY ?id """
    # perform the query
    res = graph.query(query_string)
    # get the base path
    path = str(Path().resolve())
    # print in output
    with open(path + '/output/csv/' + str(fileName), 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['id','onsetDate','diagnosisDate','deathDate','NIV_date','PEG_date','Tracheotomy_date','birthYear','alive','sex','ethnicity','height','weight_before_onset','weight','moreThan10PercentWeightloss','major_trauma_before_onset','surgical_interventions_before_onset','age_onset','prevalentLMN','prevalentUMN','mixedMN','onset_bulbar','onset_axial','onset_generalized','onset_limbs','onset_limb_type','occupation','retired_at_diagnosis','ALS_familiar_history','smoking','smoking_startYear','smoking_endYear','dailyCigarettes','packYear','turin_FUS','turin_SOD1','turin_TARDBP','turin_C9orf72_kind','lisbon_FUS_openbox','lisbon_SOD1_openbox','lisbon_TARDBP_openbox','lisbon_C9orf72','hypertension','diabetes','dyslipidemia','thyroid_disorder','autoimmune_disease','stroke','cardiac_disease','primary_neoplasm','CK_level','CK_lower_range','CK_upper_range','Albumin_level','Albumin_lower_range','Albumin_upper_range','Creatinine_level','Creatinine_lower_range','Creatinine_upper_range','Total_Cholesterol_level','Total_Cholesterol_lower_range','Total_Cholesterol_upper_range','HDL_Cholesterol_level','HDL_Cholesterol_lower_range','HDL_Cholesterol_upper_range','LDL_Cholesterol_level','LDL_Cholesterol_lower_range','LDL_Cholesterol_upper_range','Triglycerides_level','Triglycerides_lower_range','Triglycerides_upper_range','head_trauma_last_5_years','head_trauma_more_than_5_years','neck_trauma_last_5_years','neck_trauma_more_than_5_years','cervical_trauma_last_5_years','cervical_trauma_more_than_5_years','thoracic_trauma_last_5_years','thoracic_trauma_more_than_5_years','lumbo_sacral_trauma_last_5_years','lumbo_sacral_trauma_more_than_5_years','cervical_spine_surgery_last_5_years','cervical_spine_surgery_more_than_5_years','thoracic_spine_surgery_last_5_years','thoracic_spine_surgery_more_than_5_years','lumbo_sacral_spine_surgery_last_5_years','lumbo_sacral_spine_surgery_more_than_5_years','upper_limb_surgery_last_5_years','upper_limb_surgery_more_than_5_years','lower_limb_surgery_last_5_years','lower_limb_surgery_more_than_5_years','abdominal_surgery_last_5_years','abdominal_surgery_more_than_5_years','thoracic_surgery_last_5_years','thoracic_surgery_more_than_5_years','pelvic_surgery_last_5_years','pelvic_surgery_more_than_5_years','head_neck_surgery_last_5_years','head_neck_surgery_more_than_5_years'])
        for row in res:
            writer.writerow([row.id, row.onsetDate, row.diagnosisDate, row.deathDate, row.NIV_date, row.PEG_date,
                             row.Tracheotomy_date, row.birthYear, row.alive, row.sex, row.ethnicity, row.height,
                             row.weight_before_onset, row.weight, row.moreThan10PercentWeightloss,
                             row.major_trauma_before_onset, row.surgical_interventions_before_onset, row.age_onset,
                             row.prevalentLMN, row.prevalentUMN, row.mixedMN, row.onset_bulbar, row.onset_axial,
                             row.onset_generalized, row.onset_limbs, row.onset_limb_type, row.occupation,
                             row.retired_at_diagnosis, row.ALS_familiar_history, row.smoking, row.smoking_startYear,
                             row.smoking_endYear, row.dailyCigarettes, row.packYear, row.turin_FUS, row.turin_SOD1,
                             row.turin_TARDBP, row.turin_C9orf72_kind, row.lisbon_FUS_openbox, row.lisbon_SOD1_openbox,
                             row.lisbon_TARDBP_openbox, row.lisbon_C9orf72, row.hypertension, row.diabetes,
                             row.dyslipidemia, row.thyroid_disorder, row.autoimmune_disease, row.stroke,
                             row.cardiac_disease, row.primary_neoplasm, row.CK_level, row.CK_lower_range,
                             row.CK_upper_range, row.Albumin_level, row.Albumin_lower_range, row.Albumin_upper_range,
                             row.Creatinine_level, row.Creatinine_lower_range, row.Creatinine_upper_range,
                             row.Total_Cholesterol_level, row.Total_Cholesterol_lower_range,
                             row.Total_Cholesterol_upper_range, row.HDL_Cholesterol_level,
                             row.HDL_Cholesterol_lower_range, row.HDL_Cholesterol_upper_range,
                             row.LDL_Cholesterol_level, row.LDL_Cholesterol_lower_range,
                             row.LDL_Cholesterol_upper_range, row.Triglycerides_level, row.Triglycerides_lower_range,
                             row.Triglycerides_upper_range, row.head_trauma_last_5_years,
                             row.head_trauma_more_than_5_years, row.neck_trauma_last_5_years,
                             row.neck_trauma_more_than_5_years, row.cervical_trauma_last_5_years,
                             row.cervical_trauma_more_than_5_years, row.thoracic_trauma_last_5_years,
                             row.thoracic_trauma_more_than_5_years, row.lumbo_sacral_trauma_last_5_years,
                             row.lumbo_sacral_trauma_more_than_5_years, row.cervical_spine_surgery_last_5_years,
                             row.cervical_spine_surgery_more_than_5_years, row.thoracic_spine_surgery_last_5_years,
                             row.thoracic_spine_surgery_more_than_5_years, row.lumbo_sacral_spine_surgery_last_5_years,
                             row.lumbo_sacral_spine_surgery_more_than_5_years, row.upper_limb_surgery_last_5_years,
                             row.upper_limb_surgery_more_than_5_years, row.lower_limb_surgery_last_5_years,
                             row.lower_limb_surgery_more_than_5_years, row.abdominal_surgery_last_5_years,
                             row.abdominal_surgery_more_than_5_years, row.thoracic_surgery_last_5_years,
                             row.thoracic_surgery_more_than_5_years, row.pelvic_surgery_last_5_years,
                             row.pelvic_surgery_more_than_5_years, row.head_neck_surgery_last_5_years,
                             row.head_neck_surgery_more_than_5_years])


# export in csv the visits data
# input: graph, file name
# output: csv file
def visitsQuery(graph, fileName):
    # define query
    query_string = """
    PREFIX BTO_schema: <https://w3id.org/brainteaser/ontology/schema/>
    PREFIX NCIT: <http://purl.obolibrary.org/obo/NCIT_>
    SELECT ?id ?date_spiro ?fvcValue ?date_alsfrs_r ?alsfrs_r_tot_score ?bulbar_subscore ?motor_subscore ?respiratory_subscore ?q1 ?q2 ?q3 ?q4 ?q5 ?q6 ?q7 ?q8 ?q9 ?q10 ?q11 ?q12 WHERE {
    {
        # Spiro
        SELECT ?id ?date_spiro ?fvcValue WHERE {
            ?idURI a NCIT:C16960 ; BTO_schema:undergo ?eventURI . 
            ?eventURI a NCIT:C74589 ; BTO_schema:startDate ?date_spiro ; BTO_schema:consists ?testURI . 
            ?testURI a NCIT:C38081 ; BTO_schema:FVCrelative ?fvcValue . 
            bind( substr( (str(?idURI)), 48) as ?id)
            } ORDER BY ?id ?date_spiro
    }
    UNION
    {   
        # ALSFRS-R
        SELECT ?id ?date_alsfrs_r ?alsfrs_r_tot_score ?bulbar_subscore ?motor_subscore ?respiratory_subscore ?q1 ?q2 ?q3 ?q4 ?q5 ?q6 ?q7 ?q8 ?q9 ?q10 ?q11 ?q12 WHERE {
            ?idURI a NCIT:C16960 ; BTO_schema:undergo ?eventURI . 
            ?eventURI a NCIT:C74589 ; BTO_schema:startDate ?date_alsfrs_r ; BTO_schema:consists ?qRURI .
            ?qRURI a BTO_schema:ALSFRS-R ;
                BTO_schema:alsfrs-r-tot ?alsfrs_r_tot_score ;
                BTO_schema:bulbar_subscore ?bulbar_subscore ;
                BTO_schema:motor_subscore ?motor_subscore ;
                BTO_schema:respiratory_subscore ?respiratory_subscore ;
                BTO_schema:alsfrs_1 ?q1 ; 
                BTO_schema:alsfrs_2 ?q2 ; 
                BTO_schema:alsfrs_3 ?q3 ;
                BTO_schema:alsfrs_4 ?q4 ;
                BTO_schema:alsfrs_5 ?q5 ;
                BTO_schema:alsfrs_6 ?q6 ;
                BTO_schema:alsfrs_7 ?q7 ;
                BTO_schema:alsfrs_8 ?q8 ;
                BTO_schema:alsfrs_9 ?q9 ;
                BTO_schema:alsfrs_10 ?q10 ;
                BTO_schema:alsfrs_11 ?q11 ;
                BTO_schema:alsfrs_12 ?q12 .
            bind( substr( (str(?idURI)), 48) as ?id)
        } ORDER BY ?id ?date_alsfrs
    }
    }ORDER BY ?id ?date_alsfrs_r ?date_spiro """
    # perform the query
    res = graph.query(query_string)
    # get the base path
    path = str(Path().resolve())
    # print in output
    with open(path + '/output/csv/' + str(fileName), 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['id', 'date_spiro', 'fvcValue', 'date_alsfrs_r', 'alsfrs_r_tot_score', 'bulbar_subscore', 'motor_subscore', 'respiratory_subscore', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'q11', 'q12'])
        for row in res:
            writer.writerow([row.id, row.date_spiro, row.fvcValue, row.date_alsfrs_r, row.alsfrs_r_tot_score, row.bulbar_subscore, row.motor_subscore, row.respiratory_subscore, row.q1, row.q2, row.q3, row.q4, row.q5, row.q6, row.q7, row.q8, row.q9, row.q10, row.q11, row.q12] )

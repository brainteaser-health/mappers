'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# utils.py contains all the support functions for ALS data mappers of Turin, Lisbon and Madrid

# required libraries
from common import namespaces_als as ns
import mmh3
import uuid
from rdflib import Graph, URIRef
from rdflib.namespace import FOAF, XSD, RDFS, RDF

# generate URI for BTO resources
# input: hospitalName, className and resourceId
# output: URI of the element
def generateURI(hospitalName = None, className = None, resourceId = None):
    if (hospitalName != None and className != None and resourceId != None):
        # create the uri related to an entity for which we have an id in the original dataset
        # in this way in the future we can obtain again its uri id by calling this function
        elementURI = str(hospitalName)+"_" + str(resourceId) + "_"+str(className)
        elementURI = URIRef(ns.BTO_resource[hex(int(mmh3.hash128(elementURI, signed=False, seed=42)))])
    else:
        # if in the original dataset we don't have an id for the resource we randomly generate it on the fly
        elementURI = URIRef(ns.BTO_resource[hex(int(mmh3.hash128(str(uuid.uuid4()), signed=False, seed=42)))])
    # return the generated URI
    return elementURI

# initialize the graph and bind the namespaces
# output: updated graph
def initializeGraph():
    # create the graph
    graph = Graph()
    # bind the imported namespaces (from rdflib.namespace) to a prefix for more readable output
    graph.bind("foaf", FOAF)
    graph.bind("xsd", XSD)
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    # get vars from namespaces.py by excluding python vars (e.g. __init__ ) and bind the other namespaces
    namespaceList = vars(ns).items()
    for x in (namespaceList):
        if not(x[0].startswith('__') or x[0].startswith('_') or x[0] == ('Namespace')):
            # bind the other namespaces to a prefix for more readable output
            graph.bind(str(x[0]), x[1])
    # return the updated graph
    return graph

# graph serialization
# input: graph, path of the saving folder
# output: turtle file
def serializeGraph(graph, savePath, serializationType):
    print("----- start serialization (" + str(serializationType)  + ") -----")
    with open(savePath, 'w', encoding="utf-8") as file:
        file.write(graph.serialize(format=serializationType))
    print("----- end serialization (" + str(serializationType)  + ") -----")


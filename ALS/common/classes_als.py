'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# codes.py contains all the code of classes

from common import namespaces_als as ns

patient = ns.NCIT['C16960']
clinical_trial_partecipation = ns.BTO_schema['Clinical_Trial_Participation']
diagnosis = ns.NCIT['C15220']
onset = ns.NCIT['C25279']
before_onset = ns.BTO_schema['Before_Onset']
clinical_assessment = ns.MAXO['0000487']
event = ns.NCIT['C25499']
visit = ns.NCIT['C39564']
protocol_event = ns.NCIT['C74589']
c9orf72 = ns.OGG['3000203228']
fus = ns.OGG['3000002521']
tardbp = ns.OGG['3000023435']
sod1 = ns.OGG['3000006647']
alsfrs = ns.BTO_schema['ALSFRS']
alsfrs_r = ns.BTO_schema['ALSFRS-R']
pulmonary_function_test = ns.NCIT['C38081']
blood_test = ns.NCIT['C49286']
smoking = ns.NCIT['C154329']
trauma = ns.NCIT['C3671']
surgical_intervention = ns.NCIT['C15329']
place = ns.NCIT['C25319']
cities = ns.UMLS['C0008848']
towns = ns.UMLS['C0557750']
rural_area = ns.UMLS['C0178837']
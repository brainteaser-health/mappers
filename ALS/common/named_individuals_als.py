'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# named_individuals.py contains all the named individuals

from common import namespaces_als as ns

patient = ns.NCIT['C16960']
clinical_trial_madrid = ns.BTO_ni['ClinicalTrial_ALS_Madrid_1']
clinical_trial_turin = ns.BTO_ni['ClinicalTrial_ALS_Turin_1']
clinical_trial_lisbon = ns.BTO_ni['ClinicalTrial_ALS_Lisbon_1']
als = ns.BTO_ni['Amyotrophic_Lateral_Sclerosis']
relative_with_als = ns.BTO_ni['Relative_with_ALS']
niv = ns.BTO_ni['Non-Invasive_Mechanical_Ventilation']
tracheotomy = ns.BTO_ni['Tracheotomy']
peg = ns.BTO_ni['Percutaneous_Endoscopic_Gastrostomy']
smoking = ns.BTO_ni['Smoking']
hypertension = ns.BTO_ni['Hypertension']
diabetes = ns.BTO_ni['Diabetes_Mellitus']
dyslipidemia = ns.BTO_ni['Dyslipidemia']
thyroid_disorder = ns.BTO_ni['Thyroid_Gland_Disorder']
autoimmune_disease = ns.BTO_ni['Autoimmune_Disease']
stroke = ns.BTO_ni['stroke']
cardiovascular_disorder = ns.BTO_ni['Cardiovascular_Disorder']
primary_neoplasm = ns.BTO_ni['Primary_Neoplasm']
sod1 = ns.BTO_ni['SOD1']
c9orf72 = ns.BTO_ni['C9orf72']
tardbp = ns.BTO_ni['TARDBP']
fus = ns.BTO_ni['FUS']
head = ns.BTO_ni['head']
neck = ns.BTO_ni['neck']
cervical_area = ns.BTO_ni['cervical_region_of_vertebral_column']
thoracic = ns.BTO_ni['thoracic_skeleton']
thoracic_spinal_cord = ns.BTO_ni['thoracic_spinal_cord']
lumbo_sacral = ns.BTO_ni['lumbosacral_nerve_plexus']
upper_limb = ns.BTO_ni['upper_limb']
lower_limb = ns.BTO_ni['lower_limb']
abdominal_fascia = ns.BTO_ni['abdominal_fascia']
pelvic = ns.BTO_ni['pelvic_complex']
head_neck = ns.BTO_ni['head-neck']
major_trauma_before_onset = ns.BTO_ni['Major_Truma_Before_Onset']
surgical_interventions_before_onset = ns.BTO_ni['Surgical_Interventions_Before_Onset']

'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# namespaces.py contains the useful namespaces for mapping data

# required libraries
from rdflib import Namespace

# Brainteaser ontology schema
BTO_schema = Namespace("https://w3id.org/brainteaser/ontology/schema/")
# Brainteaser ontology named individuals
BTO_ni = Namespace("https://w3id.org/brainteaser/ontology/named-individual/")
# Brainteaser ontology resources
BTO_resource = Namespace("https://w3id.org/brainteaser/ontology/resource/")
# NCIT ontology
NCIT = Namespace("http://purl.obolibrary.org/obo/NCIT_")
# UMLS ontology
UMLS = Namespace("https://uts.nlm.nih.gov/uts/umls/concept/")
# OMIT ontology
OMIT = Namespace("http://purl.obolibrary.org/obo/OMIT_")
# OGG ontology
OGG = Namespace("http://purl.obolibrary.org/obo/OGG_")
# MAXO ontology
MAXO = Namespace("http://purl.obolibrary.org/obo/MAXO_")

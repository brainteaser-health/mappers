'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# common_pre_proccessing.py contains all the common pre-processing functions for ALS data

# required libraries
import pandas as pd

# get the date of last visit
# input: alsfrs, spiro dataframes
# output: dictionary with date of last visit for each patient
def getDateOfLastVisit(alsfrs, fvc):
    # initialize dictionary
    lastVisitDict = {}
    # iterate over alsfrs dataframe
    for index, row in alsfrs.iterrows():
        # date of first ALSFRS-R
        maxDate = alsfrs.loc[alsfrs.index == index].Date.max()
        # date of first Spiro
        if ((fvc.index == index).any() == True):
            dateSpiro = fvc.loc[fvc.index == index].Date.max()
            if dateSpiro > maxDate:
                maxDate = dateSpiro
        lastVisitDict[index] = maxDate
    # return the dictionary
    return lastVisitDict

# get the list of patient with at least one ALSFRS-R and one Spiro
# input: staticVars, ALSFRS, spiro dataframes
# output: list of patient with at least one ALSFRS-R and one Spiro
def getPatientWithALSFRSandSpiro(staticVars, alsfrs, spiro):
    #get unique IDs from staticVars, ALSFRS and spiro dataframes
    idStaticVars = staticVars.index.unique()
    idALSFRSList = alsfrs.index.unique()
    idSpiroList = spiro.index.unique()
    correctList = []
    incorrectList = []
    numCorrectALSFRS = 0
    numCorrectSpiro = 0
    numIncorrectALSFRS = 0
    numIncorrectSpiro = 0
    # compute the intersection between three groups
    for value in idStaticVars:
        if (value in idALSFRSList) and (value in idSpiroList):
            correctList.append(value)
            numCorrectALSFRS += len(alsfrs.loc[alsfrs.index == value])
            numCorrectSpiro += len(spiro.loc[spiro.index == value])
        else:
            incorrectList.append(value)
            numIncorrectALSFRS += len(alsfrs.loc[alsfrs.index == value])
            numIncorrectSpiro += len(spiro.loc[spiro.index == value])
    print('\nNumber of patients: %s\n'
          'Number of patients with at least one ALSFRS-R: %s\n'
          'Number of patients with at least one spiro: %s\n'
          'Number of patients with at least one ALSFRS-R and one Spiro: %s (total ASLFRS-R: %s, total Spiro: %s)'
          % (len(idStaticVars), len(idALSFRSList), len(idSpiroList), len(correctList), numCorrectALSFRS, numCorrectSpiro))
    if (len(incorrectList) > 0):
        print('List of patient IDs that do not have a visit or do not have both types of visits (ALSFRS-R and Spiro):')
        text = ''
        for elem in incorrectList:
            text = text + str(elem) + ', '
        print(text[:-2])
    # ----- test
    # correctList = [value for value in idALSFRSList if value in idSpiroList]
    # correctList = list(set(idALSFRSList) | set(idSpiroList))
    # ----- end test
    # return the list of patient with at least one ALSFRS-R and one Spiro
    return correctList

# get the list of patient with at least one ALSFRS-R
# input: staticVars, ALSFRS dataframes
# output: list of patient with at least one ALSFRS-R and the dictionary with the date of first ALSFRS-R
def getPatientWithALSFRS(staticVars, alsfrs):
    #get unique IDs from staticVars, ALSFRS and spiro dataframes
    idStaticVars = staticVars.index.unique()
    idALSFRSList = alsfrs.index.unique()
    correctList = []
    incorrectList = []
    numCorrectALSFRS = 0
    numIncorrectALSFRS = 0
    dictFirstDate = {}
    dictLastDate = {}
    # compute the intersection between three groups
    for value in idStaticVars:
        if (value in idALSFRSList):
            correctList.append(value)
            numCorrectALSFRS += len(alsfrs.loc[alsfrs.index == value])
            dateFirstALSFRS = alsfrs.loc[alsfrs.index == value].Date.min()
            dateLastALSFRS = alsfrs.loc[alsfrs.index == value].Date.max()
            dictFirstDate[value] = dateFirstALSFRS
            dictLastDate[value] = dateLastALSFRS
        else:
            incorrectList.append(value)
            numIncorrectALSFRS += len(alsfrs.loc[alsfrs.index == value])
    print('\nNumber of patients: %s\n'
          'Number of patients with at least one ALSFRS-R: %s (total ASLFRS-R: %s)'
          % (len(idStaticVars), len(idALSFRSList), numCorrectALSFRS))
    if (len(incorrectList) > 0):
        print('List of patient IDs that do not have an ALSFRS-R:')
        text = ''
        for elem in incorrectList:
            text = text + str(elem) + ', '
        print(text[:-2])
    # return the list of patient with at least one ALSFRS-R and the dictionaries with the date of first and last ALSFRS-R
    return correctList, dictFirstDate, dictLastDate

# get the list of patient with correct data
# input: static vars dataframe, list of patient with correct dates, list of patient with at least one ALSFRS-R, list of patient with correct events (NIV, PEG, Tracheo)
# output: list of patient with correct data
def getCorrectPatient(staticVars, alsfrs, fvc, patientWithCorrectDateList, patientWithALSFRS, patientWithCorrectEvent, patientAliveOrWithCorrectDeath):
    # initialize list
    correctList = []
    incorrectList = []
    # counter for ALSFRS and FVC
    alsfrsCounter = 0
    fvcCounter = 0
    # get unique IDs from staticVars
    idStaticVars = staticVars.index.unique()
    # compute the intersection between four groups
    for value in idStaticVars:
        if (value in patientWithCorrectDateList) and (value in patientWithALSFRS) and (value in patientWithCorrectEvent) and (value in patientAliveOrWithCorrectDeath):
            correctList.append(value)
        else:
            incorrectList.append(value)
    # get the ALSFRS number for these patients
    for value in correctList:
        alsfrsCounter += len(alsfrs.loc[alsfrs.index == value])
        fvcCounter += len(fvc.loc[fvc.index == value])

    print('\nNumber of patients: %s\n'
          'Number of patients with correct data: %s (total ASLFRS-R: %s, total Spiro: %s)\n'
          'Number of patients with incorrect data (excluded from mapping): %s'
          % (len(idStaticVars), len(correctList), alsfrsCounter, fvcCounter, len(incorrectList)))
    if (len(incorrectList) > 0):
        print('List of patient IDs excluded from mapping:')
        text = ''
        for elem in incorrectList:
            text = text + str(elem) + ', '
        print(text[:-2])
    # return the list
    return correctList

# get the list of patient with correct data
# input: static vars dataframe, list of patient with correct dates, list of patient with at least one ALSFRS-R and one Spiro
# output: list of patient with correct data
def getCorrectPatientOLDVERSION(staticVars, patientWithCorrectDateList, patientWithALSFRSandSPIROList):
    # initialize list
    correctList = []
    incorrectList = []
    # get unique IDs from staticVars
    idStaticVars = staticVars.index.unique()
    # compute the intersection between three groups
    for value in idStaticVars:
        if (value in patientWithCorrectDateList) and (value in patientWithALSFRSandSPIROList):
            correctList.append(value)
        else:
            incorrectList.append(value)
    print('\nNumber of patients: %s\n'
          'Number of patients with correct data: %s\n'
          'Number of patients with incorrect data (excluded from mapping): %s'
          % (len(idStaticVars), len(correctList), len(incorrectList)))
    if (len(incorrectList) > 0):
        print('List of patient IDs excluded from mapping:')
        text = ''
        for elem in incorrectList:
            text = text + str(elem) + ', '
        print(text[:-2])
    # return the list
    return correctList


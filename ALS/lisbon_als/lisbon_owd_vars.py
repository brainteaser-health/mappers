'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# lisbon_owd_vars.py contains all the custom functions to manage only the LISBON OWD vars tab

# required libraries
import pandas as pd
from common import namespaces_als as ns
from common import classes_als as classes
from common import utils_als
from common import named_individuals_als as ni
from rdflib import Literal
from rdflib.namespace import XSD, RDF

# add blood test vars
# input: graph, patient ID, diagnosis URI, row of blood test vars
# output: updated graph
def addBloodTest(graph, patientID, diagnosis, row):
    # check if a patient has a instance of class diagnosis
    if diagnosis != '999':
        # generate blood test URI
        bloodTest = utils_als.generateURI()
        # add blood test to the graph
        graph.add((bloodTest, RDF.type, classes.blood_test))
        # link blood test to diagnosis
        graph.add((diagnosis, ns.BTO_schema['consists'], bloodTest))
        # add all the blood test values
        graph = addCKLevel(graph, patientID, bloodTest, row[0], row[1], row[2], row[3])
        graph = addAlbuminLevel(graph, patientID, bloodTest, row[4], row[5], row[6], row[7])
        graph = addCreatinineLevel(graph, patientID, bloodTest, row[8], row[9], row[10], row[11])
        graph = addTotalCholesterolLevel(graph, patientID, bloodTest, row[12], row[13], row[14],row[15])
        graph = addHDLCholesterolLevel(graph, patientID, bloodTest, row[16], row[17], row[18],row[19])
        graph = addLDLCholesterolLevel(graph, patientID, bloodTest, row[20], row[21], row[22],row[23])
        graph = addTriglyceridesLevel(graph, patientID, bloodTest, row[24], row[25], row[26],row[27])
    else:
        print('[WARNING] Diagnosis not present for patient ID: %s, (Cannot add the blood test to the diagnosis!)' % (patientID))
    # return the updated graph
    return graph

# add trauma and intervention vars
# input: graph, patient ID, diagnosis URI, data of traumas and interventions in the last 5 years
# output: updated graph
def addTraumaAndInterventionLast5Years(graph, patientID, beforeOnsetLast5Years, row):
    # 0  Head/Neck trauma in the last 5 years
    # 1  Cervical trauma in the last 5 years
    # 2  Thoracic trauma in the last 5 years
    # 3  Lumbo-sacral trauma in the last 5 years
    # 4  Cervical Spine surgery in the last 5 years
    # 5  Thoracic Spine surgery in the last 5 years
    # 6  Lumbo-Sacral Spine surgery in the last 5 years
    # 7  Upper Limb surgery in the last years
    # 8  Lower Limb surgery in the last 5 years
    # 9  Abdominal surgery in the last 5 years
    # 10  Thoracic surgery in the last 5 years
    # 11  Pelvic Surgery in the last 5 years
    # 12  Head/Neck Surgery in the last 5 years

    # head/neck trauma
    if not pd.isna(row[0]):
        if (str(row[0]).replace(' ', '').lower() == 'head'):
            graph = addTrauma(graph, patientID, beforeOnsetLast5Years, ni.head)
        elif (str(row[0]).replace(' ', '').lower() == 'neck'):
            graph = addTrauma(graph, patientID, beforeOnsetLast5Years, ni.neck)
        elif (str(row[0]).replace(' ', '').lower() != 'no') and (str(row[0]).replace(' ', '').lower() != 'head') and (str(row[0]).replace(' ', '').lower() != 'neck'):
            print('[WARNING] Head/Neck trauma (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Head/Neck trauma (in the last 5 years) not given for patient ID: %s' % (patientID))
    # cervical trauma
    if not pd.isna(row[1]):
        if (str(row[1]).replace(' ', '').lower() == 'yes'):
            graph = addTrauma(graph, patientID, beforeOnsetLast5Years, ni.cervical_area)
        elif (str(row[1]).replace(' ', '').lower() != 'no') and (str(row[1]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Cervical trauma (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Cervical trauma (in the last 5 years) not given for patient ID: %s' % (patientID))
    # thoracic trauma
    if not pd.isna(row[2]):
        if (str(row[2]).replace(' ', '').lower() == 'yes'):
            graph = addTrauma(graph, patientID, beforeOnsetLast5Years, ni.thoracic)
        elif (str(row[2]).replace(' ', '').lower() != 'no') and (str(row[2]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Thoracic trauma (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Thoracic trauma (in the last 5 years) not given for patient ID: %s' % (patientID))
    # lumbo-sacral trauma
    if not pd.isna(row[3]):
        if (str(row[3]).replace(' ', '').lower() == 'yes'):
            graph = addTrauma(graph, patientID, beforeOnsetLast5Years, ni.lumbo_sacral)
        elif (str(row[3]).replace(' ', '').lower() != 'no') and (str(row[3]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Lumbo-sacral trauma (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Lumbo-sacral trauma (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add cervical spine intervention
    if not pd.isna(row[4]):
        if (str(row[4]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.cervical_area)
        elif (str(row[4]).replace(' ', '').lower() != 'no') and (str(row[4]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Cervical spine intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Cervical spine intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add thoracic spine intervention
    if not pd.isna(row[5]):
        if (str(row[5]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.thoracic_spinal_cord)
        elif (str(row[5]).replace(' ', '').lower() != 'no') and (str(row[5]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Thoracic spine intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Thoracic spine intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add lumbo sacral spine intervention
    if not pd.isna(row[6]):
        if (str(row[6]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.lumbo_sacral)
        elif (str(row[6]).replace(' ', '').lower() != 'no') and (str(row[6]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Lumbo sacral spine intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Lumbo sacral spine intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add upper limb intervention
    if not pd.isna(row[7]):
        if (str(row[7]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.upper_limb)
        elif (str(row[7]).replace(' ', '').lower() != 'no') and (str(row[7]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Upper limb intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Upper limb (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add lower limb intervention
    if not pd.isna(row[8]):
        if (str(row[8]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.lower_limb)
        elif (str(row[8]).replace(' ', '').lower() != 'no') and (str(row[8]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Lower limb intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Lower limb intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add abdominal fascia intervention
    if not pd.isna(row[9]):
        if (str(row[9]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.abdominal_fascia)
        elif (str(row[9]).replace(' ', '').lower() != 'no') and (str(row[9]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Abdominal fascia intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Abdominal fascia intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add thoracic intervention
    if not pd.isna(row[10]):
        if (str(row[10]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.thoracic)
        elif (str(row[10]).replace(' ', '').lower() != 'no') and (str(row[10]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Thoracic intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Thoracic intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add pelvic intervention
    if not pd.isna(row[11]):
        if (str(row[11]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.pelvic)
        elif (str(row[11]).replace(' ', '').lower() != 'no') and (str(row[11]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Pelvic intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Pelvic intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # add head/neck intervention
    if not pd.isna(row[12]):
        if (str(row[12]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetLast5Years, ni.head_neck)
        elif (str(row[12]).replace(' ', '').lower() != 'no') and (str(row[12]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Head/Neck intervention (in the last 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Head/Neck intervention (in the last 5 years) not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add trauma and intervention vars
# input: graph, patient ID, diagnosis URI, data of traumas and interventions (more than 5 years)
# output: updated graph
def addTraumaAndInterventionMoreThan5Years(graph, patientID, beforeOnsetMoreThan5Years, row):
    # 0  Head/Neck trauma > 5 years
    # 1  Cervical trauma > 5 years
    # 2  Thoracic trauma > 5 years
    # 3  Lumbo-sacral trauma > 5 years
    # 4  Cervical Spine surgery > 5 years
    # 5  Thoracic Spine surgery > 5 years
    # 6  Lumbo-Sacral Spine surgery > 5 years
    # 7  Upper Limb surgery > 5 years
    # 8  Lower Limb surgery > 5 years
    # 9  Abdominal surgery > 5 years
    # 10  Thoracic surgery > 5 years
    # 11  Pelvic Surgery > 5 years
    # 12  Head/Neck Surgery > 5 years

    # head/neck trauma
    if not pd.isna(row[0]):
        if (str(row[0]).replace(' ', '').lower() == 'head'):
            graph = addTrauma(graph, patientID, beforeOnsetMoreThan5Years, ni.head)
        elif (str(row[0]).replace(' ', '').lower() == 'neck'):
            graph = addTrauma(graph, patientID, beforeOnsetMoreThan5Years, ni.neck)
        elif (str(row[0]).replace(' ', '').lower() != 'no') and (str(row[0]).replace(' ', '').lower() != 'head') and (str(row[0]).replace(' ', '').lower() != 'neck'):
            print('[WARNING] Head/Neck trauma (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Head/Neck trauma (> 5 years) not given for patient ID: %s' % (patientID))
    # cervical trauma
    if not pd.isna(row[1]):
        if (str(row[1]).replace(' ', '').lower() == 'yes'):
            graph = addTrauma(graph, patientID, beforeOnsetMoreThan5Years, ni.cervical_area)
        elif (str(row[1]).replace(' ', '').lower() != 'no') and (str(row[1]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Cervical trauma (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Cervical trauma (> 5 years) not given for patient ID: %s' % (patientID))
    # thoracic trauma
    if not pd.isna(row[2]):
        if (str(row[2]).replace(' ', '').lower() == 'yes'):
            graph = addTrauma(graph, patientID, beforeOnsetMoreThan5Years, ni.thoracic)
        elif (str(row[2]).replace(' ', '').lower() != 'no') and (str(row[2]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Thoracic trauma (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Thoracic trauma (> 5 years) not given for patient ID: %s' % (patientID))
    # lumbo-sacral trauma
    if not pd.isna(row[3]):
        if (str(row[3]).replace(' ', '').lower() == 'yes'):
            graph = addTrauma(graph, patientID, beforeOnsetMoreThan5Years, ni.lumbo_sacral)
        elif (str(row[3]).replace(' ', '').lower() != 'no') and (str(row[3]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Lumbo-sacral trauma (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Lumbo-sacral trauma (> 5 years) not given for patient ID: %s' % (patientID))
    # add cervical spine intervention
    if not pd.isna(row[4]):
        if (str(row[4]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.cervical_area)
        elif (str(row[4]).replace(' ', '').lower() != 'no') and (str(row[4]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Cervical spine intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Cervical spine intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add thoracic spine intervention
    if not pd.isna(row[5]):
        if (str(row[5]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.thoracic_spinal_cord)
        elif (str(row[5]).replace(' ', '').lower() != 'no') and (str(row[5]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Thoracic spine intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Thoracic spine intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add lumbo sacral spine intervention
    if not pd.isna(row[6]):
        if (str(row[6]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.lumbo_sacral)
        elif (str(row[6]).replace(' ', '').lower() != 'no') and (str(row[6]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Lumbo sacral spine intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Lumbo sacral spine intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add upper limb intervention
    if not pd.isna(row[7]):
        if (str(row[7]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.upper_limb)
        elif (str(row[7]).replace(' ', '').lower() != 'no') and (str(row[7]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Upper limb intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Upper limb (> 5 years) not given for patient ID: %s' % (patientID))
    # add lower limb intervention
    if not pd.isna(row[8]):
        if (str(row[8]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.lower_limb)
        elif (str(row[8]).replace(' ', '').lower() != 'no') and (str(row[8]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Lower limb intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Lower limb intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add abdominal fascia intervention
    if not pd.isna(row[9]):
        if (str(row[9]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.abdominal_fascia)
        elif (str(row[9]).replace(' ', '').lower() != 'no') and (str(row[9]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Abdominal fascia intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Abdominal fascia intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add thoracic intervention
    if not pd.isna(row[10]):
        if (str(row[10]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.thoracic)
        elif (str(row[10]).replace(' ', '').lower() != 'no') and (str(row[10]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Thoracic intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Thoracic intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add pelvic intervention
    if not pd.isna(row[11]):
        if (str(row[11]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.pelvic)
        elif (str(row[11]).replace(' ', '').lower() != 'no') and (str(row[11]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Pelvic intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Pelvic intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # add head/neck intervention
    if not pd.isna(row[12]):
        if (str(row[12]).replace(' ', '').lower() == 'yes'):
            graph = addSurgicalIntervention(graph, patientID, beforeOnsetMoreThan5Years, ni.head_neck)
        elif (str(row[12]).replace(' ', '').lower() != 'no') and (str(row[12]).replace(' ', '').lower() != 'yes'):
            print('[WARNING] Head/Neck intervention (> 5 years) not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Head/Neck intervention (> 5 years) not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add smoking behaviour from OWD vars tab
# input: graph, patient ID, event smoking URI, smoking value, stopped smoking value, start and end date, packs day and packs year
# output: updated graph
def addSmokingOWD(graph, patientID, patientURI, eventSmoking, smokingValue, stoppedSmoking, startDate, endDate, packsDay, packsYear):
    # 1 = smoker patient
    if (smokingValue == 1):
        if eventSmoking == '999':
            # generate URI for new event smoking
            eventSmoking = utils_als.generateURI()
            # add event smoking to graph
            graph.add((eventSmoking, RDF.type, classes.event))
            # link event to patient
            graph.add((patientURI, ns.BTO_schema['undergo'], eventSmoking))
        # add smoking (named individual)
        graph.add((eventSmoking, ns.BTO_schema['hasRegisteredBehaviour'], ni.smoking))
        # if more informations about smoking are present, remove the named individual and create a smoking instance
        if not pd.isna(packsDay) or not pd.isna(packsYear) or not pd.isna(startDate) or not pd.isna(endDate):
            # remove named individual
            graph.remove((eventSmoking, ns.BTO_schema['hasRegisteredBehaviour'], ni.smoking))
            # generate smoking URI
            smoking = utils_als.generateURI()
            # add smoking instance to graph
            graph.add((smoking, RDF.type, classes.smoking))
            # link smoking to event
            graph.add((eventSmoking, ns.BTO_schema['hasRegisteredBehaviour'], smoking))
            if not pd.isna(packsYear):
                graph.add((smoking, ns.BTO_schema['packYear'], Literal(packsYear, datatype=XSD.float)))
            else:
                print('[WARNING] Packs year value not given for patient ID: %s' % (patientID))
            if not pd.isna(packsDay):
                graph.add((smoking, ns.BTO_schema['dailyCigarettes'], Literal(packsDay*20, datatype=XSD.float)))
            else:
                print('[WARNING] Packs day value not given for patient ID: %s' % (patientID))
            if not pd.isna(startDate):
                graph.add((smoking, ns.BTO_schema['startYear'], Literal(startDate, datatype=XSD.gYear)))
            else:
                print('[WARNING] Smoking start year not given for patient ID: %s' % (patientID))
            if not pd.isna(stoppedSmoking) and stoppedSmoking == 1:
                if not pd.isna(endDate):
                    graph.add((smoking, ns.BTO_schema['endYear'], Literal(endDate, datatype=XSD.gYear)))
                else:
                    print('[WARNING] Stopped smoking = 1, but end year not valid for patient ID: %s' % (patientID))
            elif not pd.isna(stoppedSmoking) and stoppedSmoking != 1 and stoppedSmoking != 2:
                print('[WARNING] Stopped smoking value not valid for patient ID: %s' % (patientID))
            else:
                print('[WARNING] Stopped smoking value not given for patient ID: %s' % (patientID))
    elif not pd.isna(smokingValue) and smokingValue != 1 and smokingValue != 2:
        print('[WARNING] Smoking value not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Smoking value not given for patient ID: %s' % (patientID))
    #return the updated graph
    return graph

# check data presence in blood test row
# input: blood test row
# output: boolean value
def checkBloodTestDataPresenceOWD(bloodTestRow):
    # initialize the variable with False
    check = False
    # first check if the unit of measure is not null, after check that the level, upper or lower range are not null
    x = 0
    while x < len(bloodTestRow):
        if not pd.isna(bloodTestRow[x + 3]):
            if not pd.isna(bloodTestRow[x]) or not pd.isna(bloodTestRow[x+1]) or not pd.isna(bloodTestRow[x+2]):
                check = True
                break
        x += 4
    # return boolean value
    return check

# check data presence in trauma and intervention row
# input: trauma and intervention row
# output: boolean value
def checkTraumaAndInterventionDataPresenceOWD(traumaAndInterventionRow):
    # initialize the variable with False
    check = False
    for value in traumaAndInterventionRow:
        if not pd.isna(value):
            string = str(value).replace(' ', '').lower()
            # if there is at least one value, break
            if string == 'yes' or string == 'head' or string == 'neck':
                check = True
                break
    # return boolean value
    return check

# add ck level (unit of measurement: U/L)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addCKLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        if str(unit).lower() == 'u/l' or str(unit).lower() == 'ui/l':
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['CK_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty CK level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['CK_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty CK lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['CK_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty CK upper range for patient ID: %s' % (patientID))
        else:
            print('[WARNING] Different unit of measurement (%s) for CK, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for CK, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add albumin level (unit of measurement: g/dL)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addAlbuminLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        # if unit of measurement = g/dL, store the values
        if (str(unit) == 'g/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Albumin_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin level for patient ID:  %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Albumin_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Albumin_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin upper range for patient ID: %s' % (patientID))
        # if unit of measurement = g/L, multiply the values by 10 and store the values
        elif (str(unit) == 'g/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Albumin_level'], Literal(level*10, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Albumin_lower_range'], Literal(lower*10, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Albumin_upper_range'], Literal(upper*10, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin upper range for patient ID:  %s' % (patientID))
        # if unit of measurement = mg/dL, multiply the values by 1000 and store the values
        elif (str(unit) == 'mg/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Albumin_level'], Literal(level*1000, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Albumin_lower_range'], Literal(lower*1000, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Albumin_upper_range'], Literal(upper*1000, datatype=XSD.float)))
            else:
                print('[WARNING] Empty albumin upper range for patient ID: %s' % (patientID))
        elif not pd.isna(unit):
            print('[WARNING] Different unit of measurement (%s) for albumin, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for albumin, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add creatinine level (unit of measurement: mg/dL)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addCreatinineLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        # if unit of measurement = mg/dL, store the values
        if (str(unit) == 'mg/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Creatinine_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty creatinine level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Creatinine_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty creatinine lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Creatinine_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty creatinine upper range for patient ID: %s' % (patientID))
        elif not pd.isna(unit):
            print('[WARNING] Different unit of measurement (%s) for creatinine, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for creatinine, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add total cholesterol level (unit of measurement: mg/dL)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addTotalCholesterolLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        # if unit of measurement = mg/dL, store the values
        if (str(unit) == 'mg/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol upper range for patient ID: %s' % (patientID))
        # if unit of measurement = g/L, multiply the values by 100 and store the values
        elif (str(unit) == 'g/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_level'], Literal(level*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_lower_range'], Literal(lower*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_upper_range'], Literal(upper*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol upper range for patient ID: %s' % (patientID))
        # if unit of measurement = mmol/L, multiply the values by 18 and store the values
        elif (str(unit) == 'mmol/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_level'], Literal(level*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_lower_range'], Literal(lower*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Total_Cholesterol_upper_range'], Literal(upper*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty total cholesterol upper range for patient ID: %s' % (patientID))
        elif not pd.isna(unit):
            print('[WARNING] Different unit of measurement (%s) for total cholesterol, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for total cholesterol, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add HDL total cholesterol level (unit of measurement: mg/dL)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addHDLCholesterolLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        # if unit of measurement = mg/dL, store the values
        if (str(unit) == 'mg/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol upper range for patient ID: %s' % (patientID))
        # if unit of measurement = g/L, multiply the values by 100 and store the values
        elif (str(unit) == 'g/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_level'], Literal(level*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_lower_range'], Literal(lower*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_upper_range'], Literal(upper*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol upper range for patient ID: %s' % (patientID))
        # if unit of measurement = mmol/L, multiply the values by 18 and store the values
        elif (str(unit) == 'mmol/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_level'], Literal(level*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_lower_range'], Literal(lower*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty HDL cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['HDL_Cholesterol_upper_range'], Literal(upper*18, datatype=XSD.float)))
            print('[WARNING] Empty HDL cholesterol upper range for patient ID: %s' % (patientID))
        elif not pd.isna(unit):
            print('[WARNING] Different unit of measurement (%s) for HDL cholesterol, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for HDL cholesterol, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add LDL total cholesterol level (unit of measurement: mg/dL)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addLDLCholesterolLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        # if unit of measurement = mg/dL, store the values
        if (str(unit) == 'mg/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol upper range for patient ID: %s' % (patientID))
        # if unit of measurement = g/L, multiply the values by 100 and store the values
        elif (str(unit) == 'g/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_level'], Literal(level*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_lower_range'], Literal(lower*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_upper_range'], Literal(upper*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol upper range for patient ID: %s' % (patientID))
        # if unit of measurement = mmol/L, multiply the values by 18 and store the values
        elif (str(unit) == 'mmol/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_level'], Literal(level*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_lower_range'], Literal(lower*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['LDL_Cholesterol_upper_range'], Literal(upper*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty LDL cholesterol upper range for patient ID: %s' % (patientID))
        elif not pd.isna(unit):
            print('[WARNING] Different unit of measurement (%s) for LDL cholesterol, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for LDL cholesterol, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add triglycerides level (unit of measurement: mg/dL)
# input: graph, patientID, bloodTest instance, level, lower range, upper range, unit of measurement
# output: updated graph
def addTriglyceridesLevel(graph, patientID, bloodTest, level, lower, upper, unit):
    if not pd.isna(unit) and (not pd.isna(level) or not pd.isna(lower) or not pd.isna(upper)):
        # if unit of measurement = mg/dL, store the values
        if (str(unit) == 'mg/dL'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_level'], Literal(level, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_lower_range'], Literal(lower, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_upper_range'], Literal(upper, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides upper range for patient ID: %s' % (patientID))
        # if unit of measurement = g/L, multiply the values by 100 and store the values
        elif (str(unit) == 'g/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_level'], Literal(level*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_lower_range'], Literal(lower*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_upper_range'], Literal(upper*100, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides upper range for patient ID: %s' % (patientID))
        # if unit of measurement = mmol/L, multiply the values by 18 and store the values
        elif (str(unit) == 'mmol/L'):
            if not pd.isna(level):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_level'], Literal(level*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides level for patient ID: %s' % (patientID))
            if not pd.isna(lower):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_lower_range'], Literal(lower*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides lower range for patient ID: %s' % (patientID))
            if not pd.isna(upper):
                graph.add((bloodTest, ns.BTO_schema['Triglycerides_upper_range'], Literal(upper*18, datatype=XSD.float)))
            else:
                print('[WARNING] Empty triglycerides upper range for patient ID: %s' % (patientID))
        elif not pd.isna(unit):
            print('[WARNING] Different unit of measurement (%s) for triglycerides, patient ID: %s, (Cannot add blood test!)' % (str(unit), patientID))
    else:
        print('[WARNING] Unit of measurement not given for triglycerides, patient ID: %s, (Cannot add blood test!)' % (patientID))
    # return the updated graph
    return graph

# add surgical intervention to beforeOnset
# input: graph, patient ID, before onset URI and the named individual of the anatomical area
# output: updated graph
def addSurgicalIntervention(graph, patientID, beforeOnset, anatomicalArea):
    # get URI for surgical intervention
    surgical = utils_als.generateURI()
    # add surgical intervention to graph
    graph.add((surgical, RDF.type, classes.surgical_intervention))
    # link surgical intervention to before onset
    graph.add((beforeOnset, ns.BTO_schema['consists'], surgical))
    # link anatomical area
    graph.add((surgical, ns.BTO_schema['surgicalArea'], anatomicalArea))
    # return the updated graph
    return graph

# add trauma to beforeOnset
# input: graph, patient ID, before onset URI and trauma type named individual
# output: updated graph
def addTrauma(graph, patientID, beforeOnset, traumaType):
    # get trauma URI
    trauma = utils_als.generateURI()
    # add trauma to graph
    graph.add((trauma, RDF.type, classes.trauma))
    # link trauma to before onset
    graph.add((beforeOnset, ns.BTO_schema['hasTrauma'], trauma))
    # link trauma area
    graph.add((trauma, ns.BTO_schema['traumaArea'], traumaType))
    # return the updated graph
    return graph

# generate new beforeOnset instance
# input: graph and howLong ('> 5 years' or 'in the last 5 years')
# output: updated graph and before onset URI
def appendBeforeOnset(graph, patient, howLong):
    # get before onset URI
    beforeOnset = utils_als.generateURI()
    # add before onset to graph
    graph.add((beforeOnset, RDF.type, classes.before_onset))
    # link before onset to patient
    graph.add((patient, ns.BTO_schema['undergo'], beforeOnset))
    # add howLong data property
    graph.add((beforeOnset, ns.BTO_schema['howLong'], Literal(howLong, datatype=XSD.string)))
    # return the updated graph and before onset URI
    return graph, beforeOnset
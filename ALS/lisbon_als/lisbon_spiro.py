'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# lisbon_spiro.py contains all the custom functions to manage the LISBON spiro tab

import csv
import pandas as pd
from pathlib import Path
from tabulate import tabulate
from common import namespaces_als as ns
from common import classes_als as classes
from common import utils_als
from rdflib import Literal
from rdflib.namespace import XSD, RDF

# generate a new csv file moving visits from column to row
# input: dataset path of lisbon
# output: lisbon-spiro.csv
def breakLisbonSpiroColumns(datasetPath):
    #print('----- generate a new csv file moving spiro from column to row -----')
    # get the base path
    path = str(Path().resolve())
    # read fvc tab
    fvc = pd.read_excel(datasetPath, sheet_name="%FVC", index_col="REF")
    # open new csv file into dataset folder
    with open(path + '/dataset/lisbon-fvc.csv', 'w') as file:
        writer = csv.writer(file)
        # column labels
        writer.writerow(['REF', 'Date', '%FVC'])
        # get column labels from excel file
        columnsName = fvc.columns.values
        columnsNumber = len(columnsName)
        fvcArray = []
        for index, row in fvc.iterrows():
            i = 0
            while i < columnsNumber:
                tempArray = [index]
                check = False
                for x in range(i, i + 2):
                    if not pd.isna(row[columnsName[x]]):
                        check = check or True
                if check == False:
                    i += 2
                else:
                    for x in range(i, i + 2):
                        tempArray.append(row[columnsName[x]])
                    fvcArray.append(tempArray)
                    i += 2
        # write into csv file
        for fvcTest in fvcArray:
            writer.writerow([fvcTest[0], fvcTest[1], fvcTest[2]])

        #print('----- spiro csv completed -----')

    # get errors from fvc csv file
    getErrorsFromLisbonSpiro(path + '/dataset/lisbon-fvc.csv')

# generate a new csv file only with correct spiro
# input: path of lisbon spiro csv
# output: lisbon-spiro-correct.csv
def getErrorsFromLisbonSpiro(datasetPath):
    # get the base path
    path = str(Path().resolve())
    # read csv and print info
    spiro = pd.read_csv(datasetPath, parse_dates=['Date'], na_values=['NaT'])
    #spiro.info()
    # size of spiro
    sizeSpirotab = len(spiro)
    # header for tables
    header = ['REF', 'Date', '%FVC']
    # check errors in dates
    errorDateList = []
    errorDateListCount = 0
    # get the list of patient ids
    idList = spiro.REF.unique()
    # for every id check if a date is before another
    for id in idList:
        dateList = spiro.loc[spiro.REF == id].Date.values
        indexList = spiro.loc[spiro.REF == id].index.values
        row = spiro.loc[spiro.REF == id].values
        size = len(dateList)
        for i in range(0, size - 1):
            # if not, save error
            if dateList[i] > dateList[i + 1]:
                errorDateListCount += 1
                errorDateList.append(row[i])
                spiro.drop(labels=indexList[i], axis=0, inplace=True)
    # check duplicate rows
    duplicateList = []
    duplicateListCount = 0
    indexList = []
    # get the list of patient ids
    idList = spiro.REF.unique()
    # for every id check if a date is before another
    for id in idList:
        row = spiro.loc[spiro.REF == id].values
        index = spiro.loc[spiro.REF == id].index.values
        size = len(row)
        for x in range(0, size - 1):
            for i in range(x + 1, size):
                comparison = row[x] == row[i]
                equal_arrays = comparison.all()
                if equal_arrays == True:
                    tempArr = [index[i]]
                    duplicateListCount += 1
                    for elem in row[i]:
                        tempArr.append(str(elem))
                    duplicateList.append(tempArr)
                    indexList.append(index[i])
    for index in indexList:
        spiro.drop(labels=index, axis=0, inplace=True)
    #spiro.info()
    # check correct and incorrect spiro
    totalSpiro = 0
    errorDataList = []
    errorDataListCount = 0
    correctSpiroList = []
    correctSpiroListCount = 0
    # iterate over the dataframe
    for index, row in spiro.iterrows():
        totalSpiro += 1
        if not pd.isna(row['Date']) and not pd.isna(row['%FVC']):
            correctSpiroListCount += 1
            tempArr = [row['REF']]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            correctSpiroList.append(tempArr)
        else:
            errorDataListCount += 1
            tempArr = [row['REF']]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            errorDataList.append(tempArr)

    with open(path + '/dataset/lisbon-fvc-correct.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['REF', 'Date', '%FVC'])
        for q in correctSpiroList:
            # q[0] is the index, skip it
            writer.writerow([q[1], q[2], q[3]])

    print('\nTotal number of Spiro: %s\n'
          'Number of correct Spiro: %s\n'
          'Number of incorrect Spiro: %s'
          % (sizeSpirotab,
             len(correctSpiroList),
             sizeSpirotab-len(correctSpiroList)))
    if duplicateListCount > 0:
        print('\n\n\n****************************** Duplicate Spiro. (count: ' + str(
            duplicateListCount) + ') ******************************\n')
        print(tabulate(duplicateList, headers=header))
    if errorDataListCount > 0:
        print('\n\n\n****************************** Spiro with empty dates or empty %fvc values. (count: ' + str(
            errorDataListCount) + ') ******************************\n')
        print(tabulate(errorDataList, headers=header))

# add spiro
# input: graph, patient ID, patient URI and the row of spiro
def addSpiro(graph, patientURI, row):
    # get spiro URI
    spiro = utils_als.generateURI()
    # add spiro
    graph.add((spiro, RDF.type, classes.pulmonary_function_test))
    # get event URI
    spiroEvent = utils_als.generateURI()
    # add event
    graph.add((spiroEvent, RDF.type, classes.protocol_event))
    # add date
    graph.add((spiroEvent, ns.BTO_schema['startDate'], Literal(row['Date'].date(), datatype=XSD.date)))
    # link visit to patient
    graph.add((patientURI, ns.BTO_schema['undergo'], spiroEvent))
    # link visit to spiro
    graph.add((spiroEvent, ns.BTO_schema['consists'], spiro))
    # add the value of the test
    graph.add((spiro, ns.BTO_schema['FVCrelative'], Literal(row['%FVC'], datatype=XSD.float)))
    #return updated graph
    return graph
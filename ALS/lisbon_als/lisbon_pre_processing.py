'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# lisbon_pre_processing.py contains all the pre-processing functions for LISBON data

# required libraries
import csv
from operator import itemgetter
from pathlib import Path
import pandas as pd

# get the date of first visit
# input: staticVars, alsfrs and spiro dataframes
# output: dictionary with date of first visit for each patient
def getDateOfFirstVisit(staticVars, alsfrs, spiro):
    dict = {}
    dictWithType = {}
    for index, row in staticVars.iterrows():
        # initialize minDate and minDateType to 'nan'
        minDate = 'nan'
        minDateType = 'nan'
        dateListDict = {}
        # date of first ALSFRS-r
        dateALSFRS = alsfrs.loc[alsfrs.index == index].Date.min()
        if not pd.isna(dateALSFRS):
            dateListDict['Date_ALSFRS'] = dateALSFRS
        # date of first Spiro
        dateSpiro = spiro.loc[spiro.index == index].Date.min()
        if not pd.isna(dateSpiro):
            dateListDict['Date_Spiro'] = dateSpiro
        # get min date
        if len(dateListDict) > 0:
            minDate = min(dateListDict.values())
            minDateType = min(dateListDict.items(), key=itemgetter(1))
        # save min date into dictionary
        dict[index] = [minDate]
        dictWithType[index] = [minDateType]
    with open('output/statistics/lisbon-dateOfFirstVisit.txt', 'w') as file:
        file.write('****************************** Date of first visit (LISBON) ******************************\n\n\n')
        for key in dict:
            string = str(key) + ' : ' + str(dictWithType[key]) + '\n'
            file.write(string)
    # return the dictionary
    return dict

# check if the dates are in the correct sequence
# input: staticVars
# output: correct and incorrect list of patient IDs
def checkCorrectDates(staticVars):
    # initialize the lists
    correctList = []
    incorrectList = []
    # iterate over static vars dataframe
    for index, row in staticVars.iterrows():
        # check if onset, diagnosis and death is one after the other
        if not pd.isna(row['DateOf1stSymptoms']) and not pd.isna(row['DateOfDiagnosis']) and not pd.isna(row['Status (death-1; alive-2)']) and not pd.isna(row['Date Of Last visit or Death']):
            diagnosisDate = row['DateOfDiagnosis'].date()
            onsetDate = row['DateOf1stSymptoms'].date()
            deathDate = row['Date Of Last visit or Death'].date()
            status = row['Status (death-1; alive-2)']
            if status == 1:
                if onsetDate <= diagnosisDate and diagnosisDate <= deathDate:
                    correctList.append(index)
                else:
                    #print('[WARNING] Onset, diagnosis, and death not in correct order for patient ID: %s' % (index))
                    incorrectList.append(index)
            elif status == 2:
                if onsetDate <= diagnosisDate:
                    correctList.append(index)
                else:
                    #print('[WARNING] Onset and diagnosis (alive patient) not in correct order for patient ID: %s' % (index))
                    incorrectList.append(index)
        else:
            incorrectList.append(index)
    print('\nNumber of patients: %s\n'
          'Number of patients with onset/diagnosis/death (if patient is dead) in the correct sequence: %s\n'
          'Number of patients with onset/diagnosis/death (if patient is dead) not in the correct sequence: %s'
          % (len(staticVars), len(correctList), len(incorrectList)))
    if (len(incorrectList) > 0):
        print('List of patient IDs with onset/diagnosis/death (if patient is dead) not in the correct sequence:')
        text = ''
        for elem in incorrectList:
            text = text + str(elem) + ', '
        print(text[:-2])
    # return correct and incorrect list
    return correctList

# get difference (in days) between first alsfrs and first spiro
# input: list of patient IDs with at least one ALSFRS-R and one Spiro, alsfrs and spiro dataframes
# output: csv file
def getDeltaBetweenALSFRSandSpiro(correctList, alsfrs, spiro):
    # get the base path
    path = str(Path().resolve())
    # initialize list
    list = []
    countL, countZ, countU = 0, 0, 0
    for value in correctList:
        temp = []
        dateALSFRS = alsfrs.loc[alsfrs.index == value].Date.min()
        dateSpiro = spiro.loc[spiro.index == value].Date.min()
        delta = dateALSFRS - dateSpiro
        if int(delta.days) < 0:
            countL += 1
        elif int(delta.days) == 0:
            countZ += 1
        else:
            countU += 1
        temp.append(value)
        temp.append(delta.days)
        list.append(temp)

    print('\nStatistics about delta time (dateFirstALSFRS - dateFirstSpiro):\n'
          '# of delta < 0: %s\n'
          '# of delta = 0: %s\n'
          '# of delta > 0: %s\n'
          % (countL, countZ, countU))

    # create csv
    with open(path + '/output/statistics/lisbon-delta-alsfrs-spiro.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['REF', 'dateOfFirstALSFRS - dateOfFirstSpiro'])
        for x in list:
            writer.writerow([x[0], x[1]])

# get patients with date of death >= date of last visit or alive patients
# input: staticVars, IDs of the patient with at least one ALSFRS-R, date of last visit dictionaries
# output: correct list of patient IDs
def checkDateOfDeathOrAlive(staticVars, patientIdWithALSFRS, dateOfLastVisitDict):
    # initialize the lists
    aliveList = []
    correctdeathList = []
    incorrectdeathList = []
    invalidStatusList = []
    # iterate over static vars dataframe
    for index, row in staticVars.iterrows():
        if index in patientIdWithALSFRS:
            # get date of last alsfrs
            dateOfLastVisit = dateOfLastVisitDict.get(index)
            # check alive patient
            if (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 2)):
                aliveList.append(index)
            # check date of death
            elif (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 1)):
                if (not pd.isna(row['Date Of Last visit or Death']) and (row['Date Of Last visit or Death'] >= dateOfLastVisit)):
                    correctdeathList.append(index)
                else:
                    incorrectdeathList.append(index)
            # invalid status
            else:
                invalidStatusList.append(index)

    # union of aliveList and correctdeathList
    correctList = list(set(aliveList + correctdeathList))

    print('\nNumber of patients with at least one ALSFRS-R: %s\n'
          'Number of alive patients: %s\n'
          'Number of patients with date of death >= date of last visit: %s\n'
          'Number of alive patients + Number of patients with date of death >= date of last visit: %s\n'
          'Number of patients with date of death < date of last visit: %s\n'
          'Number of patients with invalid status: %s'
          % (len(patientIdWithALSFRS), len(aliveList),
             len(correctdeathList),
             len(correctList),
             len(incorrectdeathList),
             len(invalidStatusList)))
    if (len(incorrectdeathList) > 0):
        print('List of patient IDs with date of death < date of last visit:')
        text = ''
        for elem in incorrectdeathList:
            text = text + str(elem) + ', '
        print(text[:-2])
    if (len(invalidStatusList) > 0):
        print('List of patient IDs with invalid status:')
        text = ''
        for elem in invalidStatusList:
            text = text + str(elem) + ', '
        print(text[:-2])
    # return correct list
    return correctList

# check if the dates of NIV, PEG, Tracheostomy are after the date of first visit and before the date of death (include also patients with no NIV and PEG and Tracheostomy events)
# input: staticVars, list of patient with ALSFRS and the dictionary with the dates of first visit
# output: correct and incorrect list of patient IDs
def checkStaticVarsEvents(staticVars, patientIdWithALSFRS, dateOfFirstVisitDict):
    # initialize the lists
    correctPEGList = []
    correctNIVList = []
    correctTracheoList = []
    incorrectPEGList = []
    incorrectNIVList = []
    incorrectTracheoList = []
    patientNoEventList = []
    emptyEventDataList = []
    # iterate over static vars dataframe
    for index, row in staticVars.iterrows():
        # check if index is into the list of patients with alsfrs
        if index in patientIdWithALSFRS:
            # get date of first visit = date of first ALSFRS
            dateOfFirstVisit = dateOfFirstVisitDict.get(index)
            # check NIV = 1 and NIVdate >= dateOfFirstVisit and NIVdate <= dateOfDeath
            if (not pd.isna(row['NIV (0-no; 1-yes)']) and (row['NIV (0-no; 1-yes)'] == 1)):
                if(not pd.isna(row['Date_NIV']) and (row['Date_NIV'] >= dateOfFirstVisit)):
                    if (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 2)):
                        correctNIVList.append(index)
                    elif (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 1) and (not pd.isna(row['Date Of Last visit or Death']) and (row['Date_NIV'] <= row['Date Of Last visit or Death']))):
                        correctNIVList.append(index)
                    else:
                        incorrectNIVList.append(index)
                else:
                    incorrectNIVList.append(index)
            # check Tracheostomy = 1 and TracheostomyDate >= dateOfFirstVisit and TracheostomyDate <= dateOfDeath
            if (not pd.isna(row['Tracheostomy (0-no; 1-yes)']) and (row['Tracheostomy (0-no; 1-yes)'] == 1)):
                if(not pd.isna(row['Date_tracheostomy']) and (row['Date_tracheostomy'] >= dateOfFirstVisit)):
                    if (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 2)):
                        correctTracheoList.append(index)
                    elif (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 1) and (not pd.isna(row['Date Of Last visit or Death']) and (row['Date_tracheostomy'] <= row['Date Of Last visit or Death']))):
                        correctTracheoList.append(index)
                    else:
                        incorrectTracheoList.append(index)
                else:
                    incorrectTracheoList.append(index)
            # check PEG = 1 and PEGdate >= dateOfFirstVisit and PEGdate <= dateOfDeath
            if (not pd.isna(row['PEG (0-no; 1-yes)']) and (row['PEG (0-no; 1-yes)'] == 1)):
                if(not pd.isna(row['Date_PEG']) and (row['Date_PEG'] >= dateOfFirstVisit)):
                    if (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 2)):
                        correctPEGList.append(index)
                    elif (not pd.isna(row['Status (death-1; alive-2)']) and (row['Status (death-1; alive-2)'] == 1) and (not pd.isna(row['Date Of Last visit or Death']) and (row['Date_PEG'] <= row['Date Of Last visit or Death']))):
                        correctPEGList.append(index)
                    else:
                        incorrectPEGList.append(index)
                else:
                    incorrectPEGList.append(index)
            # patients with no PEG and NIV and Tracheostomy
            if ((not pd.isna(row['NIV (0-no; 1-yes)']) and (row['NIV (0-no; 1-yes)'] == 0) and pd.isna(row['Date_NIV'])) and
                    (not pd.isna(row['Tracheostomy (0-no; 1-yes)']) and (row['Tracheostomy (0-no; 1-yes)'] == 0) and pd.isna(row['Date_tracheostomy'])) and
                    (not pd.isna(row['PEG (0-no; 1-yes)']) and (row['PEG (0-no; 1-yes)'] == 0) and pd.isna(row['Date_PEG']))):
                patientNoEventList.append(index)
            # patients with empty data in events (PEG and NIV and Tracheostomy)
            if pd.isna(row['NIV (0-no; 1-yes)']) or pd.isna(row['Tracheostomy (0-no; 1-yes)']) or pd.isna(row['PEG (0-no; 1-yes)']):
                emptyEventDataList.append(index)

    # if the patient id of a correct list is in one of the other two incorrect lists, subtract the patient id
    finalNIVList = list(set(correctNIVList) - set(incorrectTracheoList) - set(incorrectPEGList))
    finalTracheoList = list(set(correctTracheoList) - set(incorrectNIVList) - set(incorrectPEGList))
    finalPEGList = list(set(correctPEGList) - set(incorrectNIVList) - set(incorrectTracheoList))

    # perform the union of four sets
    correctList = list(set(finalNIVList + finalTracheoList + finalPEGList + patientNoEventList))
    # remove from emptyEventDataList the correct patients
    emptyEventDataList = list(set(emptyEventDataList) - set(correctList))
    # perform the union of incorrect lists
    incorrectList = list(set(incorrectPEGList + incorrectNIVList + incorrectTracheoList + emptyEventDataList))
    # sort the list
    incorrectList.sort()

    print('\nNumber of patients with at least one ALSFRS-R: %s\n'
          'Number of patients with no events (NIV and PEG and Tracheostomy): %s\n'
          'Number of patients with events (NIV or PEG or Tracheostomy) after the first ALSFRS-R and before the date of death: %s\n'
          'Number of patients with no events (NIV and PEG and Tracheostomy) + Number of patients with events (NIV or PEG or Tracheostomy) after the first ALSFRS-R and before the date of death: %s\n'
          'Number of patients with incorrect events (NIV or PEG or Tracheostomy): %s'
          % (len(patientIdWithALSFRS),
             len(patientNoEventList),
             len(correctList)-len(patientNoEventList),
             len(correctList),
             len(incorrectList)))
    if (len(incorrectList) > 0):
        print('List of patients IDs with incorrect events (NIV or PEG or Tracheostomy):')
        text = ''
        for elem in incorrectList:
            text = text + str(elem) + ', '
        print(text[:-2])

    # return correct list
    return correctList
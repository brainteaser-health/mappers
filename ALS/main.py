'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# Data Mappers for ALS data of Turin, Lisbon and Madrid

# required libraries
import csv
import sys
from pathlib import Path
import pandas as pd
from common import utils_als, common_static_vars, common_pre_processing
from common import query
from turin_als import turin_static_vars, turin_alsfrs, turin_spiro, turin_pre_processing
from lisbon_als import lisbon_static_vars, lisbon_alsfrs, lisbon_spiro, lisbon_owd_vars, lisbon_pre_processing

# main function for Turin
def mainTurin():

    print('********************* START - Turin Mapper *********************')

    # get the base path
    path = str(Path().resolve())

    # ----- redirect stdout to an output file -----
    file = open(path + "/output/log/turin-log.txt", "w", encoding="utf-8")
    stdout = sys.stdout
    sys.stdout = file

    # ----- dataset path section -----
    # path of Turin dataset
    turinPath = path + '/dataset/TURIN_Dataset-final_anonym_corrected_feb2022.xlsx'
    # path correct alsfrs csv
    turinALSFRSPath = path + '/dataset/turin-alsfrs-correct.csv'
    # path correct spiro csv
    turinSpiroPath = path + '/dataset/turin-fvc-correct.csv'
    # path of esco classification csv
    escoPath = path + '/dataset/esco-classification.csv'
    # path of limb classification csv
    ULlimbPath = path + '/dataset/UL-named-individual.csv'
    DPlimbPath = path + '/dataset/DP-named-individual.csv'
    LRlimbPath = path + '/dataset/LR-named-individual.csv'

    # ----- pre-processing operation -----
    print('\n********************* TURIN - PRE-PROCESSING OPERATIONS *********************')

    # load the static vars tab in memory
    staticVars = pd.read_excel(turinPath, sheet_name='Static vars_corr', index_col='REF', parse_dates=["Birth_year"], na_values=[' '])
    #staticVars.info()
    # load the esco classification
    esco = pd.read_csv(escoPath, sep=',', index_col='code')
    # load limb classification csv
    ULlimb = pd.read_csv(ULlimbPath, sep=',', index_col='code')
    DPlimb = pd.read_csv(DPlimbPath, sep=',', index_col='code')
    LRlimb = pd.read_csv(LRlimbPath, sep=',', index_col='code')
    # analize alsfrs and spiro tabs to get new csv files with correct alsfrs and spiro
    turin_alsfrs.getErrorsFromTurinALSFRS(turinPath)
    turin_spiro.getErrorsFromTurinSpiro(turinPath)
    # load the static vars tab in memory
    alsfrs = pd.read_csv(turinALSFRSPath, index_col="REF", parse_dates=["Date"])
    # alsfrs.info()
    # load the fvc tab in memory
    fvc = pd.read_csv(turinSpiroPath, index_col="REF", parse_dates=["Date"])
    # fvc.info()
    # calculate date of first visit for each patient (min date between first alsfrs and first spiro)
    #---dateOfFirstVisitDict = turin_pre_processing.getDateOfFirstVisit(staticVars, alsfrs, fvc)
    # check if onset, diagnosis and death (if patient not alive) dates are in the correct sequence
    correctDateList = turin_pre_processing.checkCorrectDates(staticVars)
    # get patient with at least one alsfrs and one spiro
    #---patientIdWithALSFRSandSPIRO = common_pre_processing.getPatientWithALSFRSandSpiro(staticVars, alsfrs, fvc)
    # get patient with at least one alsfrs
    patientIdWithALSFRS, dateOfFirstALSFRSDict, dateOfLastALSFRSDict = common_pre_processing.getPatientWithALSFRS(staticVars, alsfrs)
    # get date of last visit
    dateOfLastVisitDict = common_pre_processing.getDateOfLastVisit(alsfrs, fvc)
    # get patients with date of death >= date of last visit or alive patients
    patientAliveOrWithCorrectDeath = turin_pre_processing.checkDateOfDeathOrAlive(staticVars, patientIdWithALSFRS, dateOfLastVisitDict)
    # get difference in days between first spiro and first alsfrs
    #---turin_pre_processing.getDeltaBetweenALSFRSandSpiro(patientIdWithALSFRSandSPIRO, alsfrs, fvc)
    # get patient with the date of NIV/PEG/Tracheostomy after the date of first ALSFRS-R and before the date of death or with no NIV/PEG/Tracheostomy
    patientIdWithCorrectEvents = turin_pre_processing.checkStaticVarsEvents(staticVars, patientIdWithALSFRS, dateOfFirstALSFRSDict)
    # get the patient IDs to be mapped (patient with correct dates and at least one spiro and one alsfrs)
    #---correctPatientList = common_pre_processing.getCorrectPatient(staticVars, correctDateList, patientIdWithALSFRSandSPIRO)
    # get the patient IDs to be mapped (patient with correct dates and at least one alsfrs)
    correctPatientList = common_pre_processing.getCorrectPatient(staticVars, alsfrs, fvc, correctDateList, patientIdWithALSFRS, patientIdWithCorrectEvents, patientAliveOrWithCorrectDeath)

    # initialize graph
    g = utils_als.initializeGraph()
    # initialize dictionary to save the corresponding URI for each patientID
    patientDict = {}

    # ---------------------- STATIC VARS ----------------------

    print('\n********************* TURIN - STATIC VARS *********************\n')

    # iterate over the static vars dataframe
    for index, row in staticVars.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            # get date of first visit
            dateOfFirstVisit = dateOfFirstALSFRSDict.get(index)
            # add patient to graph, save patient URI and update patient dictionary
            g, patient, patientDict = common_static_vars.addPatient(g, patientDict, index, 'Turin')
            # add clinical trial and hospital
            g = turin_static_vars.addClinicalTrial(g, index, patient, dateOfFirstVisit, row['Dateofdeath_corrected'], row['Status (death-1; alive-2)'])
            # add year of birth to patient
            g = common_static_vars.addBirthYear(g, index, patient, row['Birth_year'])
            # add gender to patient
            g = common_static_vars.addGender(g, index, patient, row['Gender (1-male; 2-female)'])
            # add ethnicity to patient
            g = common_static_vars.addEthnicity(g, index, patient, row['Ethnicity(1-cau; 2-afr; 3-asi)'])
            # add death and status to patient
            g = turin_static_vars.addPatientStatus(g, index, patient, row['Status (death-1; alive-2)'], row['Dateofdeath_corrected'])
            # add diagnosis to patient
            g, diagnosis = turin_static_vars.addDiagnosis(g, index, patient, row['DateOfDiagnosis'])
            # add onset to patient
            g = turin_static_vars.addOnset(g, ULlimb, DPlimb, LRlimb, index, patient,
                               row['DateOf1stSymptoms'],
                               row['Age_onset (years)'],
                               row['UMNvsLMN predominance(1-UMN; 2-LMN; 3- both; NA)'],
                               row['Onset (1- Limbs; 2- Bulbar; 3- Axial/Resp; 4- Generalized)'],
                               row['Limb_O ULvsLL  (0-not related; 1-UL; 2- LL; 3-both; 999-not available)'],
                               row['Limbs Predominant Impairment (0-not related; 1-distal; 2-proximal; 3- both; 999-not available)'],
                               row['Limbs Predominant Side (0-not related;1-left, 2-right; 3-both; 999-not available)'])
            # add height weight weightloss
            g = common_static_vars.addHeightWeightWeightloss(g, index, patient, dateOfFirstVisit, row['Height (m)'], row['Weight at 1st visit (kg)'], row['Weightloss >10% (0-no; 1-yes)'])
            # add before onset
            g = common_static_vars.addBeforeOnset(g, index, patient, row['Weight before 1st symptoms (kg)'], row['Major Trauma before onset (0-no; 1-yes)'], row['Surgical interventions before onset (0-no; 1-yes)'])
            # add relative
            g = common_static_vars.addRelative(g, index, patient, row['ALS familiar history (0-no; 1-yes)'])
            # add occupation
            g = turin_static_vars.addOccupation(g, index, patient, esco, row['Main occupation'])
            # add retired at diagnosis
            g = common_static_vars.addRetiredAtDiagnosis(g, index, patient, row['Retired at the diagnosis (0-no; 1-yes)'])
            # add niv, tracheostomy and peg
            g = common_static_vars.addNIV(g, index, patient, row['NIV (0-no; 1-yes)'], row['Date_NIV'])
            g = common_static_vars.addTracheostomy(g, index, patient, row['Tracheostomy (0-no; 1-yes)'], row['Date_tracheostomy'])
            g = common_static_vars.addPEG(g, index, patient, row['PEG (0-no; 1-yes)'], row['Date_PEG'])
            # add smoking
            g, eventSmoking = common_static_vars.addSmoking(g, index, patient, row['Ever smoked (0-No; 1-yes)'])
            # add diseases
            g = common_static_vars.addDisease(g, index, patient, row['Blood hypertension (0-No; 1-yes)'],
                                 row['Diabetes – type I / II (0-No; 1-yes)'],
                                 row['Dyslipidemia (0-no; 1-yes)'],
                                 row['Thyroid disorder (0-no; 1-yes)'],
                                 row['Autoimmune disorder (0-no; 1-yes)'],
                                 row['Stroke (0-no; 1-yes)'],
                                 row['Cardiac disease (0-no; 1-yes)'],
                                 row['Primary cancer (0-no; 1-yes)'])
            # add gene
            g = turin_static_vars.addGene(g, index, patient, row['SOD1 Mutation '],
                              row['C9orf72 repeat-primed PCR result (A=normal; C=normal; B=expansion; Q=special shape, can be ignored for this study)'],
                              row['TARDBP mutation'],
                              row['FUS mutation'])

    # ---------------------- ALSFRS ----------------------

    print('\n********************* TURIN - ALSFRS *********************\n')

    # iterate over the alsfrs dataframe
    for index, row in alsfrs.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            # get patient URI
            patient = patientDict.get(index)
            g = turin_alsfrs.addQuestionnaire(g, patient, row)

    # ---------------------- FVC ----------------------

    print('\n********************* TURIN - FVC *********************\n')

    # iterate over the fvc dataframe
    for index, row in fvc.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            patient = patientDict.get(index)
            g = turin_spiro.addSpiro(g, patient, row)

    # ----- end redirect stdout -----
    file.close()
    sys.stdout = stdout

    # ---------------------- QUERY section ----------------------
    #print("----- start query -----")
    #query.staticVarsQuery(g, 'turin-static-vars.csv')
    #query.visitsQuery(g, 'turin-visits.csv')
    #print("----- end query -----")

    # ---------------------- OUTPUT files ----------------------
    # perform serialization (turtle)
    utils_als.serializeGraph(g, path + '/output/turin-dataset.ttl', 'turtle')
    # perform serialization (json)
    utils_als.serializeGraph(g, path + '/output/turin-dataset.json', 'json-ld')
    # perform serialization (rdf)
    utils_als.serializeGraph(g, path + '/output/turin-dataset.rdf', 'application/rdf+xml')

    # print the dictionary with the corrispondence between patient ID and patient URI
    savePath = path + '/output/dictionary/turin-patient-dictionary.csv'
    with open(savePath, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['ID', 'URI'])
        for item in patientDict.items():
            writer.writerow(item)
            
    print('********************* END - Turin Mapper *********************')

    return g

# main function for Lisbon
def mainLisbon():

    print('********************* START - Lisbon Mapper *********************')

    # get the base path
    path = str(Path().resolve())

    # ----- redirect stdout to an output file -----
    file = open(path + "/output/log/lisbon-log.txt", "w", encoding="utf-8")
    stdout = sys.stdout
    sys.stdout = file

    # ----- dataset path section -----
    # path of Lisbon dataset
    lisbonPath = path + '/dataset/Lisbon_Dataset1_FINAL_v18102021.xlsx'
    # path of Lisbon alsfrs
    lisbon_correct_alsfrs_path = path + '/dataset/lisbon-alsfrs-correct.csv'
    # path of Lisbon spiro
    lisbon_correct_fvc_path = path + '/dataset/lisbon-fvc-correct.csv'
    # path of esco classification csv
    escoPath = path + '/dataset/esco-classification.csv'
    # path of limb classification csv
    ULlimbPath = path + '/dataset/UL-named-individual.csv'
    DPlimbPath = path + '/dataset/DP-named-individual.csv'
    LRlimbPath = path + '/dataset/LR-named-individual.csv'

    # ----- pre-processing operation -----
    print('\n********************* LISBON - PRE-PROCESSING OPERATIONS *********************')

    # load the static vars tab in memory
    staticVars = pd.read_excel(lisbonPath, sheet_name='Static Vars', index_col='REF', parse_dates=["Birth_year"], na_values=[' ', 'NA', 'NaT'])
    # fill NaN cell with value 999
    staticVars['Limb_O ULvsLL  (0-not related; 1-UL; 2- LL; 3-both; 999-not available)'] = staticVars['Limb_O ULvsLL  (0-not related; 1-UL; 2- LL; 3-both; 999-not available)'].fillna(999)
    staticVars['Limbs Predominant Impairment (0-not related; 1-distal; 2-proximal; 3- both; 999-not available)'] = staticVars['Limbs Predominant Impairment (0-not related; 1-distal; 2-proximal; 3- both; 999-not available)'].fillna(999)
    staticVars['Limbs Predominant Side (0-not related;1-left, 2-right; 3-both; 999-not available)'] = staticVars['Limbs Predominant Side (0-not related;1-left, 2-right; 3-both; 999-not available)'].fillna(999)
    # convert to integer from float
    staticVars['Limb_O ULvsLL  (0-not related; 1-UL; 2- LL; 3-both; 999-not available)'] = staticVars['Limb_O ULvsLL  (0-not related; 1-UL; 2- LL; 3-both; 999-not available)'].astype(int)
    staticVars['Limbs Predominant Impairment (0-not related; 1-distal; 2-proximal; 3- both; 999-not available)'] = staticVars['Limbs Predominant Impairment (0-not related; 1-distal; 2-proximal; 3- both; 999-not available)'].astype(int)
    staticVars['Limbs Predominant Side (0-not related;1-left, 2-right; 3-both; 999-not available)'] = staticVars['Limbs Predominant Side (0-not related;1-left, 2-right; 3-both; 999-not available)'].astype(int)
    #staticVars.info()
    # load the esco classification
    esco = pd.read_csv(escoPath, sep=',', index_col='code')
    # load limb classification csv
    ULlimb = pd.read_csv(ULlimbPath, sep=',', index_col='code')
    DPlimb = pd.read_csv(DPlimbPath, sep=',', index_col='code')
    LRlimb = pd.read_csv(LRlimbPath, sep=',', index_col='code')

    # move visits from column to row and analize alsfrs and spiro datas to get new csv files with correct alsfrs and spiro
    lisbon_alsfrs.breakLisbonALSFRSColumns(lisbonPath)
    lisbon_spiro.breakLisbonSpiroColumns(lisbonPath)
    # load the alsfrs csv in memory
    alsfrs = pd.read_csv(lisbon_correct_alsfrs_path, index_col="REF", parse_dates=["Date"])
    # alsfrs.info()
    # load the fvc tab in memory
    fvc = pd.read_csv(lisbon_correct_fvc_path, index_col="REF", parse_dates=["Date"])
    # fvc.info()
    # calculate date of first visit for each patient (min date between first alsfrs and first spiro)
    # ---dateOfFirstVisitDict = lisbon_pre_processing.getDateOfFirstVisit(staticVars, alsfrs, fvc)
    # check if onset, diagnosis and death (if patient not alive) dates are in the correct sequence
    correctDateList = lisbon_pre_processing.checkCorrectDates(staticVars)
    # get patient with at least one alsfrs and one spiro
    #---patientIdWithALSFRSandSPIRO = common_pre_processing.getPatientWithALSFRSandSpiro(staticVars, alsfrs, fvc)
    # get patient with at least one alsfrs
    patientIdWithALSFRS, dateOfFirstALSFRSDict, dateOfLastALSFRSDict = common_pre_processing.getPatientWithALSFRS(staticVars, alsfrs)
    # get date of last visit
    dateOfLastVisitDict = common_pre_processing.getDateOfLastVisit(alsfrs, fvc)
    # get patients with date of death >= date of last visit or alive patients
    patientAliveOrWithCorrectDeath = lisbon_pre_processing.checkDateOfDeathOrAlive(staticVars, patientIdWithALSFRS, dateOfLastVisitDict)
    # get difference in days between first spiro and first alsfrs
    #---lisbon_pre_processing.getDeltaBetweenALSFRSandSpiro(patientIdWithALSFRSandSPIRO, alsfrs, fvc)
    # get patient with the date of NIV/PEG/Tracheostomy after the date of first ALSFRS-R or with no NIV/PEG/Tracheostomy
    patientIdWithCorrectEvents = lisbon_pre_processing.checkStaticVarsEvents(staticVars, patientIdWithALSFRS,dateOfFirstALSFRSDict)
    # get the patient IDs to be mapped (patient with correct dates and at least one spiro and one alsfrs)
    #---correctPatientList = common_pre_processing.getCorrectPatient(staticVars, correctDateList, patientIdWithALSFRSandSPIRO)
    # get the patient IDs to be mapped (patient with correct dates and at least one alsfrs)
    correctPatientList = common_pre_processing.getCorrectPatient(staticVars, alsfrs, fvc, correctDateList, patientIdWithALSFRS, patientIdWithCorrectEvents, patientAliveOrWithCorrectDeath)

    # initialize graph
    g = utils_als.initializeGraph()
    # initialize dictionary
    patientDict = {}
    diagnosisDict = {}
    eventSmokingDict = {}

    # ---------------------- STATIC VARS ----------------------

    print('\n********************* LISBON - STATIC VARS *********************\n')

    # iterate over the static vars dataframe
    for index, row in staticVars.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            # get date of first visit
            dateOfFirstVisit = dateOfFirstALSFRSDict.get(index)
            # add patient to graph, save patient URI and update patient dictionary
            g, patient, patientDict = common_static_vars.addPatient(g, patientDict, index, 'Lisbon')
            # add clinical trial and hospital
            g = lisbon_static_vars.addClinicalTrial(g, index, patient, dateOfFirstVisit, row['Date Of Last visit or Death'], row['Status (death-1; alive-2)'])
            # add year of birth to patient
            g = common_static_vars.addBirthYear(g, index, patient, row['Birth_year'])
            # add gender to patient
            g = common_static_vars.addGender(g, index, patient, row['Gender (1-male; 2-female)'])
            # add ethnicity to patient
            g = common_static_vars.addEthnicity(g, index, patient, row['Ethnicity(1-cau; 2-afr; 3-asi)'])
            # add status 'alive' and date of death to patient
            g = lisbon_static_vars.addPatientStatus(g, index, patient, row['Status (death-1; alive-2)'], row['Date Of Last visit or Death'])
            # add diagnosis to patient
            g, diagnosis = lisbon_static_vars.addDiagnosis(g, index, patient, row['DateOfDiagnosis'])
            # save diagnosis URI
            diagnosisDict[index] = diagnosis
            # add onset to patient
            g = lisbon_static_vars.addOnset(g, ULlimb, DPlimb, LRlimb, index, patient,
                               row['DateOf1stSymptoms'],
                               row['Age_onset (years)'],
                               row['UMNvsLMN predominance(1-UMN; 2-LMN; 3- both; NA)'],
                               row['Onset (1- Limbs; 2- Bulbar; 3- Axial/Resp; 4- Generalized)'],
                               row['Limb_O ULvsLL  (0-not related; 1-UL; 2- LL; 3-both; 999-not available)'],
                               row['Limbs Predominant Impairment (0-not related; 1-distal; 2-proximal; 3- both; 999-not available)'],
                               row['Limbs Predominant Side (0-not related;1-left, 2-right; 3-both; 999-not available)'])
            # add height weight weightloss
            g = common_static_vars.addHeightWeightWeightloss(g, index, patient, dateOfFirstVisit, row['Height (m)'], row['Weight at 1st visit (kg)'], row['Weightloss >10% (0-no; 1-yes)'])
            # add before onset
            g = common_static_vars.addBeforeOnset(g, index, patient, row['Weight before 1st symptoms (kg)'], row['Major Trauma before onset (0-no; 1-yes)'], row['Surgical interventions before onset (0-no; 1-yes)'])
            # add relative
            g = common_static_vars.addRelative(g, index, patient, row['ALS familiar history (0-no; 1-yes)'])
            # add occupation
            g = lisbon_static_vars.addOccupation(g, index, patient, esco, row['Main occupation'])
            # add niv, tracheostomy and peg
            g = common_static_vars.addNIV(g, index, patient, row['NIV (0-no; 1-yes)'], row['Date_NIV'])
            g = common_static_vars.addTracheostomy(g, index, patient, row['Tracheostomy (0-no; 1-yes)'], row['Date_tracheostomy'])
            g = common_static_vars.addPEG(g, index, patient, row['PEG (0-no; 1-yes)'], row['Date_PEG'])
            # add retired at diagnosis
            g = common_static_vars.addRetiredAtDiagnosis(g, index, patient, row['Retired at the diagnosis (0-no; 1-yes)'])
            # add smoking
            g, eventSmoking = common_static_vars.addSmoking(g, index, patient, row['Ever smoked (0-no; 1-yes)'])
            # save smoking event URI
            eventSmokingDict[index] = eventSmoking
            # add diseases
            g = common_static_vars.addDisease(g, index, patient, row['Blood hypertension (0-no; 1-yes)'],
                                 row['Diabetes – type I / II (0-no; 1-yes)'],
                                 row['Dyslipidemia (0-no; 1-yes)'],
                                 row['Thyroid disorder (0-no; 1-yes)'],
                                 row['Autoimmune disorder (0-no; 1-yes)'],
                                 row['Stroke (0-no; 1-yes)'],
                                 row['Cardiac disease (0-no; 1-yes)'],
                                 row['Primary cancer (0-no; 1-yes)'])
            # add gene
            g = lisbon_static_vars.addGene(g, index, patient, row['SOD1 Mutation '],
                              row['C9orf72 repeat-primed PCR result (A=normal; C=normal; B=expansion; Q=special shape, can be ignored for this study)'],
                              row['TARDBP mutation'],
                              row['FUS mutation'])

    # ---------------------- ALSFRS ----------------------

    print('\n********************* LISBON - ALSFRS *********************\n')

    # iterate over the alsfrs dataframe
    for index, row in alsfrs.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            patient = patientDict.get(index)
            g = lisbon_alsfrs.addQuestionnaire(g, index, patient, row)

    # ---------------------- FVC ----------------------

    print('\n********************* LISBON - FVC *********************\n')

    # iterate over the fvc dataframe
    for index, row in fvc.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            patient = patientDict.get(index)
            g = lisbon_spiro.addSpiro(g, patient, row)

    # ---------------------- OWD VARS ----------------------

    print('\n********************* LISBON - OWD VARS *********************\n')

    # load the owd tab in memory
    # !!! Note: if not present, enter the value 'REF' in cell A1 !!!
    owd = pd.read_excel(lisbonPath, sheet_name="OWD vars", index_col="REF", parse_dates=['Date of initiation of smoking (year)', 'Date of interruption of smoking (year) '], na_values=['NA', 'NR', 'NF', 'NR', ' '])
    # delete first row of dataframe (unused data)
    owd = owd.iloc[1:]
    #owd.info()
    # iterate over the owd dataframe
    for index, row in owd.iterrows():
        # check if index is in the list of correct patients
        if index in correctPatientList:
            patient = patientDict.get(index)
            eventSmoking = eventSmokingDict.get(index)
            diagnosis = diagnosisDict.get(index)
            # blood test columns
            bloodTestRow = row.iloc[0:28]
            # trauma and intervention columns (in the last 5 year, > 5 year)
            traumaAndInterventionLast5YearsRow = row.iloc[[34,36,37,38,42,43,44,48,49,52,54,56,58]]
            traumaAndInterventionMoreThan5YearsRow = row.iloc[[35,39,40,41,45,46,47,50,51,53,55,57,59]]
            # check data presence into 'blood test' and 'trauma and intervention' columns
            bt_flag = lisbon_owd_vars.checkBloodTestDataPresenceOWD(bloodTestRow)
            ti_flag_last_5_years = lisbon_owd_vars.checkTraumaAndInterventionDataPresenceOWD(traumaAndInterventionLast5YearsRow)
            ti_flag_more_than_5_years = lisbon_owd_vars.checkTraumaAndInterventionDataPresenceOWD(traumaAndInterventionMoreThan5YearsRow)
            if bt_flag == True:
                # add blood test to patient
                g = lisbon_owd_vars.addBloodTest(g, index, diagnosis, bloodTestRow)
            if ti_flag_more_than_5_years == True:
                # add trauma and intervention (> 5 years)
                g, beforeOnset = lisbon_owd_vars.appendBeforeOnset(g, patient, '> 5 years')
                g = lisbon_owd_vars.addTraumaAndInterventionMoreThan5Years(g, index, beforeOnset, traumaAndInterventionMoreThan5YearsRow)
            if ti_flag_last_5_years == True:
                # add trauma and intervention (in the last 5 years)
                g, beforeOnset = lisbon_owd_vars.appendBeforeOnset(g, patient, 'in the last 5 years')
                g = lisbon_owd_vars.addTraumaAndInterventionLast5Years(g, index, beforeOnset,traumaAndInterventionLast5YearsRow)
            # add smoking info
            g = lisbon_owd_vars.addSmokingOWD(g, index, patient, eventSmoking, row[28], row[30], row[29], row[31], row[32], row[33])

    # ----- end redirect stdout -----
    file.close()
    sys.stdout = stdout

    # ---------------------- QUERY section ----------------------
    #print("----- start query -----")
    #query.staticVarsQuery(g, 'lisbon-static-vars.csv')
    #query.visitsQuery(g, 'lisbon-visits.csv')
    #print("----- end query -----")

    # ---------------------- OUTPUT files ----------------------
    # perform serialization (turtle)
    utils_als.serializeGraph(g, path + '/output/lisbon-dataset.ttl', 'turtle')
    # perform serialization (json)
    utils_als.serializeGraph(g, path + '/output/lisbon-dataset.json', 'json-ld')
    # perform serialization (rdf)
    utils_als.serializeGraph(g, path + '/output/lisbon-dataset.rdf', 'application/rdf+xml')

    # print the dictionary with the corrispondence between patient ID and patient URI
    savePath = path + '/output/dictionary/lisbon-patient-dictionary.csv'
    with open(savePath, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(['Index', 'URI'])
            for item in patientDict.items():
                writer.writerow(item)

    print('********************* END - Lisbon Mapper *********************')

    return g

if __name__ == '__main__':
    # turin mapper
    g1 = mainTurin()
    # lisbon mapper
    g2 = mainLisbon()
    # merge graphs
    print('********************* START - Merge Turin and Lisbon graphs *********************')
    g = g1 + g2
    # get the base path
    path = str(Path().resolve())
    # perform serialization (turtle)
    utils_als.serializeGraph(g, path + '/output/merged-dataset.ttl', 'turtle')
    # perform serialization (json)
    utils_als.serializeGraph(g, path + '/output/merged-dataset.json', 'json-ld')
    # perform serialization (rdf)
    utils_als.serializeGraph(g, path + '/output/merged-dataset.rdf', 'application/rdf+xml')
    print('********************* END - Merge Turin and Lisbon graphs *********************')

'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# turin_alsfrs.py contains all the custom functions to manage the TURIN ALSFRS-R tab

import csv
import pandas as pd
from pathlib import Path
from tabulate import tabulate
from common import namespaces_als as ns
from common import classes_als as classes
from common import utils_als
from rdflib import Literal
from rdflib.namespace import XSD, RDF

# generate a new csv file with correct alsfrs-r
# input: path of turin alsfrs tab
# output: turin-alsfrs-correct.csv
def getErrorsFromTurinALSFRS(datasetPath):
    # get the base path
    path = str(Path().resolve())
    # read csv
    alsfrs = pd.read_excel(datasetPath, sheet_name="ALSFRS-R", index_col='N_ALSFRS', na_values=['NaT'])
    alsfrs = alsfrs.rename(columns={'Date1':'Date', 'ALSFRS_TOT':'ALSFRS-R', 'ALSFRS_1':'1', 'ALSFRS_2':'2', 'ALSFRS_3':'3', 'ALSFRS_4':'4', 'ALSFRS_5':'5', 'ALSFRS_6':'6', 'ALSFRS_7':'7', 'ALSFRS_8':'8', 'ALSFRS_9':'9', 'ALSFRS_10':'10', 'ALSFRS_11':'11', 'ALSFRS_12':'12'})
    # header for tables
    header = ['N_ALSFRS', 'REF', 'PARALS/CODALS', 'Date', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
              'ALSFRS-R']
    # size of alsfrs
    sizeALSFRStab = len(alsfrs)

    # check errors in dates
    errorDateList = []
    errorDateListCount = 0
    # get the list of patient ids
    idList = alsfrs.REF.unique()
    # for every id check if a date is before another
    for id in idList:
        dateList = alsfrs.loc[alsfrs.REF == id].Date.values
        indexList = alsfrs.loc[alsfrs.REF == id].index.values
        row = alsfrs.loc[alsfrs.REF == id].values
        size = len(dateList)
        for i in range(0, size - 1):
            # if not, save error
            if dateList[i] > dateList[i + 1]:
                errorDateListCount += 1
                errorDateList.append(indexList[i])
                errorDateList.append(row[i])
                alsfrs.drop(labels=indexList[i], axis=0, inplace=True)
    # check duplicate rows
    duplicateList = []
    duplicateListCount = 0
    indexList = []
    # for every id check if there is a duplicate row
    for id in idList:
        row = alsfrs.loc[alsfrs.REF == id].values
        index = alsfrs.loc[alsfrs.REF == id].index.values
        size = len(row)
        for x in range(0, size - 1):
            for i in range(x + 1, size):
                comparison = row[x] == row[i]
                equal_arrays = comparison.all()
                if equal_arrays == True:
                    tempArr = [index[i]]
                    duplicateListCount += 1
                    for elem in row[i]:
                        tempArr.append(str(elem))
                    duplicateList.append(tempArr)
                    indexList.append(index[i])
    for index in indexList:
        alsfrs.drop(labels=index, axis=0, inplace=True)
    #alsfrs.info()
    # check ALSFRS/ALSFRS-R with null dates
    nullDatesList = []
    nullDatesCount = 0
    # check ALSFRS/ALSFRS-R with missing data
    missingDataList = []
    missingDataListCount = 0
    # check correct and incorrect ALSFRS-R
    ALSFRSRTotalList = []
    ALSFRSRTotalListCount = 0
    # iterate over the dataframe
    for index, row in alsfrs.iterrows():
        # check ALSFRS-R with null dates
        if pd.isna(row['Date']):
            nullDatesCount += 1
            tempArr = [index]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            nullDatesList.append(tempArr)
        # check correct ALSFRS-R
        elif not pd.isna(row['Date']) and \
                (not pd.isna(row['1']) and not pd.isna(row['2']) and not pd.isna(row['3']) and not pd.isna(row['4']) and not
                pd.isna(row['5']) and not pd.isna(row['6']) and not pd.isna(row['7']) and not pd.isna(row['8']) and not
                pd.isna(row['9']) and not pd.isna(row['10']) and not pd.isna(row['11']) and not pd.isna(row['12'])):
            ALSFRSRTotalListCount += 1
            tempArr = [index]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            ALSFRSRTotalList.append(tempArr)
        else:
            missingDataListCount += 1
            tempArr = [index]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            missingDataList.append(tempArr)

    with open(path + '/dataset/turin-alsfrs-correct.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['N_ALSFRS', 'REF', 'Date', 'ALSFRS-R', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
                  '12'])

        for q in ALSFRSRTotalList:
            writer.writerow([q[0], q[1], q[3], q[16], q[4], q[5], q[6], q[7], q[8], q[9], q[10], q[11], q[12], q[13], q[14], q[15]])

    print('\nTotal number of ALSFRS/ALSFRS-R: %s\n'
          'Number of correct ALSFRS-R: %s\n'
          'Number of incorrect ALSFRS/ALSFRS-R: %s'
          % (sizeALSFRStab,
             len(ALSFRSRTotalList),
             sizeALSFRStab - len(ALSFRSRTotalList)))
    if errorDateListCount > 0:
        print('\n\n\n****************************** ALSFRS-R with wrong dates in the sequence. (count: ' + str(
            errorDateListCount) + ') ******************************\n')
        print(tabulate(errorDateList, headers=header))
    if duplicateListCount > 0:
        print('\n\n\n****************************** Duplicate ALSFRS-R. (count: ' + str(
            duplicateListCount) + ') ******************************\n')
        print(tabulate(duplicateList, headers=header))
    if nullDatesCount > 0:
        print('\n\n\n****************************** ALSFRS-R with null dates. (count: ' + str(
        nullDatesCount) + ') ******************************\n')
        print(tabulate(nullDatesList, headers=header))
    if missingDataListCount > 0:
        print('\n\n\n****************************** ALSFRS-R with missing data. (count: ' + str(
            missingDataListCount) + ') ******************************\n')
        print(tabulate(missingDataList, headers=header))

# add questionnaire
# input: graph, patient ID, patient URI and the row of alsfrs
# output: updated graph
def addQuestionnaire(graph, patientURI, row):
    # get event URI
    alsfrsEvent = utils_als.generateURI()
    # get alsfrs URI
    alsfrs = utils_als.generateURI()
    # add ALSFRS-R to graph
    graph.add((alsfrs, RDF.type, classes.alsfrs_r))
    # add visit to graph
    graph.add((alsfrsEvent, RDF.type, classes.protocol_event))
    # add visit date
    graph.add((alsfrsEvent, ns.BTO_schema['startDate'], Literal(row['Date'].date(), datatype=XSD.date)))
    # link alsfrs to event
    graph.add((alsfrsEvent, ns.BTO_schema['consists'], alsfrs))
    # link event to patient
    graph.add((patientURI, ns.BTO_schema['undergo'], alsfrsEvent))
    # add all the values of the questions and check total score
    sum = 0
    for i in range(1, 13):
        graph.add((alsfrs, ns.BTO_schema['alsfrs_' + str(i)], Literal(int(row[str(i)]), datatype=XSD.integer)))
        value = int(row[str(i)])
        sum = sum + value
    # if total score is correct, add to the graph
    if not pd.isna(row['ALSFRS-R']) and row['ALSFRS-R'] == sum:
        graph.add((alsfrs, ns.BTO_schema['alsfrs-r-tot'], Literal(int(row['ALSFRS-R']), datatype=XSD.integer)))
    # if total score is not correct, correct with the sum value and add to the graph
    elif not pd.isna(row['ALSFRS-R']) and row['ALSFRS-R'] != sum:
        graph.add((alsfrs, ns.BTO_schema['alsfrs-r-tot'], Literal(int(sum), datatype=XSD.integer)))
        print('[WARNING] Correct total score of the ALSFRS-R: %s' % (row['N_ALSFRS']))
    # if total score is not present, add the sum to the graph
    else:
        graph.add((alsfrs, ns.BTO_schema['alsfrs-r-tot'], Literal(int(sum), datatype=XSD.integer)))
        print('[WARNING] Add total score of the ALSFRS-R: %s' % (row['N_ALSFRS']))
    # add bulbar subscore (1 + 2 + 3)
    sumb = 0
    for i in range(1, 4):
        value = int(row[str(i)])
        sumb = sumb + value
    graph.add((alsfrs, ns.BTO_schema['bulbar_subscore'], Literal(int(sumb), datatype=XSD.integer)))
    print('[WARNING] Add bulbar subscore of the ALSFRS-R: %s' % (row['N_ALSFRS']))
    # add respiratory subscore (10 + 11 + 12)
    sumR = 0
    for i in range(10, 13):
        value = int(row[str(i)])
        sumR = sumR + value
    graph.add((alsfrs, ns.BTO_schema['respiratory_subscore'], Literal(int(sumR), datatype=XSD.integer)))
    print('[WARNING] Add respiratory subscore of the ALSFRS-R: %s' % (row['N_ALSFRS']))
    # add motor subscore (4 + 5 + 6 + 7 + 8 + 9)
    sumM = 0
    for i in range(4, 10):
        value = int(row[str(i)])
        sumM = sumM + value
    graph.add((alsfrs, ns.BTO_schema['motor_subscore'], Literal(int(sumM), datatype=XSD.integer)))
    print('[WARNING] Add motor subscore of the ALSFRS-R: %s' % (row['N_ALSFRS']))
    # return the updated graph
    return graph


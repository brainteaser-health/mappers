'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# turin_static_vars.py contains all the custom functions to manage only the TURIN static vars tab

# required libraries
from datetime import timedelta
import pandas as pd
from common import namespaces_als as ns
from common import classes_als as classes
from common import utils_als as utils
from common import named_individuals_als as ni
from common import common_static_vars
from rdflib import URIRef, Literal
from rdflib.namespace import XSD, RDF

# add clinical trial and hospital
# input: graph, patient ID, patient URI, date of the first visit, date of death and the status of the patient
# output: updated graph
def addClinicalTrial(graph, patientID, patientURI, dateOf1stVisit, dateOfDeath, status):
    # add clinical trial participation
    clinicalTrialPartecipation = utils.generateURI()
    # add clinical trial participation to graph
    graph.add((clinicalTrialPartecipation, RDF.type, classes.clinical_trial_partecipation))
    # add clinical trial description
    graph.add((clinicalTrialPartecipation, ns.BTO_schema['clinicalTrialDescription'], Literal('This is the retrospective clinical trial of the patient ' + patientURI.split("/")[-1], datatype=RDF.langString)))
    # add start date to clinical trial participation
    if not pd.isna(dateOf1stVisit):
        graph.add((clinicalTrialPartecipation, ns.BTO_schema['startDate'], Literal(dateOf1stVisit.date(), datatype=XSD.date)))
    else:
        print('[WARNING] Date of first visit not given for patient ID: %s, (Cannot add start date to clinical trial partecipation!)' % (patientID))
    # 1 = death patient
    if not pd.isna(status) and status == 1:
        if not pd.isna(dateOfDeath):
            graph.add((clinicalTrialPartecipation, ns.BTO_schema['endDate'], Literal(dateOfDeath.date(), datatype=XSD.date)))
        else:
            print('[WARNING] Patient died, but date of death not given for patient ID: %s, (Cannot add end date to clinical trial partecipation!)' % (
                    patientID))
    elif not pd.isna(status) and status == 2:
        print('[WARNING] Alive patient for patient ID: %s, (Cannot add end date to clinical trial partecipation!)' % (patientID))
    elif not pd.isna(status) and status != 1 and status != 2:
        print('[WARNING] Patient status not valid for patient ID: %s, (Cannot add end date to clinical trial partecipation!)' % (patientID))
    else:
        print('[WARNING] Patient status not given for patient ID: %s, (Cannot add end date to clinical trial partecipation!)' % (patientID))
    # link clinical trial participation to the patient
    graph.add((patientURI, ns.BTO_schema['enrolledIn'], clinicalTrialPartecipation))
    # link to clinical trial of turin
    graph.add((clinicalTrialPartecipation, ns.BTO_schema['partecipate'], ni.clinical_trial_turin))
    # add turin hospital
    graph.add((ni.clinical_trial_turin, ns.BTO_schema['pertain'], URIRef('https://www.unito.it/')))
    # return the updated graph
    return graph

# add stutus, date and cause of death to patient
# input: graph, patientID, patient URI, status of patient, date of death,
# optional input: addCauseOfDeath = True if we have the cause of death in the dataset and the cause of death value
# output: updated graph
def addPatientStatus(graph, patientID, patientURI, patientStatus, dateOfDeath, addCauseOfDeath = False, causeOfDeath = None):
    # patient death if status = 1
    if not pd.isna(patientStatus) and patientStatus == 1:
        # set alive property with false value
        graph.add((patientURI, ns.BTO_schema['alive'], Literal(False, datatype=XSD.boolean)))
        # check if the date of death is not null
        if not pd.isna(dateOfDeath):
            graph.add((patientURI, ns.BTO_schema['dateOfDeath'], Literal(dateOfDeath.date(), datatype=XSD.date)))
        else:
            print('[WARNING] Date of death not given for patient ID: %s' % (patientID))
        # if we have the cause of death, we set addCauseOfDeath with True value
        if addCauseOfDeath == True:
            # if the patient death is due to ALS, cause of death = 1
            if not pd.isna(causeOfDeath) and causeOfDeath == 1:
                graph.add((patientURI, ns.BTO_schema['deathDueToALS'], Literal(True, datatype=XSD.boolean)))
            # if the patient death is not due to ALS, cause of death = 0
            elif not pd.isna(causeOfDeath) and causeOfDeath == 0:
                graph.add((patientURI, ns.BTO_schema['deathDueToALS'], Literal(False, datatype=XSD.boolean)))
            elif not pd.isna(causeOfDeath) and causeOfDeath != 0 and causeOfDeath != 1:
                print('[WARNING] Cause of death not valid for patient ID: %s' % (patientID))
            else:
                print('[WARNING] Cause of death not given for patient ID: %s' % (patientID))
    # patient alive if status = 2
    elif not pd.isna(patientStatus) and patientStatus == 2:
        if not pd.isna(dateOfDeath):
            print('[WARNING] Alive patient but date of death given for patient ID: %s' % (patientID))
        else:
            # set alive property with true value
            graph.add((patientURI, ns.BTO_schema['alive'], Literal(True, datatype=XSD.boolean)))
    elif not pd.isna(patientStatus) and patientStatus != 1 and patientStatus != 2:
        print('[WARNING] Patient status not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Patient status not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add diagnosis event to patient
# input: graph, patient ID, patient URI and date of diagnosis
# output: updated graph and the diagnosis URI (useful for adding blood test vars with OWD vars dataframe)
def addDiagnosis(graph, patientID, patientURI, dateOfDiagnosis):
    # initialize the diagnosis event with 999 for those who do not have the date of diagnosis
    diagnosis = '999'
    # check if date is not null
    if not pd.isna(dateOfDiagnosis):
        # get diagnosis URI
        diagnosis = utils.generateURI()
        # add diagnosis to graph
        graph.add((diagnosis, RDF.type, classes.diagnosis))
        # since the date of diagnosis is always given on the 15th of the month,
        # subtract 14 days from the date of diagnosis to move this date to the 1st of the month
        dateOfDiagnosis = dateOfDiagnosis.date() - timedelta(days=14)
        # add start date to diagnosis
        graph.add((diagnosis, ns.BTO_schema['startDate'], Literal(dateOfDiagnosis, datatype=XSD.date)))
        # link diagnosis to the patient
        graph.add((patientURI, ns.BTO_schema['undergo'], diagnosis))
    else:
        print('[WARNING] Date of diagnosis not given for patient ID: %s, (Cannot add the diagnosis to the patient!)' % (patientID))
    # return the updated graph and the diagnosis URI
    return graph, diagnosis

# add onset to patient
# input: graph, three datasets regarding the types of limbs, patient ID, patient URI, onset date, age at onset,
#        motor neuron predominance, onset type, upper or lower limb, distal or proximal limb, left or right limb)
# output: updated graph
def addOnset(graph, ULdataframe, DPdataframe, LRdataframe, patientID, patientURI, dateOfFirstSymptoms, ageOnset, UMNvsLMNpredominance, onsetType, limbULvsLL, limbPredominantImpairment, limbPredominantSide):
    # check the validity of the date
    if not pd.isna(dateOfFirstSymptoms):
        # get onset URI
        onset = utils.generateURI()
        # add onset event to graph
        graph.add((onset, RDF.type, classes.onset))
        # link onset to the patient
        graph.add((patientURI, ns.BTO_schema['undergo'], onset))
        # since the date of onset is always given on the 15th of the month,
        # subtract 14 days from the date of onset to move this date to the 1st of the month
        dateOfFirstSymptoms = dateOfFirstSymptoms.date() - timedelta(days=14)
        # add start date to onset
        graph.add((onset, ns.BTO_schema['startDate'], Literal(dateOfFirstSymptoms, datatype=XSD.date)))
        # add properties of onset
        # age onset
        if not pd.isna(ageOnset):
            graph.add((onset, ns.BTO_schema['age_onset'], Literal(float(ageOnset), datatype=XSD.float)))
        else:
            print('[WARNING] Age at onset not given for patient ID: %s' % (patientID))
        # onset type
        if not pd.isna(onsetType):
            # 1 = limbs
            if onsetType == 1:
                # true
                graph.add((onset, ns.BTO_schema['limbs'], Literal(True, datatype=XSD.boolean)))
                # false
                graph.add((onset, ns.BTO_schema['bulbar'], Literal(False, datatype=XSD.boolean)))
                graph.add((onset, ns.BTO_schema['axial'], Literal(False, datatype=XSD.boolean)))
                graph.add((onset, ns.BTO_schema['generalized'], Literal(False, datatype=XSD.boolean)))
                # add onset limb type
                graph = common_static_vars.addLimbType(graph, onset, limbULvsLL, limbPredominantImpairment, limbPredominantSide, ULdataframe, DPdataframe, LRdataframe)
            # 2 = bulbar
            elif (onsetType == 2):
                if((limbULvsLL == 0 or limbULvsLL == 999) and
                        (limbPredominantImpairment == 0 or limbPredominantImpairment == 999) and
                        (limbPredominantSide == 0 or limbPredominantSide == 999)):
                    # true
                    graph.add((onset, ns.BTO_schema['bulbar'], Literal('True', datatype=XSD.boolean)))
                    # false
                    graph.add((onset, ns.BTO_schema['limbs'], Literal('False', datatype=XSD.boolean)))
                    graph.add((onset, ns.BTO_schema['axial'], Literal('False', datatype=XSD.boolean)))
                    graph.add((onset, ns.BTO_schema['generalized'], Literal('False', datatype=XSD.boolean)))
                else:
                    print('[WARNING] Bulbar onset type with linked limb type for patient ID: %s, (Cannot add type to onset!)' % (patientID))
            # 3 = axial
            elif (onsetType == 3):
                if ((limbULvsLL == 0 or limbULvsLL == 999) and
                        (limbPredominantImpairment == 0 or limbPredominantImpairment == 999) and
                        (limbPredominantSide == 0 or limbPredominantSide == 999)):
                    # true
                    graph.add((onset, ns.BTO_schema['axial'], Literal('True', datatype=XSD.boolean)))
                    # false
                    graph.add((onset, ns.BTO_schema['limbs'], Literal('False', datatype=XSD.boolean)))
                    graph.add((onset, ns.BTO_schema['bulbar'], Literal('False', datatype=XSD.boolean)))
                    graph.add((onset, ns.BTO_schema['generalized'], Literal('False', datatype=XSD.boolean)))
                else:
                    print('[WARNING] Axial onset type with linked limb type for patient ID: %s, (Cannot add type to onset!)' % (patientID))
            # 4 = generalized
            elif (onsetType == 4):
                # true
                graph.add((onset, ns.BTO_schema['generalized'], Literal('True', datatype=XSD.boolean)))
                # false
                graph.add((onset, ns.BTO_schema['limbs'], Literal('False', datatype=XSD.boolean)))
                graph.add((onset, ns.BTO_schema['bulbar'], Literal('False', datatype=XSD.boolean)))
                graph.add((onset, ns.BTO_schema['axial'], Literal('False', datatype=XSD.boolean)))
                # add onset limb type
                graph = common_static_vars.addLimbType(graph, onset, limbULvsLL, limbPredominantImpairment, limbPredominantSide, ULdataframe, DPdataframe, LRdataframe)
            else:
                print('[WARNING] Onset type not valid for patient ID: %s' % (patientID))
        else:
            print('[WARNING] Onset type not given for patient ID: %s' % (patientID))
        # add clinical assessment to onset
        if not pd.isna(UMNvsLMNpredominance):
            # get clinical assessment URI
            clinicalAssessment = utils.generateURI()
            # add clinical assessment to graph
            graph.add((clinicalAssessment, RDF.type, classes.clinical_assessment))
            # link clinical assessment to onset
            graph.add((onset, ns.BTO_schema['consists'], clinicalAssessment))
            # add UMNvsLMN predominance to clinical assessment
            # 1 = UMN
            if (UMNvsLMNpredominance == 1):
                graph.add((clinicalAssessment, ns.BTO_schema['prevalentUMN'], Literal(True, datatype=XSD.boolean)))
                graph.add((clinicalAssessment, ns.BTO_schema['prevalentLMN'], Literal(False, datatype=XSD.boolean)))
                graph.add((clinicalAssessment, ns.BTO_schema['mixedMN'], Literal(False, datatype=XSD.boolean)))
            # 2 = LMN
            elif (UMNvsLMNpredominance == 2):
                graph.add((clinicalAssessment, ns.BTO_schema['prevalentUMN'], Literal(False, datatype=XSD.boolean)))
                graph.add((clinicalAssessment, ns.BTO_schema['prevalentLMN'], Literal(True, datatype=XSD.boolean)))
                graph.add((clinicalAssessment, ns.BTO_schema['mixedMN'], Literal(False, datatype=XSD.boolean)))
            # 1 = MixedMN
            elif (UMNvsLMNpredominance == 3):
                graph.add((clinicalAssessment, ns.BTO_schema['prevalentUMN'], Literal(False, datatype=XSD.boolean)))
                graph.add((clinicalAssessment, ns.BTO_schema['prevalentLMN'], Literal(False, datatype=XSD.boolean)))
                graph.add((clinicalAssessment, ns.BTO_schema['mixedMN'], Literal(True, datatype=XSD.boolean)))
            else:
                print('[WARNING] UMNvsLMN predominance not valid for patient ID: %s' % (patientID))
        else:
            print('[WARNING] UMNvsLMN predominance not given for patient ID: %s' % (patientID))
    else:
        print('[WARNING] Onset date not given for patient ID: %s, (Cannot add onset to patient!)' % (patientID))
    # return the updated graph
    return graph

# add occupation to patient
# input: graph, patientID, patient URI, ESCO dataframe and the occupation value
# output: updated graph
def addOccupation(graph, patientID, patientURI, ESCOdataframe, occupationValue):
    if not pd.isna(occupationValue):
        # check if the occupation value is into the esco dataframe
        if ((ESCOdataframe.index == occupationValue).any() == True):
            # if true, get the occupation uri (named individual)
            occupation = str(ESCOdataframe.loc[ESCOdataframe.index == occupationValue].uri.values[0])
            # add occupation to patient
            graph.add((patientURI, ns.BTO_schema['hasOccupation'], URIRef(occupation)))
        else:
            print('[WARNING] Occupation (ESCO code: %i) not valid for patient ID: %s' % (occupationValue, patientID))
    else:
        print('[WARNING] Occupation not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

# add gene to patient
# input: graph, index, patient URI and sod1, c9orf72, tardbp, fus gene
# output: updated graph
def addGene(graph, patientID, patientURI, SOD1, c9orf72, TARDBP, FUS):
    # add SOD1
    if not pd.isna(SOD1) and SOD1 == 1:
        graph.add((patientURI, ns.BTO_schema['hasGene'], ni.sod1))
    elif not pd.isna(SOD1) and SOD1 != 1 and SOD1 != 0:
        print('[WARNING] SOD1 value not valid for patient ID: %s' % (patientID))
    elif pd.isna(SOD1):
        print('[WARNING] SOD1 value not given for patient ID: %s' % (patientID))
    # add C9orf72
    if not pd.isna(c9orf72):
        # check if the type is valid
        type = str(c9orf72).lower()
        if type == 'a' or type == 'b' or type == 'c' or type == 'q':
            # get instance URI
            c9orf72_ref = utils.generateURI()
            # add to the graph
            graph.add((c9orf72_ref, RDF.type, classes.c9orf72))
            # set the kind data property
            graph.add((c9orf72_ref, ns.BTO_schema['kind'], Literal(c9orf72, datatype=XSD.string)))
            # link to the patient
            graph.add((patientURI, ns.BTO_schema['hasGene'], c9orf72_ref))
        else:
            print('[WARNING] C9orf72 value not valid for patient ID: %s' % (patientID))
    else:
        print('[WARNING] C9orf72 value not given for patient ID: %s' % (patientID))
    # add TARDBP
    if not pd.isna(TARDBP) and TARDBP == 1:
        graph.add((patientURI, ns.BTO_schema['hasGene'], ni.tardbp))
    elif not pd.isna(TARDBP) and TARDBP != 1 and TARDBP != 0:
        print('[WARNING] TARDBP value not valid for patient ID: %s' % (patientID))
    elif pd.isna(TARDBP):
        print('[WARNING] TARDBP value not given for patient ID: %s' % (patientID))
    # add FUS
    if not pd.isna(FUS) and FUS == 1:
        graph.add((patientURI, ns.BTO_schema['hasGene'], ni.fus))
    elif not pd.isna(FUS) and FUS != 1 and FUS != 0:
        print('[WARNING] FUS value not valid for patient ID: %s' % (patientID))
    elif pd.isna(FUS):
        print('[WARNING] FUS value not given for patient ID: %s' % (patientID))
    # return the updated graph
    return graph

'''
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
      https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
     https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
     https://brainteaser.health/
     funded by the European Union’s Horizon 2020 research and innovation programme
     under the grant agreement No GA101017598
'''

# turin_spiro.py contains all the custom functions to manage the TURIN spiro tab

import csv
import pandas as pd
from pathlib import Path
from tabulate import tabulate
from common import namespaces_als as ns
from common import classes_als as classes
from common import utils_als
from rdflib import Literal
from rdflib.namespace import XSD, RDF

# generate a new csv file with correct spiro
# input: path of turin spiro tab
# output: turin-spiro-correct.csv
def getErrorsFromTurinSpiro(datasetPath):
    # get the base path
    path = str(Path().resolve())
    # read csv and
    spiro = pd.read_excel(datasetPath, sheet_name="%FVC", index_col='N_SPIRO', na_values=['NaT'])
    spiro = spiro.rename(columns={'DATA SPIRO':'Date'})
    # size of spiro
    sizeSpirotab = len(spiro)
    # header for txt tables
    header = ['N_SPIRO', 'REF', 'PARALS/CODALS', 'Date', '%FVC']
    # check errors in dates
    errorDateList = []
    errorDateListCount = 0
    # get the list of patient ids
    idList = spiro.REF.unique()
    # for every id check if a date is before another
    for id in idList:
        dateList = spiro.loc[spiro.REF == id].Date.values
        indexList = spiro.loc[spiro.REF == id].index.values
        row = spiro.loc[spiro.REF == id].values
        size = len(dateList)
        for i in range(0, size - 1):
            # if not, save error
            if dateList[i] > dateList[i + 1]:
                errorDateListCount += 1
                errorDateList.append(row[i])
                spiro.drop(labels=indexList[i], axis=0, inplace=True)

    duplicateList = []
    duplicateListCount = 0
    indexList = []
    # get the list of patient ids
    idList = spiro.REF.unique()
    # for every id check if a date is before another
    for id in idList:
        row = spiro.loc[spiro.REF == id].values
        index = spiro.loc[spiro.REF == id].index.values
        size = len(row)
        for x in range(0, size - 1):
            for i in range(x + 1, size):
                comparison = row[x] == row[i]
                equal_arrays = comparison.all()
                if equal_arrays == True:
                    tempArr = [index[i]]
                    duplicateListCount += 1
                    for elem in row[i]:
                        tempArr.append(str(elem))
                    duplicateList.append(tempArr)
                    indexList.append(index[i])
    for index in indexList:
        spiro.drop(labels=index, axis=0, inplace=True)
    #spiro.info()

    totalSpiro = 0
    errorDataList = []
    errorDataListCount = 0
    correctSpiroList = []
    correctSpiroListCount = 0
    # iterate over the dataframe
    for index, row in spiro.iterrows():
        totalSpiro += 1
        if not pd.isna(row['Date']) and not pd.isna(row['%FVC']):
            correctSpiroListCount += 1
            tempArr = [index]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            correctSpiroList.append(tempArr)
        else:
            errorDataListCount += 1
            tempArr = [index]
            results = row.values
            for elem in results:
                tempArr.append(str(elem))
            errorDataList.append(tempArr)
    with open(path + '/dataset/turin-fvc-correct.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['N_SPIRO', 'REF', 'Date', '%FVC'])
        for q in correctSpiroList:
            writer.writerow([q[0], q[1], q[3], q[4]])

    print('\nTotal number of Spiro: %s\n'
          'Number of correct Spiro: %s\n'
          'Number of incorrect Spiro: %s'
          % (sizeSpirotab,
             len(correctSpiroList),
             sizeSpirotab - len(correctSpiroList)))
    if duplicateListCount > 0:
        print('\n\n\n****************************** Duplicate Spiro. (count: ' + str(
            duplicateListCount) + ') ******************************\n')
        print(tabulate(duplicateList, headers=header))
    if errorDataListCount > 0:
        print('\n\n\n****************************** Spiro with empty dates or empty %fvc values. (count: ' + str(
        errorDataListCount) + ') ******************************\n')
        print(tabulate(errorDataList, headers=header))

    #with open(path + '/output/errors/turin-spiro-errors.txt', 'w') as f:
        #if duplicateListCount > 0:
            #f.write('****************************** Duplicate Spiro. (count: ' + str(
                #duplicateListCount) + ') ******************************\n\n\n')
            #f.write(tabulate(duplicateList, headers=header))
        #if errorDataListCount > 0:
            #f.write('****************************** Spiro with empty dates or empty %fvc values. (count: ' + str(
                #errorDataListCount) + ') ******************************\n\n\n')
            #f.write(tabulate(errorDataList, headers=header))

# add spiro
# input: graph, patient URI and the row of spiro
# output: updated graph
def addSpiro(graph, patient, row):
    # get spiro URI
    spiro = utils_als.generateURI('Turin', 'fvc', row['N_SPIRO'])
    # add spiro
    graph.add((spiro, RDF.type, classes.pulmonary_function_test))
    # get event URI
    spiroEvent = utils_als.generateURI()
    # add event
    graph.add((spiroEvent, RDF.type, classes.protocol_event))
    # add the date
    graph.add((spiroEvent, ns.BTO_schema['startDate'], Literal(row['Date'].date(), datatype=XSD.date)))
    #link event to patient
    graph.add((patient, ns.BTO_schema['undergo'], spiroEvent))
    # link event to spiro
    graph.add((spiroEvent, ns.BTO_schema['consists'], spiro))
    # add the value of the spiro
    graph.add((spiro, ns.BTO_schema['FVCrelative'], Literal(float(row['%FVC']), datatype=XSD.float)))
    # return the updated graph
    return graph
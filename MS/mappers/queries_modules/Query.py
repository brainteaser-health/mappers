"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
import rdflib
from pathlib import Path

class PaviaQuery():

    path_base = str(Path().resolve().parent)+"\\data\\output\\"

    registry_query_statement = """ 
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX po: <http://purl.obolibrary.org/obo/> 
PREFIX bto_schema: <https://w3id.org/brainteaser/ontology/schema/>
PREFIX bto_ni: <https://w3id.org/brainteaser/ontology/named-individual/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
select ?patient_id ?sex ?birthplace ?ethnicity ?MSinPediatricAge ?birthYear ?deathDate ?age (?type as ?MS_type_observed) (?start_date as ?date_observed_progression)  where { 
	?idURI a po:NCIT_C16960 ;
     	bto_schema:sex ?sex ;
    	bto_schema:yearOfBirth ?birthYear .
    optional{ ?idURI bto_schema:MSInPaediatricAge ?MSinPediatricAge . }
    optional{ ?idURI bto_schema:ethnicity ?id_ethnicity .}
    optional{?idURI bto_schema:hasBirthplace ?id_birthplace . } 
    optional{?idURI bto_schema:dateOfDeath ?deathDate . } 
    optional{
        ?idURI bto_schema:undergo ?visit .
        ?visit a po:NCIT_C39564 .
        ?visit bto_schema:consists ?clinical_assessment .
        ?clinical_assessment bto_schema:startDate ?start_date .
        ?clinical_assessment bto_schema:MS_type ?type .
    }
    bind( substr( (str(?idURI)), 48, 100) as ?patient_id)
    bind( substr( (str(?id_birthplace)), 56, 100) as ?birthplace)
    bind( substr( (str(?id_ethnicity)), 56, 100) as ?ethnicity)
    bind(if(bound(?deathDate), year(?deathDate)-year(?birthYear),year(now()) - year(?birthYear)) as ?age)
} order by ?patient_id ?date_observed_progression
"""
    edss_query_statement = """
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX po: <http://purl.obolibrary.org/obo/> 
PREFIX bto_schema: <https://w3id.org/brainteaser/ontology/schema/>

PREFIX bto_ni: <https://w3id.org/brainteaser/ontology/named-individual/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
 
select ?patient ?edss_date ?pyramidal ?cerebellar ?brainstem ?sensory ?bowel_and_bladder ?visual_function ?cerebral_functions ?ambulation ?totalEDSS 
where { 
	?idURI a po:NCIT_C16960 ;
    	bto_schema:undergo ?idEvent .  
    ?idEvent bto_schema:consists ?idEDSS .
    
    ?idEvent a po:NCIT_C39564 .
    ?idEDSS a po:NCIT_C98302 .
    
    # Include each of the following triples inside a 'optional' statement to get also data with missing values
    # (e.g. substitue the next row with ' optional{?idEvent bto_schema:startDate ?edss_date . } ' )
    
    ?idEvent bto_schema:startDate ?edss_date . 
    ?idEDSS bto_schema:pyramidal ?pyramidal . 
    ?idEDSS bto_schema:cerebellar ?cerebellar . 
    ?idEDSS bto_schema:brainstem ?brainstem . 
    ?idEDSS bto_schema:sensory ?sensory . 
    ?idEDSS bto_schema:bowel_and_bladder ?bowel_and_bladder . 
    ?idEDSS bto_schema:visual_function ?visual_function. 
    ?idEDSS bto_schema:cerebral_functions ?cerebral_functions . 
    ?idEDSS bto_schema:ambulation ?ambulation . 
    ?idEDSS bto_schema:totalEDSS ?totalEDSS . 

    bind( substr( (str(?idURI)), 48, 100) as ?patient)
    bind( substr( (str(?idEvent)), 48, 100) as ?event)
    bind( substr( (str(?idEDSS)), 48, 100) as ?EDSS)
    
} 
    """

    relapse_relapse_statement = """
    
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX po: <http://purl.obolibrary.org/obo/> 
PREFIX bto_schema: <https://w3id.org/brainteaser/ontology/schema/>

PREFIX bto_ni: <https://w3id.org/brainteaser/ontology/named-individual/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

select ?patient ?eventStartDate ?eventEndDate ?relapse ?sensory_relapse ?pyramidal_relapse ?brainstem_relapse ?sphincter_relapse ?vision_relapse ?cerebellum_relapse ?psychic_relapse ?others_relapse ?relapseLength ?impactOnADL ?recovery ?severity

where { 
	?idURI a po:NCIT_C16960 ;
    	bto_schema:undergo ?idEvent .  
    ?idEvent bto_schema:hasRelapse ?idRelapse .
    
    optional{?idEvent bto_schema:startDate ?eventStartDate .}
    optional{?idEvent bto_schema:startDate ?eventEndDate .}
    optional{?idRelapse bto_schema:sensory_relapse ?sensory_relapse .}
    optional{?idRelapse bto_schema:pyramidal_relapse ?pyramidal_relapse .}
    optional{?idRelapse bto_schema:brainstem_relapse ?brainstem_relapse .}
    optional{?idRelapse bto_schema:sphincter_relapse ?sphincter_relapse .}
    optional{?idRelapse bto_schema:impactOnADL ?impactOnADL .}
    optional{?idRelapse bto_schema:recovery ?recovery .}
    optional{?idRelapse bto_schema:others_relapse ?others_relapse .}
    optional{?idRelapse bto_schema:relapseLength ?relapseLength .}
    optional{?idRelapse bto_schema:severity ?severity .}
    optional{?idRelapse bto_schema:vision_relapse ?vision_relapse .}
    optional{?idRelapse bto_schema:cerebellum_relapse ?cerebellum_relapse .}
    optional{?idRelapse bto_schema:psychic_relapse ?psychic_relapse .}

    ?idEvent a po:NCIT_C25499 .

    bind( substr( (str(?idURI)), 48, 100) as ?patient)
    bind( substr( (str(?idEvent)), 48, 100) as ?event)
    bind( substr( (str(?idRelapse)), 48, 100) as ?relapse)
} ORDER BY ?patient

    """

    def __init__(self, graph):
        self.graph = graph

    def registry(self):
        results = self.graph.query(self.registry_query_statement)
        out_path = self.path_base+"registry.csv"
        with open(out_path, "w") as fout:
            fout.write("patient_id,sex,birthplace,ethnicity,MSinPediatricAge,birthYear,deathDate,age,MS_type_observed,date_observed_progression\n")
            #print(results)
            for row in results:
                fout.write(f"{row.patient_id},{row.sex},{row.birthplace},{row.ethnicity},{row.MSinPediatricAge},{row.birthYear},{row.deathDate},{row.age},{row.MS_type_observed},{row.date_observed_progression}\n")

    def edss(self):
        results = self.graph.query(self.edss_query_statement)
        out_path = self.path_base+"edss.csv"
        with open(out_path, "w") as fout:
            fout.write("patient,edss_date,pyramidal,cerebellar,brainstem,sensory,bowel_and_bladder,visual_function,cerebral_functions,ambulation,totalEDSS\n")
            #print(results)
            for row in results:
                fout.write(f"{row.patient},{row.sex},{row.edss_date},{row.pyramidal},{row.cerebellar},{row.brainstem},{row.sensory},{row.bowel_and_bladder},{row.visual_function},{row.cerebral_functions},{row.ambulation},{row.totalEDSS}\n")

    def relapse(self):
        results = self.graph.query(self.relapse_relapse_statement)
        out_path = self.path_base+"relapse.csv"
        with open(out_path, "w") as fout:
            fout.write("patient,eventStartDate,eventEndDate,sensory_relapse,pyramidal_relapse,brainstem_relapse,sphincter_relapse,vision_relapse,cerebellum_relapse,psychic_relapse,others_relapse,relapseLength,impactOnADL,recovery,severity\n")
            #print(results)
            for row in results:
                fout.write(f"{row.patient},{row.eventStartDate},{row.eventEndDate},{row.sensory_relapse},{row.pyramidal_relapse},{row.brainstem_relapse},{row.sphincter_relapse},{row.vision_relapse},{row.cerebellum_relapse},{row.psychic_relapse},{row.others_relapse},{row.relapseLength},{row.impactOnADL},{row.recovery},{row.severity}\n")
"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime as d
from datetime import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import ncit_codes as ncit
# import the uri generator
from mappers.utility_scripts import id_generator as idg


def relapseMapper(relapses, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                  forbidden_list):
    for index, row in relapses.iterrows():

        if row["Paziente ID"] in forbidden_list:
            continue

        # if this is an 'empty row' we just ignore it and continue to the next one
        if check_if_at_least_one_option_is_yes(row):
            print(f'[WARNING] - [Relapse] - Row without any relapse at row Id: {index}')
            continue

        # retrieve the patient URI
        idPatient = patientCodes[row["Paziente ID"]]

        # create the protocol event
        protocolEventId = idg.getURI("pavia", "relapse", str(index))

        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
        # link the patient to the protocol event
        g.add((idPatient, BTO_schema["undergo"], protocolEventId))
        # link add the date
        g.add((protocolEventId, BTO_schema["startDate"], Literal(row["Data inizio relapse"].date(), datatype=XSD.date)))

        # now get the end date
        if not pd.isna(row["Data inizio relapse"]) and not pd.isna(row["Durata (giorni)"]): # if the information is available
            # get the starting date as a datetime object, sum a delta of days equal to the duration of the relapse
            relapse_start_date = row["Data inizio relapse"].date() # datetime object
            relapse_end_date = relapse_start_date + d.timedelta(days=int(row["Durata (giorni)"]))
            g.add((protocolEventId, BTO_schema["endDate"],
                   Literal(relapse_end_date, datatype=XSD.date)))


        # create the entity relapse
        relapseId = idg.getURI()
        g.add((relapseId, RDF.type, PO[ncit.RELAPSE]))

        # link the event to the relapse
        g.add((protocolEventId, BTO_schema["hasRelapse"], relapseId))

        # add properties to relapse
        if not pd.isna(row["Sfinteri"]) and str(row["Sfinteri"]).lower() == "si":
            g.add((relapseId, BTO_schema["sphincter_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["Sfinteri"]) and str(row["Sfinteri"]).lower() == "no") :
            g.add((relapseId, BTO_schema["sphincter_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["TroncoEncefalo"]) and str(row["TroncoEncefalo"]).lower() == "si":
            g.add((relapseId, BTO_schema["brainstem_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["TroncoEncefalo"]) and str(row["TroncoEncefalo"]).lower() == "no") :
            g.add((relapseId, BTO_schema["brainstem_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Cervelletto"]) and str(row["Cervelletto"]).lower() == "si":
            g.add((relapseId, BTO_schema["cerebellum_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["Cervelletto"]) and str(row["Cervelletto"]).lower() == "no") :
            g.add((relapseId, BTO_schema["cerebellum_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Psichico"]) and str(row["Psichico"]).lower() == "si":
            g.add((relapseId, BTO_schema["psychic_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["Psichico"]) and str(row["Psichico"]).lower() == "no") :
            g.add((relapseId, BTO_schema["psychic_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Piramidale"]) and str(row["Piramidale"]).lower() == "si":
            g.add((relapseId, BTO_schema["pyramidal_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["Piramidale"]) and str(row["Piramidale"]).lower() == "no") :
            g.add((relapseId, BTO_schema["pyramidal_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Sensitivo"]) and str(row["Sensitivo"]).lower() == "si":
            g.add((relapseId, BTO_schema["sensory_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["Sensitivo"]) and str(row["Sensitivo"]).lower() == "no") :
            g.add((relapseId, BTO_schema["sensory_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Visivo"]) and str(row["Visivo"]).lower() == "si":
            g.add((relapseId, BTO_schema["vision_relapse"], Literal("true", datatype=XSD.boolean)))
        if (not pd.isna(row["Visivo"]) and str(row["Visivo"]).lower() == "no") :
            g.add((relapseId, BTO_schema["vision_relapse"], Literal("false", datatype=XSD.boolean)))

        # in the case of "Altro", there is a second column where the doctor specifies the system
        if not pd.isna(row["Altro sistema funzionale"]) and not pd.isna(row["Specificare"]) and str(
                row["Altro sistema funzionale"]).lower() == "si":
            g.add((relapseId, BTO_schema["others_relapse"], Literal(str(row["Specificare"]), lang="en")))
        # elif pd.isna(row["Altro sistema funzionale"]):
        #     g.add((relapseId, BTO_schema["others_relapse"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Recupero dopo relapse"]):
            if str(row["Recupero dopo relapse"]).lower() == "completo":
                g.add((relapseId, BTO_schema["recovery"], Literal("complete", lang="en")))
            elif str(row["Recupero dopo relapse"]).lower() == "parziale":
                g.add((relapseId, BTO_schema["recovery"], Literal("partial", lang="en")))
            elif str(row["Recupero dopo relapse"]).lower() == "nessun recupero":
                g.add((relapseId, BTO_schema["recovery"], Literal("no recovery", lang="en")))
            elif str(row["Recupero dopo relapse"]).lower() != "non so":
                print(f'[WARNING] - [Relapse] - Unexpected value for "recovery" at id : {row["Recupero dopo relapse"]}')

        if not pd.isna(row["Impatto sulle ADL"]):
            if str(row["Impatto sulle ADL"]).lower() == "si":
                g.add((relapseId, BTO_schema["impactOnADL"], Literal(True, datatype=XSD.boolean)))
            elif str(row["Impatto sulle ADL"]).lower() == "no":
                g.add((relapseId, BTO_schema["impactOnADL"], Literal(False, datatype=XSD.boolean)))
            elif str(row["Impatto sulle ADL"]).lower() != "non so":
                print(f'[WARNING] - [Relapse] - Unexpected value for "impactOnADL" at id : {row["Impatto sulle ADL"]}')

        if not pd.isna(row["Gravità"]):
            if str(row["Gravità"]).lower() == "moderata":
                g.add((relapseId, BTO_schema["severity"], Literal("moderate", lang="en")))
            elif str(row["Gravità"]).lower() == "lieve":
                g.add((relapseId, BTO_schema["severity"], Literal("slight", lang="en")))
            elif str(row["Gravità"]).lower() == "grave":
                g.add((relapseId, BTO_schema["severity"], Literal("serious", lang="en")))
            elif str(row["Gravità"]).lower() != "non so":
                print(f'[WARNING] - [Relapse] - Unexpected value for "severity" at id : {row["Gravità"]}')

        if not pd.isna(row["Trattamento cortisonico"]) and str(row["Trattamento cortisonico"]).lower() == "si":
            g.add((relapseId, BTO_schema["associatedSubstance"], BTO_ni["cortisone_treatment"]))

        if not pd.isna(row["Trattamento cortisonico"]) and str(row["Trattamento cortisonico"]).lower() == "si":
            # g.add((relapseId, BTO_schema["associatedSubstance"], BTO_ni["cortisone_treatment"]))
            g.add((relapseId, BTO_schema["has_cortisone_treatment"], Literal(True, datatype=XSD.boolean)))
        elif not pd.isna(row["Trattamento cortisonico"]) and str(row["Trattamento cortisonico"]).lower() == "no":
            g.add((relapseId, BTO_schema["has_cortisone_treatment"], Literal(False, datatype=XSD.boolean)))
        else:
            pass # we do not add a triple to the KB in case of missing value

        if not pd.isna(row["Trattamento in regime ambulatoriale"]):
            if str(row["Trattamento in regime ambulatoriale"]).lower() == "si":
                g.add((relapseId, BTO_schema["clinical_admission"], Literal(True, datatype=XSD.boolean)))
            elif not str(row["Trattamento in regime ambulatoriale"]).lower() == "no":
                g.add((relapseId, BTO_schema["clinical_admission"], Literal(False, datatype=XSD.boolean)))
        else:
            pass # we do not add a triple to the KB in case of missing value


        if not pd.isna(row["Trattamento in regime ospedaliero"]):
            if str(row["Trattamento in regime ospedaliero"]).lower() == "si":
                g.add((relapseId, BTO_schema["hospital_admission"], Literal(True, datatype=XSD.boolean)))
            elif not str(row["Trattamento in regime ospedaliero"]).lower() == "no":
                g.add((relapseId, BTO_schema["hospital_admission"], Literal(False, datatype=XSD.boolean)))
        else:
            pass # we do not add a triple to the KB in case of missing value

        if not pd.isna(row["Durata (giorni)"]):
            g.add((relapseId, BTO_schema["relapseLength"], Literal(int(row["Durata (giorni)"]), datatype=XSD.integer)))

    return g


def check_if_at_least_one_option_is_yes(row):
    """ There is the possibility that, in certain tuples, no information about the relapse is given.
    In these ''strange'' cases, the tuple is simply useless, and needs to be discarded.
    The method checks for this eventuality, and returns true if this is the case.
    """
    if pd.isna(row["Sfinteri"]) and pd.isna(row["TroncoEncefalo"]) and pd.isna(row["Cervelletto"]) \
            and pd.isna(row["Psichico"]) and pd.isna(row["Piramidale"]) and pd.isna(row["Sensitivo"]) \
            and pd.isna(row["Visivo"]) and pd.isna(row["Altro sistema funzionale"]):
        return True

    return False

def check_dates(onset_data, relapse_data):
    """
    Checks if the dates of the relapses come after the date of the onset
    :param onset_data:
    :param relapse_data:
    :return:
    """
    merged = onset_data.merge(relapse_data, left_on="Paziente ID", right_on="Paziente ID", how="outer")
    list_of_misplaced_dates = merged[merged["Data esordio SM"] > merged["Data inizio relapse"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Relapses] data inizio relapse sooner than data esordio SM: " + str(
        len(list_of_misplaced_dates)))
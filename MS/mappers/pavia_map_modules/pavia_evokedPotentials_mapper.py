"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
import re
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS, OWL
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
from mappers.codes import omit_codes as omit
#import uri generator
from mappers.utility_scripts import id_generator as idg
#import disease mapping
from mappers.utility_scripts import loadDisease

# a presence of information for evoked potentials (nan) corresponds to a missing triple.

def evokedPotentialMapper(comorbidities_pavia, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                          forbidden_list):

    for index, row in comorbidities_pavia.iterrows():

        if row["Paziente ID"] in forbidden_list:
            continue

        patientId = patientCodes[row["Paziente ID"]]
        # IRI for person instance
        visitId = idg.getURI("pavia","evoked_potentials", index)
        g.add((visitId, RDF.type, PO[ncit.PATIENT_VISIT]))
        if not pd.isna(row["Data esame"]):
            g.add((visitId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
        else:
            print(f'[WARNING] - Misssing date [Data esame] for clinical event\tindex : {index}')
        g.add((patientId, BTO_schema["undergo"], visitId))

        if not pd.isna(row["P.E.V. Destra"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.VISUAL_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.V. Destra"] == "Alterazione":
                # g.add((epId, BTO_schema["location"], Literal("right", datatype=RDF.langString)))
                g.add((epId, BTO_schema["location"], Literal("right", lang="en")))
                # g.add((epId, BTO_schema["potentialValue"], Literal("altered", datatype=RDF.langString)))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.V. Destra"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("right", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.V. Sinistra"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.VISUAL_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.V. Sinistra"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("left", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.V. Sinistra"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("left", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.A. Destra"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.AUDITORY_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.A. Destra"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("right", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.A. Destra"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("right", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.A. Sinistra"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.AUDITORY_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.A. Sinistra"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("left", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.A. Sinistra"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("left", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.S. Destra - Arto superiore"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.SOMATOSENSORY_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.S. Destra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("upper right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.S. Destra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("upper right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.S. Sinistra - Arto superiore"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.SOMATOSENSORY_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.S. Sinistra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("upper left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.S. Sinistra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("upper left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.S. Destra - Arto inferiore"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.SOMATOSENSORY_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.S. Destra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("lower right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.S. Destra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("lower right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.S. Sinistra - Arto inferiore"]):
            epId = idg.getURI()
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((epId, RDF.type, PO[omit.SOMATOSENSORY_EVOKED_POTENTIALS]))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.S. Sinistra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("lower left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.S. Sinistra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("lower left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.M. Destra - Arto superiore"]):
            epId = idg.getURI()
            g.add((epId, RDF.type, PO[omit.MOTOR_EVOKED_POTENTIALS]))
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.M. Destra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("upper right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.M. Destra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("upper right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.M. Sinistra - Arto superiore"]):
            epId = idg.getURI()
            g.add((epId, RDF.type, PO[omit.MOTOR_EVOKED_POTENTIALS]))
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.M. Sinistra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("upper left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.M. Sinistra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("upper left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("upper", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.M. Destra - Arto inferiore"]):
            epId = idg.getURI()
            g.add((epId, RDF.type, PO[omit.MOTOR_EVOKED_POTENTIALS]))
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.M. Destra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("lower right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.M. Destra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("lower right", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

        if not pd.isna(row["P.E.M. Sinistra - Arto inferiore"]):
            epId = idg.getURI()
            g.add((epId, RDF.type, PO[omit.MOTOR_EVOKED_POTENTIALS]))
            if not pd.isna(row["Data esame"]):
                g.add((epId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((visitId, BTO_schema["consists"], epId))
            if row["P.E.M. Sinistra - Arto superiore"] == "Alterazione":
                g.add((epId, BTO_schema["location"], Literal("lower left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))
            elif row["P.E.M. Sinistra - Arto superiore"] == "Normale":
                g.add((epId, BTO_schema["location"], Literal("lower left", lang="en")))
                # g.add((epId, BTO_schema["location"], Literal("lower", lang="en")))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))

    return  g
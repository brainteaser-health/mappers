"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
import re
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS, OWL
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
#import uri generator
from mappers.utility_scripts import id_generator as idg
#import disease mapping
from mappers.utility_scripts import loadDisease

def relativeMapper(comorbidities_pavia, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                   forbidden_list):

    path = str(Path().resolve())
    path = path + "/data/output/diseaseToNCIT.csv"
    dictNCIT = loadDisease.load(path)


    for index, row in comorbidities_pavia.iterrows():

        if row["Paziente ID"] in forbidden_list:
            continue

        disease = None
        if not pd.isna(row["ICD9"]):
            disease = row["ICD9"].split("]")[-1].strip()

        if disease in dictNCIT:
            patientId = patientCodes[row["Paziente ID"]]

            if disease.lower() == "Sclerosi multipla".lower():
                g.add((patientId, BTO_schema["hasRelative"], BTO_resource["Relative_with_MS"]))
            elif disease.lower() == "Sclerosi Laterale Amiotrofica".lower():
                g.add((patientId, BTO_schema["hasRelative"], BTO_resource["Relative_with_ALS"]))
            else:
                g.add((patientId, BTO_schema["hasRelative"], BTO_resource["Relative_with_other_relevant_disease"]))
        elif not pd.isna(disease):
            print(f'[MAPPING-SYSTEM-WARNING] - Unknown disease. It must be updated the dictionary: disease <--> ncit_code\tdisease_label = {disease} - at index = {index}')
    print()

    return  g
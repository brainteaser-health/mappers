"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import numpy as np
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import \
    ncit_codes as ncit  # file python containing NCIT codes. These can be appended to the PO urls to obtain full fledged URLs
from mappers.codes import maxo_codes as maxo

from mappers.utility_scripts import id_generator as idg
from mappers.utility_scripts import urban_degree as ud
from mappers.namespaces.ontologies import UMLS_Cities, UMLS_Rural_area, UMLS_Towns





def registryMapper(patients, onsetAndDiagnosis, g, BTO_schema, BTO_resource, BTO_ni, PO, file, projectPaths, forbidden_list):
    '''
        - Given a tab patients as a dataframe, a rdf graph and all the namespace required this function will provide
         the mapping of all the entries in patients into a graph database according to the schema defined in the brainteaser ontology.

         :param patiens table with the information of the patients read from file
         :param onsetAndDiagnosis a pandas dataframe obtained from the reading of data about onset and diagnosis of the patients
         :param BTO_schema, BTO_resource, BOT_ni: the URLs corresponding to the urls that belong to the ontology (schema) of the Brainteaser ontology,
         the ones that are resources, the ones that are Named Individuals
         :param file: the log file where we write the result of this mapping performance

         :returns the graph and also a ditionary containing the ID of the patients with their URLs
    '''

    # dictionary for recording the evaluated progression
    observedProgressions = {}
    # dictionary for recording the patient code (so then we don't have to compute it againg)
    patientCodes = {}

    # create the clinical trial entity (retrospective) referred to pavia
    PV_CLINICAL_TRIAL = "ClinicalTrial_MS_Pavia_1"  # nb, we hardcoded here the fact that this is the clinical trial about Pavia, and it is the first clinical trial
    g.add((BTO_ni[PV_CLINICAL_TRIAL], RDF.type,
           PO[ncit.CLINICAL_TRIAL]))  # we specify that this is a node of type clinical trial
    # link the trial with the clinic, in our ontology "https://www.mondino.it/" is a named individual which has type "Clinics and Hospital" ///TODO: import from the ontology (so don't re-define here)
    g.add((BTO_ni[PV_CLINICAL_TRIAL], BTO_schema["pertain"], URIRef(
        "https://www.mondino.it/")))  # we specify that this clinical trial pertains to the data obtained from Pavia (i.e., the company Mondino)

    # Import the dictionary with the urban degrees
    degurbaPath = str(Path().resolve())
    degurbaPath = degurbaPath + "/data/input/Classificazioni statistiche-e-dimensione-dei-comuni_31_12_2020.xls"
    urbanDegree = ud.UrbanDegree(absPath=degurbaPath)

    # now that it has been initialized, import/compute the values that we may need
    urbanDegree.computeDegree()
    urbanDegree.computeIstatCorrespondences()

    urbanDegree.populate_hash_dictionary(projectPaths.italian_cap_file_path)
    urbanDegree.populate_hash_dictionary(projectPaths.spanish_cap_file_path)
    urbanDegree.populate_hash_dictionary(projectPaths.portuguese_cap_file_path)

    # number of locations that are not found/mapped in the csv files
    null_zip = 0
    null_placename = 0
    null_location = 0

    # iterate over the patients dataframe
    for index, row in patients.iterrows():

        if index in forbidden_list:
            continue

        # create patient node to add to the graph
        patient = idg.getURI("pavia", "patient", str(index))  # creates the URI of the patient
        patientCodes.update({
                                index: patient})  # insert this new patient's URL in the dictionary. The index corresponds to the ID used in the Pavia's file

        id_patient = index

        if not pd.isna(row["Decorso calcolato"]):  # if Decorso calcolato is present in the row

            dates = str(row["Decorso calcolato"])  # in Decorso calcolato we have a list of dates
            dates = dates.split(";")[:-1]  # split the dates (the last one is an empty string, we do without it)
            patient_observedProgression = {}

            for event in dates:  # for each date
                type_event, date_event = event.split()
                date_event = date_event[1:-1]  # take the second element of the string, the date
                date_event = pd.to_datetime(date_event)  # convert the string to an object datetime
                # update the dictionary
                patient_observedProgression.update(
                    {date_event: (type_event, "notRegistered")})  # add the date to this dictionary

            observedProgressions.update({
                                            id_patient: patient_observedProgression})  # add these observations pertaining this patient to the dictionary

        # add triples using store's add() method.
        # add a patient
        g.add((patient, RDF.type, PO[ncit.PATIENT]))
        g.add((patient, BTO_schema["hasDisease"], BTO_ni["Multiple_Sclerosis"]))

        # add birthday to patient (if available)
        birth_year = ''
        if not (
        pd.isna(row['Data di nascita'])):  # if the element 'Data di nascita' is not not present (thus, it is present)
            birth_year = row['Data di nascita'].year
            g.add((patient, BTO_schema['yearOfBirth'],
                   Literal(birth_year, datatype=XSD.gYear)))  # take only the year

        # add date of death to patient (if not empty)
        death_date = ''
        if not (pd.isna(row['Data di morte'])):
            death_date = row['Data di morte'].date()
            g.add((patient, BTO_schema['dateOfDeath'], Literal(death_date, datatype=XSD.date)))

        # add gender to patient (if not empty)
        if not (pd.isna(row['Sesso'])):
            if row["Sesso"] == "f":
                # g.add((patient, BTO_schema['sex'], Literal("female", datatype=RDF.langString))) # it seems using this datatype langString in this way makes the triple impossible to import
                # just specifying the language tag makes the string of type rdf:langString
                g.add((patient, BTO_schema['sex'], Literal("female", lang='en')))
            if row["Sesso"] == "m":
                # g.add((patient, BTO_schema['sex'], Literal("male", datatype=RDF.langString)))
                g.add((patient, BTO_schema['sex'], Literal("male", lang='en')))

        # add MS in pediatric age
        if not pd.isna(row["SM in età pedriatrica"]):
            if row["SM in età pedriatrica"] == "Si":
                g.add((patient, BTO_schema["MSInPaediatricAge"], Literal("True", datatype=XSD.boolean)))
            elif row["SM in età pedriatrica"].lower().strip() == "no":
                g.add((patient, BTO_schema["MSInPaediatricAge"], Literal("False", datatype=XSD.boolean)))
        # in case this value were not present, no triple is added

        # ===== SPANS of Time =====
        # take the onset date of this patient
        onset_date = onsetAndDiagnosis[onsetAndDiagnosis["Paziente ID"] == index]['Data esordio SM']
        if onset_date is not None and birth_year != '':
            # there should be only 1 onset date corresponding to this patient.
            onset_date = pd.to_datetime(onset_date.values[0]).year # get the year from the date
            age_at_onset = onset_date - birth_year # get the difference (as number of years) from the birth year
            g.add((patient, BTO_schema['age_at_onset'], Literal(age_at_onset, datatype=XSD.int)))

        # ===== ETHNICITY (Bioportal) =====
        if not (pd.isna(row['Etnia'])):

            # African
            if (row['Etnia'] == 'Africana'):
                # add ethnicity to patient
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Black_African"]))

            # Hispanic
            elif (row['Etnia'] == 'Ispanica'):
                # add ethnicity to patient
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Hispanic"]))

            # Caucasian -> White
            elif (row['Etnia'] == 'Caucasica'):
                # add ethnicity to patient
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Caucasian"]))
        # in case of NaN, ethnicity is not inserted as triple

        # try to set the birthplace of the patient. Update the number of errors
        null_zip_, null_placename_, null_location_ = set_birthplace(BTO_ni, BTO_schema, urbanDegree, g, patient, str(index), row)
        null_zip += null_zip_
        null_placename += null_placename_
        null_location += null_location_

        # ===== CLINICAL TRIAL PARTICIPATION =====
        # Define a new clinical trial participation
        clinicalTrialParticipationId = idg.getURI()  # id generated automatically
        # dateFirstVisit = onsetAndDiagnosis.loc[onsetAndDiagnosis["Paziente ID"] == index][
        #     "Data prima visita nel centro"].values
        dateFirstVisit = onsetAndDiagnosis.loc[onsetAndDiagnosis["Paziente ID"] == index][
            "Data prima visita nel centro"].dt.date.values  # use the patient id to find the patient in the other file. We also convert the datetime to a date
        g.add((clinicalTrialParticipationId, RDF.type,
               BTO_schema["Clinical_Trial_Partecipation"]))  # add the c.t.p. as triple
        # link the patient to that clinical trial participation
        g.add((patient, BTO_schema["enrolledIn"], clinicalTrialParticipationId))
        # add the start date to the clinical trial (the first date found in the list of dates): it is the same date of the onset
        g.add((clinicalTrialParticipationId, BTO_schema["startDate"], Literal(dateFirstVisit[0], datatype=XSD.date)))
        # link the participation entity to the clinical trial
        g.add((clinicalTrialParticipationId, BTO_schema["participate"], BTO_ni[PV_CLINICAL_TRIAL]))

        # Observed progression mapping

        valid_MS_type = ("CIS", "PP", "PR", "RIS", "RR", "SP")

        for key, value in patient_observedProgression.items(): # for each clinical assessment done to the patient

            # print(key, value)
            typeMS = value[0]

            if typeMS.strip().upper() in valid_MS_type:
                # Observed progression
                vistId = idg.getURI()
                g.add((vistId, RDF.type, PO[ncit.PATIENT_VISIT]))
                g.add((patient, BTO_schema["undergo"], vistId))

                clinicalAssessmentId = idg.getURI()
                g.add((clinicalAssessmentId, RDF.type, PO[maxo.CLINICAL_ASSESSMENT]))
                # g.add((clinicalAssessmentId, BTO_schema["startDate"], Literal(key, datatype=XSD.date)))
                g.add((clinicalAssessmentId, BTO_schema["startDate"],
                       Literal(key.date(), datatype=XSD.date)))  # the key is the date of the visit
                g.add((vistId, BTO_schema["consists"], clinicalAssessmentId))
                g.add((clinicalAssessmentId, BTO_schema["MS_type"], Literal(typeMS, datatype=XSD.string)))
            else:
                print(f'[WARNING] - Unexpected value as type of MS observed!\tindex : {index}\tvalue : {typeMS}')

        # Notes mapping
        if not pd.isna(row["Note"]):
            g.add((patient, BTO_schema["notes"], Literal("[Anagrafica] - " + str(row["Note"]), datatype=XSD.string)))

    # print("****** PATIENT CODES: ******\n")
    #
    # for i, val in patientCodes.items():
    #     print("id : ", i, "\tURI : ", val)

    print("\n")


    print("[SYSTEM-QUALITY-REPORT] - [Personal Data] - Number of patients without zip codes: " + str(null_zip))
    print("[SYSTEM-QUALITY-REPORT] - [Personal Data] number of patients without placenames: " + str(null_placename))
    print("[SYSTEM-QUALITY-REPORT] - [Personal Data] We were unable to create a birthplace for these many patients: " + str(null_location))

    # finally, we check that the dates are correctly spaced in time
    check_dates(patients, onsetAndDiagnosis)

    return g, patientCodes


def check_dates(patients, onsetAndDiagnosis):
    # merge the two tables on patient id
    merged = patients.merge(onsetAndDiagnosis, left_on="Paziente ID", right_on="Id", how="outer")
    list_of_misplaced_dates = merged[merged["Data esordio SM"] > merged["Data diagnosi"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Personal Data] data esordio later than data diagnosi: " + str(len(list_of_misplaced_dates)))
    list_of_misplaced_dates = merged[merged["Data diagnosi"] > merged["Data prima visita nel centro"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Personal Data] data diagnosi later than data prima visita nel centro: " + str(len(list_of_misplaced_dates)))
    list_of_misplaced_dates = merged[merged["Data prima visita nel centro"] > merged["Data di morte"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Personal Data] data prima visita nel centro later than data morte: " + str(len(list_of_misplaced_dates)))




def set_birthplace(BTO_ni, BTO_schema, urbanDict, g, patient, patient_id, row):
    """
    Given a patient and their birthplace, it create
    an instance of Place with the correct sub-type (City, Town or Rural Area)
    and assigns it the correct hash
    :param BTO_ni:
    :param BTO_schema:
    :param urbanDict: object of type UrbanDegree
    :param g:
    :param patient:
    :param patient_id:
    :param row:
    """

    degurbaDict = urbanDict.deg_dict
    istatDict = urbanDict.istat_dict # dictionary with the istat values of the placenames

    null_zip =0
    null_placename = 0
    null_location = 0


    # see if the zip code and/or the placename are available
    zip = "" # cap
    placename = "" # comune

    # take the zip code
    if not pd.isna(row["CAP"]):
        zip = row["CAP"]
    else: # no zip code immediately available
        null_zip = 1
        if not pd.isna(row["Comune"]):
            # try to recover with the placename
            zip = urbanDict.find_zip_from_placename(row["Comune"])

    # take the placename
    if not pd.isna(row["Comune"]):
        placename = row["Comune"]
    else: # no placename immediately available
        null_placename = 1
        if not pd.isna(row["CAP"]): # try to recover with the zip
            placename = urbanDict.find_placename_from_zip(row["CAP"])

    # get the hash of the location of the user, if present
    hash = urbanDict.get_hash_value(zip, placename)
    istat_hash = urbanDict.compute_istat_hash(placename)

    # if the hash was found, use it to build a new instance of place
    # place_id = ''
    if hash != "" and istat_hash != '': # we create a new named individual with a url identified by the hash of the place
        # g.add((patient, BTO_schema["hasBirthplace"], BTO_ni[hash]))
        g.add((patient, BTO_schema["hasBirthplace"], BTO_ni[istat_hash]))
        g.add((BTO_ni[istat_hash], BTO_schema["hasZip"], BTO_ni[hash]))
    else:
        print("[WARNING] impossible to create named individual for the residence of patient " + patient_id
              + "\nzip was: " + zip + " placename was: " + placename)
        null_location = 1


    # if not pd.isna(row["Comune"]):
    if placename != "":
        if degurbaDict.get(placename.lower(), "") != '':
            set_place_type(BTO_ni, degurbaDict.get(placename.lower(), ""), g, istat_hash)
        else:
            ret = try_special_cases(degurbaDict, placename)
            if ret != "":
                set_place_type(BTO_ni, ret, g, istat_hash)
            else:
                print(
                    "[WARNING] this placename was not found among the mapping to town/city/rural area: " + placename)
    else:
        print(
            "[WARNING] unable to set the place type for patient: " + patient_id)

    return null_zip, null_placename, null_location


def set_place_type(BTO_ni, place, g, istat_hash):
    # now, let us make this named individual of the correct type
    if place == "city":
        g.add((BTO_ni[istat_hash], RDF.type, UMLS_Cities))
    elif place == "town or suburbs":
        g.add((BTO_ni[istat_hash], RDF.type, UMLS_Towns))
    elif place == "rural":
        g.add((BTO_ni[istat_hash], RDF.type, UMLS_Rural_area))
    else:
        print("[WARNING] some error here, this placename was not found among the mapping to town/city/rural area: " + istat_hash)


def try_special_cases(degurbaDict, placename):
    """There are certain cases that I was not able to deal with programmatically
    """
    if placename == "rivanazzano":
        return degurbaDict.get("rivanazzano terme", "")

    if placename == 'gambolo\'':
        return degurbaDict.get("gambolò", "")

    if placename == 'gattico':
        return degurbaDict.get("gattico-veruno", "")

    if placename == 'salice terme':
        return degurbaDict.get("godiasco salice terme", "")


    return ""


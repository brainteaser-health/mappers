"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
from rdflib import Namespace


# this file contains URLs that are used frequently in the ontology

# Brainteaser ontology schema
BTO_schema = Namespace("https://w3id.org/brainteaser/ontology/schema/")

# Brainteaser ontology named individual
BTO_ni = Namespace("https://w3id.org/brainteaser/ontology/named-individual/")

# Brainteaser ontology entities
BTO_resource = Namespace("https://w3id.org/brainteaser/ontology/resource/")

# NCIT ontology
PO = Namespace("http://purl.obolibrary.org/obo/")

# Ethnicity ontology
SMC = Namespace("http://purl.bioontology.org/ontology/SNOMEDCT/")

# UMLS ontology
UMLS = Namespace("https://uts.nlm.nih.gov/uts/umls/concept/")
UMLS_Cities = UMLS["C0008848"]
UMLS_Towns = UMLS["C0557750"]
UMLS_Rural_area = UMLS["C0178837"]
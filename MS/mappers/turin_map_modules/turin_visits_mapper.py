"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#hasher e uuid generator
import mmh3
import uuid
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
#import uri generator
from mappers.utility_scripts import id_generator as idg
import numpy as np

def visitsMapper(visits, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, filtering_type,
                 forbidden_list):

    out_of_range_counter = 0
    NaN_count = 0

    # count the number of Nan rows without considering Ids and date
    df = visits.iloc[:, 3:].replace("n.d.", np.NaN)

    for i, not_NaN_fields in df.count(axis="columns").items():
        if not_NaN_fields == 0:
            NaN_count += 1

    # drop rows with no information
    visits = visits.dropna(subset=["Piramidale","Cerebellare","Troncoencefalica","Sensitiva", "Sfinteriche", "Visiva", "Mentali",
                                    "Deambulazione", "Visiva", "Mentali", "Deambulazione", "Punteggio EDSS valutato dal clinico",
                                    "Peso (Kg)", "Altezza (m)", "IMC", "Stato occupazionale", "Stato fumatore",
                                    "Il/La paziente ha sviluppato una neoplasia (escludendo le NMSC) dall’ultima visita",
                                    "Il/La paziente ha sviluppato una neoplasia cutanea non melanoma (NMSC) dall’ultima visita",
                                    "Il/La paziente ha sviluppato un Herpes Zoster (Fuoco di San Antonio) dall’ultima visita",
                                    "Il/La paziente ha sviluppato un’infezione grave o associata all’immunosoppressione dall’ultima visita",
                                    "Il/La paziente ha sviluppato altre condizioni patologiche di particolare rilievo dall’ultima visita",
                                    "Trattamento corrente e storia dei trattamenti (Si/No DMT)"], how="all")


    for index, row in visits.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        # the total of the EDSS on each patient
        totalEDSS = 0

        # retrieve the patient URI
        patient = patientCodes[row["TUR_MS_CODE"]]

        # create the id for the entity visit
        visitId = idg.getURI("turin", "visit", str(index))

        # add the entity in the graph and register the type (patient visit)
        g.add((visitId, RDF.type, PO[ncit.PATIENT_VISIT]))

        # add the date to the visit
        if not pd.isna(row["Data visita"]):
            g.add((visitId, BTO_schema["startDate"], Literal(row["Data visita"].date(), datatype=XSD.date)))

        # create the link between the patient and the visit
        g.add((patient, BTO_schema["undergo"], visitId))

        if filtering_type == "strong":
            # Case 1: We accept only the rows with all edss values
            if not ((pd.isna(row["Piramidale"]) or row["Piramidale"] < 0 or row["Piramidale"] > 5)
                    or (pd.isna(row["Cerebellare"]) or row["Cerebellare"] < 0 or row["Cerebellare"] > 5)
                    or (pd.isna(row["Troncoencefalica"]) or row["Troncoencefalica"] < 0 or row["Troncoencefalica"] > 5)
                    or (pd.isna(row["Sensitiva"]) or row["Sensitiva"] < 0 or row["Sensitiva"] > 5)
                    or (pd.isna(row["Sfinteriche"]) or row["Sfinteriche"] < 0 or row["Sfinteriche"] > 5)
                    or (pd.isna(row["Visiva"]) or row["Visiva"] < 0 or row["Visiva"] > 5)
                    or (pd.isna(row["Mentali"]) or row["Mentali"] < 0 or row["Mentali"] > 5)
                    or (pd.isna(row["Deambulazione"]) or row["Deambulazione"] < 0 or row["Deambulazione"] > 10)
                    or (pd.isna(row["Punteggio EDSS valutato dal clinico"]) or row[
                        "Punteggio EDSS valutato dal clinico"] < 0 or row["Punteggio EDSS valutato dal clinico"] > 10)):

                # create the EDSS procedure
                edssId = idg.getURI()
                g.add((edssId, RDF.type, PO[ncit.EDSS]))

                # create the link between the visit and the EDSS
                g.add((visitId, BTO_schema["consists"], edssId))

                # add the date to the visit
                if not pd.isna(row["Data visita"]):
                    g.add((edssId, BTO_schema["startDate"], Literal(row["Data visita"].date(), datatype=XSD.date)))

                # add all the properties of edss
                if not pd.isna(row["Piramidale"]):
                    value = float(row["Piramidale"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["pyramidal"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - pyramidal = {row["Piramidale"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Cerebellare"]):
                    value = float(row["Cerebellare"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["cerebellar"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebellar = {row["Cerebellare"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Troncoencefalica"]):
                    value = float(row["Troncoencefalica"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["brainstem"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - brainstem = {row["Troncoencefalica"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Sensitiva"]):
                    value = float(row["Sensitiva"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["sensory"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(f'[WARNING] - EDSS-value out of range - sensory = {row["Sensitiva"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Sfinteriche"]):
                    value = float(row["Sfinteriche"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["bowel_and_bladder"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - bowel_and_bladder = {row["Sfinteriche"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Visiva"]):
                    value = float(row["Visiva"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["visual_function"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - visual_function = {row["Visiva"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Mentali"]):
                    value = float(row["Mentali"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["cerebral_functions"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebral_functions = {row["Mentali"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Deambulazione"]):
                    value = float(row["Deambulazione"])
                    if value >= 0 and value <= 10:
                        g.add((edssId, BTO_schema["ambulation"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - ambulation = {row["Deambulazione"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Punteggio EDSS valutato dal clinico"]):
                    value = float(row["Punteggio EDSS valutato dal clinico"])
                    if value >= 0 and value <= 10:
                        g.add((edssId, BTO_schema["totalEDSS"], Literal(value, datatype=XSD.float)))
                        # totalEDSS += value
                        pass
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - totalEDSS = {row["Punteggio EDSS valutato dal clinico"]} - index = {index}')
                        out_of_range_counter += 1

                # add the total EDSS to the graph
                g.add((edssId, BTO_schema["edssSum"], Literal(totalEDSS, datatype=XSD.float)))
            else:
                # Just count the "out of range" values (else used in "Case 1")
                if not pd.isna(row["Piramidale"]):
                    value = float(row["Piramidale"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - pyramidal = {row["Piramidale"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Cerebellare"]):
                    value = float(row["Cerebellare"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebellar = {row["Cerebellare"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Troncoencefalica"]):
                    value = float(row["Troncoencefalica"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - brainstem = {row["Troncoencefalica"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Sensitiva"]):
                    value = float(row["Sensitiva"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(f'[WARNING] - EDSS-value out of range - sensory = {row["Sensitiva"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Sfinteriche"]):
                    value = float(row["Sfinteriche"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - bowel_and_bladder = {row["Sfinteriche"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Visiva"]):
                    value = float(row["Visiva"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - visual_function = {row["Visiva"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Mentali"]):
                    value = float(row["Mentali"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebral_functions = {row["Mentali"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Deambulazione"]):
                    value = float(row["Deambulazione"])
                    if value >= 0 and value <= 10:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - ambulation = {row["Deambulazione"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Punteggio EDSS valutato dal clinico"]):
                    value = float(row["Punteggio EDSS valutato dal clinico"])
                    if value >= 0 and value <= 10:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - totalEDSS = {row["Punteggio EDSS valutato dal clinico"]} - index = {index}')
                        out_of_range_counter += 1

        elif filtering_type == "weak":
            # Case 2: We accept missing values
            if ((not pd.isna(row["Piramidale"]) and row["Piramidale"] >= 0 and row["Piramidale"] <= 5)
                    or (not pd.isna(row["Cerebellare"]) and row["Cerebellare"] >= 0 and row["Cerebellare"] <= 5)
                    or (not pd.isna(row["Troncoencefalica"]) and row["Troncoencefalica"] >= 0 and row[
                        "Troncoencefalica"] <= 5)
                    or (not pd.isna(row["Sensitiva"]) and row["Sensitiva"] >= 0 and row["Sensitiva"] <= 5)
                    or (not pd.isna(row["Sfinteriche"]) and row["Sfinteriche"] >= 0 and row["Sfinteriche"] <= 5)
                    or (not pd.isna(row["Visiva"]) and row["Visiva"] >= 0 and row["Visiva"] <= 5)
                    or (not pd.isna(row["Mentali"]) and row["Mentali"] >= 0 and row["Mentali"] <= 5)
                    or (not pd.isna(row["Deambulazione"]) and row["Deambulazione"] >= 0 and row["Deambulazione"] <= 10)
                    or (not pd.isna(row["Punteggio EDSS valutato dal clinico"]) and row[
                        "Punteggio EDSS valutato dal clinico"] >= 0 and row[
                            "Punteggio EDSS valutato dal clinico"] <= 10)):
                # create the EDSS procedure
                edssId = idg.getURI()
                g.add((edssId, RDF.type, PO[ncit.EDSS]))

                # create the link between the visit and the EDSS
                g.add((visitId, BTO_schema["consists"], edssId))

                # add the date to the visit
                if not pd.isna(row["Data visita"]):
                    g.add((edssId, BTO_schema["startDate"], Literal(row["Data visita"], datatype=XSD.date)))

                    total_is_nan = False  # boolean to keep track of the fact that we found at least one NaN alue among the EDSS

                # add all the properties of edss
                if not pd.isna(row["Piramidale"]):
                    value = float(row["Piramidale"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["pyramidal"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - pyramidal = {row["Piramidale"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["pyramidal"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["pyramidal"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Cerebellare"]):
                    value = float(row["Cerebellare"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["cerebellar"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebellar = {row["Cerebellare"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["cerebellar"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["cerebellar"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Troncoencefalica"]):
                    value = float(row["Troncoencefalica"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["brainstem"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - brainstem = {row["Troncoencefalica"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["brainstem"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["brainstem"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Sensitiva"]):
                    value = float(row["Sensitiva"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["sensory"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(f'[WARNING] - EDSS-value out of range - sensory = {row["Sensitiva"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["sensory"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["sensory"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Sfinteriche"]):
                    value = float(row["Sfinteriche"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["bowel_and_bladder"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - bowel_and_bladder = {row["Sfinteriche"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["bowel_and_bladder"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["bowel_and_bladder"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Visiva"]):
                    value = float(row["Visiva"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["visual_function"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - visual_function = {row["Visiva"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["visual_function"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["visual_function"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Mentali"]):
                    value = float(row["Mentali"])
                    if value >= 0 and value <= 5:
                        g.add((edssId, BTO_schema["cerebral_functions"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebral_functions = {row["Mentali"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["cerebral_functions"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["cerebral_functions"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["Deambulazione"]):
                    value = float(row["Deambulazione"])
                    if value >= 0 and value <= 10:
                        g.add((edssId, BTO_schema["ambulation"], Literal(value, datatype=XSD.float)))
                        totalEDSS += value
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - ambulation = {row["Deambulazione"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["ambulation"], Literal('NaN')))
                        total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["ambulation"], Literal('NaN')))
                    total_is_nan = True

                if not pd.isna(row["EDSS calcolato"]):
                    valid = True
                    try:
                        value = float(row["EDSS calcolato"])
                    except ValueError:
                        valid = False
                        print("[WARNING] EDSS calcolato invalid value " + row['EDSS calcolato'])
                    if valid:
                        if value >= 0 and value <= 10:
                            g.add((edssId, BTO_schema["computedEDSS"], Literal(value, datatype=XSD.float)))
                        else:
                            print(
                                f'[WARNING] - EDSS-value out of range - computed EDSS = {row["EDSS calcolato"]} - index = {index}')
                            g.add((edssId, BTO_schema["computedEDSS"], Literal('NaN')))
                            out_of_range_counter += 1
                            # total_is_nan = True
                else:
                    g.add((edssId, BTO_schema["computedEDSS"], Literal('NaN')))
                    # total_is_nan = True

                if not pd.isna(row["Punteggio EDSS valutato dal clinico"]):
                    value = float(row["Punteggio EDSS valutato dal clinico"])
                    if value >= 0 and value <= 10:
                        g.add((edssId, BTO_schema["EDSSasEvaluatedByClinician"], Literal(value, datatype=XSD.float)))
                        # totalEDSS += value
                        pass
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - totalEDSS = {row["Punteggio EDSS valutato dal clinico"]} - index = {index}')
                        out_of_range_counter += 1
                        g.add((edssId, BTO_schema["EDSSasEvaluatedByClinician"], Literal('NaN')))
                else:
                    g.add((edssId, BTO_schema["EDSSasEvaluatedByClinician"], Literal('NaN')))

                # add the total
                if total_is_nan :
                    g.add((edssId, BTO_schema["totalEDSS"], Literal('NaN')))
                else:
                    g.add((edssId, BTO_schema["totalEDSS"], Literal(totalEDSS, datatype=XSD.float)))


            else:
                # Just count the "out of range" values (else used in "Case 1")
                if not pd.isna(row["Piramidale"]):
                    value = float(row["Piramidale"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - pyramidal = {row["Piramidale"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Cerebellare"]):
                    value = float(row["Cerebellare"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebellar = {row["Cerebellare"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Troncoencefalica"]):
                    value = float(row["Troncoencefalica"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - brainstem = {row["Troncoencefalica"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Sensitiva"]):
                    value = float(row["Sensitiva"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(f'[WARNING] - EDSS-value out of range - sensory = {row["Sensitiva"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Sfinteriche"]):
                    value = float(row["Sfinteriche"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - bowel_and_bladder = {row["Sfinteriche"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Visiva"]):
                    value = float(row["Visiva"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - visual_function = {row["Visiva"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Mentali"]):
                    value = float(row["Mentali"])
                    if value >= 0 and value <= 5:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - cerebral_functions = {row["Mentali"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Deambulazione"]):
                    value = float(row["Deambulazione"])
                    if value >= 0 and value <= 10:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - ambulation = {row["Deambulazione"]} - index = {index}')
                        out_of_range_counter += 1
                if not pd.isna(row["Punteggio EDSS valutato dal clinico"]):
                    value = float(row["Punteggio EDSS valutato dal clinico"])
                    if value >= 0 and value <= 10:
                        # In range
                        None
                    else:
                        print(
                            f'[WARNING] - EDSS-value out of range - totalEDSS = {row["Punteggio EDSS valutato dal clinico"]} - index = {index}')
                        out_of_range_counter += 1

        #create the clinical assessment for register the weigth and the height
        if (not pd.isna(row["Peso (Kg)"])) or (not pd.isna(row["Altezza (m)"])):
            clinicalAssessmentId = idg.getURI()
            g.add((clinicalAssessmentId, RDF.type, PO[maxo.CLINICAL_ASSESSMENT]))
            # create the link between the visit and the clinical assessment
            g.add((visitId, BTO_schema["consists"], clinicalAssessmentId))
            #add the weigth and the height
            if not pd.isna(row["Peso (Kg)"]):
                g.add((clinicalAssessmentId, BTO_schema["Weigth"], Literal(float(row["Peso (Kg)"]), datatype=XSD.float)))
                # add the weigth and the height
            if not pd.isna(row["Altezza (m)"]):
                g.add((clinicalAssessmentId, BTO_schema["Height"], Literal(float(row["Altezza (m)"]), datatype=XSD.float)))

        #register the comorbidity observed:
        if not pd.isna(row["Il/La paziente ha sviluppato una neoplasia (escludendo le NMSC) dall’ultima visita"]) and str(row["Il/La paziente ha sviluppato una neoplasia (escludendo le NMSC) dall’ultima visita"]).lower() == "si":
            comorbidityId = idg.getURI()
            g.add((comorbidityId, RDF.type, PO[ncit.COMORBIDITY]))
            #create the link between the visit and the registered comorbidity
            g.add((visitId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))
            #create the link between the neoplasia and the comorbidity
            g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Neoplasia"]))
            g.add((comorbidityId, BTO_schema["startYear"], Literal(row["Data visita"], datatype=XSD.gYear)))
        if not pd.isna(row["Il/La paziente ha sviluppato una neoplasia cutanea non melanoma (NMSC) dall’ultima visita"]) and str(row["Il/La paziente ha sviluppato una neoplasia cutanea non melanoma (NMSC) dall’ultima visita"]).lower() == "si":
            comorbidityId = idg.getURI()
            g.add((comorbidityId, RDF.type, PO[ncit.COMORBIDITY]))
            #create the link between the visit and the registered comorbidity
            g.add((visitId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))
            #create the link between the Nonmelanoma skin cancer and the comorbidity
            g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["NMSC"]))
            g.add((comorbidityId, BTO_schema["startYear"], Literal(row["Data visita"].date().year, datatype=XSD.gYear)))
        if not pd.isna(row["Il/La paziente ha sviluppato un Herpes Zoster (Fuoco di San Antonio) dall’ultima visita"]) and str(row["Il/La paziente ha sviluppato un Herpes Zoster (Fuoco di San Antonio) dall’ultima visita"]).lower() == "si":
            comorbidityId = idg.getURI()
            g.add((comorbidityId, RDF.type, PO[ncit.COMORBIDITY]))
            #create the link between the visit and the registered comorbidity
            g.add((visitId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))
            #create the link between the Nonmelanoma skin cancer and the comorbidity
            g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Herpes_Zoster"]))
            g.add((comorbidityId, BTO_schema["startYear"], Literal(row["Data visita"].date().year, datatype=XSD.gYear)))
        if not pd.isna(row["Il/La paziente ha sviluppato un’infezione grave o associata all’immunosoppressione dall’ultima visita"]) and str(row["Il/La paziente ha sviluppato un’infezione grave o associata all’immunosoppressione dall’ultima visita"]).lower() == "si":
            comorbidityId = idg.getURI()
            g.add((comorbidityId, RDF.type, PO[ncit.COMORBIDITY]))
            #create the link between the visit and the registered comorbidity
            g.add((visitId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))
            #create the link between the Nonmelanoma skin cancer and the comorbidity
            g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Severe_Or_Immunosuppressive-related_Infection"]))
            g.add((comorbidityId, BTO_schema["startYear"], Literal(row["Data visita"].date().year, datatype=XSD.gYear)))

    print(f'\n\n[SYSTEM-QUALITY-REPORT] - [Visit] - Number of inconsistent data in EDSS values due to "out of range" exception : {out_of_range_counter}\n\n')
    print(f'[SYSTEM-QUALITY-REPORT] - [Visit] - Number of rows with no informations about the visit (except for "Id" / "Paziente ID" / "Data visita") : {NaN_count}\n\n')

    return g

def check_dates(onset_and_diagnosis, visits):
    """

    :param onset_and_diagnosis:
    :param visits:
    :return:
    """
    merged = onset_and_diagnosis.merge(visits, left_on="TUR_MS_CODE", right_on="TUR_MS_CODE", how="outer")
    list_of_misplaced_dates = merged[merged["Data visita"] < merged["Data esordio SM"]].index.to_list() # strange that a visit is performed before the onset (but not impossible, actually)
    # list_of_misplaced_dates = merged[merged["Data visita"] < merged["Data diagnosi"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Visits] Data visita sooner than data esordio SM: " + str(
        len(list_of_misplaced_dates)))
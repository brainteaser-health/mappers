"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
from mappers.namespaces.ontologies import UMLS_Cities, UMLS_Towns, UMLS_Rural_area

from mappers.utility_scripts import id_generator as idg
from mappers.utility_scripts import urban_degree as ud
'''
    - Given a tab patients as a dataframe, a rdf graph and all the namespace required this function will provide
     the mapping of all the entries in patients into a graph database according to the schema defined in the brainteaser ontology.
'''

#NB: for Turin, it appears that no information about the place of birth is available

def registryMapper(patients, onsetAndDiagnosis, g, BTO_schema, BTO_resource, BTO_ni, PO, file, projectPaths,
                   forbidden_list):

    #dictionary for recording the evaluated progression
    observedProgressions = {}
    #dictionary for recording the patient code (so then we don't have to compute it againg)
    patientCodes = {}

    #create the clinical trial entity (retrospective) referred to pavia
    PV_CLINICAL_TRIAL = "ClinicalTrial_MS_Turin_1"
    g.add((BTO_ni[PV_CLINICAL_TRIAL], RDF.type, PO[ncit.CLINICAL_TRIAL]))
    #link the trial with the clinic, in our ontology "https://www.mondino.it/" is a named individual which has type "Clinics and Hospital" ///TODO: import from the ontology (so don't re-define here)
    g.add((BTO_ni[PV_CLINICAL_TRIAL],BTO_schema["pertain"], URIRef("https://www.unito.it/")))

    # import the dictionary for urban degrees
    degurbaPath = str(Path().resolve())
    degurbaPath = degurbaPath + "/data/input/Classificazioni statistiche-e-dimensione-dei-comuni_31_12_2020.xls"
    urbanDegree = ud.UrbanDegree(absPath=degurbaPath)

    degurbaDict = ud.UrbanDegree(absPath=degurbaPath).computeDegree()
    urbanDegree.computeIstatCorrespondences()

    urbanDegree.populate_hash_dictionary(projectPaths.italian_cap_file_path)
    urbanDegree.populate_hash_dictionary(projectPaths.spanish_cap_file_path)
    urbanDegree.populate_hash_dictionary(projectPaths.portuguese_cap_file_path)

    # null_zip = 0
    # null_placename = 0
    # null_location = 0

    # iterate over the patients dataframe
    for index, row in patients.iterrows():

        if index in forbidden_list: # in case this patient cannot be added, pass to the next one
            continue

        # create patient node to add to the graph
        patient = idg.getURI("turin", "patient", str(index))
        patientCodes.update({index:patient})

        id_patient = index

        if not pd.isna(row["Decorso calcolato"]):

            dates = str(row["Decorso calcolato"])
            dates = dates.split(";")[:-1]
            patient_observedProgression = {}

            for event in dates:
                type_event, date_event = event.split()
                date_event = date_event[1:-1]
                date_event = pd.to_datetime(date_event)
                #update the dictionary
                patient_observedProgression.update({date_event:(type_event, "notRegistered")})

            observedProgressions.update({id_patient:patient_observedProgression})

        # add triples using store's add() method.
        # add a patient
        g.add((patient, RDF.type, PO[ncit.PATIENT]))
        g.add((patient, BTO_schema["hasDisease"], BTO_ni["Multiple_Sclerosis"]))

        # add birthday to patient (if not empty)
        if not (pd.isna(row['Anno Nascita'])):
            g.add((patient, BTO_schema['yearOfBirth'], Literal(row['Anno Nascita'].year, datatype=XSD.gYear)))

        # add date of death to patient (if not empty)
        if not (pd.isna(row['Data di morte'])):
            g.add((patient, BTO_schema['dateOfDeath'], Literal(row['Data di morte'].date(), datatype=XSD.date)))

        # add gender to patient (if not empty)
        if not (pd.isna(row['Sesso'])):
            if row["Sesso"] == "f":
                g.add((patient, BTO_schema['sex'], Literal("female", lang="en")))
            if row["Sesso"] == "m":
                g.add((patient, BTO_schema['sex'], Literal("male", lang="en")))

        #add MS in pediatric age
        if not pd.isna(row["SM in età pedriatrica"]):
            if row["SM in età pedriatrica"] == "Si":
                g.add((patient, BTO_schema["MSInPaediatricAge"], Literal("True", datatype=XSD.boolean)))
            elif row["SM in età pedriatrica"].lower().strip() == "no":
                g.add((patient, BTO_schema["MSInPaediatricAge"], Literal("False", datatype=XSD.boolean)))

        # add ethnicity to patient (if not empty)
        if not (pd.isna(row['Etnia'])):

            # African
            if (row['Etnia'] == 'Africana'):
                # add ethnicity to patient
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Black_African"]))

            # Hispanic
            elif (row['Etnia'] == 'Ispanica'):
                # add ethnicity to patient
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Hispanic"]))

            # Caucasian -> White
            elif (row['Etnia'] == 'Caucasica'):
                # add ethnicity to patient
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Caucasian"]))



        #Define a new clinical trial participation
        clinicalTrialParticipationId = idg.getURI()
        dateFirstVisit = onsetAndDiagnosis.loc[onsetAndDiagnosis["TUR_MS_CODE"] == index ]["Data prima visita nel centro"].values
        g.add((clinicalTrialParticipationId, RDF.type, BTO_schema["Clinical_Trial_Partecipation"]))
        #add the patient to that clinical trial participation
        g.add((patient, BTO_schema["enrolledIn"], clinicalTrialParticipationId))
        if len(dateFirstVisit) > 0:
            if not pd.isnull(dateFirstVisit[0]):
                #add the start date
                g.add((clinicalTrialParticipationId, BTO_schema["startDate"], Literal(pd.to_datetime(dateFirstVisit[0]).date(), datatype=XSD.date)))
        #link the participation entity to the clinical trial
        g.add((clinicalTrialParticipationId, BTO_schema["participate"], BTO_ni[PV_CLINICAL_TRIAL]))

        # Observed progression mapping

        valid_MS_type = ("CIS" , "PP" , "PR" , "RIS" , "RR" , "SP")

        if not pd.isna(row["Decorso calcolato"]):
            for key, value in patient_observedProgression.items():

                #print(key, value)
                typeMS = value[0]

                if typeMS.strip().upper() in valid_MS_type:
                    # Observed progression
                    vistId = idg.getURI()
                    g.add((vistId, RDF.type, PO[ncit.PATIENT_VISIT]))
                    g.add((patient, BTO_schema["undergo"], vistId))

                    clinicalAssessmentId = idg.getURI()
                    g.add((clinicalAssessmentId, RDF.type, PO[maxo.CLINICAL_ASSESSMENT]))
                    g.add((clinicalAssessmentId, BTO_schema["startDate"], Literal(key.date(), datatype=XSD.date)))# the key is the date of the visit
                    g.add((vistId, BTO_schema["consists"], clinicalAssessmentId))
                    g.add((clinicalAssessmentId, BTO_schema["MS_type"], Literal(typeMS, datatype=XSD.string)))
                else:
                    print(f'[WARNING] - Unexpected value as type of MS observed!\tindex : {index}\tvalue : {typeMS}')

        # Notes mapping
        if not pd.isna(row["Note"]):
            g.add((patient, BTO_schema["notes"], Literal("[Anagrafica] - "+str(row["Note"]), datatype=XSD.string)))


    # print("****** PATIENT CODES: ******\n")
    #
    # for i , val in patientCodes.items():
    #     print("id : ",i,"\tURI : ",val)

    print("\n")

    # finally, we check that the dates are correctly spaced in time
    check_dates(patients, onsetAndDiagnosis)

    return g, patientCodes


def check_dates(patients, onsetAndDiagnosis):
    # merge the two tables on patient id
    merged = patients.merge(onsetAndDiagnosis, left_on="TUR_MS_CODE", right_on="TUR_MS_CODE", how="outer")
    list_of_misplaced_dates = merged[merged["Data esordio SM"] > merged["Data diagnosi"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Personal Data] data esordio later than data diagnosi: " + str(len(list_of_misplaced_dates)))
    list_of_misplaced_dates = merged[merged["Data diagnosi"] > merged["Data prima visita nel centro"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Personal Data] data diagnosi later than data prima visita nel centro: " + str(len(list_of_misplaced_dates)))
    list_of_misplaced_dates = merged[merged["Data prima visita nel centro"] > merged["Data di morte"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Personal Data] data prima visita nel centro later than data morte: " + str(len(list_of_misplaced_dates)))

"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
import re
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
#import uri generator
from mappers.utility_scripts import id_generator as idg
#import disease mapping
from mappers.utility_scripts import loadDisease

def diseaseMapper(clinicalEvents, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                  forbidden_list):

    #load the dictionary for parsing the diseases
    path = str(Path().resolve())
    path = path + "/data/output/diseaseToNCIT.csv"
    dictNCIT = loadDisease.load(path)

    # errors counter
    missing_endDate = 0

    missing_eventDate = 0

    unstructured_info_in_notes = 0

    discarded_rows = 0

    mapped_events = 0

    covid_test = 0

    for index, row in clinicalEvents.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        patientId = patientCodes[row["TUR_MS_CODE"]]

        if not pd.isna(row["LLT"]):

            disease = row["LLT"].split("]")[-1].strip()

            if disease in dictNCIT and re.match("Si", str(row["Correlato al trattamento"])) == None:

                mapped_events += 1

                # create the event and link the patient to it
                protocolEventId = idg.getURI()
                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                g.add((patientId, BTO_schema["undergo"], protocolEventId))
                if not pd.isna(row["Data evento"]):
                    g.add((protocolEventId, BTO_schema["startDate"], Literal(pd.to_datetime(row["Data evento"]).date(), datatype=XSD.date)))
                else:
                    print(f'[WARNING] - Misssing date [Data evento] for clinical event\tindex : {index}')
                    missing_eventDate += 1

                # create the comorbidity and link the event to it
                comorbidityId = idg.getURI("pavia", "comorbidity", index)
                g.add((comorbidityId, RDF.type, PO[ncit.COMORBIDITY]))
                g.add((protocolEventId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))
                if not pd.isna(row["Data evento"]):
                    g.add((comorbidityId, BTO_schema["startYear"], Literal(pd.to_datetime(row["Data evento"]).date().year,
                                                                           datatype=XSD.gYear)))
                if not pd.isna(row["Data esito"]):
                    g.add((comorbidityId, BTO_schema["endYear"], Literal(pd.to_datetime(row["Data esito"]).date().year,
                                                                         datatype=XSD.gYear)))
                else:
                    print(f'[WARNING] - Misssing date [Data esito] for clinical event\tindex : {index}')
                    missing_endDate += 1
                if not pd.isna((row["Gravità"])):
                    if row["Gravità"] == "Moderato":
                        g.add((comorbidityId, BTO_schema["severity"], Literal("moderate", lang="en")))
                    if row["Gravità"] == "Lieve":
                        g.add((comorbidityId, BTO_schema["severity"], Literal("slight", lang="en")))
                    if row["Gravità"] == "Grave":
                        g.add((comorbidityId, BTO_schema["severity"], Literal("serious", lang="en")))
                    if row["Gravità"] == "Rischio della vita":
                        g.add((comorbidityId, BTO_schema["severity"], Literal("life risk", lang="en")))

                #link the comorbidity to the event
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni[dictNCIT[disease]]))

                # Notes mapping
                if not pd.isna(row["Note"]):
                    g.add((patientId, BTO_schema["notes"], Literal("[Eventi clinici] - " + str(row["Note"]), datatype=XSD.string)))
                    unstructured_info_in_notes += 1
            else:
                discarded_rows += 1

                # Notes mapping
                if not pd.isna(row["Note"]):
                    g.add((patientId, BTO_schema["notes"],
                           Literal("[Eventi clinici] - " + str(row["Note"]), datatype=XSD.string)))
                    unstructured_info_in_notes += 1

                if re.search("covid", str(row["LLT"]).lower()) or re.search("tampone", str(row["LLT"]).lower()):
                    covid_test += 1
                    print(f'[WARNING] - [Clinical Event] - Row discarded, the event refers to a covid-19 test\tevent id : {index}')
                elif re.match("Si", str(row["Correlato al trattamento"])) == None:
                    print(
                        f'[WARNING] - [Clinical Event] - Row discarded because it\'s not a comorbidity\tevent id : {index}\tevent label : {row["LLT"]}')
                else:
                    print(f'[WARNING] - [Clinical Event] - Row discarded\tevent id : {index}\tevent label : {row["LLT"]}')
        else:
            discarded_rows += 1

            # Notes mapping
            if not pd.isna(row["Note"]):
                g.add((patientId, BTO_schema["notes"],
                       Literal("[Eventi clinici] - " + str(row["Note"]), datatype=XSD.string)))
                unstructured_info_in_notes += 1

            print(f'[WARNING] - [Clinical Event] - Row discarded\tevent id : {index}')

    print(
        f'\n\n[SYSTEM-QUALITY-REPORT] - [Clinical Event] - Number of events (among the ones correctly mapped according to the ontology) without date : {missing_eventDate}\n\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Clinical Event] - Number of events (among the ones correctly mapped according to the ontology) without end date : {missing_endDate}\n\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Clinical Event] - Number of events correctly mapped  : {mapped_events}\n\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Clinical Event] - Number of discarded rows  : {discarded_rows}'
        f'\n\t[SYSTEM-QUALITY-REPORT] - [Clinical Event] - Of which related to covid test : {covid_test}\n\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Clinical Event] - Number of rows with unstructured information presents in natural language in the field "Note": {unstructured_info_in_notes}\n\n')

    return g
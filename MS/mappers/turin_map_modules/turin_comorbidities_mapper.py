"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
import re
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
#import uri generator
from mappers.utility_scripts import id_generator as idg
#import disease mapping
from mappers.utility_scripts import loadDisease

def comorbidityMapper(comorbidities_pavia, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                      forbidden_list):
    path = str(Path().resolve())
    path = path + "/data/output/diseaseToNCIT.csv"
    dictNCIT = loadDisease.load(path)

    # errors counters
    unmapped_counter = 0
    tot = 0

    for index, row in comorbidities_pavia.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        if not pd.isna(row["ICD9"]):
            tot += 1
            disease = row["ICD9"].split("]")[-1].strip()
            if disease in dictNCIT:
                patientId = patientCodes[row["TUR_MS_CODE"]]
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                g.add((patientId, BTO_schema["undergo"], protocolEventId))
                comorbidityId = idg.getURI("pavia", "comorbidity", index)
                g.add((comorbidityId, RDF.type, PO[ncit.COMORBIDITY]))
                g.add((protocolEventId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))
                if not pd.isna(row["Data inizio"]):
                    g.add((comorbidityId, BTO_schema["startYear"], Literal(row["Data inizio"].date().year, datatype=XSD.gYear)))
                    g.add((protocolEventId, BTO_schema["startYear"], Literal(row["Data inizio"].date().year, datatype=XSD.gYear)))
                else:
                    print(f'[WARNING] - Misssing date [Data inizio] for clinical event\tindex : {index}')
                if not pd.isna(row["Data fine"]):
                    g.add((comorbidityId, BTO_schema["endYear"], Literal(row["Data fine"].date().year, datatype=XSD.gYear)))
                else:
                    print(f'[WARNING] - Misssing date [Data fine] for clinical event\tindex : {index}')
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni[dictNCIT[disease]]))

                # Notes mapping
                if not pd.isna(row["Note"]):
                    g.add((patientId, BTO_schema["notes"],
                           Literal("[Co-Morbilità] - " + str(row["Note"]), datatype=XSD.string)))
            else:
                unmapped_counter += 1

    print(f'\n\n[MAPPING-SYSTEM-WARNING] - Total number of comorbidities in the comorbidities table : {tot} - Number of unmapped comorbidities: {unmapped_counter}\n\n')

    return  g
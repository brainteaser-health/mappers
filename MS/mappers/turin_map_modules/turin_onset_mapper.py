"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import math

import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#hasher e uuid generator
import mmh3
import uuid
#import the ncit codes
from mappers.codes import ncit_codes as ncit
#import uri generator
from mappers.pavia_map_modules.pavia_onset_mapper import translate_criterion_from_italian_to_english
from mappers.utility_scripts import id_generator as idg


def onset_and_diagnosisMapper(onsetAndDiagnosis, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                              forbidden_list, patients_turin):

    for index, row in onsetAndDiagnosis.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        #create the id for the entity onset
        onsetId = idg.getURI("turin", "onset", str(index))

        # create and add the new entity onset
        g.add((onsetId, RDF.type, PO[ncit.ONSET]))
        # add the date
        if not pd.isna(row["Data esordio SM"]):
            g.add((onsetId, BTO_schema["startDate"], Literal(row["Data esordio SM"].date(), datatype=XSD.date)))

        #retrieve the patient node to add to the graph
        patient = patientCodes[row["TUR_MS_CODE"]]

        #add the link between patient and onset
        g.add((patient, BTO_schema["undergo"], onsetId))

        # add age at onset
        patient_id = row["TUR_MS_CODE"]  # take the id of the patient
        # use this id to find the birth_year of the patient
        patient_birth_year = patients_turin[patients_turin["TUR_MS_CODE"] == patient_id]["Anno Nascita"]
        onset_date = row["Data esordio SM"].date()  # take the onset date
        if len(patient_birth_year) == 1 and not pd.isna(onset_date):
            patient_birth_year = pd.to_datetime(patient_birth_year.values[0]).year
            if pd.isna(
                    patient_birth_year):  # this in theory never happens if we are here, but you know how things go...
                break
            onset_year = onset_date.year
            age_onset = onset_year - patient_birth_year
            g.add((onsetId, BTO_schema['age_onset'], Literal(age_onset, datatype=XSD.int)))

        if not pd.isna(row["Sintomi troncoencefalici"]):
            if row["Sintomi troncoencefalici"].lower() == "si":
                g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Brainstem_Symptom"]))
                g.add((onsetId, BTO_schema["hasBrainstemSymptom"], Literal("true", datatype=XSD.boolean)))
            elif row["Sintomi troncoencefalici"].lower() == "no":
                g.add((onsetId, BTO_schema["hasBrainstemSymptom"], Literal("false", datatype=XSD.boolean)))
        if not pd.isna(row["Sintomi vie ottiche"]):
            if row["Sintomi vie ottiche"].lower() == "si":
                g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Eye_Symptom"]))
                g.add((onsetId, BTO_schema["hasEyeSymptom"], Literal("true", datatype=XSD.boolean)))
            elif row["Sintomi vie ottiche"].lower() == "no":
                g.add((onsetId, BTO_schema["hasEyeSymptom"], Literal("false", datatype=XSD.boolean)))
        if not pd.isna(row["Sintomi midollo spinale"]):
            if row["Sintomi midollo spinale"].lower() == "si":
                g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Spinal_Cord_Symptom"]))
                g.add((onsetId, BTO_schema["hasSpinalCordSymptom"], Literal("true", datatype=XSD.boolean)))
            elif row["Sintomi midollo spinale"].lower() == "no":
                g.add((onsetId, BTO_schema["hasSpinalCordSymptom"], Literal("false", datatype=XSD.boolean)))
        if not pd.isna(row["Sintomi sopratentoriali"]):
            if row["Sintomi sopratentoriali"].lower() == "si":
                g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Supratentorial_Symptom"]))
                g.add((onsetId, BTO_schema["hasSupratentorialSymptom"], Literal("true", datatype=XSD.boolean)))
            elif row["Sintomi sopratentoriali"].lower() == "si":
                g.add((onsetId, BTO_schema["hasSupratentorialSymptom"], Literal("false", datatype=XSD.boolean)))

        # before we leave here, we check for "Other Symptons/Altri sintomi", to see if there is something we could add
        if not pd.isna(row["Altri sintomi"]) and row[
            "Altri sintomi"].lower() == "si":  # if the clinician said that there are other symptoms
            if not pd.isna(row["Specificare"]):  # go and read these symptoms
                g.add((onsetId, BTO_schema["otherSymptoms"],
                       Literal(row["Specificare"])))  # and add it to the onset as a string

        # create the id for the entity diagnosis
        diagnosisId = idg.getURI("turin", "diagnosis", str(index))

        # create and add the new entity diagnosis
        g.add((diagnosisId, RDF.type, PO[ncit.DIAGNOSIS]))
        # add the date
        if not pd.isna(row["Data diagnosi"]):
            g.add((diagnosisId, BTO_schema["startDate"], Literal(row["Data diagnosi"].date(), datatype=XSD.date)))

        # DIAGNOSTIC DELAY
        if not pd.isna(row["Data esordio SM"]) and not pd.isna(row["Data diagnosi"]):
            diagnosis_date = row["Data diagnosi"].date()
            onset_date = row["Data esordio SM"].date()
            diagnostic_delay = diagnosis_date - onset_date  # difference in days
            diagnostic_delay = math.floor(diagnostic_delay.days / 30) # convert to months (more or less)
            g.add((onsetId, BTO_schema["diagnosticDelay"], Literal(diagnostic_delay, datatype=XSD.int)))

        if not pd.isna(row["Criterio conferma diagnosi"]):
            criterion_in_english = translate_criterion_from_italian_to_english(row["Criterio conferma diagnosi"])
            g.add((diagnosisId, BTO_schema["criteria"], Literal(criterion_in_english) ))

        #create the link between the patient and the diagnosis
        g.add((patient, BTO_schema["undergo"], diagnosisId))


    return  g
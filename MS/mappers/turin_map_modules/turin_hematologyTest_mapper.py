"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
#import the uri generator
from mappers.utility_scripts import  id_generator as idg


class HematologyTest():

    # missing values counter
    UM_missing_counter = 0

    def __init__(self):
        self.UM_missing_counter = 0

    def testMap_positiveNegative(self, value, labelValue, mUnit, labelMUnit, outcome, ontoName, ontoNameOutcome, index,
                                 g,
                                 hematologyTestId, BTO_schema):
        to_return = 0  # signals if we were able to add a triple
        if not ontoName == None:
            if not pd.isna(value) and not pd.isna(mUnit):
                if mUnit == "U/L":
                    print(
                        f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
                elif mUnit == "10e9/L":
                    print(
                        f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
                elif mUnit == "%":
                    g.add((hematologyTestId, BTO_schema[ontoName],
                           Literal(float(value), datatype=XSD.float)))
                    to_return = 1
            elif not pd.isna(value) and pd.isna(mUnit):
                print(
                    f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
                self.UM_missing_counter += 1

        if not pd.isna(outcome):
            if str(outcome).strip().lower() == "Pos.".lower():
                g.add((hematologyTestId, BTO_schema[ontoNameOutcome],
                       Literal("Positive", lang="en")))
                to_return = 1
            elif str(outcome).strip().lower() == "Neg.".lower():
                g.add((hematologyTestId, BTO_schema[ontoNameOutcome],
                       Literal("Negative", lang="en")))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_Percentage(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                           hematologyTestId, BTO_schema):
        to_return = 0  # signals if we were able to add a triple
        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "%":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_pmol(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                     hematologyTestId, BTO_schema):
        to_return = 0

        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "g/dl":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "pmol/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_10e9L(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                      hematologyTestId, BTO_schema):
        to_return = 0

        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e12/L":
                conversion_factor = 1000
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value) * conversion_factor, datatype=XSD.float)))
                to_return = 1
            elif mUnit == "10e9/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_10e12L(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                       hematologyTestId, BTO_schema):
        to_return = 0

        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                conversion_factor = 0.001
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value) * conversion_factor, datatype=XSD.float)))
                to_return = 1
            elif mUnit == "10e12/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_gdl(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                    hematologyTestId, BTO_schema):
        to_return = 0

        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "g/dl":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_mmolL(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                      hematologyTestId, BTO_schema):
        to_return = 0
        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert U/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "g/dl":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "mmol/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_UL(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                   hematologyTestId, BTO_schema):
        to_return = 0

        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "mmol/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert mmol/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "g/dl":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "U/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_umolL(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g,
                      hematologyTestId, BTO_schema):
        to_return = 0

        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "mmol/L":
                conversionFactor = 0.001
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value) * conversionFactor, datatype=XSD.float)))
                to_return = 1
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "g/dl":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "umol/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
                to_return = 1
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
                to_return = 1
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
                to_return = 1
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        return to_return

    def testMap_umolL(self, value, labelValue, mUnit, labelMUnit, normal, ontoName, ontoNameNormal, index, g, hematologyTestId, BTO_schema):
        if not pd.isna(value) and not pd.isna(mUnit):
            if mUnit == "mmol/L":
                conversionFactor = 0.001
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value) * conversionFactor, datatype=XSD.float)))
            elif mUnit == "%":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert percentage value in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "10e9/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert 10e9/L in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "g/dl":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "U/L":
                print(
                    f'[MAPPING-SYSTEM-WARNING]-Convert g/dl in table "Ematochimici_di_routine" for "{labelMUnit}" - id: {index}')
            elif mUnit == "umol/L":
                g.add((hematologyTestId, BTO_schema[ontoName],
                       Literal(float(value), datatype=XSD.float)))
        elif not pd.isna(value) and pd.isna(mUnit):
            print(
                f'[WARNING]-Measurement unit missing in table "Ematochimici_di_routine" for "{labelValue}" - id: {index}')
            self.UM_missing_counter += 1

        if not pd.isna(normal):
            if str(normal).strip().lower() == "si":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(True, datatype=XSD.boolean)))
            elif str(normal).strip().lower() == "no":
                g.add((hematologyTestId, BTO_schema[ontoNameNormal],
                       Literal(False, datatype=XSD.boolean)))
            else:
                print(
                    f'[WARNING]-Unexpected value in table "Ematochimici_di_routine" - id: {index}\tRelated to "{labelValue} (Normale)" field')

        if not pd.isna(value) or not pd.isna(normal):
            return 1
        else:
            return 0


    def hematologyTestMapper(self, routineHematologyTest, specificHematologyTest, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                             forbidden_list):

        empty_rows = 0 # number of tuples without (useful for us) data for the blood tests

        # routine hematology test data mapper
        for index, row in routineHematologyTest.iterrows():

            if row["TUR_MS_CODE"] in forbidden_list:
                continue

            data_found = 0 # counters the data found for the hematology test of current tuple

            # retrieve the patient URI
            patientId = patientCodes[row["TUR_MS_CODE"]]

            # create the protocol event
            protocolEventId = idg.getURI("turin", "routineHematology", str(index))
            hematologyTestId = idg.getURI()

            data_found += self.testMap_10e12L(row["Leucociti"],"Leucociti",row["Leucociti (U.M.)"],"Leucociti (U.M.)",row["Leucociti (Normale)"],"bloodLeukocytes","bloodLeukocytesNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_10e12L(row["Eritrociti"],"Eritrociti",row["Eritrociti (U.M.)"],"Eritrociti (U.M.)",row["Eritrociti (Normale)"],"bloodErythrocytes","bloodErythrocytesNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_Percentage(row["Linfociti"],"Linfociti",row["Linfociti (U.M.)"],"Linfociti (U.M.)",row["Linfociti (Normale)"],"bloodLymphocytes","bloodLymphocytesNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_Percentage(row["Neutrofili"],"Neutrofili",row["Neutrofili (U.M.)"],"Neutrofili (U.M.)",row["Neutrofili (Normale)"],"bloodNeutrophil","bloodNeutrophilNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_Percentage(row["Monociti"],"Monociti",row["Monociti (U.M.)"],"Monociti (U.M.)",row["Monociti (Normale)"],"bloodMonocytes","bloodMonocytesNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_Percentage(row["Eosinofili"],"Eosinofili",row["Eosinofili (U.M.)"],"Eosinofili (U.M.)",row["Eosinofili (Normale)"],"bloodEosinophil","bloodEosinophilNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_Percentage(row["Basofili"],"Basofili",row["Basofili (U.M.)"],"Basofili (U.M.)",row["Basofili (Normale)"],"bloodBasophil","bloodBasophilNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_10e9L(row["Piastrine"],"Piastrine",row["Piastrine (U.M.)"],"Piastrine (U.M.)",row["Piastrine (Normale)"],"bloodPlatelet","bloodPlateletNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_gdl(row["Emoglobina"],"Emoglobina",row["Emoglobina (U.M.)"],"Emoglobina (U.M.)",row["Emoglobina (Normale)"],"bloodHemoglobin","bloodHemoglobinNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_gdl(row["Proteine totali"],"Proteine totali",row["Proteine totali (U.M.)"],"Proteine totali (U.M.)",row["Proteine totali (Normale)"],"bloodTotalProtein","bloodTotalProteinNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_gdl(row["Albumina"],"Albumina",row["Albumina (U.M.)"],"Albumina (U.M.)",row["Albumina (Normale)"],"Albumin_level","bloodAlbuminNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_mmolL(row["Calcemia"],"Calcemia",row["Calcemia (U.M.)"],"Calcemia (U.M.)",row["Calcemia (Normale)"],"bloodCalcium","bloodCalciumNormal",index,g,hematologyTestId,BTO_schema)

            data_found += self.testMap_umolL(row["Urea"], "Urea", row["Urea (U.M.)"], "Urea (U.M.)", row["Urea (Normale)"],
                          "bloodUrea", "bloodUreaNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_mmolL(row["Uricemia"], "Uricemia", row["Uricemia (U.M.)"], "Uricemia (U.M.)", row["Uricemia (Normale)"],
                          "bloodUricaemia", "bloodUricaemiaNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_mmolL(row["Creatinina"], "Creatinina", row["Creatinina (U.M.)"], "Creatinina (U.M.)", row["Creatinina (Normale)"],
                          "bloodCreatinine", "bloodCreatinineNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_UL(row["SGOT/AST"], "SGOT/AST", row["SGOT/AST (U.M.)"], "SGOT/AST (U.M.)", row["SGOT/AST (Normale)"],
                          "bloodSGOT_AST", "bloodSGOT_AST_Normal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_UL(row["SGTP/ALT"], "SGTP/ALT", row["SGTP/ALT (U.M.)"], "SGTP/ALT (U.M.)", row["SGTP/ALT (Normale)"],
                          "bloodSGPT_ALT", "bloodSGPT_ALT_Normal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_UL(row["Gamma-GT"], "Gamma-GT", row["Gamma-GT (U.M.)"], "Gamma-GT (U.M.)", row["Gamma-GT (Normale)"],
                          "bloodGGT", "bloodGGTNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_mmolL(row["Bilirubina"], "Bilirubina", row["Bilirubina (U.M.)"], "Bilirubina (U.M.)",
                          row["Bilirubina (Normale)"],
                          "bloodBilirubin", "bloodBilirubinNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_UL(row["Fosfatasi alcalina"], "Fosfatasi alcalina", row["Fosfatasi alcalina (U.M.)"], "Fosfatasi alcalina (U.M.)",
                          row["Fosfatasi alcalina (Normale)"],
                          "bloodAlkalinePhosphatase", "bloodAlkalinePhosphataseNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_UL(row["Amilasi"], "Amilasi", row["Amilasi (U.M.)"],
                       "Amilasi (U.M.)",
                       row["Amilasi (Normale)"],
                       "bloodAmylase", "bloodAmylaseNormal", index, g, hematologyTestId, BTO_schema)

            data_found += self.testMap_UL(row["Lipasi"], "Lipasi", row["Lipasi (U.M.)"],
                       "Lipasi (U.M.)",
                       row["Lipasi (Normale)"],
                       "bloodLipase", "bloodLipaseNormal", index, g, hematologyTestId, BTO_schema)

            if data_found > 0:
                # we have some data. Add it to the graph
                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))
                g.add((hematologyTestId, RDF.type, PO[ncit.HEMATOLOGY_TEST]))
                if not pd.isna(row["Data del prelievo"]):
                    try:
                        g.add((protocolEventId, BTO_schema["startDate"],
                               Literal(row["Data del prelievo"].date(), datatype=XSD.date)))
                        g.add((hematologyTestId, BTO_schema["startDate"],
                               Literal(row["Data del prelievo"].date(), datatype=XSD.date)))
                    except:
                        print(f'[WARNING]-An error occurs - id: {index}\tRelated to hematology test date!\n')

                g.add((protocolEventId, BTO_schema["consists"], hematologyTestId))
            else:
                print("[WARNING] No blood test data for patient " + str(row["TUR_MS_CODE"]))
                empty_rows += 1

        print(
            f'\n\n[SYSTEM-QUALITY-REPORT] - [Blood Test] - Number of useless tuples for routine blood tests: '
            f'{empty_rows}\n\n')

        empty_rows = 0

        # specific hematology test data mapper
        for index, row in specificHematologyTest.iterrows():

            if row["TUR_MS_CODE"] in forbidden_list:
                continue


            data_found = 0

            # retrieve the patient URI
            patientId = patientCodes[row["TUR_MS_CODE"]]

            # create the protocol event
            protocolEventId = idg.getURI("turin", "specificHematology", str(index))

            hematologyTestId = idg.getURI()



            data_found +=  self.testMap_10e9L(row["Ab anti-microsomiali"], "Ab anti-microsomiali", row["Ab anti-microsomiali (U.M.)"],
                            "Ab anti-microsomiali (U.M.)",
                            row["Ab anti-microsomiali (Normale)"],
                            "bloodAbTMS", "bloodAbTMSNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Ab anti-tireoglobulina"], "Ab anti-tireoglobulina", row["Ab anti-tireoglobulina (U.M.)"],
                               "Ab anti-tireoglobulina (U.M.)",
                               row["Ab anti-tireoglobulina (Normale)"],
                               "anti-Tg", "anti-TgNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_pmol(row["FT3"], "FT3", row["FT3 (U.M.)"],
                               "FT3 (U.M.)",
                               row["FT3 (Normale)"],
                               "bloodFT3", "bloodFT3Normal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_pmol(row["FT4"], "FT4", row["FT4 (U.M.)"],
                               "FT4 (U.M.)",
                               row["FT4 (Normale)"],
                               "bloodFT4", "bloodFT4Normal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["TSH"], "TSH", row["TSH (U.M.)"],
                               "TSH (U.M.)",
                               row["TSH (Normale)"],
                               "bloodTSH", "bloodTSHNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["ANA"], "ANA", row["ANA (U.M.)"],
                            "ANA (U.M.)",
                            row["ANA (Normale)"],
                            "bloodANA", "bloodANANormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-mitocondriale"], "Anti-mitocondriale", row["Anti-mitocondriale (U.M.)"],
                            "Anti-mitocondriale (U.M.)",
                            row["Anti-mitocondriale (Normale)"],
                            "bloodAMA", "bloodAMANormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-mucosa gastrica"], "Anti-mucosa gastrica", row["Anti-mucosa gastrica (U.M.)"],
                            "Anti-mucosa gastrica (U.M.)",
                            row["Anti-mucosa gastrica (Normale)"],
                            "bloodAPCA", "bloodAPCANormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-smoth muscale"], "Anti-smoth muscale", row["Anti-smoth muscale (U.M.)"],
                            "Anti-smoth muscale (U.M.)",
                            row["Anti-smoth muscale (Normale)"],
                            "bloodASMA", "bloodASMANormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-Ro/SSA"], "Anti-Ro/SSA", row["Anti-Ro/SSA (U.M.)"],
                            "Anti-Ro/SSA (U.M.)",
                            row["Anti-Ro/SSA (Normale)"],
                            "bloodAnti-SSA_Ro", "bloodAnti-SSA_RoNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["La"], "La", row["La (U.M.)"],
                            "La (U.M.)",
                            row["La (Normale)"],
                            "bloodLa", "bloodLaNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Sm"], "Sm", row["Sm (U.M.)"],
                            "Sm (U.M.)",
                            row["Sm (Normale)"],
                            "bloodSm", "bloodSmNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["RNP"], "RNP", row["RNP (U.M.)"],
                            "RNP (U.M.)",
                            row["RNP (Normale)"],
                            "bloodRNP", "bloodRNPNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Scl-70"], "Scl-70", row["Scl-70 (U.M.)"],
                            "Scl-70 (U.M.)",
                            row["Scl-70 (Normale)"],
                            "bloodScl-70", "bloodScl-70Normal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Jo1"], "Jo1", row["Jo1 (U.M.)"],
                            "Jo1 (U.M.)",
                            row["Jo1 (Normale)"],
                            "bloodJo1", "bloodJo1Normal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-DNA"], "Anti-DNA", row["Anti-DNA (U.M.)"],
                            "Anti-DNA (U.M.)",
                            row["Anti-DNA (Normale)"],
                            "bloodAnti-DNA", "bloodAnti-DNANormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["ANCA"], "ANCA", row["ANCA (U.M.)"],
                            "ANCA (U.M.)",
                            row["ANCA (Normale)"],
                            "bloodANCA", "bloodANCANormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-LKM"], "Anti-LKM", row["Anti-LKM (U.M.)"],
                            "Anti-LKM (U.M.)",
                            row["Anti-LKM (Normale)"],
                            "bloodAnti-LKM", "bloodAnti-LKMNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-cardiolipina"], "Anti-cardiolipina", row["Anti-cardiolipina (U.M.)"],
                            "Anti-cardiolipina (U.M.)",
                            row["Anti-cardiolipina (Normale)"],
                            "bloodAnti-cardiolipin", "bloodAnti-cardiolipinNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["LAC"], "LAC", row["LAC (U.M.)"],
                            "LAC (U.M.)",
                            row["LAC (Normale)"],
                            "bloodLAC", "bloodLACNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_UL(row["Anti-transglutaminasi"], "Anti-transglutaminasi", row["Anti-transglutaminasi (U.M.)"],
                            "Anti-transglutaminasi (U.M.)",
                            row["Anti-transglutaminasi (Normale)"],
                            "bloodAnti-transglutaminase", "bloodAnti-transglutaminaseNormal", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["HBV-Antigene di superficie virus Epatite B"], "HBV-Antigene di superficie virus Epatite B", row["HBV-Antigene di superficie virus Epatite B (U.M.)"],
                            "HBV-Antigene di superficie virus Epatite B (U.M.)",
                            row["HBV-Antigene di superficie virus Epatite B (Esito)"],
                            None, "HBV_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["Anticorpo anti-HCV"],
                                          "Anticorpo anti-HCV", row["Anticorpo anti-HCV (U.M.)"],
                                          "Anticorpo anti-HCV (U.M.)",
                                          row["Anticorpo anti-HCV (Esito)"],
                                          None, "HCV_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["Anticorpo anti-HIV"],
                                          "Anticorpo anti-HIV", row["Anticorpo anti-HIV (U.M.)"],
                                          "Anticorpo anti-HIV (U.M.)",
                                          row["Anticorpo anti-HIV (Esito)"],
                                          None, "HIV_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["Anticorpo anti-varicella"],
                                          "Anticorpo anti-varicella", row["Anticorpo anti-varicella (U.M.)"],
                                          "Anticorpo anti-varicella (U.M.)",
                                          row["Anticorpo anti-varicella (Esito)"],
                                          None, "anti-chickenpox_antibody_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["Anticorpo neutralizzante anti-IFN"],
                                          "Anticorpo neutralizzante anti-IFN", row["Anticorpo neutralizzante anti-IFN (U.M.)"],
                                          "Anticorpo neutralizzante anti-IFN (U.M.)",
                                          row["Anticorpo neutralizzante anti-IFN (Esito)"],
                                          None, "Neutralizing_anti-IFN_antibody_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["beta-hCG plasmatica (gonadotropina corionica)"],
                                          "beta-hCG plasmatica (gonadotropina corionica)", row["beta-hCG plasmatica (gonadotropina corionica) (U.M.)"],
                                          "beta-hCG plasmatica (gonadotropina corionica) (U.M.)",
                                          row["beta-hCG plasmatica (gonadotropina corionica) (Esito)"],
                                          None, "plasma_beta-hCG_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["Test Quantiferon TB Gold"],
                                          "Test Quantiferon TB Gold", row["Test Quantiferon TB Gold (U.M.)"],
                                          "Test Quantiferon TB Gold (U.M.)",
                                          row["Test Quantiferon TB Gold (Esito)"],
                                          None, "quantiferon_TB_Gold_test", index, g, hematologyTestId, BTO_schema)
            data_found += self.testMap_positiveNegative(row["Test di Mantoux"],
                                          "Test di Mantoux", row["Test di Mantoux (U.M.)"],
                                          "Test di Mantoux (U.M.)",
                                          row["Test di Mantoux (Esito)"],
                                          None, "mantoux_test", index, g, hematologyTestId, BTO_schema)

            if data_found > 0:
                # we have some data. Add it to the graph
                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))
                g.add((hematologyTestId, RDF.type, PO[ncit.HEMATOLOGY_TEST]))
                if not pd.isna(row["Data del prelievo"]):
                    try:
                        g.add((protocolEventId, BTO_schema["startDate"],
                               Literal(row["Data del prelievo"], datatype=XSD.date)))
                        g.add((hematologyTestId, BTO_schema["startDate"],
                               Literal(row["Data del prelievo"], datatype=XSD.date)))
                    except:
                        print(f'[WARNING]-An error occurs - id: {index}\tRelated to hematology test date!\n')

                g.add((protocolEventId, BTO_schema["consist"], hematologyTestId))
            else:
                print("[WARNING] No blood test (specific) data for patient " + str(row["TUR_MS_CODE"]))
                empty_rows += 1

        print(
            f'\n\n[SYSTEM-QUALITY-REPORT] - [Blood Test] - Number of useless tuples for specific blood tests: '
            f'{empty_rows}\n\n')
                                          

        print(f'\n\n[SYSTEM-QUALITY-REPORT] - [Blood Test] - Number of missing measurement unit: {self.UM_missing_counter}\n\n')
        return g
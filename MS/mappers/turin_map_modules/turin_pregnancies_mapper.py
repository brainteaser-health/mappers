"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
#import the uri generator
from mappers.utility_scripts import  id_generator as idg

class Pregnancy():

    def pregnancyMapper(self, pregnancies, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                        forbidden_list):

        pregIntervalByPatient = {} #{ patientId: [interval1; interval2; ...], ...}

        inconsistency_counter = 0

        for index, row in pregnancies.iterrows():

            if row["TUR_MS_CODE"] in forbidden_list:
                continue

            #patient uri
            patientId = patientCodes[row["TUR_MS_CODE"]]
            pregnancyStartDate = row["Data Inizio Gravidanza"]
    
            if not row["TUR_MS_CODE"] in pregIntervalByPatient:
                # first pregnancy registered for the current patient OR previous pregnancies have no end date (so it's impossible to find the pregnancy interval, and we just register the data that we have)
                if not pd.isna(row["Data Fine Gravidanza"]):
                    #register the intervall of pregnancy
    
                    # check the consistency of data
                    if row["Data Inizio Gravidanza"] <= row["Data Fine Gravidanza"] and pd.Interval(row["Data Inizio Gravidanza"], row["Data Fine Gravidanza"], closed="both").length < pd.Timedelta('365 days 00:00:00'):
                        interval = pd.Interval(row["Data Inizio Gravidanza"], row["Data Fine Gravidanza"], closed="both")
                        pregIntervalByPatient.update({row["TUR_MS_CODE"] : [interval]})
    
                        #create the pregnancy
                        pregnancyId = idg.getURI("turin", "pregnancy", index)
                        g.add((pregnancyId, RDF.type, PO[ncit.PREGNANCY]))
                        protocolEventId = idg.getURI()
                        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                        g.add((patientId, BTO_schema["undergo"], protocolEventId))
                        g.add((protocolEventId, BTO_schema["hasPregnancy"], pregnancyId))
    
                        g.add((pregnancyId, BTO_schema["startDate"], Literal(row["Data Inizio Gravidanza"].date(), datatype=XSD.date)))
                        if not pd.isna(row["Data Fine Gravidanza"]):
                            g.add((pregnancyId, BTO_schema["endDate"], Literal(row["Data Fine Gravidanza"].date(), datatype=XSD.date)))
                        if not pd.isna(row["Evento Fine Gravidanza"]):
                            if str(row["Evento Fine Gravidanza"]) == "Gravidanza a termine":
                                g.add((pregnancyId, BTO_schema["endEvent"], Literal("Delivery", lang="en")))
                            if str(row["Evento Fine Gravidanza"]) == "Aborto spontaneo":
                                g.add((pregnancyId, BTO_schema["endEvent"], Literal("Spontaneous abortion", lang="en")))
    
                        #create the diagnosis, add the relatie date and link it to the pregnancy entity
                        if not pd.isna(row["Data Diagnosi Gravidanza"]):
                            g.add((pregnancyId, BTO_schema["pregnancyDiagnosisDate"],
                                   Literal(row["Data Diagnosi Gravidanza"].date(), datatype=XSD.date)))
                            # pregDiagnosisId = idg.getURI("pavia", "pregnancy", str(index))
                            # g.add((pregDiagnosisId, RDF.type, PO[ncit.DIAGNOSIS]))
                            # g.add((patientId, BTO_schema["undergo"], pregDiagnosisId))
                            # g.add((pregDiagnosisId, BTO_schema["startDate"],
                            #        Literal(row["Data Diagnosi Gravidanza"].date(), datatype=XSD.date)))
                            # g.add((pregDiagnosisId, BTO_schema["hasPregnancy"], pregnancyId))

                    elif row["Data Inizio Gravidanza"] > row["Data Fine Gravidanza"]:
                        print(f'[WARNING] - Inconsistency of data for pregnancy: id = {index}\tpregnancy start date = {row["Data Inizio Gravidanza"]}\tpregnancy end date = {row["Data Fine Gravidanza"]}')
                        inconsistency_counter += 1
                    elif pd.Interval(row["Data Inizio Gravidanza"], row["Data Fine Gravidanza"], closed="both").length > pd.Timedelta('365 days 00:00:00'):
                        print(f'[WARNING] - Inconsistency of data for pregnancy: id = {index}\tpregnancy duration = {pd.Interval(row["Data Inizio Gravidanza"], row["Data Fine Gravidanza"], closed="both").length}')
                        inconsistency_counter += 1
    
            elif not pd.isna(row["Data Fine Gravidanza"]):
                #case there are previous pregnancies, we need to check that the intervals (new one and previuos ones) are not overlapping
    
                #check the consistency of data
                if row["Data Inizio Gravidanza"] <= row["Data Fine Gravidanza"]:
    
                        patientIntervals = pregIntervalByPatient[row["TUR_MS_CODE"]]
                        newInterval = pd.Interval(row["Data Inizio Gravidanza"], row["Data Fine Gravidanza"], closed="both")
    
                        inconsistent = False
                        if newInterval.length > pd.Timedelta('365 days 00:00:00'):
                            inconsistent = True
                            print(f'[WARNING] - Patient: " + {row["TUR_MS_CODE"]} + " - duration of its pregnancy: {newInterval.length}')
                            inconsistency_counter += 1
    
                        overlapping = False
                        for i in patientIntervals:
                            #print("paziente: "+ str(row["Paziente ID"])+" "+str(i.length))
                            if newInterval.overlaps(i):
                                print("[WARNING] - Patient: " + str(row["TUR_MS_CODE"]) + " overlap on two different pregnancies")
                                inconsistency_counter += 1
                                overlapping = True
                                break
                        if not overlapping and not inconsistent:
                            #add the new interval
                            patientIntervals.append(newInterval)
                            pregIntervalByPatient.update({row["TUR_MS_CODE"]: patientIntervals})
                            # create the pregnancy
                            pregnancyId = idg.getURI("turin", "pregnancy", index)
                            g.add((pregnancyId, RDF.type, PO[ncit.PREGNANCY]))
                            protocolEventId = idg.getURI()
                            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                            g.add((patientId, BTO_schema["undergo"], protocolEventId))
                            g.add((protocolEventId, BTO_schema["hasPregnancy"], pregnancyId))
    
                            g.add((pregnancyId, BTO_schema["startDate"], Literal(row["Data Inizio Gravidanza"].date(), datatype=XSD.date)))
                            if not pd.isna(row["Data Fine Gravidanza"]):
                                g.add((pregnancyId, BTO_schema["endDate"], Literal(row["Data Fine Gravidanza"].date(), datatype=XSD.date)))
                            if not pd.isna(row["Evento Fine Gravidanza"]):
                                if str(row["Evento Fine Gravidanza"]) == "Gravidanza a termine":
                                    g.add((pregnancyId, BTO_schema["endEvent"], Literal("Delivery", lang="en")))
                                if str(row["Evento Fine Gravidanza"]) == "Aborto spontaneo":
                                    g.add((pregnancyId, BTO_schema["endEvent"],
                                           Literal("Spontaneous abortion", lang="en")))
    
                            # create the diagnosis, add the relatie date and link it to the pregnancy entity
                            if not pd.isna(row["Data Diagnosi Gravidanza"]):
                                g.add((pregnancyId, BTO_schema["pregnancyDiagnosisDate"],
                                       Literal(row["Data Diagnosi Gravidanza"].date(), datatype=XSD.date)))
                                # pregDiagnosisId = idg.getURI("pavia", "pregnancy", str(index))
                                # g.add((pregDiagnosisId, RDF.type, PO[ncit.DIAGNOSIS]))
                                # g.add((patientId, BTO_schema["undergo"], pregDiagnosisId))
                                # g.add((pregDiagnosisId, BTO_schema["startDate"],
                                #        Literal(row["Data Diagnosi Gravidanza"].date(), datatype=XSD.date)))
                                # g.add((pregDiagnosisId, BTO_schema["hasPregnancy"], pregnancyId))
                else:
                    print(
                        f'[WARNING] - Inconsistency of data for pregnancy: id = {index}\tpregnancy start date = {row["Data Inizio Gravidanza"]}\tpregnancy end date = {row["Data Fine Gravidanza"]}')
                    inconsistency_counter += 1
            else:
                #case: there are previous pregnancies but this one have no end date
                # create the pregnancy
                pregnancyId = idg.getURI("turin", "pregnancy", index)
                g.add((pregnancyId, RDF.type, PO[ncit.PREGNANCY]))
                protocolEventId = idg.getURI()
                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                g.add((patientId, BTO_schema["undergo"], protocolEventId))
                g.add((protocolEventId, BTO_schema["hasPregnancy"], pregnancyId))
    
                g.add((pregnancyId, BTO_schema["startDate"], Literal(row["Data Inizio Gravidanza"].date(), datatype=XSD.date)))
                if not pd.isna(row["Data Fine Gravidanza"]):
                    g.add((pregnancyId, BTO_schema["endDate"], Literal(row["Data Fine Gravidanza"].date(), datatype=XSD.date)))
                if not pd.isna(row["Evento Fine Gravidanza"]):
                    if str(row["Evento Fine Gravidanza"]) == "Gravidanza a termine":
                        g.add((pregnancyId, BTO_schema["endEvent"], Literal("Delivery", lang="en")))
                    if str(row["Evento Fine Gravidanza"]) == "Aborto spontaneo":
                        g.add(
                            (pregnancyId, BTO_schema["endEvent"], Literal("Spontaneous abortion", lang="en")))
    
                # create the diagnosis, add the relatie date and link it to the pregnancy entity
                if not pd.isna(row["Data Diagnosi Gravidanza"]):
                    g.add((pregnancyId, BTO_schema["pregnancyDiagnosisDate"],
                           Literal(row["Data Diagnosi Gravidanza"].date(), datatype=XSD.date)))
                    # pregDiagnosisId = idg.getURI("pavia", "pregnancy", str(index))
                    # g.add((pregDiagnosisId, RDF.type, PO[ncit.DIAGNOSIS]))
                    # g.add((patientId, BTO_schema["undergo"], pregDiagnosisId))
                    # g.add((pregDiagnosisId, BTO_schema["startDate"],
                    #        Literal(row["Data Diagnosi Gravidanza"].date(), datatype=XSD.date)))
                    # g.add((pregDiagnosisId, BTO_schema["hasPregnancy"], pregnancyId))
        print(f'\n\n[SYSTEM-QUALITY-REPORT] - [Pregnancy] - Number of inconsistent rows in pregnancies = {inconsistency_counter}\n')

        return g

def check_dates(onset_data, pregnancies_data):
    """

    :param onset_data:
    :param pregnancies_data:
    :return:
    """
    merged = onset_data.merge(pregnancies_data, left_on="TUR_MS_CODE", right_on="TUR_MS_CODE", how="outer")
    list_of_misplaced_dates = merged[merged["Data esordio SM"] > merged["Data Inizio Gravidanza"]].index.to_list()
    print("[SYSTEM-QUALITY-REPORT]  - [Pregnancies] Onsets happened later than pregnancy: " + str(
        len(list_of_misplaced_dates)))

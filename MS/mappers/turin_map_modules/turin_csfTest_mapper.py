"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
#import the uri generator
from mappers.utility_scripts import  id_generator as idg


def csfTestMapper(csfTest, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                  forbidden_list):

    for index, row in csfTest.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        # retrieve the patient URI
        idPatient = patientCodes[row["TUR_MS_CODE"]]

        # create the protocol event
        protocolEventId = idg.getURI("turin", "csfTest", str(index))

        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))

        # link the patient to the protocol event
        g.add((idPatient, BTO_schema["undergo"], protocolEventId))

        csfTestId = idg.getURI()
        g.add((csfTestId, RDF.type, PO[ncit.CSF_ANALYSIS]))
        g.add((protocolEventId, BTO_schema["consists"], csfTestId))

        if not pd.isna(row["Data esame"]):
            try:
                g.add((protocolEventId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
                g.add((csfTestId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            except:
                print(f'[WARNING]-An error occurs at index: {index}\tRelated to csf test date!\n')
        else:
            print(f'[WARNING] - Misssing date [Data esame] for clinical event\tindex : {index}')

        if not pd.isna(row["Liquor cerebro-spinale"]):
            if str(row["Liquor cerebro-spinale"]).lower().strip() == "Alterazione tipica per SM".lower():
                g.add((csfTestId, BTO_schema["cerebrospinalFluid"], Literal("Typical alteration for MS", lang="en")))
            elif str(row["Liquor cerebro-spinale"]).lower().strip() == "Normale".lower():
                g.add((csfTestId, BTO_schema["cerebrospinalFluid"], Literal("Normal", lang="en")))
            elif str(row["Liquor cerebro-spinale"]).lower().strip() == "Alterazione atipica per SM".lower():
                g.add((csfTestId, BTO_schema["cerebrospinalFluid"], Literal("Atypical alteration for MS", lang="en")))
            elif str(row["Liquor cerebro-spinale"]).lower().strip() == "Traumatico".lower():
                g.add((csfTestId, BTO_schema["cerebrospinalFluid"], Literal("Traumatic", lang="en")))
            else:
                print(f'[WARNING]-Unexpected value for "cerebrospinalFluid" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')

        if not pd.isna(row["Conta leucociti (/mm3)"]):
            if row["Conta leucociti (/mm3)"] < 0:
                print(f'[WARNING]-Negative value for "leukocyteCounts" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')
            else:
                g.add((csfTestId, BTO_schema["leukocyteCounts"], Literal(row["Conta leucociti (/mm3)"], datatype=XSD.float)))

        if not pd.isna(row["Proteine (mg/l)"]):
            if row["Proteine (mg/l)"] < 0:
                print(f'[WARNING]-Negative value for "protein" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')
            else:
                g.add((csfTestId, BTO_schema["protein"], Literal(row["Proteine (mg/l)"], datatype=XSD.float)))

        if not pd.isna(row["Glucosio (mg/l)"]):
            if row["Glucosio (mg/l)"] < 0:
                print(f'[WARNING]-Negative value for "bloodSugar" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')
            else:
                g.add((csfTestId, BTO_schema["bloodSugar"], Literal(row["Glucosio (mg/l)"], datatype=XSD.float)))

        if not pd.isna(row["Albumina (mg/l)"]):
            if row["Albumina (mg/l)"] < 0:
                print(f'[WARNING]-Negative value for "albumin" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')
            else:
                g.add((csfTestId, BTO_schema["albumin"], Literal(row["Albumina (mg/l)"], datatype=XSD.float)))

        if not pd.isna(row["Q Albumina"]):
            if row["Q Albumina"] < 0:
                print(f'[WARNING]-Negative value for Q-albumin at index: {index}\tRelated to patient id: {row["Q Albumina"]}\n')
            else:
                g.add((csfTestId, BTO_schema["Q-albumin"], Literal(row["Q Albumina"], datatype=XSD.float)))

        if not pd.isna(row["IgG (mg/l)"]):
            if row["IgG (mg/l)"] < 0:
                print(f'[WARNING]-Negative value for "IgG" at index: {index}\tRelated to patient id: {row["Q Albumina"]}\n')
            else:
                g.add((csfTestId, BTO_schema["IgG"], Literal(row["IgG (mg/l)"], datatype=XSD.float)))

        if not pd.isna(row["Indice di IgG"]):
            if row["Indice di IgG"] < 0:
                print(f'[WARNING]-Negative value for "IgG-index" at index: {index}\tRelated to patient id: {row["Q Albumina"]}\n')
            else:
                g.add((csfTestId, BTO_schema["IgG-index"], Literal(row["Indice di IgG"], datatype=XSD.float)))

        if not pd.isna(row["Bande oligoclonali"]):
            if str(row["Bande oligoclonali"]).lower().strip() == "Presenti".lower():
                g.add((csfTestId, BTO_schema["oligoclonalBands"], Literal(True, datatype=XSD.boolean)))
            elif str(row["Bande oligoclonali"]).lower().strip() == "Assenti".lower():
                g.add((csfTestId, BTO_schema["oligoclonalBands"], Literal(False, datatype=XSD.boolean)))
            elif str(row["Bande oligoclonali"]).lower().strip() == "Non effettuato".lower():
                None
            else:
                print(f'[WARNING]-Unexpected value for "oligoclonalBands" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')

        if not pd.isna(row["Numero di bande presenti"]):
            try:
                if int(row["Numero di bande presenti"]) == 999:
                    None
                elif int(row["Numero di bande presenti"]) > 0:
                    g.add((csfTestId, BTO_schema["numberOfOligoclonalBands"], Literal(int(row["Numero di bande presenti"]), datatype=XSD.integer)))
                else:
                    print(f'[WARNING]-Unexpected value for "numberOfOligoclonalBands" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')
            except Exception as e:
                print("Oops!", e.__class__, "occurred.")
                print("Next entry.")
                print()

        if not pd.isna(row["Tipo"]):
            try:
                if str(row["Tipo"]) == "Bande oligoclonali liquorali":
                    g.add((csfTestId, BTO_schema["typeOfOligoclonalBands"], Literal("oligoclonal bands in CSF", lang="en")))
                elif str(row["Tipo"]) == "Bande oligoclonali sieriche e liquorali":
                    g.add((csfTestId, BTO_schema["typeOfOligoclonalBands"], Literal("oligoclonal bands in CSF & serum", lang="en")))
                else:
                    print(f'[WARNING]-Unexpected value for "typeOfOligoclonalBands" at index: {index}\tRelated to patient id: {row["Paziente ID"]}\n')
            except Exception as e:
                print("Oops!", e.__class__, "occurred.")
                print("Next entry.")
                print()

    return g
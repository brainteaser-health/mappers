"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
#import the uri generator
from mappers.utility_scripts import  id_generator as idg

def rmiMapper(rmi, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
              forbidden_list):

    for index, row in rmi.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        # retrieve the patient URI
        idPatient = patientCodes[row["TUR_MS_CODE"]]

        # create the protocol event
        protocolEventId = idg.getURI("turin", "rmi", str(index))

        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))

        # link the patient to the protocol event
        g.add((idPatient, BTO_schema["undergo"], protocolEventId))

        rmiId = idg.getURI()
        g.add((rmiId, RDF.type, PO[ncit.MAGNETIC_RESONANCE_IMAGING]))
        g.add((protocolEventId, BTO_schema["consists"], rmiId))
        if not pd.isna(row["Data esame"]):
            g.add((protocolEventId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))
            g.add((rmiId, BTO_schema["startDate"], Literal(row["Data esame"].date(), datatype=XSD.date)))

        if not pd.isna(row["Area SNC esaminata"]):
            if row["Area SNC esaminata"].strip() == "Encefalo":
                g.add((rmiId, BTO_schema["area"], BTO_ni["Brain_Stem"]))
            elif row["Area SNC esaminata"].strip() == "Midollo spinale completo":
                g.add((rmiId, BTO_schema["area"], BTO_ni["Spinal_Cord"]))
            elif row["Area SNC esaminata"].strip() == "Tratto cervicale del midollo spinale":
                g.add((rmiId, BTO_schema["area"], BTO_ni["cervical_spinal_cord"]))
            elif row["Area SNC esaminata"].strip() == "Tratto toracico del midollo spinale":
                g.add((rmiId, BTO_schema["area"], BTO_ni["thoracic_spinal_cord"]))
            else:
                print(f'[WARNING] - Unexpected value as anatomical area\tindex : {index}\tvalue : {row["Area SNC esaminata"]}')

        if not pd.isna(row["Presenza di Lesioni in T1"]):
            if str(row["Presenza di Lesioni in T1"]).strip().lower() == "si":
                g.add((rmiId, BTO_schema["lesionsT1"], Literal("true", datatype=XSD.boolean)))
            elif str(row["Presenza di Lesioni in T1"]).strip().lower() == "no":
                g.add((rmiId, BTO_schema["lesionsT1"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row[
                           "Presenza di Lesioni in T1 captanti Gadolinio"]):  # the doctor said if the lesions are present or not
            if str(row["Presenza di Lesioni in T1 captanti Gadolinio"]).strip().lower() == "si":  # they are present
                if not pd.isna(row["Numero di lesioni in T1 captanti Gadolinio"]) and int(row[
                                                                                              "Numero di lesioni in T1 captanti Gadolinio"]) > 0:  # there are actually some lesions
                    # add true for the presence and the value
                    g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("true", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["numberOfLesionsT1-Gadolinium"],
                           Literal(row["Numero di lesioni in T1 captanti Gadolinio"], datatype=XSD.integer)))
                else:
                    # the medic forgot to specify the value, or the value is <= 0
                    pass
            elif str(row[
                         "Presenza di Lesioni in T1 captanti Gadolinio"]).strip().lower() == "no":  # no lesion was present
                if not pd.isna(row["Numero di lesioni in T1 captanti Gadolinio"]) and int(row[
                                                                                              "Numero di lesioni in T1 captanti Gadolinio"]) <= 0:
                    g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("false", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["numberOfLesionsT1-Gadolinium"],
                           Literal(row["Numero di lesioni in T1 captanti Gadolinio"], datatype=XSD.integer)))
                elif not pd.isna(row["Numero di lesioni in T1 captanti Gadolinio"]) and row[
                    "Numero di lesioni in T1 captanti Gadolinio"] > 0:
                    # the medic said no lesions, but then they added a positive number of lesions. In this case we do not add anything to the KB
                    pass
                elif pd.isna(row[
                                 "Numero di lesioni in T1 captanti Gadolinio"]):  # the medic did not bother to write 0, we write 0 by ourselves
                    g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("false", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["numberOfLesionsT1-Gadolinium"],
                           Literal("0", datatype=XSD.integer)))
        else:  # the medic did not bother to write yes/no
            if not pd.isna(row["Numero di lesioni in T1 captanti Gadolinio"]):  # but they bothered to write the number
                if int(row["Numero di lesioni in T1 captanti Gadolinio"]) == 0:  # if it is 0, no lesions were present
                    g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("false", datatype=XSD.boolean)))
                elif int(row[
                             "Numero di lesioni in T1 captanti Gadolinio"]) > 0:  # if it is positive, some lesions are present
                    g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("true", datatype=XSD.boolean)))

                g.add((rmiId, BTO_schema["numberOfLesionsT1-Gadolinium"],
                       Literal(row["Numero di lesioni in T1 captanti Gadolinio"], datatype=XSD.integer)))

            # if not pd.isna(row["Presenza di Lesioni in T1 captanti Gadolinio"]):
            #     if str(row["Presenza di Lesioni in T1 captanti Gadolinio"]).strip().lower() == "si":
            #         g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("true", datatype=XSD.boolean)))
            #     elif str(row["Presenza di Lesioni in T1 captanti Gadolinio"]).strip().lower() == "no":
            #         g.add((rmiId, BTO_schema["lesionsT1-Gadolinium"], Literal("false", datatype=XSD.boolean)))
            #
            # if not pd.isna(row["Numero di lesioni in T1 captanti Gadolinio"]):
            #     if row["Numero di lesioni in T1 captanti Gadolinio"] >= 0:
            #         g.add((rmiId, BTO_schema["numberOfLesionsT1-Gadolinium"],
            #                Literal(row["Numero di lesioni in T1 captanti Gadolinio"], datatype=XSD.integer)))
            #     else:
            #         print(
            #             f'[WARNING] - Unexpected negative value (= {row["Numero di lesioni in T1 captanti Gadolinio"]}) in field "Numero di lesioni in T1 captanti Gadolinio" at index: {index}')

        if not pd.isna(row[
                           "Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]):  # the doctor said if the lesions are present or not
            if str(row[
                       "Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]).strip().lower() == "si":  # they are present
                if not pd.isna(row["Numero nuove lesioni o aumentate di volume in T2"]) and \
                        int(row[
                                "Numero nuove lesioni o aumentate di volume in T2"]) > 0:  # there are actually some lesions
                    # add true for the presence and the value
                    g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("true", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["numberOfNewOrEnlargedLesionsT2"],
                           Literal(row["Numero nuove lesioni o aumentate di volume in T2"], datatype=XSD.integer)))
                else:
                    # the medic forgot to specify the value, or the value is <= 0
                    pass
            elif str(row[
                         "Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]).strip().lower() == "no":  # no lesion was present
                if not pd.isna(row["Numero nuove lesioni o aumentate di volume in T2"]) and row[
                    "Numero nuove lesioni o aumentate di volume in T2"] <= 0:
                    g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("false", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["numberOfNewOrEnlargedLesionsT2"],
                           Literal(row["Numero nuove lesioni o aumentate di volume in T2"], datatype=XSD.integer)))
                elif not pd.isna(row["Numero nuove lesioni o aumentate di volume in T2"]) and row[
                    "Numero nuove lesioni o aumentate di volume in T2"] > 0:
                    # the medic said no lesions, but then they added a positive number of lesions. In this case we do not add anything to the KB
                    pass
                elif pd.isna(row[
                                 "Numero nuove lesioni o aumentate di volume in T2"]):  # the medic did not bother to write 0, we write 0 by ourselves
                    g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("false", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["numberOfNewOrEnlargedLesionsT2"],
                           Literal("0", datatype=XSD.integer)))
        else:  # the medic did not bother to write yes/no
            if not pd.isna(
                    row["Numero nuove lesioni o aumentate di volume in T2"]):  # but they bothered to write the number
                if int(row[
                           "Numero nuove lesioni o aumentate di volume in T2"]) == 0:  # if it is 0, no lesions were present
                    g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("false", datatype=XSD.boolean)))
                elif int(row[
                             "Numero nuove lesioni o aumentate di volume in T2"]) > 0:  # if it is positive, some lesions are present
                    g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("true", datatype=XSD.boolean)))

                g.add((rmiId, BTO_schema["numberOfNewOrEnlargedLesionsT2"],
                       Literal(row["Numero nuove lesioni o aumentate di volume in T2"], datatype=XSD.integer)))

            # are there new T2 lesions?
            # if not pd.isna(
            #         row["Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]):
            #     if str(row[
            #                "Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]).strip().lower() == "si":
            #         g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("true", datatype=XSD.boolean)))
            #     elif str(row[
            #                  "Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]).strip().lower() == "no":
            #         g.add((rmiId, BTO_schema["newOrEnlargedLesionsT2"], Literal("false", datatype=XSD.boolean)))
            #
            # # Number of new lesioni in T2
            # if not pd.isna(row["Numero nuove lesioni o aumentate di volume in T2"]):
            #     number_of_lesions = int(row["Numero nuove lesioni o aumentate di volume in T2"])
            #     g.add((rmiId, BTO_schema["numberOfNewOrEnlargedLesionsT2"],
            #            Literal(number_of_lesions, datatype=XSD.int)))

        if not pd.isna(row["Presenza Lesioni contigue estese maggiori o uguali a 3 segmenti vertebrali"]):
            if str(row[
                       "Presenza Lesioni contigue estese maggiori o uguali a 3 segmenti vertebrali"]).strip().lower() == "si":
                g.add((rmiId, BTO_schema["contiguousLesions"], Literal("true", datatype=XSD.boolean)))
            elif str(row[
                         "Presenza Lesioni contigue estese maggiori o uguali a 3 segmenti vertebrali"]).strip().lower() == "no":
                g.add((rmiId, BTO_schema["contiguousLesions"], Literal("false", datatype=XSD.boolean)))

        if not pd.isna(row["Numero totale di lesioni in T2"]):
            if row["Numero totale di lesioni in T2"] != "Non noto":
                if row["Numero totale di lesioni in T2"] != "0":  # a range is provided
                    g.add((rmiId, BTO_schema["lesionsT2"],
                           Literal("true", datatype=XSD.boolean)))
                else:
                    g.add((rmiId, BTO_schema["lesionsT2"],
                           Literal("false", datatype=XSD.boolean)))
                g.add((rmiId, BTO_schema["totalLesionsT2"],
                       Literal(row["Numero totale di lesioni in T2"], datatype=XSD.string)))
        else:
            # deal with the case of missing data
            if not pd.isna(row["Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]) \
                    and str(row["Presenza di nuove lesioni o di lesioni aumentate di volume in T2 rispetto a RMN precedente"]).strip().lower() == "si":
                # we use this other field to perform 'padding' for this data
                if not pd.isna(row["Numero nuove lesioni o aumentate di volume in T2"]) and \
                        int(row[
                                "Numero nuove lesioni o aumentate di volume in T2"]) > 0:  # check the presence of this field
                    g.add((rmiId, BTO_schema["lesionsT2"], Literal("true", datatype=XSD.boolean)))
                    g.add((rmiId, BTO_schema["totalLesionsT2"],
                           Literal(row["Numero nuove lesioni o aumentate di volume in T2"], datatype=XSD.integer)))


    return g
"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import ncit_codes as ncit
# import the uri generator
from mappers.pavia_map_modules.pavia_treatments_mapper import check_form_medicine_name
from mappers.utility_scripts import id_generator as idg

import re

from support.TranslationMethods import translate_frequency_from_italian_to_english, \
    translate_administration_route__from_italian_to_english, translate_measurement_unit


def treatmentMapper(treatments, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, file,
                    forbidden_list):
    # Counter for keeping track of the number of treatments which have no specification about their type (symptomatic/specific)
    no_SpecSympt_counter = 0

    no_MeasurerementUnit_counter = 0

    ATC_corrected = 0

    ATC_not_corrected = 0

    # Import the controlled dictionary {medicine_label : ATC_code} from csv file
    file = str(Path().resolve()) + "/data/input/atc_label.csv"
    df = pd.read_csv(file, usecols=["label", "atc"])

    # create the dictionary to store and access runtime the pairs (medicine_label, atc_code)
    atc_dict = {}
    for i, row in df.iterrows():
        atc_dict.update({row["label"]: row["atc"]})

    for index, row in treatments.iterrows():

        if row["TUR_MS_CODE"] in forbidden_list:
            continue

        # retrieve the patient URI
        idPatient = patientCodes[row["TUR_MS_CODE"]]

        # create the protocol event
        protocolEventId = idg.getURI("turin", "treatments", str(index))

        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
        # link the patient to the protocol event
        g.add((idPatient, BTO_schema["undergo"], protocolEventId))

        therapeuticProcedureId = idg.getURI()
        if not pd.isna(row["Tipo"]) and row["Tipo"] == "Specifico":
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
        elif not pd.isna(row["Tipo"]) and row["Tipo"] == "Sintomatico":
            g.add((therapeuticProcedureId, RDF.type, PO[ncit.SYMPTOMATIC_TREATMENT]))
        else:
            g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
            print(
                f'[WARNING] - Therapeutic procedure with no distintion between "specific" or "symptomatic"!\tid : {index}')
            no_SpecSympt_counter += 1

        if not pd.isna(row["Data inizio"]):
            g.add((therapeuticProcedureId, BTO_schema["startDate"],
                   Literal(row["Data inizio"].date(), datatype=XSD.date)))
        if not pd.isna(row["Data fine"]):
            g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["Data fine"].date(), datatype=XSD.date)))

        g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

        measurement_unit_is_present = False
        administration_amount_is_present = False

        administrationId = idg.getURI()
        g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
        g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
        if not pd.isna(row["Frequenza somministrazione"]):
            frequency_in_english = translate_frequency_from_italian_to_english(row["Frequenza somministrazione"])
            g.add((administrationId, BTO_schema["frequency"],
                   Literal(frequency_in_english, datatype=XSD.string)))
        if not pd.isna(row["Via somministrazione"]):
            administration_route = translate_administration_route__from_italian_to_english(row["Via somministrazione"])
            g.add((administrationId, BTO_schema["administrationRoute"],
                   Literal(administration_route, datatype=XSD.string)))
        if not pd.isna(row["Dose"]):
            g.add((administrationId, BTO_schema["dose"], Literal(row["Dose"], datatype=XSD.float)))
            administration_amount_is_present = True
        if not pd.isna(row["Unità di misura"]):
            measurement_unit = translate_measurement_unit(row["Unità di misura"])
            g.add(
                (administrationId, BTO_schema["measurementUnit"], Literal(measurement_unit, datatype=XSD.string)))
            measurement_unit_is_present = True
        if not pd.isna(row["Dose"]) and pd.isna(row["Unità di misura"]):
            print(f'[WARNING] - Treatment which the dose is available but the measurement unit isn\'t\tid : {index}')
            no_MeasurerementUnit_counter += 1
        if not pd.isna(row["Causa sospensione"]):
            g.add((administrationId, BTO_schema["endReason"], Literal(row["Causa sospensione"], lang="en")))
        if not pd.isna(row["Farmaco"]) and check_form_medicine_name(row["Farmaco"]):
            g.add((administrationId, BTO_schema["medicineName"], Literal(row["Farmaco"], datatype=XSD.string)))

        # set if the administration has been suspended (or not)
        if not pd.isna(row["Trattamento sospeso"]):
            if str(row["Trattamento sospeso"]).lower() == "si":
                g.add((administrationId, BTO_schema["suspended_treatment"], Literal(True, datatype=XSD.boolean)))
            elif str(row["Trattamento sospeso"]).lower() == "no":
                g.add((administrationId, BTO_schema["suspended_treatment"], Literal(False, datatype=XSD.boolean)))
        else:
            pass  # no triple is added to the KB is the doctor did not write anything

        # if not measurement_unit_is_present and not administration_amount_is_present:
        #     # if we do not have any information about the measurements units
        #     if not pd.isna(row["Farmaco"]):  # but we have some information in the field 'farmaco'
        #         # there the good doctor may have inserted the quantity in mg. We look for it
        #         look_for_quantity_in_free_text_using_regex(row["Farmaco"], g, administrationId, BTO_schema)

        # link to the medicine's ATC code
        if not pd.isna(row["Codice ATC"]):
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni[row["Codice ATC"]]))
        else:
            # ATC code is missing; Try to find the medicine label in the dictionary
            if not pd.isna(row["Farmaco"]):
                if row["Farmaco"] in atc_dict:
                    g.add((administrationId, BTO_schema["isRelated"], BTO_ni[atc_dict[row["Farmaco"]]]))
                    ATC_corrected += 1
                else:
                    ATC_not_corrected += 1

        # Notes mapping
        if not pd.isna(row["Note"]):
            g.add(
                (idPatient, BTO_schema["notes"], Literal("[Trattamenti] - " + str(row["Note"]), datatype=XSD.string)))

    print(
        f'\n\n[SYSTEM-QUALITY-REPORT] - [Therapeutic Procedure] - Number of therapeutic procedure with no distinction between "specific" or "symptomatic" : {no_SpecSympt_counter}\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Therapeutic Procedure] - Number of treatments which the dose is available but the measurement unit isn\'t : {no_MeasurerementUnit_counter}\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Therapeutic Procedure] - Number of rows without ATC label that the mapping system can correct : {ATC_corrected}\n')
    print(
        f'[SYSTEM-QUALITY-REPORT] - [Therapeutic Procedure] - Number of rows without ATC label that the mapping system can\'t correct : {ATC_not_corrected}\n')

    return g


def look_for_quantity_in_free_text_using_regex(text, g, administrationId, BTO_schema):
    # first, look for MG:
    info_regex = re.compile('[0-9]+ *MG')
    mo = info_regex.search(text)
    if mo is not None:
        # get the number
        reg = re.compile('[0-9]+')
        quantity = reg.search(mo.group())
        # add the number and the quantity to the graph
        g.add((administrationId, BTO_schema["measurementUnit"], Literal("mg", datatype=XSD.string)))
        g.add((administrationId, BTO_schema["dose"], Literal(quantity.group(), datatype=XSD.float)))
        return

    # let us try the same, but with Grams (G)
    info_regex = re.compile('[0-9]+ *G')
    mo = info_regex.search(text)
    if mo is not None:
        # get the number
        reg = re.compile('[0-9]+')
        quantity = reg.search(mo.group())
        # add the number and the quantity to the graph
        g.add((administrationId, BTO_schema["measurementUnit"], Literal("g", datatype=XSD.string)))
        g.add((administrationId, BTO_schema["dose"], Literal(quantity.group(), datatype=XSD.float)))
        return

    # try with milliliters (ML)
    info_regex = re.compile('[0-9]+ *ML')
    mo = info_regex.search(text)
    if mo is not None:
        # get the number
        reg = re.compile('[0-9]+')
        quantity = reg.search(mo.group())
        # add the number and the quantity to the graph
        g.add((administrationId, BTO_schema["measurementUnit"], Literal("ml", datatype=XSD.string)))
        g.add((administrationId, BTO_schema["dose"], Literal(quantity.group(), datatype=XSD.float)))
        return

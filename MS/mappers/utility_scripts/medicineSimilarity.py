"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
from pathlib import Path
import pandas as pd
from re import search
import os

path = Path().resolve().parent.parent
medicinePath = str(path)+"\\data\\input\\pavia\\Trattamenti.csv"
intermedio = str(path)+"\\data\\input\\intermedio.csv"
outpath = str(path)+"\\data\\input\\atc_label.csv"
print(medicinePath)

medicine_df = pd.read_csv(medicinePath, usecols=["Farmaco","Codice ATC"])

# dataframe with labelled medicine
ATC_labelled_medicine = medicine_df.dropna(subset=["Codice ATC"]).drop_duplicates()

# create dictionary with medicine(key) and atc(value)
dictNameATC = {}
for index, row in ATC_labelled_medicine.iterrows():
    if row["Farmaco"] in dictNameATC:
        print(f'error at index: {index}')
    else:
        dictNameATC.update({row["Farmaco"]:row["Codice ATC"]})

newDictATC = dictNameATC.copy()

# dataframe with medicine without ATC label
ATC_unlabelled_medicine = medicine_df[medicine_df["Codice ATC"].isnull()].drop_duplicates().Farmaco
print(ATC_unlabelled_medicine)
for element in ATC_unlabelled_medicine.items():
    print(element)
# exact match
count = 0
for index, medicine in ATC_unlabelled_medicine.items():
    for name in dictNameATC.keys():
        if search(str(medicine),str(name)):
            print(f'Match found at index {index }: [unlabelled]: {medicine} <---> [labelled]: {name} / {dictNameATC[name]}')
            try:
                startIndex = str(name).find(str(medicine))
                print(f'Start index: {startIndex}')
            except:
                None
            if len(medicine)>4 and startIndex == 0:
                print(f'Match OK!')
                newDictATC.update({medicine:dictNameATC[name]})
            elif len(medicine)>2:
                print(f'Not trustable match!')
                match = input(f'Type \'y\' to accept the match or any other key to reject it\n')
                # supervised match
                if match=='y':
                    newDictATC.update({medicine: dictNameATC[name]})
                    print(f'Match accepted')
                else:
                    print(f'Match rejected')
            else:
                print(f'Match rejected')
            count += 1
            print()
print(f'Total matches: {count}')

for element in newDictATC.items():
    print(element)

df = pd.DataFrame.from_dict(newDictATC, orient="index")
print(df)
df.to_csv(intermedio)

fin = open(intermedio)
fout = open(outpath, "wt")
count = 0
for line in fin:
    if count == 0:
        fout.write( line.replace(',0', 'label,atc') )
fin.close()
fout.close()

try:
    os.remove(intermedio)
except OSError as e:
    print(e)


"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
import hashlib

import pandas as pd
from pathlib import Path


class UrbanDegree:

    def __init__(self, absPath):
        self.df = pd.read_excel(absPath, sheet_name="Comuni al 31122020")
        self.deg_dict = {}
        self.istat_dict ={}
        self.poscode_hash_dictionary = {}  # describes the hashes corresponding to the locations
        self.placename_hash_dictionary = {}
        self.zip_to_placename_dictionary = {} # a dictionary that links a zip to its corresponding placename
        self.placename_to_zip_dictionary = {} # a dictionary that links a placename to its corresponding zip

    def print_dict(self):

        for index, label in self.deg_dict.items():
            print(index, label)

    def computeDegree(self):
        """ Given a xls file containing a list of locations with their corresponging Urban Degree,
        assigns the degrees to the elements of this object. We will have a dictionary where each town is associated
        to its urban degree (there are 3 urban degrees as of now).
        """
        # the value 'Denominazione (Italiana e straniera) represents the name of the place, e.g. Milano or Azzanello
        # the value 'Grado di urbanizzazione' contains a int: 1, 2 or 3, describing the type of the place.
        for index, row in self.df.iterrows():
            if not pd.isna(row["Grado di urbanizzazione"]):
                if str(row["Denominazione (Italiana e straniera)"]) in self.deg_dict:
                    # the same place, e.g. Milan, cannot have 2 classifications. This is impossible with the files
                    # we have, but in the future, with different files, it is a possibility (albeit a difficult one).
                    # print("[warning collision] - " + str(row["Denominazione (Italiana e straniera)"]))
                    pass
                else:
                    if row["Grado di urbanizzazione"] == 1:
                        self.deg_dict.update({str(row["Denominazione (Italiana e straniera)"]).lower(): "city"})
                        # print("comune: " + str(row["Denominazione (Italiana e straniera)"]) + " type: CITY")
                    elif row["Grado di urbanizzazione"] == 2:
                        self.deg_dict.update({str(row["Denominazione (Italiana e straniera)"]).lower(): "town or suburbs"})
                        # print("comune: " + str(row["Denominazione (Italiana e straniera)"]) + " type: TOWN OR SUBURB")
                    elif row["Grado di urbanizzazione"] == 3:
                        self.deg_dict.update({str(row["Denominazione (Italiana e straniera)"]).lower(): "rural"})
                        # print("comune: " + str(row["Denominazione (Italiana e straniera)"]) + " type: RURAL")
        return self.deg_dict


    def computeIstatCorrespondences(self):
        for index, row in self.df.iterrows():
            # for each location (comune), connect the name of the location with its istat code
            # NB: index 2 here below stands for "Codice Istat del Comune (alfanumerico)"
            self.istat_dict.update({str(row["Denominazione (Italiana e straniera)"]).lower() : row[2]})

    def populate_hash_dictionary(self, filePath):
        """
        Each location is characterized by its Placename (Comune) POSCODE (zip code) and country_code.
        The poscode and country code produce a hash. We assign to each person the hash of the place they reside.
        This method populates a dictionary with the information contained in a txt file with the combination of poscode (key),
        placename and hash.

        :param filePath: path of the file containing the hashes of the locations
        :return:
        """

        data_file = pd.read_csv(filePath)

        for index, row in data_file.iterrows():
            # we populate all the dictionaries based on the postal code and the placename
            self.poscode_hash_dictionary.update({row["POSCODE"]: row["SHA3_256_HEX_DIG"]})
            self.placename_hash_dictionary.update({row["PLACENAME"].lower(): row["SHA3_256_HEX_DIG"]})
            self.zip_to_placename_dictionary.update({row["POSCODE"]: row["PLACENAME"].lower()})
            self.placename_to_zip_dictionary.update({row["PLACENAME"].lower(): row["POSCODE"]})

    def find_zip_from_placename(self, placename):
        # first, we need to convert the placename to a string that can be found in the dictionary
        p_ = placename.replace("-", " ")\
            .replace("è", "e'").\
            replace("ì", "i'")\
            .replace("ò", "o'")\
            .replace("à", "a'").replace("ù", "o'")

        p_ = p_.lower()

        # some special cases that I really do not know how else to deal with
        if p_ == "urago d\'oglio":
            return self.placename_to_zip_dictionary.get("urago d'oglio")

        if p_ == "corteolona e genzone":
            return self.placename_to_zip_dictionary.get("corteolona")

        return self.placename_to_zip_dictionary.get(p_, "")


    def find_placename_from_zip(self, zip_):
        """
        Uses the information of the zip to retrieve a placename. Returns the empty string if not found
        :param zip_:
        :return:
        """

        if(str.isdigit(zip_)):
            return self.zip_to_placename_dictionary.get(int(zip_), "")

        return ""




    def get_hash_value(self, CAP, placename):

        p_ = placename.lower().replace("\\", "")

        # first, try the zip code, which is the best information
        if CAP != '' and str.isdigit(str(CAP)):
            if int(CAP) in self.poscode_hash_dictionary:
                return self.poscode_hash_dictionary.get(int(CAP), "")


        # if the zip is not available, we try to use the placename
        if p_ != '':
            if p_ in self.placename_hash_dictionary:
                return self.placename_hash_dictionary.get(p_, "")

        # very special cases I do not know how else to deal with
        if p_ == "rivanazzano":
            return self.placename_hash_dictionary.get("Rivanazzano Terme", "")

        if p_ == "urago d\'oglio":
            return self.placename_hash_dictionary.get("Urago D'Oglio")

        if CAP != '' and str.isdigit(str(CAP)) : # we build the hash by ourselves
            try:
                # very special cases I do not know how else to deal with
                # try to produce the hash by ourselves
                s = hashlib.sha3_256() # object to produce the hash
                b = bytes("IT " + str(CAP), 'utf-8')
                s.update(b)
                return s.hexdigest()
            except:
                print("[DEBUG] error with the conversion in HASH of one value")

        return ""


    def compute_istat_hash(self, placename):
        p_ = placename.lower().replace("\\", "")
        istat_code = self.istat_dict.get(p_, "")
        if istat_code != "":
            # create and return the hash of this istat code
            try:
                s = hashlib.sha3_256()  # object to produce the hash
                b = bytes(str(istat_code), 'utf-8')
                s.update(b)
                return s.hexdigest()
            except:
                print("[DEBUG] error with the conversion in HASH of one value")
        else :
            # this is a placename for which we do not find the istat code
            try:
                s = hashlib.sha3_256()  # object to produce the hash
                b = bytes(str(p_), 'utf-8')
                s.update(b)
                return s.hexdigest()
            except:
                print("[DEBUG] error with the conversion in HASH of one value")

"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
from pathlib import Path
import re

def quality_report(log_file_path, save_file):

    base_path_quality_report = str(Path().resolve())+"/data/output/serializations/quality_report/"
    out_file_path = base_path_quality_report + save_file

    with open(log_file_path, "r", encoding="utf-8") as in_file:
        with open(out_file_path, "w", encoding="utf-8") as out_file:

            out_file.write("DATASET QUALITY AND WARNINGS\n\n")

            out_file.write("\n\n\t----- Aggregate info ------\n\n")

            for line in in_file:
                if re.search("SYSTEM-QUALITY-REPORT",line):
                    out_file.write("\t\t"+line+"\n\n")

    with open(log_file_path, "r", encoding="utf-8") as in_file:
        with open(out_file_path, "a", encoding="utf-8") as out_file:

            out_file.write("\t----- Complete log list ---\n\n")

            for line in in_file:
                if re.search("\[WARNING\]", line):
                    out_file.write("\t\t"+line+"\n")
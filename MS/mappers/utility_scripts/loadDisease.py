"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
import re
from pathlib import Path

def load(path):
    diseaseToNCIT = pd.read_csv(path)
    dictNCIT = {}
    for i, r in diseaseToNCIT.iterrows():
        dictNCIT.update({str(r[0]).strip(): str(r[1])})
    return dictNCIT
"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
from mappers.namespaces import ontologies

import mmh3
import uuid

def getURI(hospital = None, ontoClass = None, elementId = None):
    '''
            This method, given the hospital name, the ontology class of the resource and the id on the original dataset (if any)
            will return the URI that identifies the resource in the graph.
    '''
    if hospital != None and ontoClass != None and elementId != None:
        # create the uri related to an entity for which we have an id in the original dataset
        # in this way in the future we can obtain again its uri id by calling this function
        elementURI = str(hospital)+"_" + str(elementId) + "_"+str(ontoClass)
        elementURI = ontologies.BTO_resource[str(mmh3.hash128(elementURI, signed=False, seed=42))]
    else:
        # if in the original dataset we don't have an id for the resource we randomly generate it on the fly with seed 42
        elementURI = ontologies.BTO_resource[str(mmh3.hash128(str(uuid.uuid4()), signed=False, seed=42))]

    return elementURI


"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""

import pandas as pd
import hashlib

"""
Use this class to import the urban degrees from Spain
(this code is intended to be used with Sermas' data
and keep it in memory

"""


class SermasUrbanDictionary:

    def __init__(self, file_path):
        """
        reads the xls file where the urban dictionary for Sermas is stored
        :param file_path:
        """
        self.df = pd.read_csv(file_path)

        # a dictionary where the degrees are stored
        self.place_degree_dictionary = {}
        # dictionary containing couples zip code - urban degree
        self.zip_degree_dictionary = {}

        self.last_chance_zip_to_location = {} # we had some problems with the files containing the information about
                                                # the correspondence between zip and locations.
                                                # This is a dictionary that we use as a last resort to obtain this name given the zip

        self.poscode_hash_dictionary = {}  # describes the hashes corresponding to the locations
        self.placename_hash_dictionary = {}
        self.zip_to_placename_dictionary = {}  # a dictionary that links a zip to its corresponding placename
        self.placename_to_zip_dictionary = {}  # a dictionary that links a placename to its corresponding zip

    def populateUrbanDictionary(self):
        for index, row in self.df.iterrows():  # iterate through the tuples of the file
            if not pd.isna(row["LAU NAME NATIONAL"]) and not row["LAU NAME NATIONAL"].lower() in self.place_degree_dictionary:
                case = row["DEGURBA"]
                if case == 1:
                    self.place_degree_dictionary.update({row["LAU NAME NATIONAL"].lower(): "city"})
                elif case == 2:
                    self.place_degree_dictionary.update({row["LAU NAME NATIONAL"].lower(): "town or suburbs"})
                elif case == 3:
                    self.place_degree_dictionary.update({row["LAU NAME NATIONAL"].lower(): "rural"})

            if not pd.isna(row["Postal Code"]) and \
                    not pd.isna(row["DEGURBA"]) and \
                    not row["Postal Code"] in self.place_degree_dictionary:
                case = row["DEGURBA"]
                if case == 1:
                    self.zip_degree_dictionary.update({str(row["Postal Code"]): "city"})
                elif case == 2:
                    self.zip_degree_dictionary.update({str(row["Postal Code"]): "town or suburbs"})
                elif case == 3:
                    self.zip_degree_dictionary.update({str(row["Postal Code"]): "rural"})

            if not pd.isna(row["Postal Code"]) and not pd.isna(row["LAU NAME NATIONAL"]):
                    self.last_chance_zip_to_location.update({row["Postal Code"]: row["LAU NAME NATIONAL"].lower()})



    def populate_hash_dictionary(self, filePath):
        """
        Each location is characterized by its Placename (Comune) POSCODE (zip code) and country_code.
        The poscode and country code produce a hash. We assign to each person the hash of the place they reside.
        This method populates a dictionary with the information contained in a txt file with the combination of poscode (key),
        placename and hash.

        :param filePath: path of the file containing the hashes of the locations
        :return:
        """

        data_file = pd.read_csv(filePath)

        for index, row in data_file.iterrows():
            # we populate both the dictionaries based on the postal code and the placename
            self.poscode_hash_dictionary.update({row["POSCODE"]: row["SHA3_256_HEX_DIG"]})
            self.placename_hash_dictionary.update({row["PLACENAME"].lower(): row["SHA3_256_HEX_DIG"]})
            self.zip_to_placename_dictionary.update({row["POSCODE"]: row["PLACENAME"].lower()})
            self.placename_to_zip_dictionary.update({row["PLACENAME"].lower(): row["POSCODE"]})

    def find_zip_from_placename(self, placename):
        # first, we need to convert the placename to a string that can be found in the dictionary
        p_ = placename.replace("-", " ")\
            .replace("è", "e'").\
            replace("ì", "i'")\
            .replace("ò", "o'")\
            .replace("à", "a'").replace("ù", "o'")

        p_ = p_.lower()

        # some special cases that I really do not know how else to deal with
        if p_ == "urago d\'oglio":
            return self.placename_to_zip_dictionary.get("urago d'oglio")

        if p_ == "corteolona e genzone":
            return self.placename_to_zip_dictionary.get("corteolona")

        return self.placename_to_zip_dictionary.get(p_, "")


    def find_placename_from_zip(self, zip_):
        """
        Uses the information of the zip to retrieve a placename. Returns the empty string if not found
        :param zip_:
        :return:
        """

        try:
            if str.isdigit(zip_):
                return self.zip_to_placename_dictionary.get(int(zip_), "")
        except:
            pass

        try:
            if zip_ in self.zip_to_placename_dictionary:
                return self.zip_to_placename_dictionary.get(zip_, "")
        except:
            pass

        try:
            return self.last_chance_zip_to_location.get(zip_, "")
        except:
            pass


        return ""




    def get_hash_value(self, CAP, placename):

        p_ = placename.lower().replace("\\", "")

        # first, try the zip code, which is the best information
        if CAP != '' and str.isdigit(str(CAP)):
            if int(CAP) in self.poscode_hash_dictionary:
                return self.poscode_hash_dictionary.get(int(CAP), "")


        # if the zip is not available, we try to use the placename
        if p_ != '':
            if p_ in self.placename_hash_dictionary:
                return self.placename_hash_dictionary.get(p_, "")

        if CAP != '':
            try:
                # very special cases I do not know how else to deal with
                # try to produce the hash by ourselves
                s = hashlib.sha3_256() # object to produce the hash
                b = bytes("ES " + str(int(CAP)), 'utf-8')
                s.update(b)
                return s.hexdigest()
            except:
                print("[DEBUG] error with the conversion in HASH of one value")



        return ""


"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
PATIENT = "NCIT_C16960"
CLINICAL_TRIAL = "NCIT_C71104"
ONSET = "NCIT_C25279"
DIAGNOSIS = "NCIT_C15220"
PATIENT_VISIT = "NCIT_C39564"
EDSS = "NCIT_C98302"
COMORBIDITY = "NCIT_C16457"
PROTOCOL_EVENT = "NCIT_C74589"
RELAPSE = "NCIT_C38155"
PREGNANCY = "NCIT_C25742"
BEHAVIOUR = "NCIT_C19683"
SMOKING = "NCIT_C154329"
PHYSICAL_ACTIVITY = "NCIT_C17708"
LIFESTYLE = "NCIT_C16795"
RELATIVE = "NCIT_C21480"
THERAPEUTIC_PROCEDURE = "NCIT_C49236"
ADMINISTRATION = "NCIT_C25409"
MAGNETIC_RESONANCE_IMAGING = "NCIT_C16809"
CSF_ANALYSIS = "NCIT_C173272"
HEMATOLOGY_TEST = "NCIT_C49286"
SYMPTOMATIC_TREATMENT = "NCIT_C170740"
PLACE = "NCIT_C25319"
"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
# import uri generator
from mappers.utility_scripts import id_generator as idg


def comorbiditiesMapper(madridData, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes, dictNCIT):
    # # load the dictionary for parsing the diseases
    # diseaseToNCIT = pd.read_csv(
    #     "C:\\Users\\menno\\Documents\\manuel\\università\\Magistrale\\Tesi\\bettin\\map\\venv\\new\\data\\output\\diseaseToNCIT.csv")
    # dictNCIT = {}
    # for i, r in diseaseToNCIT.iterrows():
    #     dictNCIT.update({str(r[0]).strip(): str(r[1])})

    for index, row in madridData.iterrows():

        patientId = patientCodes[row["patient_id"]]

        # if the patient presents a comorbidity in the cell and the value in the cell is '0'. (if it were '1', that means
        # that no comorbidity is present
        if not pd.isna(row["comorbidities___0"]) and row["comorbidities___0"] == 0:

            # observe that a patient may have more than one comorbidity
            # (it is in the nature of a comorbidity, actually)
            # thus the following is correctly a list of if, and not else if

            protocolEventId = idg.getURI() # create the URI of the protocol event that is this comorbidity

            # I moved these five lines outside the single ifs - create the protocol event where the patient registered their comorbidities
            g.add((protocolEventId, RDF.type, BTO_schema[ncit.PROTOCOL_EVENT]))
            g.add((patientId, BTO_schema["undergo"], protocolEventId))
            # create the comorbidity instance and link it to the protocol event that registered it
            comorbidityId = idg.getURI()
            g.add((comorbidityId, RDF.type, BTO_schema[ncit.COMORBIDITY]))
            g.add((protocolEventId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))

            if not pd.isna(row["comorbidities___1"]) and row["comorbidities___1"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["anxiety"]))
            if not pd.isna(row["comorbidities___2"]) and row["comorbidities___2"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Anxiety"]))
            if not pd.isna(row["comorbidities___3"]) and row["comorbidities___3"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Depression"]))
            if not pd.isna(row["comorbidities___3"]) and row["comorbidities___3"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Hypertension"]))
            if not pd.isna(row["comorbidities___4"]) and row["comorbidities___4"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Dyslipidemia"]))
            if not pd.isna(row["comorbidities___5"]) and row["comorbidities___5"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Diabetes_Mellitus"]))
            if not pd.isna(row["comorbidities___6"]) and row["comorbidities___6"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Obesity"]))
            if not pd.isna(row["comorbidities___7"]) and row["comorbidities___7"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Hyperthyroidism"]))
            if not pd.isna(row["comorbidities___8"]) and row["comorbidities___8"] == 1:
                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni["Hypothyroidism"]))
            if not pd.isna(row["other_comorbidity"]):
                # TODO NB we are setting a generic comorbidity_description property that is not present, as of today, in the ontology
                # the alternative is to parse the field and automatically find the correct disease to be linked here
                g.add((comorbidityId, BTO_schema["comorbidity_description"], Literal(row["other_comorbidity"], lang="en")))



            # TODO: add the parser for treatment (but first I have to import all the medicine required in the ontology)
            # for now it is added as a string
            if not pd.isna(row["comorbidity_treatment"]):
                g.add((comorbidityId, BTO_schema["treatment"], Literal(row["comorbidity_treatment"], lang="en")))

        '''
        ///TODO: see google keeps notes, the idea is that those are not comorbidities, so how can I map them in the ontology?? 
        /// should I use "hasDisease ----> Disease ????

        '''

        if not pd.isna(row["other_autoimmune"]) and row["other_autoimmune"] == 1:
            # if the autoimmune disease is present and it is one of those that we considered in our dictionary
            if not pd.isna(row["autoimmune_disease"]) and row["autoimmune_disease"] in dictNCIT:
                disease = dictNCIT[row["autoimmune_disease"]]
                protocolEventId = idg.getURI() # prepre the protocol event
                g.add((protocolEventId, RDF.type, BTO_schema[ncit.PROTOCOL_EVENT]))
                g.add((patientId, BTO_schema["undergo"], protocolEventId))
                comorbidityId = idg.getURI()
                g.add((comorbidityId, RDF.type, BTO_schema[ncit.COMORBIDITY]))
                g.add((protocolEventId, BTO_schema["hasRegisteredComorbidity"], comorbidityId))

                g.add((comorbidityId, BTO_schema["determinedBy"], BTO_ni[disease]))


    return g

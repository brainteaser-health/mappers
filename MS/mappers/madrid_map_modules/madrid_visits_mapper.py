"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#hasher e uuid generator
import mmh3
import uuid
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
#import uri generator
from mappers.utility_scripts import id_generator as idg

def visitsMapper(data, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes):

    for index, row in data.iterrows():

        # patient uri
        patient = patientCodes[row["patient_id"]]

        # create the id for the entity visit
        visitId = idg.getURI()

        #add in the graph a triple specifying that this is a visit to the patient
        g.add((visitId, RDF.type, PO[ncit.PATIENT_VISIT]))

        #create the link between the patient and the visit: we observe behaviour, bmi etc..
        g.add((patient, BTO_schema["undergo"], visitId))

        #create the clinical assessment where we observe: weight, heigth
        clinicalAssessmentId = idg.getURI()
        g.add((clinicalAssessmentId, RDF.type, PO[maxo.CLINICAL_ASSESSMENT]))

        #create the link
        g.add((visitId, BTO_schema["consists"], clinicalAssessmentId))

        #add all the properties
        if not pd.isna(row["height"]):
            g.add((clinicalAssessmentId, BTO_schema["Height"], Literal(row["height"], datatype=XSD.float)))

        if not pd.isna(row["weight"]):
            g.add((clinicalAssessmentId, BTO_schema["Weight"], Literal(row["weight"], datatype=XSD.float)))

        if not pd.isna(row["bmi"]):
            g.add((clinicalAssessmentId, BTO_schema["bmi"], Literal(row["bmi"], datatype=XSD.float)))


        if not pd.isna(row["smoking"]) and row["smoking"] > 0:
            # in our ontology only with the end date we can distinguish between ex and current smoker
            behaviourId = idg.getURI()
            g.add((behaviourId, RDF.type, PO[ncit.SMOKING]))
            g.add((visitId, BTO_schema["hasRegisteredBehaviour"], behaviourId))
        '''
        # we are not modelling this behaviour (alcohol)
        if not pd.isna(row["alcohol"]) and row["alcohol"] > 0:
            # in our ontology only with the end date we can distinguish between ex and current smoker
            behaviourId = idg.getURI()
            g.add((behaviourId, RDF.type, PO[ncit.AL]))
            g.add((visitId, BTO_schema["hasRegisteredBehaviour"], behaviourId))
        '''
        if not pd.isna(row["diet"]):

            if row["diet"] == 0:
                behaviourId = idg.getURI()
                g.add((behaviourId, RDF.type, PO[ncit.LIFESTYLE]))
                g.add((visitId, BTO_schema["hasRegisteredBehaviour"], behaviourId))
                g.add((behaviourId, BTO_schema["diet"], Literal("Mediterranean", lang="en")))
            elif row["diet"] == 3:
                behaviourId = idg.getURI()
                g.add((behaviourId, RDF.type, PO[ncit.LIFESTYLE]))
                g.add((visitId, BTO_schema["hasRegisteredBehaviour"], behaviourId))
                g.add((behaviourId, BTO_schema["diet"], Literal("Other", lang="en")))

        if not pd.isna(row["exercise"]) and row["exercise"]:
            behaviourId = idg.getURI()
            g.add((behaviourId, RDF.type, PO[ncit.PHYSICAL_ACTIVITY]))
            g.add((visitId, BTO_schema["hasRegisteredBehaviour"], behaviourId))
            g.add((behaviourId, BTO_schema["weeklyFrequence"], Literal(row["exercise"], datatype=XSD.integer)))

    return g
"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
#import the uri generator
from mappers.utility_scripts import  id_generator as idg

'''
    Fields involved:
        actual_dmt
        clinical_trial
        start_date
        previous_dmt___0
        previous_dmt___1
        previous_dmt___2
        previous_dmt___3
        previous_dmt___4
        previous_dmt___5
        previous_dmt___6
        previous_dmt___7
        previous_dmt___8
        previous_dmt___9
        previous_dmt___10
        previous_dmt___11
        previous_dmt___12
        start_ifn
        end_date_ifn
        start_glatiramer
        end_date_glatiramer
        start_teri
        end_teri
        start_dimethyl
        end_dimethyl
        start_fingo
        end_fingolimod
        start_cladri
        end_cladribine
        start_sipo
        end_sipo
        start_nata
        end_nata
        start_ocre
        end_ocre
        start_ritu
        end_ritu
        start_alemtu
        end_alemtu
        previous_trial
        disease_modifyng_treatment_complete
        symptoms_treatment___0
        symptoms_treatment___1
        symptoms_treatment___2
        symptoms_treatment___3
        symptoms_treatment___4
        symptoms_treatment___5
        symptoms_treatment___6
        symptoms_treatment___7
'''

def treatmentsMapper(treatments, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes):

    missing_expected_values = 0

    for index, row in treatments.iterrows():

        # retrieve the patient URI
        patientId = patientCodes[row["patient_id"]]
        '''
            Actual DMT: None, Interferon Beta, Glatimer Acetate, Teriflunomide, Dimethyl Fumarate, 
            Fingolimod, Cladribine, Siponimod, Natalizumab, Ocrelizumab, Rituximab, Alemtuzumab, 
            Clinical Trial (treatment on clinical trial) 
        '''
        if row["actual_dmt"] == 0:
            None

        elif row["actual_dmt"] > 0:
            # this section moved from the various elif
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))

            # add the start date
            if not pd.isna(row["start_date"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_date"].date(), datatype=XSD.date)))
            # =======

            if row["actual_dmt"] == 1:
                # interferon beta-1a
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L03AB07"]))
            elif row["actual_dmt"] == 2:
                # Glatimer Acetate
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L03AX13"]))
            elif row["actual_dmt"] == 3:
                # Teriflunomide
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA31"]))
            elif row["actual_dmt"] == 4:
                # Dimethyl Fumarate
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AX07"]))
            elif row["actual_dmt"] == 5:
                # Fingolimod
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA27"]))
            elif row["actual_dmt"] == 6:
                # Cladribine
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L01BB04"]))
            elif row["actual_dmt"] == 7:
                # Siponimod
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA42"]))
            elif row["actual_dmt"] == 8:
                # Natalizumab
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA23"]))
            elif row["actual_dmt"] == 9:
                # Ocrelizumab
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA36"]))
            elif row["actual_dmt"] == 10:
                # Rituximab
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L01XC02"]))
            elif row["actual_dmt"] == 11:
                # Alemtuzumab
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA34"]))
            elif row["actual_dmt"] == 12:
                if not pd.isna(row["other_treatment"]):
                    g.add((administrationId, BTO_schema["medicineName"], Literal(row["other_treatment"], datatype=XSD.string)))
                else:
                    missing_expected_values += 1
                    print(f'[WARNING] - [DMT] - Actual dmt set on "other" but no information about them are available\tindex : {index}\tcolumn : "other_treatment"')
                    # in this case I add a generic "other" treatment to not lose information
                    g.add((administrationId, BTO_schema["medicineName"], Literal("other", datatype=XSD.string)))

        if not pd.isna(row["actual_dmt"]):
            pass


        # Important note for future: actual_dmt == 13 means that the patient is under some clinical trial for the therapy, but the medicine is not available since it's still under study. We decided, together with the clinitians, to not treat this case.

        if not pd.isna(row["previous_dmt___1"]) and row["previous_dmt___1"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # interferon beta-1a
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L03AB07"]))

            if not pd.isna(row["start_ifn"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_ifn"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_date_ifn"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_date_ifn"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___2"]) and row["previous_dmt___2"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Glatimer Acetate
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L03AX13"]))

            if not pd.isna(row["start_glatiramer"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_glatiramer"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_date_glatiramer"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_date_glatiramer"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___3"]) and row["previous_dmt___3"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Teriflunomide
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA31"]))

            if not pd.isna(row["start_teri"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_teri"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_teri"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_teri"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___4"]) and row["previous_dmt___4"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Dimethyl Fumarate
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AX07"]))

            if not pd.isna(row["start_dimethyl"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_dimethyl"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_dimethyl"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_dimethyl"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___5"]) and row["previous_dmt___5"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Fingolimod
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA27"]))

            if not pd.isna(row["start_fingo"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_fingo"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_fingolimod"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_fingolimod"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___6"]) and row["previous_dmt___6"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Cladribine
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L01BB04"]))

            if not pd.isna(row["start_cladri"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_cladri"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_cladribine"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_cladribine"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___7"]) and row["previous_dmt___7"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Siponimod
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA42"]))

            if not pd.isna(row["start_sipo"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_sipo"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_sipo"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_sipo"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___8"]) and row["previous_dmt___8"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Natalizumab
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA23"]))

            if not pd.isna(row["start_nata"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_nata"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_nata"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_nata"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___9"]) and row["previous_dmt___9"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Ocrelizumab
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA36"]))

            if not pd.isna(row["start_ocre"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_ocre"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_ocre"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_ocre"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___10"]) and row["previous_dmt___10"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Rituximab
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L01XC02"]))

            if not pd.isna(row["start_ritu"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_ritu"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_ritu"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_ritu"].date(), datatype=XSD.date)))

        if not pd.isna(row["previous_dmt___11"]) and row["previous_dmt___11"] == 1:
            # create the protocol event
            protocolEventId = idg.getURI()

            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            # link the patient to the protocol event
            g.add((patientId, BTO_schema["undergo"], protocolEventId))

            therapeuticProcedureId = idg.getURI()
            g.add((therapeuticProcedureId, RDF.type, BTO_schema["Disease_Modifying_Therapy"]))
            g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

            administrationId = idg.getURI()
            g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            # Alemtuzumab
            g.add((administrationId, BTO_schema["isRelated"], BTO_ni["L04AA34"]))

            if not pd.isna(row["start_alemtu"]):
                g.add((therapeuticProcedureId, BTO_schema["startDate"], Literal(row["start_alemtu"].date(), datatype=XSD.date)))
            if not pd.isna(row["end_alemtu"]):
                g.add((therapeuticProcedureId, BTO_schema["endDate"], Literal(row["end_alemtu"].date(), datatype=XSD.date)))

        if not pd.isna(row["symptoms_treatment___0"]) and row["symptoms_treatment___0"] == 1:

            None

        else:
            # Case: Famprydine
            if not pd.isna(row["symptoms_treatment___1"]) and row["symptoms_treatment___1"] == 1:
                # create the protocol event
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))

                therapeuticProcedureId = idg.getURI()
                g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
                g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

                administrationId = idg.getURI()
                g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
                g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
                # Famprydine
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["N07XX07"]))

            # Case: Baclofen
            if not pd.isna(row["symptoms_treatment___2"]) and row["symptoms_treatment___2"] == 1:
                # create the protocol event
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))

                therapeuticProcedureId = idg.getURI()
                g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
                g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

                administrationId = idg.getURI()
                g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
                g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
                # Baclofen
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["M03BX01"]))

            # Case: Amantadine
            if not pd.isna(row["symptoms_treatment___3"]) and row["symptoms_treatment___3"] == 1:
                # create the protocol event
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))

                therapeuticProcedureId = idg.getURI()
                g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
                g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

                administrationId = idg.getURI()
                g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
                g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
                # Amantadine
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["N04BB01"]))

            # Case: Modafinil
            if not pd.isna(row["symptoms_treatment___4"]) and row["symptoms_treatment___4"] == 1:
                # create the protocol event
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))

                therapeuticProcedureId = idg.getURI()
                g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
                g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

                administrationId = idg.getURI()
                g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
                g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
                # Modafinil
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["N06BA07"]))

            # Case: Carbamazepine
            if not pd.isna(row["symptoms_treatment___5"]) and row["symptoms_treatment___5"] == 1:
                # create the protocol event
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))

                therapeuticProcedureId = idg.getURI()
                g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
                g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

                administrationId = idg.getURI()
                g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
                g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
                # Carbamazepine
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni["N03AF01"]))

            # Case: Treatment for urinary incontinence
            # if not pd.isna(row["symptoms_treatment___6"]) and row["symptoms_treatment___6"] == 1:
            #     # create the protocol event
            #     protocolEventId = idg.getURI()
            #
            #     g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
            #     # link the patient to the protocol event
            #     g.add((patientId, BTO_schema["undergo"], protocolEventId))
            #
            #     therapeuticProcedureId = idg.getURI()
            #     g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
            #     g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))
            #
            #     administrationId = idg.getURI()
            #     g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
            #     g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
            #     # Treatment for urinary incontinence - we do not know this yet
            #     g.add((administrationId, BTO_schema["isRelated"], BTO_ni["..."]))



            # Case: Others - apparently there is not a 'symptoms treatment 7'
            '''
            if not pd.isna(row["symptoms_treatment___7"]) and row["symptoms_treatment___7"] == 1:
                # create the protocol event
                protocolEventId = idg.getURI()

                g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
                # link the patient to the protocol event
                g.add((patientId, BTO_schema["undergo"], protocolEventId))

                therapeuticProcedureId = idg.getURI()
                g.add((therapeuticProcedureId, RDF.type, PO[ncit.THERAPEUTIC_PROCEDURE]))
                g.add((protocolEventId, BTO_schema["consists"], therapeuticProcedureId))

                administrationId = idg.getURI()
                g.add((administrationId, RDF.type, PO[ncit.ADMINISTRATION]))
                g.add((therapeuticProcedureId, BTO_schema["hasAdministration"], administrationId))
                # Others
                g.add((administrationId, BTO_schema["isRelated"], BTO_ni[""]))
            '''

    return g
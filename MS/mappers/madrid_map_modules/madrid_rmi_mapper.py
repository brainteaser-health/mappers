"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
from rdflib import RDF, Literal, XSD

from mappers.utility_scripts import  id_generator as idg
from mappers.codes import ncit_codes as ncit # ncit ontological codes

import pandas as pd



def madrid_rmi_mapping(data, g, BTO_schema, PO, patientCodes):
    """
    Maps the Sermas data to take the information about the MRI (Magnetic Resonance Imaging)
    :param data:
    :param g:
    :param BTO_schema:
    :param BTO_ni:
    :param PO:
    :param patientCodes:
    :return:
    """

    for index, row in data.iterrows():
        # first, we need to check if there is something to extract here (i.e., if there is an MRI at all)
        if not check_for_presence_of_mri(row): # nothing to see here, move to the next row
            continue

        # retrieve the patient URI - to know who we are dealing with
        patientId = patientCodes[row["patient_id"]]

        # the MRI is something that happens during a protocol event, we create one URI for it
        protocolEventId = idg.getURI("madrid", "rmi", str(index))
        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
        g.add((patientId, BTO_schema["undergo"], protocolEventId)) # link the event to the patient

        # we create the MRI instance and link it to the protocol event
        rmiId = idg.getURI()
        g.add((rmiId, RDF.type, PO[ncit.MAGNETIC_RESONANCE_IMAGING]))
        g.add((protocolEventId, BTO_schema["consists"], rmiId))

        # set the date
        set_madrid_mri_date(BTO_schema, g, protocolEventId, rmiId, row)

        # the field 'firts_mri' is just a placeholder for the system used in Sermás to tell us that the
        # data are present in the next fields

        # the number of lesions found by using Gadolinium
        if not pd.isna(row["gadolinium_2"]):
            if row["gadolinium_2"] >= 0:
                g.add((rmiId, BTO_schema["numberOfLesionsT1-Gadolinium"], Literal(row["gadolinium_2"], datatype=XSD.integer)))
            else:
                print(f'[WARNING] - Unexpected negative value (= {row["gadolinium_2"]}) in field "Numero di lesioni in T1 captanti Gadolinio" at index: {index}')

        set_burden_t1(BTO_schema, g, rmiId, row)

        set_burden_t2(BTO_schema, g, rmiId, row)

        set_brain_atrophy(BTO_schema, g, rmiId, row)

        # todo for now we were told to ignore this data
        # set_spinal_lesion(BTO_schema, g, rmiId, row)

def check_for_presence_of_mri(row):
    """
    Returns true if there is some information in the row for RMI,
    false otherwise
    :param row:
    :return: true if there is some information concerning an MRI, false otherwise
    """
    if pd.isna(row["baseline_mri"]) \
            and pd.isna(row["burden_t1"]) \
            and pd.isna(row["burden_t2"]) \
            and pd.isna(row["gadolinium_2"]) \
            and pd.isna(row["brain_atrophy"]):
            # and pd.isna(row["spinal_lesion"]):
        return False
    return True



# todo We do not know if this mapping is correct
def set_spinal_lesion(BTO_schema, g, rmiId, row):
    if not pd.isna(row["spinal_lesion"]):
        if row["spinal_lesion"] == 0:
            g.add((rmiId, BTO_schema["spinal_lesion"],
                   Literal("yes")))
        elif row["spinal_lesion"] == 1:
            g.add((rmiId, BTO_schema["spinal_lesion"],
                   Literal("no")))
        elif row["spinal_lesion"] == 2:
            g.add((rmiId, BTO_schema["spinal_lesion"],
                   Literal("not available")))
        # elif row["spinal_lesion"] == 3:
        #     g.add((rmiId, BTO_schema["spinal_lesion"],
        #            Literal("not available")))
        else:
            print("[WARNING] - RMI - unrecognized spinal lesion code: " + str(row["spinal_lesion"]))


def set_brain_atrophy(BTO_schema, g, rmiId, row):
    if not pd.isna(row["brain_atrophy"]):
        if row["brain_atrophy"] == 0:
            g.add((rmiId, BTO_schema["brain_atrophy"],
                   Literal("none")))
        elif row["brain_atrophy"] == 1:
            g.add((rmiId, BTO_schema["brain_atrophy"],
                   Literal("mild")))
        elif row["brain_atrophy"] == 2:
            g.add((rmiId, BTO_schema["brain_atrophy"],
                   Literal("moderate")))
        elif row["brain_atrophy"] == 3:
            g.add((rmiId, BTO_schema["brain_atrophy"],
                   Literal("severe")))
        else:
            print("[WARNING] - RMI - unrecognized brain atrophy code: " + str(row["brain_atrophy"]))


def set_burden_t2(BTO_schema, g, rmiId, row):
    # set burden of type t2
    if not pd.isna(row["burden_t2"]):
        if row["burden_t2"] < 9:
            g.add((rmiId, BTO_schema["t2_burden"],
                   Literal("mild")))
        elif 9 <= row["burden_t2"] <= 15:
            g.add((rmiId, BTO_schema["t2_burden"],
                   Literal("moderate")))
        elif row["burden_t2"] > 15:
            g.add((rmiId, BTO_schema["t2_burden"],
                   Literal("high")))
        else:
            print("[WARNING] - RMI - unrecognized burden of type 2 code: " + str(row["burden_t2"]))


def set_burden_t1(BTO_schema, g, rmiId, row):
    # the presence of burden of type T1
    if not pd.isna(row["burden_t1"]):
        if row["burden_t1"] == 1:
            g.add((rmiId, BTO_schema["t1_burden"],
                   Literal("mild")))
        elif row["burden_t1"] == 2:
            g.add((rmiId, BTO_schema["t1_burden"],
                   Literal("moderate")))
        elif row["burden_t1"] == 3:
            g.add((rmiId, BTO_schema["t1_burden"],
                   Literal("high")))
        else:
            print("[WARNING] - RMI - unrecognized spinal lesion code: " + str(row["burden_t1"]))


def set_madrid_mri_date(BTO_schema, g, protocolEventId, rmiId, row):
    """ Sets the value of the column baseline_mri as the date (startDate, to be more precise)
    of the protocol event where the MRI was registered, and as the startDate of the MRI instance itself

    """
    # set the date of the MRI
    if not pd.isna(row["baseline_mri"]):
        g.add((protocolEventId, BTO_schema["startDate"], Literal(row["baseline_mri"].date(), datatype=XSD.date)))
        g.add((rmiId, BTO_schema["startDate"], Literal(row["baseline_mri"].date(), datatype=XSD.date)))




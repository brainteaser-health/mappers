"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
#import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
#import uri generator
from mappers.utility_scripts import id_generator as idg

'''
    Field related to pregnancies:
        pregnancy
        pregnancy_status
        pregnancy_complications___0
        pregnancy_complications___1
        pregnancy_complications___2
        pregnancy_complications___3
        date_delivery
        previous_pregnancies
        number_previous
        previous_complications___0
        previous_complications___1
        previous_complications___2
        previous_complications___3
        previous_abortions
        
    Looks like that for previous pregnancies we have just info about if happend that the patient had any complications,
    but we cannot relate those complication to a particular pregnancy
'''

def pregnanciesMapper(madridData, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes):

    for index, row in madridData.iterrows():

        patientId = patientCodes[row["patient_id"]]

        if not pd.isna(row["pregnancy"]) and row["pregnancy"] == 1:
            pregnancyId = idg.getURI()
            g.add((pregnancyId, RDF.type, PO[ncit.PREGNANCY]))

            protocolEventId = idg.getURI()
            g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))

            g.add((patientId, BTO_schema["undergo"], protocolEventId))
            g.add((protocolEventId, BTO_schema["hasPregnancy"], pregnancyId))

            '''
                Pregnancy Status (Ongoing, Abortion, Delivery (date), Lactation) 
            '''
            if pd.isna(row["pregnancy_status"]) and ( row["pregnancy_status"] == 2 or row["pregnancy_status"] == 3 ):
                g.add((pregnancyId, BTO_schema["endEvent"], Literal("Delivery", lang="en")))
            elif pd.isna(row["pregnancy_status"]) and ( row["pregnancy_status"] == 1 ):
                g.add((pregnancyId, BTO_schema["endEvent"], Literal("Abortion", lang="en")))

            '''
                Pregnancy Complications: None, Maternal, Newborn, Unknown
            '''
            if not pd.isna(row["pregnancy_complications___0"]) and row["pregnancy_complications___0"] == 1:
                # a '1' in pregnancy_complications__0 means that a complication is absent.
                if pd.isna(row["pregnancy_complications___1"]) and row["pregnancy_complications___1"] == 1:
                    print(print(f'[WARNING]-Pregnancy\'s data inconsistency observed! - id: {index}'))
                elif pd.isna(row["pregnancy_complications___2"]) and row["pregnancy_complications___2"] == 1:
                    print(print(f'[WARNING]-Pregnancy\'s data inconsistency observed! - id: {index}'))
                elif pd.isna(row["pregnancy_complications___3"]) and row["pregnancy_complications___3"] == 1:
                    print(print(f'[WARNING]-Pregnancy\'s data inconsistency observed! - id: {index}'))
                # I put "None" to register that fact, because for the open world assumption if a tuple is missing we cannot infer that it's false!
                g.add((pregnancyId, BTO_schema["complications"], Literal("None", lang="en")))
            elif not pd.isna(row["pregnancy_complications___1"]) and row["pregnancy_complications___1"] == 1:
                g.add((pregnancyId, BTO_schema["complications"], Literal("Maternal", lang="en")))
            elif not pd.isna(row["pregnancy_complications___2"]) and row["pregnancy_complications___2"] == 1:
                g.add((pregnancyId, BTO_schema["complications"], Literal("Newborn", lang="en")))
            elif not pd.isna(row["pregnancy_complications___3"]) and row["pregnancy_complications___3"] == 1:
                # In this case I can put nothing because the information in unknown
                None

            if pd.isna(row["date_delivery"]):
                g.add((pregnancyId, BTO_schema["endDate"], Literal(row["date_delivery"].date(), datatype=XSD.date)))

            '''
                Previous pregnancies:
                
                Previous Pregnancies. Number of Previous Uncomplicated Pregnancies.
                Previous Pregnancies Complications (None, Maternal, Newborn, Unknown) 
                Number of Previous Abortions 
                
                To be asked (see google keep's note)
            '''
            #if pd.isna(row["previous_pregnancies"]) and row["previous_pregnancies"] == 1:


    return g
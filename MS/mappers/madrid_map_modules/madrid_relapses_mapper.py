"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""

import pandas as pd

from rdflib import RDF, Literal
from mappers.codes import ncit_codes as ncit
from rdflib.namespace import FOAF, XSD, RDFS
from mappers.utility_scripts import id_generator as idg


def relapseMapper(data, g, BTO_schema, PO, patientCodes):
    """ Takes the information about the relapse for Madrid
    """
    for index, row in data.iterrows():
        # take the patient uri
        idPatient = patientCodes[row["patient_id"]]
        # the relapse is registered during a protocol event. Create it and add it to the graph
        protocolEventId = idg.getURI("madrid", "relapse", str(index))
        g.add((protocolEventId, RDF.type, PO[ncit.PROTOCOL_EVENT]))
        # link the patient to the event
        g.add((idPatient, BTO_schema["undergo"], protocolEventId))
        # add the date
        g.add((protocolEventId, BTO_schema["startDate"], Literal(row["last_relapse"].date(), datatype=XSD.date)))

        # create the entity relapse and link it to the protocol event
        relapseId = idg.getURI()
        g.add((relapseId, RDF.type, PO[ncit.RELAPSE]))
        g.add((protocolEventId, BTO_schema["hasRelapse"], relapseId))
        # now we set values true/false on the considered relapse
        set_relapse(BTO_schema, g, relapseId, row)


def set_relapse(BTO_schema, g, relapseId, row):
    # specify the type of the relapse
    if (not pd.isna(row["type_last___1"])) or row["type_last___1"] == 0:  # if option 1 not present or set to 0
        g.add((relapseId, BTO_schema["vision_relapse"], Literal("false", datatype=XSD.boolean)))
    elif row["type_last___1"] == 1:
        g.add((relapseId, BTO_schema["vision_relapse"], Literal("true", datatype=XSD.boolean)))

    if (not pd.isna(row["type_last___2"])) or row["type_last___2"] == 0:  # if option 1 not present or set to 0
        bol = Literal("false", datatype=XSD.boolean)
    elif row["type_last___2"] == 1:
        bol = Literal("true", datatype=XSD.boolean)
    g.add((relapseId, BTO_schema["brainstem_relapse"], bol))

    if (not pd.isna(row["type_last___3"])) or row["type_last___3"] == 0:  # if option 1 not present or set to 0
        g.add((relapseId, BTO_schema["pyramidal_relapse"], Literal("false", datatype=XSD.boolean)))
    elif row["type_last___3"] == 1:
        g.add((relapseId, BTO_schema["pyramidal_relapse"], Literal("true", datatype=XSD.boolean)))

    if (not pd.isna(row["type_last___4"])) or row["type_last___4"] == 0:  # if option 1 not present or set to 0
        g.add((relapseId, BTO_schema["cerebellum_relapse"], Literal("false", datatype=XSD.boolean)))
    elif row["type_last___4"] == 1:
        g.add((relapseId, BTO_schema["cerebellum_relapse"], Literal("true", datatype=XSD.boolean)))

    if (not pd.isna(row["type_last___5"])) or row["type_last___5"] == 0:  # if option 1 not present or set to 0
        g.add((relapseId, BTO_schema["sensory_relapse"], Literal("false", datatype=XSD.boolean)))
    elif row["type_last___5"] == 1:
        g.add((relapseId, BTO_schema["sensory_relapse"], Literal("true", datatype=XSD.boolean)))

# todo ask SERMAS about these
    # BOWEL and BLADDER
    if (not pd.isna(row["type_last___6"])) or row["type_last___6"] == 0:  # if option 1 not present or set to 0
        g.add((relapseId, BTO_schema["sphincter_relapse"], Literal("false", datatype=XSD.boolean)))
    elif row["type_last___6"] == 1:
        g.add((relapseId, BTO_schema["sphincter_relapse"], Literal("true", datatype=XSD.boolean)))


    # CEREBRAL
    if (not pd.isna(row["type_last___7"])) or row["type_last___7"] == 0:  # if option 1 not present or set to 0
        g.add((relapseId, BTO_schema["psychic_relapse"], Literal("false", datatype=XSD.boolean)))
    elif row["type_last___1"] == 1:
        g.add((relapseId, BTO_schema["psychic_relapse"], Literal("true", datatype=XSD.boolean)))

"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
from mappers.codes import omit_codes as omit
# import uri generator
from mappers.utility_scripts import id_generator as idg

'''
    Fields related to the evoked potential test:
        vep ---> Visual 
        sep ---> Sensitive
        mep ---> Motor
        
    Field related to Optical Coherence Tomography:
        oct
'''


def evokedPotentialsMapper(madridData, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes):
    for index, row in madridData.iterrows():
        patientId = patientCodes[row["patient_id"]]
        if (pd.isna(row["vep"]) and row["vep"] > 0) or (not pd.isna(row["sep"]) and row["sep"] > 0) or (
                not pd.isna(row["mep"]) and row["mep"] > 0):
            visitId = idg.getURI()
            g.add((visitId, RDF.type, PO[ncit.PATIENT_VISIT]))
            g.add((patientId, BTO_schema["undergo"], visitId))
            # Visual
            if not pd.isna(row["vep"]) and row["vep"] == 1:
                epId = idg.getURI()
                g.add((epId, RDF.type, PO[omit.VISUAL_EVOKED_POTENTIALS]))
                g.add((visitId, BTO_schema["consists"], epId))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))
            elif not pd.isna(row["vep"]) and row["vep"] == 2:
                epId = idg.getURI()
                g.add((epId, RDF.type, PO[omit.VISUAL_EVOKED_POTENTIALS]))
                g.add((visitId, BTO_schema["consists"], epId))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))

            # Sensitive
            if not pd.isna(row["sep"]) and row["sep"] == 1:
                epId = idg.getURI()
                g.add((epId, RDF.type, PO[omit.SOMATOSENSORY_EVOKED_POTENTIALS]))
                g.add((visitId, BTO_schema["consists"], epId))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))
            elif not pd.isna(row["sep"]) and row["sep"] == 2:
                epId = idg.getURI()
                g.add((epId, RDF.type, PO[omit.SOMATOSENSORY_EVOKED_POTENTIALS]))
                g.add((visitId, BTO_schema["consists"], epId))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))

            # Motor
            if not pd.isna(row["mep"]) and row["mep"] == 1:
                epId = idg.getURI()
                g.add((epId, RDF.type, PO[omit.MOTOR_EVOKED_POTENTIALS]))
                g.add((visitId, BTO_schema["consists"], epId))
                g.add((epId, BTO_schema["potentialValue"], Literal("normal", lang="en")))
            elif not pd.isna(row["mep"]) and row["mep"] == 2:
                epId = idg.getURI()
                g.add((epId, RDF.type, PO[omit.MOTOR_EVOKED_POTENTIALS]))
                g.add((visitId, BTO_schema["consists"], epId))
                g.add((epId, BTO_schema["potentialValue"], Literal("altered", lang="en")))

            # Optical Coherence Tomography
            if not pd.isna(row["oct"]) and row["oct"] == 1:
                OctId = idg.getURI()
                g.add((OctId, RDF.type, URIRef("https://uts.nlm.nih.gov/uts/umls/concept/C0920367")))
                g.add((visitId, BTO_schema["consists"], OctId))
                g.add((OctId, BTO_schema["octValue"], Literal("normal", lang="en")))
            elif not pd.isna(row["oct"]) and row["oct"] == 2:
                OctId = idg.getURI()
                g.add((OctId, RDF.type, URIRef("https://uts.nlm.nih.gov/uts/umls/concept/C0920367")))
                g.add((visitId, BTO_schema["consists"], OctId))
                g.add((OctId, BTO_schema["octValue"], Literal("altered", lang="en")))
    return g

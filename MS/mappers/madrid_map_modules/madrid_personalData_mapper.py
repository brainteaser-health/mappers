"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# hasher e uuid generator
import mmh3
import uuid
# import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import umls_codes as umls
from mappers.namespaces.ontologies import PO, UMLS_Cities, UMLS_Towns, UMLS_Rural_area

from mappers.utility_scripts import id_generator as idg
from mappers.utility_scripts.SermasUrbanDictionary import SermasUrbanDictionary

'''
    -Given a tab patients as a dataframe, an rdf graph and all the namespace required this function will provide
     the mapping of all the entries in patients into a graph database according to the schema defined in the brainteaser ontology.
'''


def set_patient_places(BTO_ni, BTO_schema, sermas_urban_dictionary, g, patient, index, row):
    """
    Deals with the places connected to the patient: current location birthplace, previous location.

    :param BTO_ni:
    :param BTO_schema:
    :param sermas_urban_dictionary:
    :param g:
    :param patient:
    :param index:
    :param row:
    :return: unknown_residence_zip (1 if the current location is unknown), null_zip (1 if no zip code is available),
    null_birthplace (1 if there is no place_borth), null_previous_zip (1 if there is no previous zip cpde)
    """
    unknown_postal_codes = 0
    missing_living_years = 0
    missing_postal_codes = 0

    unknown_residence_zip = 0  # describes if the residence zip is available or not
    null_zip = 0
    null_birtplace = 0
    null_previous_zip = 0
    unknown_previous_zip = 0

    degurba_dict = sermas_urban_dictionary.zip_degree_dictionary

    # get the hash corresponding to the place where this patient was born
    zip = ""  # cap
    current_residence_name = ""

    birthplace = ""
    previous_zip = ""
    previous_place_name = ""

    # take the zip code - place of current residence of the patient
    if not pd.isna(row["zip"]):
        zip = row["zip"]
        current_residence_name = sermas_urban_dictionary.find_placename_from_zip(zip)
    # else:  # no zip code immediately available
    #     if not pd.isna(row["place_birth"]):
    #         # try to recover with the birthplace
    #         zip = sermas_urban_dictionary.find_zip_from_placename(row["place_birth"])

    # keep trace of the fact we have no zip code
    if zip == "":
        null_zip = 1

    # take the place of birth
    if not pd.isna(row["place_birth"]):
        birthplace = row["place_birth"]
    else:  # no placename immediately available
        if not pd.isna(row["zip"]):  # try to recover with the zip
            birthplace = sermas_urban_dictionary.find_placename_from_zip(row["zip"])

    # keep trace of the fact we have no place of birth
    if birthplace == '':
        null_birtplace = 1

    # take the previous residence place
    if not pd.isna(row["previous_pc"]):
        previous_zip = row["previous_pc"]
        try:
            previous_place_name = sermas_urban_dictionary.find_placename_from_zip(int(previous_zip))
        except:
            pass

    # keep trace of the fact there is no previous residence zip code
    if previous_zip == '':
        null_previous_zip = 1

    current_residence_hash = sermas_urban_dictionary.get_hash_value(zip, current_residence_name)
    birthplace_hash = sermas_urban_dictionary.get_hash_value("", birthplace)
    previous_hash = sermas_urban_dictionary.get_hash_value(previous_zip, "")

    # link the patient to the current residence
    placeId = ""
    if current_residence_hash != "":
        # we create a named individual with URL composed by the hash value
        g.add((patient, BTO_schema['hasResidence'], BTO_ni[current_residence_hash]))


        # older solution where we instantiated. Kept for safety reasons
        # placeId = idg.getURI()
        # g.add((patient, BTO_schema['hasResidence'], placeId))
        # g.add((placeId, BTO_schema["isCurrent"], Literal("True", datatype=XSD.boolean)))
        # g.add((placeId, BTO_schema["hasHash"], Literal(current_residence_hash)))


        place = degurba_dict.get(zip, "")
        if place == '':  # there is the possibility that the zip is not linked to a place type. We do another try with the residence
            place = sermas_urban_dictionary.place_degree_dictionary.get(current_residence_name, '')

        if place != '':
            set_place_type(BTO_ni, place, g, birthplace_hash, birthplace, BTO_ni[current_residence_hash])
        # g.add((patient, BTO_schema["hasBirthplace"], BTO_ni[hash]))
        # if not pd.isna(row["years_current_pc"]):  # set the time the patient has been there -- we decided to ignore this information
        #     g.add(
        #         (placeId, BTO_schema["residenceYears"], Literal(row["years_current_pc"], datatype=XSD.integer)))
    else:
        print("[WARNING] we were unable to create the residence place for patient of id: " + patient
              + "\nzip was" + str(zip) + " placename was " + current_residence_name)
        unknown_residence_zip = 1

    # now we link the patient to its birthplace (if present)
    # now we link the birthplace to its type (if available)
    if birthplace_hash != "":
        g.add((patient, BTO_schema['hasBirthplace'], BTO_ni[birthplace_hash]))

        # placeId = idg.getURI()  # create an id for the birthplace
        # g.add((patient, BTO_schema['hasBirthplace'], placeId))  # add it to the patient
        # g.add((placeId, BTO_schema["hasHash"], Literal(birthplace_hash)))  # add it its hash
        # now link the birthplace to its type

        place = sermas_urban_dictionary.place_degree_dictionary.get(birthplace.lower(), "")
        if place != '':
            set_place_type(BTO_ni, place, g, birthplace_hash, birthplace, BTO_ni[birthplace_hash])
        else:
            ret = try_special_cases(degurba_dict, birthplace)
            if ret != "":
                set_place_type(BTO_ni, ret, g, birthplace_hash, birthplace, BTO_ni[birthplace_hash])
            else:
                print(
                    "[WARNING] unable to give a type to birthplace : " + birthplace)
    else:
        print("[WARNING] no birthplace available for patient : " + patient)


    # we decide to ignore the previous postal code

    # finally, set the previous residence
    # first, set the previous location of the patient - I know, this little piece of code here is obnoxious
    # if not pd.isna(row["previous_pc"]):  # previous postal code
    #     previous_hash = sermas_urban_dictionary.get_hash_value(row["previous_pc"], previous_place_name)
    #     if not pd.isna(row["years_previous_pc"]):
    #         if row["years_previous_pc"] + row["years_current_pc"] == row["age"]:
    #             if previous_place_name != '':
    #                 if sermas_urban_dictionary.place_degree_dictionary.get(previous_place_name, '') == "town or suburbs":
    #                     placeId = idg.getURI()
    #                     g.add((placeId, RDF.type, PO[umls.TOWNS]))
    #                     if previous_hash != "":
    #                         g.add((placeId, BTO_schema["hasHash"], Literal(previous_hash)))
    #                     if pd.isna(row["years_previous_pc"]):
    #                         g.add((patient, BTO_schema['hasResidence'], placeId))
    #                         if not pd.isna(row["years_previous_pc"]):
    #                             g.add((placeId, BTO_schema["residenceYears"],
    #                                    Literal(row["years_previous_pc"], datatype=XSD.integer)))
    #                         g.add((placeId, BTO_schema["isCurrent"], Literal("false", datatype=XSD.boolean)))
    #                 elif sermas_urban_dictionary.place_degree_dictionary.get(previous_place_name, '') == "city":
    #                     placeId = idg.getURI()
    #                     g.add((placeId, RDF.type, PO[umls.CITIES]))
    #                     if previous_hash != "":
    #                         g.add((placeId, BTO_schema["hasHash"], Literal(previous_hash)))
    #                     if pd.isna(row["years_previous_pc"]):
    #                         g.add((patient, BTO_schema['hasResidence'], placeId))
    #                         if not pd.isna(row["years_previous_pc"]):
    #                             g.add((placeId, BTO_schema["residenceYears"],
    #                                    Literal(row["years_previous_pc"], XSD.integer)))
    #                         g.add((placeId, BTO_schema["isCurrent"], Literal("false", datatype=XSD.boolean)))
    #                 elif sermas_urban_dictionary.place_degree_dictionary.get(previous_place_name, '') == "rural":
    #                     placeId = idg.getURI()
    #                     g.add((placeId, RDF.type, PO[umls.RURAL_AREA]))
    #                     if previous_hash != "":
    #                         g.add((placeId, BTO_schema["hasHash"], Literal(previous_hash)))
    #                     if pd.isna(row["years_previous_pc"]):
    #                         g.add((patient, BTO_schema['hasResidence'], placeId))
    #                         if not pd.isna(row["years_previous_pc"]):
    #                             g.add((placeId, BTO_schema["residenceYears"],
    #                                    Literal(row["years_previous_pc"], datatype=XSD.integer)))
    #                         g.add((placeId, BTO_schema["isCurrent"], Literal("false", datatype=XSD.boolean)))
    #             else:
    #                 print(
    #                     f'[WARNING] - Unknown postal code; Its relative degurba is unavailable\tid : {index};\tvalue : {row["previous_pc"]}\n')
    #                 unknown_postal_codes += 1
    #                 unknown_previous_zip = 1
    #         else:
    #             print("[WARNING] something strange with the age of patient regarding its previous place" + index)
    #     else:
    #         print(f'[WARNING] - [Personal Data] - The living years for the previuos postal code are not available\n')
    #         missing_living_years += 1

    return unknown_residence_zip, null_zip, null_birtplace, null_previous_zip, unknown_previous_zip

    # if not pd.isna(row["zip"]):
    #     # case: the current postal code is the birth one too.
    #     if str(row["zip"]) in degurba_dict:
    #         if degurba_dict[str(row["zip"])] == "town or suburbs":
    #             placeId = idg.getURI()
    #             g.add((placeId, RDF.type, PO[umls.TOWNS]))
    #             g.add((patient, BTO_schema['hasResidence'], placeId))
    #             g.add((patient, BTO_schema['hasBirthplace'], placeId))
    #             g.add((placeId, BTO_schema["isCurrent"], Literal("True", datatype=XSD.boolean)))
    #             if not pd.isna(row["years_current_pc"]):
    #                 g.add(
    #                     (placeId, BTO_schema["residenceYears"], Literal(row["years_current_pc"], datatype=XSD.integer)))
    #         elif degurba_dict[str(row["zip"])] == "city":
    #             placeId = idg.getURI()
    #             g.add((placeId, RDF.type, PO[umls.CITIES]))
    #             g.add((patient, BTO_schema['hasResidence'], placeId))
    #             g.add((patient, BTO_schema['hasBirthplace'], placeId))
    #             g.add((placeId, BTO_schema["isCurrent"], Literal("True", XSD.boolean)))
    #             if not pd.isna(row["years_current_pc"]):
    #                 g.add(
    #                     (placeId, BTO_schema["residenceYears"], Literal(row["years_current_pc"], datatype=XSD.integer)))
    #         elif degurba_dict[str(row["zip"])] == "rural":
    #             placeId = idg.getURI()
    #             g.add((placeId, RDF.type, PO[umls.RURAL_AREA]))
    #             g.add((patient, BTO_schema['hasResidence'], placeId))
    #             g.add((patient, BTO_schema['hasBirthplace'], placeId))
    #             g.add((placeId, BTO_schema["isCurrent"], Literal("True", XSD.boolean)))
    #             if not pd.isna(row["years_current_pc"]):
    #                 g.add(
    #                     (placeId, BTO_schema["residenceYears"], Literal(row["years_current_pc"], datatype=XSD.integer)))
    #     else:
    #         print(
    #             f'[WARNING] - Unknown postal code; Its relative degurba is unavailable\tpatient id : {index};\tzip value : {row["zip"]}\n')
    #         unknown_postal_codes += 1
    # else:
    #     print(f'[WARNING] - Current postal code is missing\n')
    #     missing_postal_codes += 1


def set_place_type(BTO_ni, place, g, hash, placename, place_id):
    # now, let us make this named individual of the correct type
    if place == "city":
        # g.add((BTO_ni[hash], RDF.type, UMLS_Cities))
        g.add((place_id, RDF.type, UMLS_Cities))
    elif place == "town or suburbs":
        # g.add((BTO_ni[hash], RDF.type, UMLS_Towns))
        g.add((place_id, RDF.type, UMLS_Towns))
    elif place == "rural":
        # g.add((BTO_ni[hash], RDF.type, UMLS_Rural_area))
        g.add((place_id, RDF.type, UMLS_Rural_area))
    else:
        print(
            "[WARNING] some error here, this placename was not found among the mapping to town/city/rural area: " + placename)


def try_special_cases(degurbaDict, placename):
    """There are certain cases that I was not able to deal with programmatically
    """

    return ""


def registryMapper(data, g, BTO_schema, BTO_resource, BTO_ni, PO, UMLS, degurba_dict, sermas_urban_dictionary):
    # dictionary for recording the evaluated progression
    observedProgressions = {}
    # dictionary for recording the patient code (so then we don't have to compute it againg)
    patientCodes = {}

    # create the clinical trial entity (retrospective) for madrid
    PV_CLINICAL_TRIAL = "ClinicalTrial_MS_Madrid_1"
    g.add((BTO_ni[PV_CLINICAL_TRIAL], RDF.type, PO[ncit.CLINICAL_TRIAL]))
    g.add((BTO_ni[PV_CLINICAL_TRIAL], BTO_schema["pertain"], URIRef("https://www.comunidad.madrid/servicios/salud/")))

    # counter variables
    unknown_postal_codes = 0
    missing_living_years = 0
    missing_postal_codes = 0

    unknown_residence_zips = 0
    null_zips = 0
    null_birtplaces = 0
    null_previous_zips = 0
    unknown_previous_zips = 0

    # iterate over the patients dataframe
    for index, row in data.iterrows():

        if pd.isna(row['patient_id']):
            continue

        # create patient node to add to the graph
        # since available data are not the entire dataset that we are suppose to have, I don't know if for each patient there will be more than a single row or not.
        # so I'm not going to use the "patient_id" column as index.
        patient = idg.getURI("madrid", "patient", str(row["patient_id"]))
        patientCodes.update({int(row["patient_id"]): patient})

        # add triples using store's add() method.
        # add a patient
        g.add((patient, RDF.type, PO[ncit.PATIENT]))
        g.add((patient, BTO_schema["hasDisease"], BTO_ni["Multiple_Sclerosis"]))

        # check for an autoimmune disease
        # if not pd.isna(row["other_autoimmune"]) and row["other_autoimmune"] == 1: # there is another autoimmune disease
        #     if (not pd.isna(row["autoimmune_disease"])) :
        #         if row["autoimmune_disease"] == "sjoren" : # sjoren syndrome

        # Define a new clinical trial participation
        clinicalTrialParticipationId = idg.getURI()

        g.add((clinicalTrialParticipationId, RDF.type, BTO_schema["Clinical_Trial_Participation"]))
        # add the patient to that clinical trial participation
        g.add((patient, BTO_schema["enrolledIn"], clinicalTrialParticipationId))
        # add the strart date
        if not pd.isna(row["informed_consent"]):
            dateFirstVisit = row["informed_consent"].date()
            g.add((clinicalTrialParticipationId, BTO_schema["startDate"], Literal(dateFirstVisit, datatype=XSD.date)))
        # link the participation entity to the clinical trial
        g.add((clinicalTrialParticipationId, BTO_schema["participate"], BTO_ni[PV_CLINICAL_TRIAL]))

        # add birthday to patient (if not empty)
        if not (pd.isna(row["dob"])):
            g.add((patient, BTO_schema['yearOfBirth'], Literal(row["dob"].year, datatype=XSD.gYear)))

        # add gender to patient (if not empty)
        if not (pd.isna(row['gender'])):
            if row['gender'] == 0:
                g.add((patient, BTO_schema['sex'], Literal("female", lang="en")))
            if row['gender'] == 1:
                g.add((patient, BTO_schema['sex'], Literal("male", lang="en")))

        # MS in pediatric age
        if not pd.isna(row["date_first_symptom"]) and not (pd.isna(row["dob"])):
            onsetAgeDays = row["date_first_symptom"] - row["dob"]
            # 1 year == 365.25 days
            # 18 years == 6574.5 days
            # if onsetAgeDays < 6575 days then MSinPediatricAge
            if onsetAgeDays < pd.Timedelta(6574.5, "days"):
                g.add((patient, BTO_schema["MSInPaediatricAge"], Literal("True", datatype=XSD.boolean)))
            elif onsetAgeDays >= pd.Timedelta(6574.5, "days"):
                g.add((patient, BTO_schema["MSInPaediatricAge"], Literal("False", datatype=XSD.boolean)))

        # add ethnicity to patient (if not empty)
        if not (pd.isna(row["ethnicity"])):

            # Caucasian
            if (row["ethnicity"] == 0):
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Caucasian"]))
            # African
            elif (row["ethnicity"] == 1):
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Black_African"]))
            # Asiatic
            elif (row["ethnicity"] == 2):
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Asiatic"]))
                # Unknown
            elif (row["ethnicity"] == 3):
                g.add((patient, BTO_schema['ethnicity'], BTO_ni["Unknown"]))
            else:
                print(f'[WARNING] - Unknown or not reported ethnicity;\tid : {index};\tvalue : {row["ethnicity"]}\n')

        # set the dates for the patient. Keep track of possible errors
        unknown_residence_zip, null_zip, null_birtplace, null_previous_zip, unknown_previous_zip = set_patient_places(BTO_ni, BTO_schema,
                                                                                                sermas_urban_dictionary,
                                                                                                g, patient,
                                                                                                str(index), row)

        unknown_residence_zips += unknown_residence_zip
        null_zips += null_zip
        null_birtplaces += null_birtplace
        null_previous_zips += null_previous_zip
        unknown_previous_zips += unknown_previous_zip

        if not pd.isna(row["menopause"]) and row["menopause"] == 1:
            g.add((patient, BTO_schema["menopause"], Literal("True", datatype=XSD.boolean)))

        if not pd.isna(row["marital_status"]):
            if row["marital_status"] == 0:
                g.add((patient, BTO_schema["maritalStatus"], Literal("Single", lang="en")))
            elif row["marital_status"] == 1:
                g.add((patient, BTO_schema["maritalStatus"], Literal("Married", lang="en")))
            elif row["marital_status"] == 2:
                g.add((patient, BTO_schema["maritalStatus"], Literal("Widowed", lang="en")))
            elif row["marital_status"] == 3:
                g.add((patient, BTO_schema["maritalStatus"], Literal("Divorced", lang="en")))
            elif row["marital_status"] == 4:
                g.add((patient, BTO_schema["maritalStatus"], Literal("Separated", lang="en")))
            elif row["marital_status"] == 5:
                g.add((patient, BTO_schema["maritalStatus"], Literal("Registered Partnership", lang="en")))

        if not pd.isna(row["family_ms"]) and row["family_ms"] == 1:
            g.add((patient, BTO_schema["hasRelative"], BTO_resource["Relative_with_MS"]))

    # print(patientCodes)

    print("[SUM-UP] we were unable to set a location for these many patients " + str(unknown_residence_zips))
    print("[SUM-UP] missing zip codes " + str(null_zips))
    print("[SUM-UP] missing birthplaces " + str(null_birtplaces))
    print("[SUM-UP] missing previous zip codes " + str(null_previous_zips))
    print("[SUM-UP] unknown number of previous zip codes " + str(unknown_previous_zips))

    return patientCodes

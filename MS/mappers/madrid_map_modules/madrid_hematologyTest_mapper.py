"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""

"""
    BLOOD TEST DATA NOT AVAILABLE YET!

    From documentation:

    "We have not yet added blood test data until
    we reach an agreement with the rest of our
    clinical colleagues on which of all the data
    we carry out on patients we should register
    (since there are many from the diagnosis and
    during the follow-up and the lab analysis are
    different depending on the treatment, clinical
    situation and comorbidities of each individual
    patient)"

"""
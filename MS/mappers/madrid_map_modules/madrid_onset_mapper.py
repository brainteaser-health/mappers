"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
# import uri generator
from mappers.utility_scripts import id_generator as idg

'''
    Fields related to the onset event:
        date_first_symptom ---> onset date
        region_first ---> Symptom
        first_fs___1 ---> Symptom (visual/optic)
        first_fs___2 ---> Symptom (brainstem)
        first_fs___3 ---> Symptom (pyramidal)
        first_fs___4 ---> Symptom (cerebellar)
        first_fs___5 ---> Symptom (sensory)
        first_fs___6 ---> Symptom (bowel and bladder)
        first_fs___7 ---> Symptom (cerebral)
        related_symptoms___0 ---> Symptom (none)
        related_symptoms___1 ---> Symptom (fatigue)
        related_symptoms___2 ---> Symptom (spasticity)
        first_edss
'''


def onsetMapper(madridData, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes):
    for index, row in madridData.iterrows():
        patientId = patientCodes[row["patient_id"]]
        # create the id for the entity onset
        onsetId = idg.getURI()

        # create and add the new entity onset
        g.add((onsetId, RDF.type, PO[ncit.ONSET]))
        # add the date
        if not pd.isna(row["date_first_symptom"]):
            g.add((onsetId, BTO_schema["startDate"], Literal(row["date_first_symptom"].date(), datatype=XSD.date)))

        # add the link between patient and onset
        g.add((patientId, BTO_schema["undergo"], onsetId))

        # Handle region_first --> need to implement a similarity function for parsing values

        # list of possible symptoms for the onset
        if not pd.isna(row["first_fs___1"]) and row["first_fs___1"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Eye_Symptom"]))

        if not pd.isna(row["first_fs___2"]) and row["first_fs___2"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Brainstem_Symptom"]))

        if not pd.isna(row["first_fs___3"]) and row["first_fs___3"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Pyramidal_Symptom"]))

        if not pd.isna(row["first_fs___4"]) and row["first_fs___4"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Cerebellar_Symptom"]))

        if not pd.isna(row["first_fs___5"]) and row["first_fs___5"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Sensory_Disorder"]))

        if not pd.isna(row["first_fs___6"]) and row["first_fs___6"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Bowel_and_Bladder"]))

        if not pd.isna(row["first_fs___7"]) and row["first_fs___7"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Cerebellar_Symptom"]))

        if not pd.isna(row["related_symptoms___1"]) and row["related_symptoms___1"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Fatigue"]))

        if not pd.isna(row["related_symptoms___2"]) and row["related_symptoms___2"] == 1:
            g.add((onsetId, BTO_schema["hasSymptom"], BTO_ni["Spasticity"]))

        if not pd.isna(row["first_edss"]) and row["first_edss"] >= 0:
            first_edssId = idg.getURI()
            g.add((first_edssId, RDF.type, PO[ncit.EDSS]))
            g.add((first_edssId, BTO_schema["totalEDSS"], Literal(row["first_edss"], datatype=XSD.float)))

    return g

"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
# required libraries
import pandas as pd
import datetime
import os
from pathlib import Path
# load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
# import the ncit codes
from mappers.codes import ncit_codes as ncit
from mappers.codes import maxo_codes as maxo
# import uri generator
from mappers.utility_scripts import id_generator as idg

'''
    Fields related to the onset event:
        
'''
def onsetMapper(madridData, g, BTO_schema, BTO_resource, BTO_ni, PO, patientCodes):
    for index, row in madridData.iterrows():
        patientId = patientCodes[row["patient_id"]]
        # create the id for the entity onset
        csfTestId = idg.getURI()

        # add the link between patient and onset
        g.add((patientId, BTO_schema["undergo"], csfTestId))

        # Handle region_first --> need to implement a similarity function for parsing values

        if not pd.isna(row["ocb___0"]) and row["ocb___0"] == 1:
            g.add((csfTestId, BTO_schema["hasSymptom"], BTO_ni["Sensory_Disorder"]))

        if not pd.isna(row["ocb___1"]) and row["ocb___1"] == 1:
            g.add((csfTestId, BTO_schema["hasSymptom"], BTO_ni["Eye_Symptom"]))

        if not pd.isna(row["ocb___2"]) and row["ocb___2"] == 1:
            g.add((csfTestId, BTO_schema["hasSymptom"], BTO_ni["Brainstem_Symptom"]))

        if not pd.isna(row["ocb___3"]) and row["ocb___3"] == 1:
            g.add((csfTestId, BTO_schema["hasSymptom"], BTO_ni["Pyramidal_Symptom"]))

        if not pd.isna(row["ocb___4"]) and row["ocb___4"] == 1:
            g.add((csfTestId, BTO_schema["hasSymptom"], BTO_ni["Cerebellar_Symptom"]))

        if not pd.isna(row["ocb___5"]) and row["ocb___5"] == 1:
            g.add((csfTestId, BTO_schema["hasSymptom"], BTO_ni["Sensory_Disorder"]))


    return g
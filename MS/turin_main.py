
"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""

#### IMPORT LOCAL MODULES ####




import gc
### IMPORT EXTERNAL MODULES ###
import os
import re
import sys

import pandas as pd
from rdflib import Graph

# namespaces
from mappers.namespaces import ontologies
from mappers.turin_map_modules import turin_clinicalEvents_mapper
from mappers.turin_map_modules import turin_comorbidities_mapper
from mappers.turin_map_modules import turin_csfTest_mapper
from mappers.turin_map_modules import turin_evokedPotentials_mapper
from mappers.turin_map_modules import turin_familyMedicalHistory_mapper
from mappers.turin_map_modules import turin_hematologyTest_mapper
from mappers.turin_map_modules import turin_onset_mapper
# Pavia mappers
# Turin mappers
from mappers.turin_map_modules import turin_personalData_mapper
from mappers.turin_map_modules import turin_pregnancies_mapper
from mappers.turin_map_modules import turin_relapses_mapper
from mappers.turin_map_modules import turin_rmi_mapper
from mappers.turin_map_modules import turin_treatments_mapper
from mappers.turin_map_modules import turin_visits_mapper
# Madrid mappers
# Query
# Quality report
from mappers.utility_scripts.quality import quality_report as qr
# local utilities scripts
from support import ProjectPaths, Binding
from support.Filtering import choose_a_filtering_type
from support.FindForbiddenPatients import find_forbidden_patients_for_turin

'''
Script that creates the graph for the data coming from Pavia Turin data. 
Once the graph is created (always held in-memory) it is later exported in 3 different formats
(turtle, xml/rdf, json/ld).

    Mapping main module:

    -From this module we call other mappers that provide the map from the data to RDF

    -Once obtained the final RDF graph we serialize it.

'''

"""path pointing to the configuration file containing other paths"""
configurationPath = "./configuration/paths.ini"

def main():
    # parameters and URLs
    # initialize all the paths that we need in the object ProjectPaths, using configuration file paths.ini
    projectPaths = ProjectPaths.ProjectPaths()
    projectPaths.config(configurationPath, os.getcwd())

    print("# PATH WE ARE USING: ")
    print("##\tTurin data path base:\t" + projectPaths.turinDataPathBase)
    print("##\toutput path:\t" + projectPaths.savePath)
    print()

    # create the graph
    g = Graph()
    # bind the namespaces to a prefix for a more readable output
    Binding.bind(g)

    # ============ Ask the user to choose between strong and weak filtering type ===========
    filtering_type = choose_a_filtering_type()
    projectPaths.config_log_giles(filtering_type, 'turin')

    ##################### TURIN Mapper ######################
    print("--- start Turin mapping -----------------\n")

    # output redirection
    log_file_turin = projectPaths.savePath + f"log/turin-{filtering_type}_filtering-sysout.log"
    file = open(log_file_turin, "w")
    stdout = sys.stdout
    sys.stdout = file
    file.reconfigure(encoding="utf-8")

    # ===== ANAGRAFICA =====
    # read the input
    patients_turin = pd.read_csv(projectPaths.turinDataPathBase + "Anagrafica.csv", index_col="TUR_MS_CODE",
                                 parse_dates=["Anno Nascita", "Data di morte"])

    onsetAndDiagnosis_turin = pd.read_csv(projectPaths.turinDataPathBase + "Esordio_e_diagnosi.csv",
                                          parse_dates=["Data esordio SM", "Data diagnosi",
                                                       "Data prima visita nel centro"])

    forbidden_list = find_forbidden_patients_for_turin(patients_turin, onsetAndDiagnosis_turin)

    # update the RDF graph

    g, patientCodes = turin_personalData_mapper.registryMapper(patients_turin, onsetAndDiagnosis_turin, g,
                                                               ontologies.BTO_schema, ontologies.BTO_resource,
                                                               ontologies.BTO_ni, ontologies.PO, file,
                                                               projectPaths,
                                                               forbidden_list)
    # free the memory
    gc.collect()

    # ===== ONSET AND DIAGNOSIS =====
    del patients_turin
    patients_turin = pd.read_csv(projectPaths.turinDataPathBase + "Anagrafica.csv",
                                 parse_dates=["Anno Nascita", "Data di morte"])
    # update the RDF graph
    g = turin_onset_mapper.onset_and_diagnosisMapper(onsetAndDiagnosis_turin, g, ontologies.BTO_schema,
                                                     ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO,
                                                     patientCodes, file,
                                                     forbidden_list,
                                                     patients_turin)
    # free the memory
    # del onsetAndDiagnosis_turin
    del patients_turin
    gc.collect()

    # ===== VISITS =====
    # read the input
    visits_turin = pd.read_csv(projectPaths.turinDataPathBase + "Visite.csv", index_col="Id", parse_dates=["Data visita"])

    # update the RDF graph
    g = turin_visits_mapper.visitsMapper(visits_turin, g,
                                         ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni,
                                         ontologies.PO, patientCodes, filtering_type,
                                         forbidden_list)

    turin_visits_mapper.check_dates(onsetAndDiagnosis_turin, visits_turin)

    # free the memory
    del visits_turin
    gc.collect()

    # ===== RELAPSE =====
    # read the input
    relapses_turin = pd.read_csv(projectPaths.turinDataPathBase + "Riacutizzazioni.csv", index_col="Id",
                                 parse_dates=["Data inizio relapse"])
    # update the RDF graph
    g = turin_relapses_mapper.relapseMapper(relapses_turin, g, ontologies.BTO_schema, ontologies.BTO_resource,
                                            ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                            forbidden_list)

    turin_relapses_mapper.check_dates(onsetAndDiagnosis_turin, relapses_turin)
    # free the memory
    del relapses_turin
    gc.collect()

    # ===== PREGNANCIES =====
    # read the input
    pregnancies_turin = pd.read_csv(projectPaths.turinDataPathBase + "Gravidanze.csv",
                                    usecols=[ "TUR_MS_CODE", "Data Inizio Gravidanza", "Data Diagnosi Gravidanza",
                                              "Data Fine Gravidanza", "Evento Fine Gravidanza"],
                                    parse_dates=["Data Inizio Gravidanza", "Data Diagnosi Gravidanza",
                                                 "Data Fine Gravidanza"])
    # update the RDF graph
    g = turin_pregnancies_mapper.Pregnancy().pregnancyMapper(pregnancies_turin, g, ontologies.BTO_schema,
                                                             ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO,
                                                             patientCodes, file,
                                                             forbidden_list)


    turin_pregnancies_mapper.check_dates(onsetAndDiagnosis_turin, pregnancies_turin)
    # free the memory
    del pregnancies_turin
    gc.collect()

    # ===== CLINICAL EVENTS =====
    # read the input
    clinicalEvents_turin = pd.read_csv(projectPaths.turinDataPathBase + "Eventi_clinici.csv", index_col="Id",
                                       parse_dates=["Data evento", "Data esito"])
    # update the RDF graph
    g = turin_clinicalEvents_mapper.diseaseMapper(clinicalEvents_turin, g, ontologies.BTO_schema,
                                                  ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO,
                                                  patientCodes, file,
                                                  forbidden_list)
    # free the memory
    del clinicalEvents_turin
    gc.collect()

    # ===== COMORBIDITIES =====
    # read the input
    comorbidities_turin = pd.read_csv(projectPaths.turinDataPathBase + "Co-Morbilità.csv", index_col="Id",
                                      parse_dates=["Data inizio", "Data fine"])
    # update the RDF graph
    g = turin_comorbidities_mapper.comorbidityMapper(comorbidities_turin, g, ontologies.BTO_schema,
                                                     ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO,
                                                     patientCodes, file,
                                                     forbidden_list)
    # free the memory
    del comorbidities_turin
    gc.collect()

    # ===== RELATIVES =====
    # read the input
    relatives_turin = pd.read_csv(projectPaths.turinDataPathBase + "Anamnesi_Patologica_Familiare.csv", index_col="Id",
                                  parse_dates=["Data inizio"]).dropna(subset=["TUR_MS_CODE", "ICD9"])
    # update the RDF graph
    g = turin_familyMedicalHistory_mapper.relativeMapper(relatives_turin, g, ontologies.BTO_schema,
                                                         ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO,
                                                         patientCodes, file,
                                                         forbidden_list)
    # free the memory
    del relatives_turin
    gc.collect()

    # ===== EVOKED POTENTIALS =====
    # read the input
    evokedPotentials_turin = pd.read_csv(projectPaths.turinDataPathBase + "Potenziali_Evocati.csv", index_col="Id",
                                         parse_dates=["Data esame"]).dropna(subset=["TUR_MS_CODE"])
    # update the RDF graph
    g = turin_evokedPotentials_mapper.evokedPotentialMapper(evokedPotentials_turin, g, ontologies.BTO_schema,
                                                            ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO,
                                                            patientCodes, file,
                                                            forbidden_list)
    # free the memory
    del evokedPotentials_turin
    gc.collect()

    # start the treatments mapper
    # read the input
    treatments_turin = pd.read_csv(projectPaths.turinDataPathBase + "Trattamenti.csv", index_col="Id",
                                   parse_dates=["Data inizio", "Data fine"]).dropna(subset=["TUR_MS_CODE"])
    # ===== TREATMENTS =====
    g = turin_treatments_mapper.treatmentMapper(treatments_turin, g, ontologies.BTO_schema, ontologies.BTO_resource,
                                                ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                forbidden_list)
    # free the memory
    del treatments_turin
    gc.collect()

    # ===== RMI =====
    # read the input
    rmi_turin = pd.read_csv(projectPaths.turinDataPathBase + "RMN.csv", index_col="Id",
                            parse_dates=["Data esame"]).dropna(subset=["TUR_MS_CODE"])
    # update the RDF graph
    g = turin_rmi_mapper.rmiMapper(rmi_turin, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni,
                                   ontologies.PO, patientCodes, file,
                                   forbidden_list)
    # free the memory
    del rmi_turin
    gc.collect()

    # =====CSF =====
    # read the input
    csf_turin = pd.read_csv(projectPaths.turinDataPathBase + "Liquor.csv", index_col="Id",
                            parse_dates=["Data esame"]).dropna(subset=["TUR_MS_CODE"])
    # update the RDF graph
    g = turin_csfTest_mapper.csfTestMapper(csf_turin, g, ontologies.BTO_schema, ontologies.BTO_resource,
                                           ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                           forbidden_list)
    # free the memory
    del csf_turin
    gc.collect()

    # ===== Hematology TEST =====
    # read the input
    routineHematologyTest = pd.read_csv(projectPaths.turinDataPathBase + "Ematochimici_di_routine.csv", index_col="Id",
                                        parse_dates=["Data del prelievo"])
    specificHematologyTest = pd.read_csv(projectPaths.turinDataPathBase + "Ematochimici_specifici.csv", index_col="Id",
                                         parse_dates=["Data del prelievo"])
    # update the RDF graph
    bloodTest = turin_hematologyTest_mapper.HematologyTest()
    g = bloodTest.hematologyTestMapper(routineHematologyTest, specificHematologyTest, g, ontologies.BTO_schema,
                                       ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                       forbidden_list)
    # free the memory
    del routineHematologyTest
    del specificHematologyTest
    gc.collect()

    # end of the out redirection
    file.close()
    sys.stdout = stdout

    print("--- ended Turin mapping -----------------\n")


    # Serialize the RDF graph obtained
    flag = True
    file_name = 'turin'

    # while flag:
    #     file_name = input("Insert a name for the serialized file (do not add the file extention to the name): \t")
    #     if re.search("\\.",file_name) == None:
    #         flag = False

    print("\n--- saving serialization ----------------\n")
    with open(projectPaths.savePath + file_name + ".ttl", 'w', encoding="utf-8") as file:
        file.write(g.serialize(format='turtle'))
    print("\n--- turtle file correctly serialized  ---\n")

    # with open(projectPaths.savePath + file_name + ".rdf", 'w', encoding="utf-8") as file:
    #     file.write(g.serialize(format='application/rdf+xml'))
    # print("\n--- XML/RDF file correctly serialized ---\n")
    # with open(projectPaths.savePath + file_name + ".json", 'w', encoding="utf-8") as file:
    #     file.write(g.serialize(format='json-ld'))
    # print("\n--- json-ld file correctly serialized ---\n")


    print("\n--- saving completed --------------------\n")

    ### Statistics and data quality ######
    # Turin
    qr.quality_report(log_file_turin, "turin_quality_report.txt")


if __name__ == "__main__":
    main()
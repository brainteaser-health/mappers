from googletrans import Translator, constants
from pprint import pprint

translator = Translator()
translation = translator.translate("Ogni 1 giorno", src='it', dest='en')
print(f"{translation.origin} ({translation.src}) --> {translation.text} ({translation.dest})")
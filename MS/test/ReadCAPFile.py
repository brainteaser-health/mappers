import pandas as pd

""" A simple test to see how to import the hash codes corresponding to the Placename
"""


class ImportLocationsHashes:

    def __init__(self):
        self.locations_hashes_dictionary = {}


    def populate_dictionary_with_info_in_file(self, path):
        """
        Given a file where the placenames (comuni) together with their hash code are located, populates
        the instance dictionary with the placename as key, and the hash ad value

        :param path:
        :return:
        """
        # read the csv - a good choice for the index would have been the CAP, but it is not always present for the patients
        cap_file = pd.read_csv(path, index_col="PLACENAME")

        for index, row in cap_file.iterrows():
            # take the placename
            self.locations_hashes_dictionary.update({str(index) : str(row["SHA3_256_HEX_DIG"])})




if __name__ == "__main__":
    execution = ImportLocationsHashes()
    path_patients = "/Users/dennisdosso/Documents/Brainteaser Bitbucket Project/mappers/MS/data/input/pavia/Anagrafica.csv"

    path_cap = "/Users/dennisdosso/Documents/Brainteaser Bitbucket Project/CAP/IT.txt.csv"
    execution.populate_dictionary_with_info_in_file(path_cap)

    path_cap = "/Users/dennisdosso/Documents/Brainteaser Bitbucket Project/CAP/ES.txt.csv"
    execution.populate_dictionary_with_info_in_file(path_cap)

    path_cap = "/Users/dennisdosso/Documents/Brainteaser Bitbucket Project/CAP/PT.txt.csv"
    execution.populate_dictionary_with_info_in_file(path_cap)




    # patients_pavia = pd.read_csv(path_patients, index_col="Paziente ID",
    #                              parse_dates=["Data di nascita", "Data di morte"])

    # execution.import_has_of_locations_from_file(path_cap, patients_pavia)



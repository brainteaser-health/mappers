import configparser
import os

# script to test the possibility to read configuration files containing data
# (sort of like properties file, if you are more used to them)


# first, test the current directory if you are unsure where this process is running
print(os.getcwd())

# first off: create the object
config = configparser.ConfigParser()
# read the file (be careful on the path)
config.read("../configuration/paths.ini")
print(config['INPUT'])
# now you can access the data based on the sections, e.g.:

print(config['INPUT']['PaviaDataDirectory'])


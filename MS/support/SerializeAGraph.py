import re


def serialize_graph(g, directory_path):
    """

    :param g: graph to serialize
    :param directory_path: path of the directory where to write the files
    :return: null
    """
    # we ask the user for a name for the serialization files
    flag = True
    while flag:
        file_name = input("Insert a name for the serialized file (do not add the file extention to the name): \t")
        if re.search("\\.", file_name) == None:
            flag = False

    print("serializing the graph (this may take some minutes, depending on the size of the graph)...")

    print("Turtle serialization...")
    with open(directory_path + file_name + ".ttl", 'w', encoding='utf-8') as file:
        file.write(g.serialize(format="turtle"))
    print("... done")

    print("XML/RDF serialization...")
    with open(directory_path + file_name + ".rdf", 'w', encoding='utf-8') as file:
        file.write(g.serialize(format="application/rdf+xml"))
    print("... done")

    print("JSON/LD serialization...")
    with open(directory_path + file_name + ".json", 'w', encoding='utf-8') as file:
        file.write(g.serialize(format="json-ld"))
    print("... done")







def find_forbidden_patients_for_turin(patients_basic_data, patients_onset_data):
    """
    Checks some values in a couple of pandas dataframes and it
    returns a list of patients id corresponding to entries that lack some critical information,
    and thus need to be left out from the database

    :param patients_basic_data:
    :param patients_onset_data:
    :return:
    """
    # let us select all rows with a nan values on column
    forbidden_rows = patients_onset_data[patients_onset_data["Data esordio SM"].isnull()]
    forbidden_ids = forbidden_rows["TUR_MS_CODE"].to_list()

    forbidden_rows = patients_onset_data[patients_onset_data["Data prima visita nel centro"].isnull()]
    forbidden_ids.extend(forbidden_rows["TUR_MS_CODE"].to_list())

    return forbidden_ids



def find_forbidden_patients_for_pavia(patients_basic_data, patients_onset_data):
    """
    Checks some values in a couple of pandas dataframes and it
    returns a list of patients id corresponding to entries that lack some critical information,
    and thus need to be left out from the database

    :param patients_basic_data:
    :param patients_onset_data:
    :return:
    """
    # let us select all rows with a nan values on column
    forbidden_rows = patients_onset_data[patients_onset_data["Data esordio SM"].isnull()]
    forbidden_ids = forbidden_rows["Paziente ID"].to_list()

    forbidden_rows = patients_onset_data[patients_onset_data["Data prima visita nel centro"].isnull()]
    forbidden_ids.extend(forbidden_rows["Paziente ID"].to_list())

    return forbidden_ids

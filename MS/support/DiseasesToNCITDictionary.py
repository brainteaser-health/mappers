"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""

import pandas as pd


class DiseasesToNCITDictionary:

    def __init__(self, file_path):
        """

        :param file_path: path of the csv file containing the dictionary
        """
        self.ds = pd.read_csv(file_path)
        # the dictionary that holds the mapping
        self.disease_dictionary = {}

        # populate it
        self.populate_dictionary()

    def populate_dictionary(self):
        """
        Populates self.disease_dictionary with the mappings between the name of the disease in the excel file and
        its name in NCIT.

        :return:
        """
        for index, row in self.ds.iterrows():  # iterate through the tuples of the file
            if not row["Name"] in self.disease_dictionary:
                self.disease_dictionary.update({row["Name"]: row["ontoName"]})

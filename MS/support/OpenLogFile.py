import sys


def open_log_file(log_file_path):
    """Given the path of a file, it opens the object file in 'write' option. It also
    sets the system standard output to write in the file.

    It returns the file where to write and the original standard output, so you can
    further change it
    """
    file = open(log_file_path, "w")
    stdout = sys.stdout  # save the standard output in a separate variable
    sys.stdout = file  # from now on, sys out prints in the log file
    file.reconfigure(encoding="utf-8")
    return file, stdout

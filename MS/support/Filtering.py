def choose_a_filtering_type():
    """Asks the user to input one between 'strong' and 'weak'. These define the filtering
    types used in the application
    :return filtering_type a string with the decision of the user ('strong' or 'weak')"""
    acceptable_values = ("strong", "weak")
    filtering_type = ""

    return 'weak'
    # while not (filtering_type in acceptable_values):
    #     filtering_type = str(input('\nChoose the filtering type ("strong" or "weak"):\t'))
    #     if not (filtering_type in acceptable_values):
    #         print("Input ERROR!\n\nTry again...")
    # return filtering_type

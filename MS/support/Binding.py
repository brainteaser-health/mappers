from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS
from mappers.namespaces import ontologies


def bind(g):
    """ Given a graph g, binds the namespaces to a prefix for more readable output"""
    g.bind("foaf", FOAF)
    g.bind("xsd", XSD)
    g.bind("rdfs", RDFS)
    g.bind("bto_schema", ontologies.BTO_schema)
    g.bind("bto_ni", ontologies.BTO_ni)
    g.bind("bto_resource", ontologies.BTO_resource)
    g.bind("po", ontologies.PO)
    g.bind("rdf", RDF)
    g.bind("umls", ontologies.UMLS)
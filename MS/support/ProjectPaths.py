import configparser
import os
from pathlib import Path


class ProjectPaths:
    def __init__(self):
        self.paviaDataPathBase = ""
        self.turinDataPathBase = ""
        self.madridDataPath = ""
        self.savePath = ""
        self.logFilePavia = ""

        self.paviaAnagrafica = ""
        self.sermasUrbanDictionary = ""
        self.disease_to_NCIT_dictionary = "" # where the dictionary with the mapping between a disease and its NCIT name is located

        self.italian_cap_file_path = ""
        self.spanish_cap_file_path = ""
        self.portuguese_cap_file_path = ""


    def config(self, configurationFile, basePath):
        """configures all the path fields of this object instance
        :param configurationFile path of the file containing the properties to set
        :param basePath path of the working directory (used with other, relatiove paths, as basis to build the other
        paths)
        """
        config = configparser.ConfigParser()
        config.read(configurationFile)
        self.paviaDataPathBase = basePath + config['INPUT']['PaviaDataDirectory']
        self.turinDataPathBase = basePath + config['INPUT']['TurinDataDirectory']
        self.madridDataPath = basePath + config['INPUT']['MadridDataFile']
        self.savePath = basePath + config['OUTPUT']['OutputDataPath']

        self.paviaAnagrafica = self.paviaDataPathBase + config['FILENAMES']['PaviaAnagrafica']
        self.sermasUrbanDictionary = basePath + config['INPUT']['SermasUrbanDictionary']
        self.disease_to_NCIT_dictionary = basePath + config['INPUT']['disease_to_NCIT_dictionary']

        self.italian_cap_file_path = config["LOCATION_HASHES"]["it"]
        self.spanish_cap_file_path = config["LOCATION_HASHES"]["es"]
        self.portuguese_cap_file_path = config["LOCATION_HASHES"]["pt"]

    def config_log_giles(self, filtering_type, what='pavia'):
        """:param filtering_type the type of filtering applied to the project. For now, we expect 'strong' or
        'weak'
        """
        if what == 'pavia':
            self.logFilePavia = self.savePath + f"log/pavia-{filtering_type}_filtering-sysout.log"
        elif what == 'turin':
            self.logFilePavia = self.savePath + f"log/turin-{filtering_type}_filtering-sysout.log"




# test main
if __name__ == "__main__":
    execution = ProjectPaths()
    execution.config("./configuration/paths.ini", os.getcwd())


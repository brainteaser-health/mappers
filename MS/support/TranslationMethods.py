# this script contains a list of methods that we use to perform translations in the project (usually from Italian
# to English, but never say never in the future)


def translate_frequency_from_italian_to_english(to_translate: str):
    """ Translates in English the administration frequency for the therapeutic procedures.
    If we do not have an option covering the case,
    we return the input string unchanged"""
    if to_translate == "3 a settimana":
        return "three times a week"

    if to_translate == "2 a settimana":
        return "twice a week"

    if to_translate == "Ogni 1 giorno":
        return "once a day"

    if to_translate == "Ogni 1 mese":
        return "once a month"

    if to_translate == "Ogni 2 giorni":
        return "once every two days"

    if to_translate == "2 al giorno":
        return "twice daily"

    if to_translate == "2 al mese":
        return "twice a month"

    if to_translate == "3 al giorno":
        return "three times a day"

    if to_translate == "Ogni 1 settimana":
        return "once a week"

    if to_translate == "Ogni 4 settimane":
        return "once a month"

    if to_translate == "Ogni 6 settimane":
        return "once every 6 weeks"

    if to_translate == "Ogni 8 settimane":
        return "once every 8 weeks"

    if to_translate == "Ogni 1 ora":
        return "every hour"

    if to_translate == "4 al giorno":
        return "four times a day"

    if to_translate == "Ogni 4 ore":
        return "once every 4 hours"

    if to_translate == "Ogni 3 mesi":
        return "once every 3 months"

    if to_translate == "Ogni 6 mesi":
        return "once every 6 months"

    if to_translate == "Altro":
        return "other"

    return to_translate


def translate_administration_route__from_italian_to_english(route: str):
    if route == "Orale":
        return "oral"

    if route == "Altro":
        return "other"

    if route == "IV":
        return "intravenous"

    if route == "IM":
        return "intramuscular"

    if route == "SC":
        return "subcutaneous"

    return route


def translate_measurement_unit(unit: str):
    if unit == "Altro":
        return "other"

    if unit == "Applicazione spray":
        return "spray application"

    if unit == "CPRS":
        return "pills"

    # in case we do not know, just return what we saw
    return unit
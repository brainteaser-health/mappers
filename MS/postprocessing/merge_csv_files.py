""" The machine learning guys first wanted a unique csv file with the data from all the
hospitals, then they wanted me to add a variable to distinguish the different tuples in the csv file.
"""

import pandas as pd


def read_csv_file_and_add_variable_at_its_end(file_path, value_to_add):
    """ Reads a csv file as pandas dataframe. Add a column at the end, with title 'origin' and as value
    always 'value_to_add' """

    dataframe = pd.read_csv(file_path)
    rows = len(dataframe)  # number of rows in this csv file
    new_column = [value_to_add for i in range(0, rows)]  # create the new column
    dataframe['centre'] = new_column  # add it
    return dataframe


def expand_to_create_new_patients_symbol_table(dataframe):
    """ Uses the patients_symptoms input dataframe to create a new dataframe, this time containing more columns"""
    # header of the new dataframe
    columns = ['patient_id', 'brainstem', 'eye', 'spinal_cord', 'supratentorial', 'origin']
    # dictionary keeping, for each patient, the list of its symptoms
    patients_dict = {}
    new_dataframe = pd.dataframe(columns=columns)
    for index, row in dataframe.iterrows():
        patient_id = row["patient_id"]
        symptom = row["symptom_name"]
        origin = row["origin"]
        if patient_id not in patients_dict:
            # a new entry
            patients_dict.update({patient_id, [origin, symptom]})
        else:
            # update the list of symptoms for this patient
            symptom_list = patients_dict.get(patient_id)
            symptom_list.append(symptom)
            patients_dict.update({patient_id, symptom})

    # now we are ready to create ne new dataframe
    for patient in patients_dict:
        new_row = [patient]
        symptom_list = patients_dict[patient]  # get the list of symptoms of this patient

        # let us consider the 4 different symptoms
        if 'Brainsteam Symptom' in symptom_list:
            new_row.append('True')
        else:
            new_row.append('False')

        if "Eye Symptom" in symptom_list:
            new_row.append('True')
        else:
            new_row.append('False')

        if "Spinal Cord Symptom" in symptom_list:
            new_row.append('True')
        else:
            new_row.append('False')

        if "Supratentorial Symptom" in symptom_list:
            new_row.append('True')
        else:
            new_row.append('False')

        new_row.append(patients_dict[patient][0])
        new_dataframe.append(new_row, ignore_index=True)

    return new_dataframe


if __name__ == "__main__":
    # path of the directory containing all the rest
    dir_path = '/Volumes/GoogleDrive/.shortcut-targets-by-id/1E3nyU4BG1ZxurNjIu1Gpl51iUmMeBZMr/BRAINTEASER/retrospective-datasets/output_data/MS/current_data'

    # names of the subdirectories corresponding to the places from where we are getting these data
    origins = ['pavia', 'turin']

    # output = ''

    # name of the files (all must have the csv extension)
    filenames = ['edss', 'evoked_potentials', 'mri', 'ms_type',  'relapses',
                 'therapeutic_procedures', 'patients_data_and_symptoms']

    for name in filenames:
        # for pavia
        path = dir_path + "/" + origins[0] + "/" + name + ".csv"
        first_dataframe = read_csv_file_and_add_variable_at_its_end(path, origins[0])

        # for turin
        path = dir_path + "/" + origins[1] + "/" + name + ".csv"
        second_dataframe = read_csv_file_and_add_variable_at_its_end(path, origins[1])

        dataframe = [first_dataframe, second_dataframe]  # concatenate the dataframes
        dataframe = pd.concat(dataframe)


        output_path = dir_path + "/" + name + ".csv"
        dataframe.to_csv(output_path, index=False)

    print('All done')

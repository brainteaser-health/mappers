"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""
import sys

from mappers.madrid_map_modules import madrid_visits_mapper, madrid_personalData_mapper, madrid_comorbidities_mapper, \
    madrid_pregnancies_mapper, madrid_onset_mapper, madrid_evokedPotentials_mapper, madrid_treatments_mapper, \
    madrid_relapses_mapper, madrid_rmi_mapper
from mappers.namespaces import ontologies
from mappers.pavia_map_modules import pavia_personalData_mapper
from support.DiseasesToNCITDictionary import DiseasesToNCITDictionary

'''
   Main script that imports triples from the data concerning Madrid (Sermas), creates the
   RDF in-memory graph, and serializes it separately.
'''

from support import ProjectPaths, Binding
from rdflib import Graph
from mappers.utility_scripts import SermasUrbanDictionary

from support.SerializeAGraph import serialize_graph

import os
import pandas as pd

# dates that are used when parsing the Excel file from SERMAS
dates = ["informed_consent", "dob", "start_date", "date_first_symptom", "date_delivery", "last_relapse"]  # dates


class MadridImporter:

    def __init__(self):
        # path where to find the configuration file
        self.configurationPath = "./configuration/paths.ini"
        # read the configurations
        self.paths = ProjectPaths.ProjectPaths()
        self.paths.config(self.configurationPath, os.getcwd())
        self.g = Graph()

        # read and populate the dictionary with the information about the rural areas
        self.sermasUrbanDictionary = SermasUrbanDictionary.SermasUrbanDictionary(self.paths.sermasUrbanDictionary)
        self.sermasUrbanDictionary.populateUrbanDictionary()
        self.sermasUrbanDictionary.populate_hash_dictionary(self.paths.spanish_cap_file_path)

        self.diseaseToNCITDictionary = DiseasesToNCITDictionary(self.paths.disease_to_NCIT_dictionary)



    def import_triples(self):
        """
        Using the excel file provided by Sermas, extracts the triples from it. They are stored in self.g, that is also
        returned.

        :return: the graph obtained from the triples created starting from the excel file
        """
        Binding.bind(self.g)  # set the prefixes in this graph

        # redurect the standard output to the file where we log
        log_file_madrid = self.paths.savePath + "log/madrid_filtering-sysout.log"
        file = open(log_file_madrid, "w")
        stdout = sys.stdout # save the standard output
        sys.stdout = file
        file.reconfigure(encoding="utf-8")

        # read (only) the patients data
        # columns = ["patient_id", "informed_consent", "dob", "gender", "menopause", "ethnicity", "rural_urban",
        #            "years_education", "profession", "marital_status", "clinical_trial", "start_date",
        #            "date_first_symptom"]  # columns in which we are interested from the excel
        # madrid_patients = pd.read_excel(self.paths.madridDataPath, usecols=columns ,parse_dates=dates)

        # read the WHOLE table patients of madrid (Sermas)
        madridData = pd.read_excel(self.paths.madridDataPath, parse_dates=dates)

        # extract triples

        # ===== ANAGRAFICA =====
        # patientCodes is a dictionary containing the ID of the patients (as in the excel) together with their URLs.
        # it is used in the next steps of the creation of triples
        patientCodes = madrid_personalData_mapper.registryMapper(madridData, self.g, ontologies.BTO_schema,
                                                                 ontologies.BTO_resource, ontologies.BTO_ni,
                                                                 ontologies.PO, ontologies.UMLS,
                                                                 self.sermasUrbanDictionary.place_degree_dictionary,
                                                                 self.sermasUrbanDictionary)

        # ===== ONSET AND DIAGNOSIS =====
        madrid_onset_mapper.onsetMapper(madridData, self.g, ontologies.BTO_schema, ontologies.BTO_resource,
                                            ontologies.BTO_ni, ontologies.PO, patientCodes)


        # ===== VISITS =====
        madrid_visits_mapper.visitsMapper(madridData, self.g, ontologies.BTO_schema, ontologies.BTO_resource,
                                          ontologies.BTO_ni, ontologies.PO, patientCodes)

        # ===== RELAPSE =====
        madrid_relapses_mapper.relapseMapper(madridData, self.g, ontologies.BTO_schema, ontologies.PO, patientCodes)


        # ===== COMORBIDITIES =====
        madrid_comorbidities_mapper.comorbiditiesMapper(madridData, self.g, ontologies.BTO_schema, ontologies.BTO_resource,
                                                        ontologies.BTO_ni, ontologies.PO, patientCodes,
                                                        self.diseaseToNCITDictionary.disease_dictionary)


        # ===== PREGNANCIES =====
        madrid_pregnancies_mapper.pregnanciesMapper(madridData, self.g, ontologies.BTO_schema, ontologies.BTO_resource,
                                                    ontologies.BTO_ni, ontologies.PO, patientCodes)


        # ===== EVOKED POTENTIALS =====
        madrid_evokedPotentials_mapper.evokedPotentialsMapper(madridData, self.g, ontologies.BTO_schema,
                                                                  ontologies.BTO_resource,
                                                                  ontologies.BTO_ni, ontologies.PO, patientCodes)

        # ===== TREATMENTS =====
        madrid_treatments_mapper.treatmentsMapper(madridData, self.g, ontologies.BTO_schema,
                                                      ontologies.BTO_resource, ontologies.BTO_ni,
                                                      ontologies.PO, patientCodes)

        # ===== MRI =====
        madrid_rmi_mapper.madrid_rmi_mapping(madridData, self.g, ontologies.BTO_schema, ontologies.PO, patientCodes)

        # ===== CSF =====
        # ...

        # ===== Hematology TEST =====
        '''We have not yet added blood test data until we reach an agreement with the rest of our clinical colleagues 
        on which of all the data we carry out on patients we should register (since there are many from the diagnosis 
        and during the follow-up and the lab analysis are different depending on the treatment, clinical situation and 
        comorbidities of each individual patient)'''
        # ...

        # free some memory
        del madridData

        # close the output file
        file.close()
        # restore the standard output
        sys.stdout = stdout


        return self.g


if __name__ == "__main__":
    execution = MadridImporter()

    # extract triples from Madrid's files
    execution.import_triples()
    # write the graph in serialization format
    serialize_graph(execution.g, execution.paths.savePath)

    print("all done")


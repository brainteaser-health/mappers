"""
Copyright 2021-2022 University of Padua, Italy
Licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License
    https://creativecommons.org/licenses/by-sa/4.0/
You may obtain a copy of the License at
    https://creativecommons.org/licenses/by-sa/4.0/legalcode
This work has been supported by the BRAINTEASER project
    https://brainteaser.health/
    funded by the European Union’s Horizon 2020 research and innovation programme
    under the grant agreement No GA101017598
"""

#### IMPORT LOCAL MODULES ####




# namespaces
from mappers.namespaces import ontologies

# local utilities scripts
from support import ProjectPaths, Binding
from support.Filtering import choose_a_filtering_type
from support.FindForbiddenPatients import find_forbidden_patients_for_pavia
from support.OpenLogFile import open_log_file

# Pavia mappers
from mappers.pavia_map_modules import  pavia_personalData_mapper
from mappers.pavia_map_modules import pavia_onset_mapper
from mappers.pavia_map_modules import pavia_visits_mapper
from mappers.pavia_map_modules import pavia_relapses_mapper
from mappers.pavia_map_modules import pavia_pregnancies_mapper
from mappers.pavia_map_modules import pavia_clinicalEvents_mapper
from mappers.pavia_map_modules import pavia_comorbidities_mapper
from mappers.pavia_map_modules import pavia_familyMedicalHistory_mapper
from mappers.pavia_map_modules import pavia_evokedPotentials_mapper
from mappers.pavia_map_modules import pavia_treatments_mapper
from mappers.pavia_map_modules import pavia_rmi_mapper
from mappers.pavia_map_modules import pavia_csfTest_mapper
from mappers.pavia_map_modules import pavia_hematologyTest_mapper

# Query
from mappers.queries_modules import Query as pq
# Quality report
from mappers.utility_scripts.quality import quality_report as qr

### IMPORT EXTERNAL MODULES ###
import datetime
import os
import gc
import sys
import re
import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, RDFS




'''
Script that creates the graph for the data coming from Pavia (Mondino). 
Once the graph is created (always held in-memory) it is later exported in 3 different formats
(turtle, xml/rdf, json/ld).

    Mapping main module:
    
    -From this module we call other mappers that provide the map from the data to RDF
    
    -Once obtained the final RDF graph we serialize it.

'''

"""path pointing to the configuration file containing other paths"""
configurationPath = "./configuration/paths.ini"

def main():

    # parameters and URLs

    # initialize all the paths that we need in the object ProjectPaths, using configuration file paths.ini
    projectPaths = ProjectPaths.ProjectPaths()
    projectPaths.config(configurationPath, os.getcwd())

    print("# PATH WE ARE USING: ")
    print("##\tPavia data path base:\t" + projectPaths.paviaDataPathBase)
    print("##\tTurin data path base:\t" + projectPaths.turinDataPathBase)
    print("##\tMadrid data path:\t" + projectPaths.madridDataPath)
    print("##\toutput path:\t"+ projectPaths.savePath)
    print()

    # create the graph
    g = Graph()
    # bind the namespaces to a prefix for a more readable output
    Binding.bind(g)

    # ============ Ask the user to choose between strong and weak filtering type ===========
    filtering_type = choose_a_filtering_type()
    projectPaths.config_log_giles(filtering_type)

    # ========= import Pavia triples =========
    print("\n ===== importing triples from Pavia's files =====\n")

    # open the file where we will write the logs of this execution
    # log_file_pavia =  projectPaths.savePath + f"log/pavia-{filtering_type}_filtering-sysout.log"
    log_file_pavia = projectPaths.logFilePavia


    # file = open(log_file_pavia, "w")
    # stdout = sys.stdout # save the standard output in a separate variable
    # sys.stdout = file # from now on, sys out prints in the log file
    # file.reconfigure(encoding="utf-8")

    file, stdout = open_log_file(log_file_pavia)



    # ===== ANAGRAFICA =====
    # read the input data about the patient. Parse the dates of birth and death
    # patients_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Anagrafica.csv", index_col="Paziente ID", parse_dates=["Data di nascita", "Data di morte"])
    patients_pavia = pd.read_csv(projectPaths.paviaAnagrafica, index_col="Paziente ID",
                                 parse_dates=["Data di nascita", "Data di morte"])

    # read csv file containing information about first onset of the disease and its diagnosis
    onsetAndDiagnosis_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Esordio_e_diagnosi.csv", index_col="Id", parse_dates=["Data esordio SM", "Data diagnosi", "Data prima visita nel centro"])

    # the first thing that we need to do is find patients that cannot be inserted in the database due to their lack of
    # some critical data
    forbidden_list = find_forbidden_patients_for_pavia(patients_pavia, onsetAndDiagnosis_pavia)




    # update the RDF graph - import the triples in
    g, patientCodes = pavia_personalData_mapper.registryMapper(patients_pavia,
                                                               onsetAndDiagnosis_pavia,
                                                               g,
                                                               ontologies.BTO_schema,
                                                               ontologies.BTO_resource,
                                                               ontologies.BTO_ni,
                                                               ontologies.PO,
                                                               file,
                                                               projectPaths,
                                                               forbidden_list)

    del patients_pavia
    gc.collect() # runs the garbage collector

    # ===== ONSET and DIAGNOSIS =====
    patients_pavia = pd.read_csv(projectPaths.paviaAnagrafica,
                                 parse_dates=["Data di nascita", "Data di morte"])
    # update the RDF graph
    g = pavia_onset_mapper.onset_and_diagnosisMapper(onsetAndDiagnosis_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                     forbidden_list, patients_pavia)


    # free the memory
    del patients_pavia
    # del onsetAndDiagnosis_pavia
    gc.collect()


    # ===== VISITS =====
    # read the input
    visits_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Visite.csv",
                               index_col="Id", parse_dates=["Data visita"])

    # update the RDF graph (EDSS are here)
    g = pavia_visits_mapper.visitsMapper(visits_pavia, g,
                                         ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, filtering_type,
                                         forbidden_list)

    pavia_visits_mapper.check_dates(onsetAndDiagnosis_pavia, visits_pavia)

    # free the memory
    del visits_pavia
    gc.collect()

    # ===== RELAPSES =====
    # read the input
    relapses_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Riacutizzazioni.csv", index_col="Id", parse_dates=["Data inizio relapse"])
    # update the RDF graph
    g = pavia_relapses_mapper.relapseMapper(relapses_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                            forbidden_list)

    pavia_relapses_mapper.check_dates(onsetAndDiagnosis_pavia, relapses_pavia)

    # free the memory
    del relapses_pavia
    gc.collect()

    # ===== PREGNANCIES =====
    # read the input
    pregnancies_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Gravidanze.csv", index_col="Id",
                                usecols=["Id","Paziente ID", "Data Inizio Gravidanza", "Data Diagnosi Gravidanza","Data Fine Gravidanza", "Evento Fine Gravidanza"],
                                parse_dates=["Data Inizio Gravidanza", "Data Diagnosi Gravidanza", "Data Fine Gravidanza"])
    # update the RDF graph
    g = pavia_pregnancies_mapper.Pregnancy().pregnancyMapper(pregnancies_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                             forbidden_list)

    pavia_pregnancies_mapper.check_dates(onsetAndDiagnosis_pavia, pregnancies_pavia)
    # free the memory
    del pregnancies_pavia
    gc.collect()

    # ===== CLINICAL EVENTS =====
    # read the input
    clinicalEvents_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Eventi_clinici.csv", index_col="Id", parse_dates=["Data evento", "Data esito"])
    # update the RDF graph
    g = pavia_clinicalEvents_mapper.diseaseMapper(clinicalEvents_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                  forbidden_list)
    # free the memory
    del clinicalEvents_pavia
    gc.collect()

    # ===== COMORBIDITIES =====
    # read the input
    comorbidities_pavia = pd.read_csv(projectPaths.paviaDataPathBase+"Co-Morbilità.csv", index_col="Id", parse_dates=["Data inizio", "Data fine"])
    # update the RDF graph
    g = pavia_comorbidities_mapper.comorbidityMapper(comorbidities_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                     forbidden_list)
    # free the memory
    del comorbidities_pavia
    gc.collect()

    # ===== RELATIVES =====
    # read the input
    relatives_pavia = pd.read_csv(projectPaths.paviaDataPathBase + "Anamnesi_Patologica_Familiare.csv", index_col="Id",
                                      parse_dates=["Data inizio"]).dropna(subset=["Paziente ID", "ICD9"])
    # update the RDF graph
    g = pavia_familyMedicalHistory_mapper.relativeMapper(relatives_pavia, g, ontologies.BTO_schema,  ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                         forbidden_list)
    # free the memory
    del relatives_pavia
    gc.collect()

    # ===== EVOKED POTENTIALS =====
    # read the input
    evokedPotentials_pavia = pd.read_csv(projectPaths.paviaDataPathBase + "Potenziali_Evocati.csv", index_col="Id",
                                  parse_dates=["Data esame"]).dropna(subset=["Paziente ID"])
    # update the RDF graph
    g = pavia_evokedPotentials_mapper.evokedPotentialMapper(evokedPotentials_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                            forbidden_list)
    # free the memory
    del evokedPotentials_pavia
    gc.collect()

    # ===== TREATMENT (therapeutic procedure) =====
    # read the input
    treatments_pavia = pd.read_csv(projectPaths.paviaDataPathBase + "Trattamenti.csv", index_col="Id",
                                         parse_dates=["Data inizio", "Data fine"]).dropna(subset=["Paziente ID"])
    # update the RDF graph
    g = pavia_treatments_mapper.treatmentMapper(treatments_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                                forbidden_list)
    # free the memory
    del treatments_pavia
    gc.collect()

    # ===== MRI =====
    # read the input
    rmi_pavia = pd.read_csv(projectPaths.paviaDataPathBase + "RMN.csv", index_col="Id",
                                   parse_dates=["Data esame"]).dropna(subset=["Paziente ID"])
    # update the RDF graph
    g = pavia_rmi_mapper.rmiMapper(rmi_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                   forbidden_list)
    # free the memory
    del rmi_pavia
    gc.collect()

    # ===== CSF =====
    # read the input
    csf_pavia = pd.read_csv(projectPaths.paviaDataPathBase + "Liquor.csv", index_col="Id",
                            parse_dates=["Data esame"]).dropna(subset=["Paziente ID"])
    # update the RDF graph
    g = pavia_csfTest_mapper.csfTestMapper(csf_pavia, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                           forbidden_list)
    # free the memory
    del csf_pavia
    gc.collect()

    # ===== Hematology tests (BLOOD TEST) =====
    # read the input
    routineHematologyTest = pd.read_csv(projectPaths.paviaDataPathBase + "Ematochimici_di_routine.csv", index_col="Id",
                            parse_dates=["Data del prelievo"]).dropna(subset=["Paziente ID"])
    specificHematologyTest = pd.read_csv(projectPaths.paviaDataPathBase + "Ematochimici_specifici.csv", index_col="Id",
                            parse_dates=["Data del prelievo"]).dropna(subset=["Paziente ID"])
    # update the RDF graph
    bloodTest = pavia_hematologyTest_mapper.HematologyTest() # here trying to be more object oriented
    g = bloodTest.hematologyTestMapper(routineHematologyTest, specificHematologyTest, g, ontologies.BTO_schema, ontologies.BTO_resource, ontologies.BTO_ni, ontologies.PO, patientCodes, file,
                                       forbidden_list)
    # free the memory
    del routineHematologyTest
    del specificHematologyTest

    del onsetAndDiagnosis_pavia
    gc.collect()

    # end of the out redirection
    file.close()
    sys.stdout = stdout # restore the system standard output

    print("===== Pavia's file mapping completed =====\n")


    #### Queries ########
    #
    # query = pq.Query(g)
    #
    # query.registry()
    # query.edss()
    # query.relapse()
    #
    #####################


    # Serialize the RDF graph obtained
    flag = True
    file_name = "pavia"

    # while flag:
    #     file_name = input("Insert a name for the serialized file (do not add the file extention to the name): \t")
    #     if re.search("\\.",file_name) == None:
    #         flag = False

    print("\n--- saving serialization ----------------\n")
    with open(projectPaths.savePath + file_name + ".ttl", 'w', encoding="utf-8") as file:
        file.write(g.serialize(format='turtle'))
    print("\n--- turtle file correctly serialized  ---\n")
    # with open(projectPaths.savePath + file_name + ".rdf", 'w', encoding="utf-8") as file:
    #     file.write(g.serialize(format='application/rdf+xml'))
    # print("\n--- XML/RDF file correctly serialized ---\n")
    # with open(projectPaths.savePath + file_name + ".json", 'w', encoding="utf-8") as file:
    #     file.write(g.serialize(format='json-ld'))
    # print("\n--- json-ld file correctly serialized ---\n")
    print("\n--- saving completed --------------------\n")

    ### Statistics and data quality ######
    # Pavia
    qr.quality_report(log_file_pavia, "pavia_quality_report.txt")


if __name__ == "__main__":
    main()